#ifndef CUSTOMSORTMODEL_H
#define CUSTOMSORTMODEL_H

#include <QSortFilterProxyModel>

namespace Columns
{
    enum Columns
    {
        Date,
        Time,
        Duration,
        Categories,
        FileSize,
        FileName
    };
}

class CustomSortModel : public QSortFilterProxyModel
{
    Q_OBJECT

    public:
        CustomSortModel(bool showServiceData = false, int filterColumn = 0);
        virtual ~CustomSortModel();
        void setCategoriesToPass(QStringList categoriesToPass);
        void setFilterColumn(int filterColumn);

    public slots:
        void refilter(bool showServiceData);

    protected:
        bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;
        bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;

    private:
        bool showServiceData;
        QStringList categoriesToPass;
        int filterColumn;
};

#endif // CUSTOMSORTMODEL_H
