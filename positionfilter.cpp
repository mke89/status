#include "positionfilter.h"


PositionFilter::PositionFilter()
    :
    STROBE_RADIUS(0.5),
    alpha(1),
    beta(1),
    latFilter(1,1,1),
    lonFilter(1,1,1)
{

}

PositionFilter::PositionFilter(double alpha, double beta, int maxSilentIndex=1, bool usingGamma=false)
    :
      STROBE_RADIUS(0.5),
      alpha( alpha ),
      beta( beta ),
      latFilter( alpha,beta,maxSilentIndex ),
      lonFilter( alpha,beta,maxSilentIndex )
{
    latFilter.setUseGamma(usingGamma);
    lonFilter.setUseGamma(usingGamma);
}

QPointF  PositionFilter::process(double latitude, double longitude, double secondsDiff)
{
    double fLatitude =latFilter.process(latitude,secondsDiff);
    double fLongitude=lonFilter.process(longitude,secondsDiff);


    return QPointF(fLatitude,fLongitude);
}

QPointF PositionFilter::process(const QPointF &point, double secondsDiff)
{
    return process(point.x(),point.y(),secondsDiff);
}

GPoint PositionFilter::gPureSimulatedPredict(double timeDiffSecs ) const
{
    double lat = latFilter.getPureSimulatedValue(timeDiffSecs) ;
    double lon = lonFilter.getPureSimulatedValue(timeDiffSecs);

    if(  std::isnan(lat) || std::isnan(lon) )
    {
        return GPoint();
    }

    return GPoint(lat,lon);
}

void PositionFilter::setCoefficents(double newAlpa, double newBeta, double newGamma)
{
    latFilter.setCoefficents(newAlpa, newBeta, newGamma);
    lonFilter.setCoefficents(newAlpa, newBeta, newGamma);
}

double PositionFilter::getLatSpeed()
{
    return latFilter.velocityX;
}

double PositionFilter::getLonSpeed()
{
    return lonFilter.velocityX;
}

QDataStream &operator<<(QDataStream &stream, const PositionFilter &positionFilter)
{
    stream << positionFilter.alpha << positionFilter.beta << positionFilter.latFilter << positionFilter.lonFilter;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, PositionFilter &positionFilter)
{
    stream >> positionFilter.alpha >> positionFilter.beta >> positionFilter.latFilter >> positionFilter.lonFilter;
    return stream;
}
