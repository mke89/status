#ifndef RDPSYSTEM_H
#define RDPSYSTEM_H

#include <QObject>
#include <QTimer>
#include "radarstorage.h"
#include "settings.h"
#include "pointscontainer.h"



class RdpSystem : public QObject
{
    Q_OBJECT

private:
    struct BucketIndex
    {
        RadarIdentificator radarKey;
        int index;
        PointsContainer::ContainerType type;

        BucketIndex(RadarIdentificator radarKey, int index, PointsContainer::ContainerType type):
                    radarKey(radarKey), index(index), type(type)
        {}

        bool operator == (const BucketIndex &other)
        {
            return  index   == other.index &&
                    type    == other.type &&
                    radarKey== other.radarKey;
        }
    };

    static constexpr double FILTER_MAX_VAL = 1.0;   //0.9;  //0.5
    static constexpr double FILTER_MIN_VAL = 0.5;   //0.55; //0.3
    static constexpr double SUITABLE_VALUE_THRESHOLD = 0.7;
    static constexpr double SEARCH_STROBE_MULTIPLIER = 1.3;


public:
    explicit RdpSystem(bool testMode = false, QObject *parent = 0);
    QList<TargetPoint> getTracks();

    QList< QPair<RadarIdentificator,int> > getTracksKeys();
    QList<TargetPoint> getTrackPoints(QPair<RadarIdentificator,int> id);
    QList<TargetPoint> getAllTracks();

    friend QDataStream &operator<<(QDataStream &stream, const RdpSystem &rdpSystem);
    friend QDataStream &operator>>(QDataStream &stream, RdpSystem &rdpSystem);

private:
    void createNewBucket(RadarIdentificator radarKey, SimplePoint point, quint16 trackId);
    void autotune(const RadarIdentificator radarKey, const int bucketNum, const SimplePoint point);
    SimplePoint getNextFilteredTrackPoint(const SimplePoint& newPoint, RadarIdentificator radarKey,
                                          int bucketNum, double timeDiff);
    QMultiHash<RadarIdentificator, int> getBucketsToRemove();
    void createBucketsListsForPoints(QMultiMap<SimplePoint, BucketIndex>& allPoints, QList<BucketIndex>& indexes);
    void markTempBucketsToRemove(const QList<SimplePoint> &pointsToPostAnalyze, QMultiMap<SimplePoint, BucketIndex>& allPoints,
                                 QList<BucketIndex>& alreadyDeletedIndexes, int bucketNum);
    void deleteMarkedBuckets(QList<BucketIndex> &alreadyDeletedIndexes);
    bool processTrack(RadarIdentificator &radarKey, SimplePoint &point);
    bool processPotentialTrack(RadarIdentificator &radarKey, SimplePoint &point);
    bool processSinglePoint(RadarIdentificator &radarKey, SimplePoint &point);
    bool processUnmarkedPoints(RadarIdentificator &radarKey, SimplePoint &point);
    void sendTracks(bool tracksUpdated);
    bool updateContainerOrTrackState(SimplePoint &newPoint, RadarIdentificator& radarKey,
                                     int bucketIndex, QList<SimplePoint>& pointsToPostAnalyze);
    bool processNearestPointsOfAirTarget(const RadarIdentificator& radarKey, int containerNum,
                                         const SimplePoint& newPoint);

    TargetPoint convertToTargetPoint(SimplePoint &point, MainTrackIndex index);
    QPointF rotatePoint(QPointF centerPoint, QPointF rotationPoint, double rotationAngleDeg);
    double getPointAngle(QPointF centerPoint, QPointF interestPoint);

    quint16 generateUnicTrackNumber();
    void releaseUnicTrackNumber(quint16 trashNumber);
    void saveBuckets();
    void checkTracksNumbersIntervalsCorrectness();

    void sendEndPoints(QList<SimplePoint> endPoints);

signals:
    void tracksReday(QList<TargetPoint> tracksPoints);

public slots:
    void addPoint(RadarIdentificator &radarId, const SimplePoint& targetPoint);
    void addPointList(const QList<SimplePoint> &pointList, RadarIdentificator radarId);
    void preProcess();
    void postProcess();
    void processPrediction();
    QList<SimplePoint> removePastDueBuckets(const QMultiHash< RadarIdentificator, int> &toDelete);
    void markAllPointsAsSended(RadarIdentificator rid, int containerIndex);

private:

    QTimer predictionTimer;
    RadarStorage storage;
    QHash<RadarIdentificator, QList<PointsContainer>> buckets;
    QHash<RadarIdentificator, double> radarLastPointTime;
    QSet<quint16> trackNumbersStorage;
    quint16 lastAvailableNumber;

    bool enableTrackFiltering;
    quint16 MIN_TRACK_NUMBER;
    quint16 MAX_TRACK_NUMBER;
    bool testMode;
    int PREDICTIONS_LIMIT;
    int removeOldTracksCoefficent;
};

#endif // RDPSYSTEM_H
