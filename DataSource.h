#ifndef DATASOURCE_H
#define DATASOURCE_H
#include <QList>
#include <QStandardItemModel>
#include "AssessmentSortModel.h"
#include "trackprofile.h"
#include "LocatorViewSortModel.h"
#include "RlsTracksViewController.h"
#include "MlatTracksViewController.h"
#include "SdpdTracksViewController.h"
#include "spoint.h"

class DataSource
{
    public:
        enum Type
        {
            NONE    = 0,
            PSR     = 1,
            SSR     = 2,
            MLAT    = 3, // МПСН
            SDP     = 4
        };

        Type type;
        QList<TrackProfile>     trackProfiles;
        QStandardItemModel      *model;
        LocatorSortModel        *sorter;
        LocatorsTracksViewController *controller;
        QTableView *tableView;
        HidedColumns *hidedColumns;
        LocatorModelColumns *locatorModelColumn;
        QHash<TrackType::TrackType, int> tracksCounts;
        QHash<TrackType::TrackType, int> pointsCounts;
        QList<SPoint> unusedPoints;
        bool filterConnected;
        DataSource();
        DataSource(LocatorSortModel *locatorViewSortModel, QTableView *tableView,
                   LocatorModelColumns *locatorModelColumns, HidedColumns *hidedColumns);
        DataSource(const DataSource &dataSource);
        DataSource& operator=(const DataSource &right);
        ~DataSource();
};


class LocatorsTracksViewControllerFactory
{
    public:
        static LocatorsTracksViewController* create(const QString &locatorType, QTableView *tableView,
                                                    LocatorModelColumns *locatorModelColumns,
                                                    HidedColumns *hidedColumns, QStandardItemModel *model,
                                                    QList<TrackProfile> *trackProfiles);
};

class HidedColumnsFactory
{
    public:
        static HidedColumns* create(const QString &locatorType);
};

class LocatorModelColumnsFactory
{
    public:
        static LocatorModelColumns* create(const QString &locatorType);
};

#endif // DATASOURCE_H
