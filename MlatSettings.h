#ifndef MLATSETTINGS_H
#define MLATSETTINGS_H

#include "FlightZoneUtils.h"

struct MlatSettings
{
    int flightZoneType;
    QString locationZoneCoords;
    double airdromeUpdateDeltaTimeMs;
    double trackUpdateDeltaTimeMs;
    double altitudeFromM;
    double altitudeToM;

    MlatSettings();
    void load();
    void save();
    static bool polygonCoordinatesCorrect();
};


#endif // MLATSETTINGS_H
