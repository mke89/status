#ifndef TRACKIDKEEPER_H
#define TRACKIDKEEPER_H


class TrackIdKeeper
{
    int id;
    TrackIdKeeper();
    TrackIdKeeper(const TrackIdKeeper&);
    TrackIdKeeper& operator=(TrackIdKeeper&);

    public:
        static TrackIdKeeper& Instance();
        int nextId();
};

#endif // TRACKIDKEEPER_H
