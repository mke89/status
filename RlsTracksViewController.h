#ifndef RLSTRACKSVIEWCONTROLLER_H
#define RLSTRACKSVIEWCONTROLLER_H

#include "LocatorsTracksViewController.h"

class RlsTracksViewController: public LocatorsTracksViewController
{
    public:
        RlsTracksViewController() = delete;
        RlsTracksViewController(QTableView *tableView, LocatorModelColumns *locatorModelColumns,
                                HidedColumns *hidedColumns, QStandardItemModel *model,
                                QList<TrackProfile> *trackProfiles):
            LocatorsTracksViewController(tableView, locatorModelColumns, hidedColumns,
                                         model, trackProfiles)
        {}
        ~RlsTracksViewController(){}
        virtual void processColumnsHiding();
        virtual void addTrackToTable(const TrackProfile &trackProfile, int index);
        virtual void fillView();
        virtual void processRbsClicked();
        virtual void processUvdClicked();
        virtual void processAdsbClicked(bool flag);
};

#endif // RLSTRACKSVIEWCONTROLLER_H
