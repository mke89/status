#include "DataSource.h"
#include <QDebug>


DataSource::DataSource():
    model(nullptr),
    sorter(nullptr),
    controller(nullptr),
    tableView(nullptr),
    hidedColumns(nullptr),
    locatorModelColumn(nullptr),
    filterConnected(false)
{
    model = new QStandardItemModel();

    int rlsType = Settings::value(SettingsKeys::MLAT_RLS_TYPE).toInt();
    const QString &locatorType = LocatorUtils::getLocatorCaptionByType((LocatorUtils::LocatorType)rlsType);
    sorter = LocatorSortModelFactory::create(locatorType);
    sorter->setSourceModel(model);
}

DataSource::DataSource(LocatorSortModel *locatorViewSortModel, QTableView *tableView,
                       LocatorModelColumns *locatorModelColumns, HidedColumns *hidedColumns)
{
    model = new QStandardItemModel();
    sorter = locatorViewSortModel;
    sorter->setSourceModel(model);
    this->tableView = tableView;
    this->locatorModelColumn = locatorModelColumns;
    this->hidedColumns = hidedColumns;
    this->controller = LocatorsTracksViewControllerFactory::create(sorter->locatiorType(),
                                                                   this->tableView, locatorModelColumn,
                                                                   this->hidedColumns, model,
                                                                   &trackProfiles);
}

DataSource::DataSource(const DataSource &dataSource):
    trackProfiles(dataSource.trackProfiles)
{
    model = new QStandardItemModel();
    sorter = LocatorSortModelFactory::create(dataSource.sorter->locatiorType());
    sorter->setSourceModel(model);
    for(int i = 0; i < dataSource.model->rowCount(); i++)
    {
        model->appendRow(dataSource.model->item(i)->clone());
    }

    this->tableView = dataSource.tableView;
    locatorModelColumn = LocatorModelColumnsFactory::create(dataSource.sorter->locatiorType());
    hidedColumns = HidedColumnsFactory::create(dataSource.sorter->locatiorType());
    controller = LocatorsTracksViewControllerFactory::create(dataSource.sorter->locatiorType(),
                                                             tableView, locatorModelColumn,
                                                             hidedColumns, model, &trackProfiles);
    tracksCounts = dataSource.tracksCounts;
    pointsCounts = dataSource.pointsCounts;
    unusedPoints = dataSource.unusedPoints;
}

DataSource& DataSource::operator=(const DataSource &right)
{
    if(this == &right)
    {
        return *this;
    }

    model = new QStandardItemModel();
    sorter = LocatorSortModelFactory::create(right.sorter->locatiorType());
    sorter->setSourceModel(model);
    for(int i = 0; i < right.model->rowCount(); i++)
    {
        model->appendRow(right.model->item(i)->clone());
    }

    this->tableView = right.tableView;
    locatorModelColumn = LocatorModelColumnsFactory::create(right.sorter->locatiorType());
    hidedColumns = HidedColumnsFactory::create(right.sorter->locatiorType());
    controller = LocatorsTracksViewControllerFactory::create(right.sorter->locatiorType(),
                                                             tableView, locatorModelColumn,
                                                             hidedColumns, model, &trackProfiles);
    tracksCounts = right.tracksCounts;
    pointsCounts = right.pointsCounts;
    unusedPoints = right.unusedPoints;

    return *this;
}

DataSource::~DataSource()
{
    sorter->deleteLater();
    model->deleteLater();
    delete controller;
    delete hidedColumns;
    delete locatorModelColumn;
}

LocatorsTracksViewController* LocatorsTracksViewControllerFactory::create(const QString &locatorType, QTableView *tableView,
                                                                          LocatorModelColumns *locatorModelColumns,
                                                                          HidedColumns *hidedColumns, QStandardItemModel *model,
                                                                          QList<TrackProfile> *trackProfiles)
{
    if(locatorType == LocatorTypeCaption::rlsCaption)
    {
        return new RlsTracksViewController(tableView, locatorModelColumns,
                                           hidedColumns, model, trackProfiles);
    }

    if(locatorType == LocatorTypeCaption::mlatCaption)
    {
        return new MlatTracksViewController(tableView, locatorModelColumns,
                                            hidedColumns, model, trackProfiles);
    }

    if(locatorType == LocatorTypeCaption::sdpdCaption)
    {
        return new SdpdTracksViewController(tableView, locatorModelColumns,
                                            hidedColumns, model, trackProfiles);
    }

    return nullptr;
}

HidedColumns* HidedColumnsFactory::create(const QString &locatorType)
{
    if(locatorType == LocatorTypeCaption::rlsCaption)
    {
        return new RlsHidedColumns();
    }

    if(locatorType == LocatorTypeCaption::mlatCaption)
    {
        return new MlatHidedColumns();
    }

    if(locatorType == LocatorTypeCaption::sdpdCaption)
    {
        return new SdpdHidedColumns();
    }

    return nullptr;
}

LocatorModelColumns* LocatorModelColumnsFactory::create(const QString &locatorType)
{
    if(locatorType == LocatorTypeCaption::rlsCaption)
    {
        return new RlsModelColumns();
    }

    if(locatorType == LocatorTypeCaption::mlatCaption)
    {
        return new MlatModelColumns();
    }

    if(locatorType == LocatorTypeCaption::sdpdCaption)
    {
        return new SdpdModelColumns();
    }

    return nullptr;
}






