#include "MlatTracksViewController.h"
#include "settings.h"
#include "MapStrings.h"
#include "adsbutils.h"

void MlatTracksViewController::processColumnsHiding()
{
    const MlatHidedColumns *mlatHidedColumns = dynamic_cast<const MlatHidedColumns*>(hidedColumns);
    const MlatModelColumns *mlatModelColumns = dynamic_cast<const MlatModelColumns*>(locatorModelColumns);

    bool hideSquawk = tableView->isColumnHidden(mlatModelColumns->Squawk) && mlatHidedColumns->hideSquawk;
    bool hideAddr24Bit = tableView->isColumnHidden(mlatModelColumns->Addr24Bit) && mlatHidedColumns->hideAddr24Bit;
    bool hideCallsign = tableView->isColumnHidden(mlatModelColumns->Callsign) && mlatHidedColumns->hideCallsign;

    tableView->setColumnHidden(mlatModelColumns->Squawk, hideSquawk);
    tableView->setColumnHidden(mlatModelColumns->Addr24Bit, hideAddr24Bit);
    tableView->setColumnHidden(mlatModelColumns->Callsign, hideCallsign);
}

void MlatTracksViewController::addTrackToTable(const TrackProfile &trackProfile, int index)
{
    const MlatModelColumns *mlatModelColumns = dynamic_cast<const MlatModelColumns*>(locatorModelColumns);
    MlatHidedColumns *mlatHidedColumns = dynamic_cast<MlatHidedColumns*>(hidedColumns);
    int trackNumber = index;

    int squawk = trackProfile.reply.size() > 0  ? trackProfile.reply.last() : 0;
    const auto direction = trackProfile.direction;

    QStandardItem *item;
    item = new QStandardItem();
    item->setData(QString::number(trackProfile.trackKey.trackNumber), Qt::DisplayRole);
    //item->setData(trackProfiles.directionToString(), Qt::ToolTipRole);
    item->setData((quint64)(&trackProfile), Qt::UserRole + 2);
    item->setData(trackProfile.directionToRussianString(), Qt::UserRole + 3);
    item->setTextAlignment(Qt::AlignCenter);

    if(direction == TrackProfile::DEPARTURE)
    {
        item->setBackground(QColor(179, 236, 255));
    }
    else if(direction == TrackProfile::ARRIVAL)
    {
        item->setBackground(QColor(194, 169, 122));
    }
    else if(direction == TrackProfile::TRANSIT)
    {
        item->setBackground(QColor(189, 189, 189));
    }
    else if(direction == TrackProfile::GROUND)
    {
        item->setBackground(QColor(219, 240, 204));
    }
    else if(direction == TrackProfile::NONE)
    {
        item->setBackground(QColor(255, 255, 255));
    }

    model->setItem(trackNumber, mlatModelColumns->HiddenService, item);

    item = new QStandardItem();
    item->setData(QString("%1").arg(squawk, 4, 10, QChar('0')), Qt::DisplayRole);
    item->setData(squawk, Qt::UserRole + 2);
    item->setTextAlignment(Qt::AlignCenter);
    model->setItem(trackNumber, mlatModelColumns->Squawk, item);
    if(squawk != 0) mlatHidedColumns->hideSquawk = false;

    item = new QStandardItem();
    item->setData(QString::number(trackProfile.address24bit, 16), Qt::DisplayRole);
    item->setData(trackProfile.address24bit, Qt::UserRole + 2);
    model->setItem(trackNumber, mlatModelColumns->Addr24Bit, item);
    if(trackProfile.address24bit != 0) mlatHidedColumns->hideAddr24Bit = false;

    item = new QStandardItem();
    item->setData(trackProfile.callsign, Qt::DisplayRole);
    model->setItem(trackNumber, mlatModelColumns->Callsign, item);
    if(!trackProfile.callsign.isEmpty()) mlatHidedColumns->hideCallsign = false;

    item = new QStandardItem();
    double verticalSpeedMS = Settings::value(SettingsKeys::AIRCRAFT_AVG_VERTICAL_SPEED).toDouble();
    const QList<double> &altitudes = Statistics::MismatchInfo::filterAltitudeGlitches(trackProfile.points, verticalSpeedMS);
    double minAltitude = trackProfile.minAltitude(altitudes);
    double maxAltitude = trackProfile.maxAltitude(altitudes);
    item->setData(trackProfile.minMaxAltitudeToString(minAltitude, maxAltitude), Qt::DisplayRole);
    item->setData(minAltitude, Qt::UserRole + 2);
    item->setData(maxAltitude, Qt::UserRole + 3);
    item->setTextAlignment(Qt::AlignCenter);
    model->setItem(trackNumber, mlatModelColumns->Altitude, item);

    item = new QStandardItem();
    item->setData(QString::number(trackProfile.lengthKm), Qt::DisplayRole);
    item->setData(trackProfile.lengthKm, Qt::UserRole + 2);
    model->setItem(trackNumber, mlatModelColumns->TotalLength, item);

    item = new QStandardItem();
    item->setData(trackProfile.startDate.toString("dd.MM.yyyy hh:mm:ss"), Qt::DisplayRole);
    item->setData(trackProfile.startDate.toMSecsSinceEpoch()/1000, Qt::UserRole + 2);
    model->setItem(trackNumber, mlatModelColumns->StartDate, item);

    item = new QStandardItem();
    QString pointsCount = QString("%1 / %2").arg(trackProfile.points.size()).
                                             arg(trackProfile.reconstructedTrajectory.size());
    //item->setData( QString::number(trackProfile.points.size()), Qt::DisplayRole);
    item->setData(pointsCount, Qt::DisplayRole);
    item->setData(trackProfile.points.size(), Qt::UserRole + 2);
    item->setTextAlignment(Qt::AlignCenter);
    model->setItem(trackNumber, mlatModelColumns->PointsCount, item);

    item = new QStandardItem();
    QString pDStr = trackProfile.mlatStatistics.PU > 1 ? "NaN" : MapGeometry::fromDouble(trackProfile.mlatStatistics.PU, 3);
    item->setData(pDStr, Qt::DisplayRole);
    item->setData(trackProfile.mlatStatistics.PU, Qt::UserRole + 2);
    model->setItem(trackNumber, mlatModelColumns->TrackUpdateTime, item);
}

void MlatTracksViewController::fillView()
{
    if((!model) || (!tableView) || (!trackProfiles))
    {
        return;
    }

    const MlatModelColumns *mlatModelColumns = dynamic_cast<const MlatModelColumns*>(locatorModelColumns);
    const QStringList &tableHeaders = mlatModelColumns->headersList();
    model->clear();
    model->setRowCount(0);
    model->setColumnCount(tableHeaders.size());
    model->setHorizontalHeaderLabels(tableHeaders);

    for(int column = 0; column < (int)ViewsColumns::Mlat::ColumnsCount; column++)
    {
        model->horizontalHeaderItem(column)->setToolTip(mlatModelColumns->tooltips[column]);
        tableView->setColumnWidth(column, mlatModelColumns->columnsWidth[column]);
    }

    for(int i = 0; i < trackProfiles->size(); i++)
    {
        addTrackToTable((*trackProfiles)[i], i);
    }

    MlatHidedColumns *mlatHidedColumns = dynamic_cast<MlatHidedColumns*>(hidedColumns);
    tableView->setColumnHidden(mlatModelColumns->Squawk, mlatHidedColumns->hideSquawk);
    tableView->setColumnHidden(mlatModelColumns->Addr24Bit, mlatHidedColumns->hideAddr24Bit);
    tableView->setColumnHidden(mlatModelColumns->Callsign, mlatHidedColumns->hideCallsign);

    tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
}

void MlatTracksViewController::processRbsClicked()
{

}

void MlatTracksViewController::processUvdClicked()
{

}

void MlatTracksViewController::processAdsbClicked(bool flag)
{
    Q_UNUSED(flag);
}

