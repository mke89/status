#ifndef FLIGHTZONEUTILS_H
#define FLIGHTZONEUTILS_H

#include <QString>
#include <QStringList>

namespace ZoneTypeCaption
{
    static QString trackCaption = "Трассовая";
    static QString airdromeCaption = "Аэродромная";
    static QString undefinedCaption  = "Undefined";
}

class FlightZoneUtils
{
    public:
        enum ZoneType
        {
            TRACK,
            AIRDROME,
            ZonesCount,
            Undefined,
        };

        FlightZoneUtils();

        static ZoneType getZoneTypeByCaption(const QString &caption);
        static QStringList getZonesTypes();
        static int filterZoneIndex(int index);
        static QString getZoneCaptionByType(ZoneType zoneType);
};

#endif // FLIGHTZONEUTILS_H
