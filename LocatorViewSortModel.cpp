#include "LocatorViewSortModel.h"
#include "adsbutils.h"
#include "LocatorUtils.h"


LocatorSortModel* LocatorSortModelFactory::create(const QString &locatorType)
{
    if(locatorType == LocatorTypeCaption::rlsCaption)
    {
        return new RlsSortModel();
    }

    if(locatorType == LocatorTypeCaption::mlatCaption)
    {
        return new MlatSortModel();
    }

    if(locatorType == LocatorTypeCaption::sdpdCaption)
    {
        return new SdpdSortModel();
    }

    return nullptr;
}

LocatorSortModel::LocatorSortModel()
{
}

void LocatorSortModel::setFilterSettings(StatusSettings settings)
{
    filterSettings = settings;
}

void LocatorSortModel::refilter(QList<TrackType::TrackType> filters)
{
    passFilters = filters;
    invalidateFilter();
}

RlsSortModel::RlsSortModel()
{
}

QString RlsSortModel::locatiorType() const
{
    return LocatorTypeCaption::rlsCaption;
}

bool RlsSortModel::lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const
{
    const QAbstractItemModel *const srcModel = sourceModel();
    if(!srcModel)
    {
        return false;
    }

    QVariant leftData;
    QVariant rightData;

    if((source_left.column() == rlsModelColumns.UvdReplay) ||
       (source_left.column() == rlsModelColumns.Squawk) ||
       (source_left.column() == rlsModelColumns.Addr24Bit) ||
       (source_left.column() == rlsModelColumns.TotalLength) ||
       (source_left.column() == rlsModelColumns.StartDate) ||
       (source_left.column() == rlsModelColumns.PointsCount) ||
       (source_left.column() == rlsModelColumns.PO) ||
       (source_left.column() == rlsModelColumns.PN) ||
       (source_left.column() == rlsModelColumns.PH) ||
       (source_left.column() == rlsModelColumns.SkoRho) ||
       (source_left.column() == rlsModelColumns.SkoTheta))
    {
        leftData  = srcModel->data(source_left, Qt::UserRole + 2);
        rightData = srcModel->data(source_right, Qt::UserRole + 2);

        if(!(leftData.isValid() && rightData.isValid()))
        {
            leftData = srcModel->data(source_left, Qt::DisplayRole);
            rightData = srcModel->data(source_right, Qt::DisplayRole);
        }
    }
    else if((source_left.column() == rlsModelColumns.HiddenService))
    {
        leftData = srcModel->data(source_left, Qt::DisplayRole).toInt();
        rightData = srcModel->data(source_right, Qt::DisplayRole).toInt();
    }
    else if((source_left.column() == rlsModelColumns.Callsign))
    {
        leftData = srcModel->data(source_left, Qt::DisplayRole);
        rightData = srcModel->data(source_right, Qt::DisplayRole);
    }
    else
    {
        leftData  = srcModel->data(source_left, Qt::UserRole + 2);
        rightData = srcModel->data(source_right, Qt::UserRole + 2);

        if((QString(leftData.typeName()) == "MinMaxNUCp") && (QString(rightData.typeName()) == "MinMaxNUCp"))
        {
            MinMaxNUCp leftNUCPs = leftData.value<MinMaxNUCp>();
            MinMaxNUCp rightNUCPs = rightData.value<MinMaxNUCp>();

            if(leftNUCPs.second == rightNUCPs.second)
            {
                return leftNUCPs.first < rightNUCPs.first;
            }

            return leftNUCPs.second < rightNUCPs.second;
        }
    }

    if(leftData.type() == QVariant::Int)
    {
        return leftData.toInt() < rightData.toInt();
    }

    if(leftData.type() == QVariant::UInt)
    {
        return leftData.toUInt() < rightData.toUInt();
    }

    if(leftData.type() == QVariant::Double)
    {
        return leftData.toDouble() < rightData.toDouble();
    }

    if(leftData.type() == QVariant::LongLong)
    {
        return leftData.toLongLong() < rightData.toLongLong();
    }

    return leftData < rightData;
}

bool RlsSortModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    QModelIndex index = sourceModel()->index(source_row, 0, source_parent);
    QVariant value = sourceModel()->data(index, Qt::UserRole + 2);

    if(value.isValid())
    {
        TrackProfile *trackProfile = (TrackProfile *)value.toULongLong();

        if(filterSettings.useGoodTracksOnly)
        {
            if(trackProfile->points.size() < filterSettings.minPointsInGoodTrack)
            {
                return false;
            }

            if(trackProfile->pO < filterSettings.minPOProbability)
            {
                return false;
            }

            if(trackProfile->pN < filterSettings.minPNProbability)
            {
                return false;
            }

            if(trackProfile->pH < filterSettings.minPHProbability)
            {
                return false;
            }

            if((trackProfile->skoTheta * converter::DEGREES_TO_MIN) > filterSettings.maxRmsThetaMinutes)
            {
                return false;
            }

            if((trackProfile->skoRho * converter::NMILE_TO_METRES) > filterSettings.maxRmsRhoMeters)
            {
                return false;
            }
        }

        if(passFilters.contains((TrackType::TrackType)trackProfile->trackKey.type))
        {
            return true;
        }
    }

    return false;
}

MlatSortModel::MlatSortModel()
{
}

QString MlatSortModel::locatiorType() const
{
    return LocatorTypeCaption::mlatCaption;
}

bool MlatSortModel::lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const
{
    const QAbstractItemModel *const srcModel = sourceModel();
    if(!srcModel)
    {
        return false;
    }

    QVariant leftData;
    QVariant rightData;

    if((source_left.column() == mlatModelColumns.Squawk) ||
       (source_left.column() == mlatModelColumns.Addr24Bit) ||
       (source_left.column() == mlatModelColumns.TotalLength) ||
       (source_left.column() == mlatModelColumns.StartDate) ||
       (source_left.column() == mlatModelColumns.PointsCount))
    {
        leftData  = srcModel->data(source_left, Qt::UserRole + 2);
        rightData = srcModel->data(source_right, Qt::UserRole + 2);

        if(!(leftData.isValid() && rightData.isValid()))
        {
            leftData = srcModel->data(source_left, Qt::DisplayRole);
            rightData = srcModel->data(source_right, Qt::DisplayRole);
        }
    }
    else if((source_left.column() == mlatModelColumns.HiddenService))
    {
        leftData = srcModel->data(source_left, Qt::DisplayRole).toInt();
        rightData = srcModel->data(source_right, Qt::DisplayRole).toInt();
    }
    else if((source_left.column() == mlatModelColumns.Callsign))
    {
        leftData = srcModel->data(source_left, Qt::DisplayRole);
        rightData = srcModel->data(source_right, Qt::DisplayRole);
    }
    else
    {
        leftData  = srcModel->data(source_left, Qt::UserRole + 2);
        rightData = srcModel->data(source_right, Qt::UserRole + 2);
    }

    if(leftData.type() == QVariant::Int)
    {
        return leftData.toInt() < rightData.toInt();
    }

    if(leftData.type() == QVariant::UInt)
    {
        return leftData.toUInt() < rightData.toUInt();
    }

    if(leftData.type() == QVariant::Double)
    {
        return leftData.toDouble() < rightData.toDouble();
    }

    if(leftData.type() == QVariant::LongLong)
    {
        return leftData.toLongLong() < rightData.toLongLong();
    }

    return leftData < rightData;
    return false;
}

bool MlatSortModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    QModelIndex index = sourceModel()->index(source_row, 0, source_parent);
    QVariant value = sourceModel()->data(index, Qt::UserRole + 2);

    if(value.isValid())
    {
        TrackProfile *trackProfile = (TrackProfile *)value.toULongLong();

        if(filterSettings.useGoodTracksOnly)
        {
            if(trackProfile->points.size() < filterSettings.minPointsInGoodTrack)
            {
                return false;
            }
        }

        if(passFilters.contains((TrackType::TrackType)trackProfile->trackKey.type))
        {
            return true;
        }
    }

    return false;
}

SdpdSortModel::SdpdSortModel()
{
}

QString SdpdSortModel::locatiorType() const
{
    return LocatorTypeCaption::sdpdCaption;
}

bool SdpdSortModel::lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const
{
    const QAbstractItemModel *const srcModel = sourceModel();
    if(!srcModel)
    {
        return false;
    }

    QVariant leftData;
    QVariant rightData;

    if((source_left.column() == sdpdModelColumns.Squawk) ||
       (source_left.column() == sdpdModelColumns.Addr24Bit) ||
       (source_left.column() == sdpdModelColumns.TotalLength) ||
       (source_left.column() == sdpdModelColumns.StartDate) ||
       (source_left.column() == sdpdModelColumns.PointsCount))
    {
        leftData  = srcModel->data(source_left, Qt::UserRole + 2);
        rightData = srcModel->data(source_right, Qt::UserRole + 2);

        if(!(leftData.isValid() && rightData.isValid()))
        {
            leftData = srcModel->data(source_left, Qt::DisplayRole);
            rightData = srcModel->data(source_right, Qt::DisplayRole);
        }
    }
    else if((source_left.column() == sdpdModelColumns.HiddenService))
    {
        leftData = srcModel->data(source_left, Qt::DisplayRole).toInt();
        rightData = srcModel->data(source_right, Qt::DisplayRole).toInt();
    }
    else if((source_left.column() == sdpdModelColumns.Callsign))
    {
        leftData = srcModel->data(source_left, Qt::DisplayRole);
        rightData = srcModel->data(source_right, Qt::DisplayRole);
    }
    else
    {
        leftData  = srcModel->data(source_left, Qt::UserRole + 2);
        rightData = srcModel->data(source_right, Qt::UserRole + 2);
    }

    if(leftData.type() == QVariant::Int)
    {
        return leftData.toInt() < rightData.toInt();
    }

    if(leftData.type() == QVariant::UInt)
    {
        return leftData.toUInt() < rightData.toUInt();
    }

    if(leftData.type() == QVariant::Double)
    {
        return leftData.toDouble() < rightData.toDouble();
    }

    if(leftData.type() == QVariant::LongLong)
    {
        return leftData.toLongLong() < rightData.toLongLong();
    }

    return leftData < rightData;
    return false;
}

bool SdpdSortModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    QModelIndex index = sourceModel()->index(source_row, 0, source_parent);
    QVariant value = sourceModel()->data(index, Qt::UserRole + 2);

    if(value.isValid())
    {
        TrackProfile *trackProfile = (TrackProfile *)value.toULongLong();

        if(filterSettings.useGoodTracksOnly)
        {
            if(trackProfile->points.size() < filterSettings.minPointsInGoodTrack)
            {
                return false;
            }
        }

        if(passFilters.contains((TrackType::TrackType)trackProfile->trackKey.type))
        {
            return true;
        }
    }

    return false;
}






