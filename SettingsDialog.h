#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include <QString>
#include "settings.h"

struct StatusSettings
{
    enum CalculationMethod
    {
        Chebyshev = 0,
        Wavelets,
        AB_Filter,
        Kalman_Filter,
        PolinominalInterpolation,
        Coiflet,
    };

    int chebyshevSourceTime;
    bool approximatePointsForChebyshew;
    CalculationMethod calcMethod;
    int waveletCalculationMethod;
    QVariantHash calcParams;
    int    minPointsForSplitting;
    double minPercentForAdding;
    bool   useGoodTracksOnly;
    double minPOProbability;
    double minPNProbability;
    double minPHProbability;
    double maxRmsRhoMeters;
    double maxRmsThetaMinutes;
    int minPointsInGoodTrack;

    bool   useRadarSettings;
    double rotationSecManual; // из интерфейса, из данных значение должно иметь приоритет
    double maxDistance; // km
    double minDistance; // km
    double minAlitude;  // m
    double maxAlitude;  // m
    double blindAngle;  // grad

    QString referncePoint; // second point

    bool nextTrackOnRbsChange;
    bool nextTrackOnDeltaAzimuth;
    bool bindByTrackNumber;
    int  newTrackDeltaAzimuthDegree;
    int  maxLocatorDistanceKm;
    int  minTrackPointsCount;

    int  distanceThresholdKm;
    int  timeThresholdSec;

    bool useTracksValidation;
    double rhoValidationThreshold;
    double thetaValidationThreshold;

    int minIntersectedPoints;
    double aircraftAvgVerticalSpeed;

    bool showPa;
    bool showPc;
    bool showPh;

    double unstableZoneWidthPercent;
    int cutBytes;

    StatusSettings()
    {
        load();
    }

    void load()
    {
        approximatePointsForChebyshew = Settings::value(SettingsKeys::APPROXIMATE_POINTS_FOR_CHEBYSHEW).toBool();
        calcMethod = (CalculationMethod)Settings::value(SettingsKeys::CALCULATION_METHOD).toInt();
        waveletCalculationMethod = Settings::value(SettingsKeys::WAVELETS_CALCULATION_METHOD).toInt();
        chebyshevSourceTime = Settings::value(SettingsKeys::CHEBYSHEV_SOURCE_TIME).toInt();
        minPointsForSplitting = Settings::value(SettingsKeys::MIN_POINTS_FOR_SPLIT).toInt();
        minPercentForAdding = Settings::value(SettingsKeys::MIN_PERCENT_FOR_SPLIT_ADD).toDouble();
        useGoodTracksOnly = Settings::value(SettingsKeys::SHOW_ONLY_GOOD_TRACKS).toBool();
        minPOProbability = Settings::value(SettingsKeys::MIN_PROBABILITY_P_O).toDouble();
        minPNProbability = Settings::value(SettingsKeys::MIN_PROBABILITY_P_N).toDouble();
        minPHProbability = Settings::value(SettingsKeys::MIN_PROBABILITY_P_H).toDouble();
        maxRmsRhoMeters = Settings::value(SettingsKeys::MAX_RMS_RHO).toDouble();
        maxRmsThetaMinutes = Settings::value(SettingsKeys::MAX_RMS_THETA).toDouble();
        minPointsInGoodTrack = Settings::value(SettingsKeys::GOOD_TRACK_SIZE).toInt();

        rotationSecManual = Settings::value(SettingsKeys::ROTATION_SEC_MANUAL).toDouble();
        maxDistance       = Settings::value(SettingsKeys::MAX_DISTANCE       ).toDouble(); // km
        minDistance       = Settings::value(SettingsKeys::MIN_DISTANCE       ).toDouble(); // km
        minAlitude        = Settings::value(SettingsKeys::MIN_ALITUDE        ).toDouble();  // m
        maxAlitude        = Settings::value(SettingsKeys::MAX_ALITUDE        ).toDouble();  // m
        blindAngle        = Settings::value(SettingsKeys::BLIND_ANGLE        ).toDouble();  // gr
        referncePoint     = Settings::value(SettingsKeys::REFERENCE_POINT    ).toString();  // latlon

        nextTrackOnRbsChange = Settings::value(SettingsKeys::NEW_TRACK_ON_RBS_CHANGE).toBool();
        nextTrackOnDeltaAzimuth = Settings::value(SettingsKeys::NEW_TRACK_ON_DELTA_CHANGE).toBool();
        newTrackDeltaAzimuthDegree = Settings::value(SettingsKeys::NEW_TRACK_DELTA_AZIMUTH).toInt();
        maxLocatorDistanceKm = Settings::value(SettingsKeys::MAX_LOCATOR_DISTANCE).toInt();
        minTrackPointsCount = Settings::value(SettingsKeys::MIN_TRACK_POINTS_COUNT).toInt();

        distanceThresholdKm = Settings::value(SettingsKeys::DISTANCE_THRESHOLD_KM).toInt();
        timeThresholdSec = Settings::value(SettingsKeys::TIME_THRESHOLD_SEC).toInt();
        bindByTrackNumber = Settings::value(SettingsKeys::BIND_BY_TRACK_NUMBER).toBool();

        useTracksValidation = Settings::value(SettingsKeys::USE_TRACKS_VALIDATION).toBool();
        rhoValidationThreshold = Settings::value(SettingsKeys::RHO_VALIDATION_THRESHOLD).toDouble();
        thetaValidationThreshold = Settings::value(SettingsKeys::THETA_VALIDATION_THRESHOLD).toDouble();
        minIntersectedPoints = Settings::value(SettingsKeys::MIN_INTERSECTED_POINTS).toInt();

        aircraftAvgVerticalSpeed = Settings::value(SettingsKeys::AIRCRAFT_AVG_VERTICAL_SPEED).toDouble();

        showPa = Settings::value(SettingsKeys::MISMATCH_REPORT_SHOW_P_A).toBool();
        showPc = Settings::value(SettingsKeys::MISMATCH_REPORT_SHOW_P_C).toBool();
        showPh = Settings::value(SettingsKeys::MISMATCH_REPORT_SHOW_P_H).toBool();

        unstableZoneWidthPercent = Settings::value(SettingsKeys::UNSTABLE_ZONE_WIDTH_PERCENT).toDouble();
        cutBytes = Settings::value(SettingsKeys::CUT_BYTES).toInt();

        QString params = Settings::value(SettingsKeys::CALCULATION_PARAMS).toString();
        if(params.size() == 0)
        {
            params="ch_n:7;ch_r:3;w_n:3;ab_a:0.7;ab_b:0.5;kf_e_mea:2;kf_e_est:2;kf_q:0.01;pl_n:3";
        }

        QStringList valPairs = params.split(";");

        for( QString valPair: valPairs )
        {
            QStringList tmp=valPair.split(":");
            if(tmp.size() ==2 )
                calcParams.insert(tmp.value(0).trimmed(), tmp.value(1).toDouble());
        }
    }

    void save()
    {
        Settings::setValue(SettingsKeys::APPROXIMATE_POINTS_FOR_CHEBYSHEW, approximatePointsForChebyshew);
        Settings::setValue(SettingsKeys::CALCULATION_METHOD, calcMethod);
        Settings::setValue(SettingsKeys::CHEBYSHEV_SOURCE_TIME, chebyshevSourceTime);
        Settings::setValue(SettingsKeys::WAVELETS_CALCULATION_METHOD, waveletCalculationMethod);
        Settings::setValue(SettingsKeys::MIN_POINTS_FOR_SPLIT, minPointsForSplitting);
        Settings::setValue(SettingsKeys::MIN_PERCENT_FOR_SPLIT_ADD, minPercentForAdding);
        Settings::setValue(SettingsKeys::SHOW_ONLY_GOOD_TRACKS, useGoodTracksOnly);
        Settings::setValue(SettingsKeys::MIN_PROBABILITY_P_O, minPOProbability);
        Settings::setValue(SettingsKeys::MIN_PROBABILITY_P_N, minPNProbability);
        Settings::setValue(SettingsKeys::MIN_PROBABILITY_P_H, minPHProbability);
        Settings::setValue(SettingsKeys::MAX_RMS_RHO, maxRmsRhoMeters);
        Settings::setValue(SettingsKeys::MAX_RMS_THETA, maxRmsThetaMinutes);
        Settings::setValue(SettingsKeys::GOOD_TRACK_SIZE, minPointsInGoodTrack);

        Settings::setValue(SettingsKeys::ROTATION_SEC_MANUAL, rotationSecManual);
        Settings::setValue(SettingsKeys::MAX_DISTANCE       , maxDistance      ); // km
        Settings::setValue(SettingsKeys::MIN_DISTANCE       , minDistance      ); // km
        Settings::setValue(SettingsKeys::MIN_ALITUDE        , minAlitude       );  // m
        Settings::setValue(SettingsKeys::MAX_ALITUDE        , maxAlitude       );  // m
        Settings::setValue(SettingsKeys::BLIND_ANGLE        , blindAngle       );  // gr
        Settings::setValue(SettingsKeys::REFERENCE_POINT    , referncePoint    );  // latlon

        Settings::setValue(SettingsKeys::NEW_TRACK_ON_RBS_CHANGE, nextTrackOnRbsChange);
        Settings::setValue(SettingsKeys::NEW_TRACK_ON_DELTA_CHANGE, nextTrackOnDeltaAzimuth);
        Settings::setValue(SettingsKeys::NEW_TRACK_DELTA_AZIMUTH, newTrackDeltaAzimuthDegree);
        Settings::setValue(SettingsKeys::MAX_LOCATOR_DISTANCE, maxLocatorDistanceKm);
        Settings::setValue(SettingsKeys::MIN_TRACK_POINTS_COUNT, minTrackPointsCount);

        Settings::setValue(SettingsKeys::DISTANCE_THRESHOLD_KM, distanceThresholdKm);
        Settings::setValue(SettingsKeys::TIME_THRESHOLD_SEC, timeThresholdSec);
        Settings::setValue(SettingsKeys::BIND_BY_TRACK_NUMBER, bindByTrackNumber);

        Settings::setValue(SettingsKeys::USE_TRACKS_VALIDATION, useTracksValidation);
        Settings::setValue(SettingsKeys::RHO_VALIDATION_THRESHOLD, rhoValidationThreshold);
        Settings::setValue(SettingsKeys::THETA_VALIDATION_THRESHOLD, thetaValidationThreshold);

        Settings::setValue(SettingsKeys::MIN_INTERSECTED_POINTS, minIntersectedPoints);

        QStringList valPairs;
        for( QString key: calcParams.keys() )
        {
            QString val=QString("%1:%2").arg(key).arg(calcParams[key].toDouble());
            valPairs<<val;
        }
        QString res=valPairs.join(";");
        Settings::setValue(SettingsKeys::CALCULATION_PARAMS, res);

        Settings::setValue(SettingsKeys::AIRCRAFT_AVG_VERTICAL_SPEED, aircraftAvgVerticalSpeed);

        Settings::setValue(SettingsKeys::MISMATCH_REPORT_SHOW_P_A, showPa);
        Settings::setValue(SettingsKeys::MISMATCH_REPORT_SHOW_P_C, showPc);
        Settings::setValue(SettingsKeys::MISMATCH_REPORT_SHOW_P_H, showPh);

        Settings::setValue(SettingsKeys::UNSTABLE_ZONE_WIDTH_PERCENT, unstableZoneWidthPercent);
        Settings::setValue(SettingsKeys::CUT_BYTES, cutBytes);
    }
};

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(QWidget *parent = 0);
    ~SettingsDialog();

signals:
    void sendUpdatedSettings(StatusSettings);

private slots:
    void on_pb_apply_clicked();
    void on_pb_cancel_clicked();

private:
    void loadSettings();
    StatusSettings saveSettings();
    void setWaveletCalculationMethod(int calcMethod);
    void setChebyshevSourceTime(int sourceTime);
    int getWaveletCalculationMethod();
    int getChebyshevSourceTime();

    void hideMethodsSettings();
    void hideChebyshevMethodsSettings();
    void hideWaveletsMethodsSettings();
    void hideABFilterMethodsSettings();
    void hideKalmanMethodsSettings();
    void hidePlynomialMethodsSettings();
    void hideSynchronization();

private:
    Ui::SettingsDialog *ui;
};

#endif // SETTINGSDIALOG_H
