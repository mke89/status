#include "MapActions.h"
#include "MapSpherical.h"
#include "gpoint.h"
#include "gcalc.h"
#include "settings.h"
#include "adsbutils.h"
#include "channelkeeper.h"
#include "assessmentprocedureswidget.h"
#include <QHostInfo>

MapActions &MapActions::Instance()
{
    static MapActions theSingleInstance;
    return theSingleInstance;
}

MapActions::MapActions()
{

}

void MapActions::showTrackOnMap(const TrackProfile &trackProfile, bool withFormulars,
                                bool withLine, bool useRecalc, bool withMenu)
{
    static QStringList stylesList;
    static int styleNumber = 0;
    if(stylesList.isEmpty())
    {
        stylesList  <<  "Style 01";
        stylesList  <<  "Style 02";
        stylesList  <<  "Style 03";
        stylesList  <<  "Style 04";
        stylesList  <<  "Style 05";
        stylesList  <<  "Style 06";
        stylesList  <<  "Style 07";
        stylesList  <<  "Style 08";
        stylesList  <<  "Style 09";
    }

    xProtoHashPacker hpCommand(300);
    hpCommand.addData("I300/010", "type", "TO_MAP_OBJECT_PUT");
    hpCommand.addData("I300/020", "object_type", "FLIGHT_ROUTE");
    hpCommand.addData("I300/060", "host_name", QHostInfo::localHostName());
    hpCommand.addData("I300/060", "app_name", "packet_inspector");

    xProtoHashPacker hpTrackData(312);
    const qint32 trkTargAddr = trackProfile.trackKey.trackNumber;
    QString styleName = stylesList.value((styleNumber++) % 9);

    QString title;
    if(!trackProfile.callsign.isEmpty())
    {
        title = trackProfile.callsign;
    }
    else if(trackProfile.address24bit > 0)
    {
        title = "24 бит аддр.: " + QString::number(trackProfile.address24bit, 16);
    }
    else if((trackProfile.reply.size() > 0) && (trackProfile.reply.last() != 0))
    {
        title = "Код А: " + QString::number(trackProfile.reply.last(), 10);
    }
    else if(trackProfile.uvdReplay != 0)
    {
        title = "УВД: " + QString::number(trackProfile.uvdReplay, 10);
    }
    else
    {
        title = "TA:" + QString::number(trackProfile.trackKey.trackNumber);
    }

    qDebug() << "title: " << title;

    QString planId = QString("packet_inspector_%1_%2_%3").arg(trkTargAddr).arg(useRecalc).arg(styleName);
    hpTrackData.addData("I312/010", "id", planId);
    hpTrackData.addData("I312/040", "style", styleName);
    hpTrackData.addData("I312/020", "acid", title);
    hpTrackData.addData("I312/090", "skeleton", withLine == 0 ? 0 : 0);

    xProtoHashPacker hpRouteElement(313);
    if(useRecalc)
    {
        GPoint recalcPoint(Settings::instance().value(SettingsKeys::LOCATOR_POSITION).toString());
        //MapGeometry::point centerPoint(recalcPoint.latitude(),recalcPoint.longitude());
        for(const auto &point: trackProfile.reconstructedTrajectory)
        {
            if(MapGeometry::isNan(point.rho) || MapGeometry::isNan(point.theta))
                continue;

            GPoint pnt = GCalc().azimutWalk(recalcPoint, point.theta, point.rho * converter::NMILE_TO_METRES / 1000.);

            hpRouteElement.addData("I313/010", "element_type", "POINT");
            hpRouteElement.addData("I313/030", "lat", pnt.latitude());
            hpRouteElement.addData("I313/030", "lon", pnt.longitude());
            hpRouteElement.addData("I313/030", "line_width", 5);

            if(!MapGeometry::isNan(point.time))
            {
                hpRouteElement.addData("I313/060", "sec_min", point.time);
                hpRouteElement.addData("I313/060", "sec_max", point.time);
            }

            hpRouteElement.addData("I313/020", "name", "Сглаженный трек");
            hpRouteElement.addData("I313/070", "show_form", withFormulars ? 1 : 1); //0 = off formular
            hpRouteElement.addNewRecord();
        }
    }
    else
    {
        for(const auto &point: trackProfile.points)
        {
            if(MapGeometry::isNan(point.latitude) || MapGeometry::isNan(point.longitude))
                continue;

            hpRouteElement.addData("I313/010", "element_type", "POINT");
            hpRouteElement.addData( "I313/030", "lat",  point.latitude );
            hpRouteElement.addData( "I313/030", "lon",  point.longitude );
            hpRouteElement.addData("I313/030", "line_width", 5);

            if(!MapGeometry::isNan(point.dateTime))
            {
                hpRouteElement.addData("I313/060", "sec_min", point.dateTime);
                hpRouteElement.addData("I313/060", "sec_max", point.dateTime);
            }

            hpRouteElement.addData("I313/020", "name", "Исходный трек");
            hpRouteElement.addData("I313/070", "show_form", withFormulars ? 1 : 1); //0 = off formular
            hpRouteElement.addNewRecord();
        }
    }

    hpTrackData.addExplicitData("I312/060", hpRouteElement.getPackedData());

    QByteArray packet = hpCommand.getPackedData();
    packet.append(hpTrackData.getPackedData());
    ChannelKeeper::Instance().sendToMap(packet);

    if(withMenu)
    {
        hpCommand.clear();
        hpCommand.addData("I300/010", "type", "MENU_COMMAND_ADD" );
        hpCommand.addData("I300/020", "object_type", "FLIGHT_ROUTE" );
        hpCommand.addData("I300/060", "host_name", QHostInfo::localHostName());
        hpCommand.addData("I300/060", "app_name", "pivp2panda" );

        xProtoHashPacker cmdRemoveTrajectory(340);
        createExecutableMenu(cmdRemoveTrajectory, MapCommands::DELETE_TARJECTORY, planId, "Удалить траекторию с карты");
        createExecutableMenu(cmdRemoveTrajectory, MapCommands::SELECT_TRAJECTORY, planId, "Выделить траекторию");
        createExecutableMenu(cmdRemoveTrajectory, MapCommands::SHOW_POINTS_LIST, planId, "Показать список точек");

        ChannelKeeper::Instance().sendToMap(hpCommand.getPackedData() + cmdRemoveTrajectory.getPackedData());
    }
}

void MapActions::createExecutableMenu(xProtoHashPacker &menuCmd, const QString &commandId, const QString &itemId, const QString &text)
{
    menuCmd.addData("I340/010", "commandId", commandId);
    menuCmd.addData("I340/010", "itemId", itemId);
    menuCmd.addData("I340/010", "text", text);
    menuCmd.addData("I340/010", "enabled", 1);
    menuCmd.addData("I340/010", "blocking", 1);
    menuCmd.addData("I340/010", "checkable", 0);
    menuCmd.addData("I340/010", "checked", 0);
    menuCmd.addNewRecord();
}

