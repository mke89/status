#include "AssessmentSortModel.h"
#include "adsbutils.h"

SortModel::SortModel(): filterSettings(StatusSettings())
{
}

void SortModel::setFilterSettings(StatusSettings settings)
{
    filterSettings = settings;
}

void SortModel::refilter(QList<TrackType::TrackType> filters)
{
    passFilters = filters;
    invalidateFilter();
}

bool SortModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    QModelIndex index = sourceModel()->index(source_row, 0, source_parent);
    QVariant value = sourceModel()->data(index, Qt::UserRole + 2);

    if(value.isValid())
    {
        TrackProfile *trackProfile = (TrackProfile *)value.toULongLong();

        if(filterSettings.useGoodTracksOnly)
        {
            if(trackProfile->points.size() < filterSettings.minPointsInGoodTrack)
            {
                return false;
            }

            if(trackProfile->pO < filterSettings.minPOProbability)
            {
                return false;
            }

            if(trackProfile->pN < filterSettings.minPNProbability)
            {
                return false;
            }

            if(trackProfile->pH < filterSettings.minPHProbability)
            {
                return false;
            }

            if((trackProfile->skoTheta * converter::DEGREES_TO_MIN) > filterSettings.maxRmsThetaMinutes)
            {
                return false;
            }

            if((trackProfile->skoRho * converter::NMILE_TO_METRES) > filterSettings.maxRmsRhoMeters)
            {
                return false;
            }
        }

        if(passFilters.contains((TrackType::TrackType)trackProfile->trackKey.type))
        {
            return true;
        }
    }

    return false;
}

bool SortModel::lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const
{
    const QAbstractItemModel *const srcModel = sourceModel();
    if(!srcModel) return false;

    QVariant leftData;
    QVariant rightData;

    if((source_left.column() == StatisticsColumns::UvdReplay) ||
       (source_left.column() == StatisticsColumns::Squawk) ||
       (source_left.column() == StatisticsColumns::Addr24Bit) ||
       (source_left.column() == StatisticsColumns::TotalLength) ||
       (source_left.column() == StatisticsColumns::StartDate) ||
       (source_left.column() == StatisticsColumns::PointsCount) ||
       (source_left.column() == StatisticsColumns::PO) ||
       (source_left.column() == StatisticsColumns::PN) ||
       (source_left.column() == StatisticsColumns::PH) ||
       (source_left.column() == StatisticsColumns::SkoRho) ||
       (source_left.column() == StatisticsColumns::SkoTheta))
    {
        leftData  = srcModel->data(source_left, Qt::UserRole + 2);
        rightData = srcModel->data(source_right, Qt::UserRole + 2);

        if(!(leftData.isValid() && rightData.isValid()))
        {
            leftData = srcModel->data(source_left, Qt::DisplayRole);
            rightData = srcModel->data(source_right, Qt::DisplayRole);
        }
    }
    else if((source_left.column() == StatisticsColumns::HiddenService))
    {
        leftData = srcModel->data(source_left, Qt::DisplayRole).toInt();
        rightData = srcModel->data(source_right, Qt::DisplayRole).toInt();
    }
    else if((source_left.column() == StatisticsColumns::Callsign))
    {
        leftData = srcModel->data(source_left, Qt::DisplayRole);
        rightData = srcModel->data(source_right, Qt::DisplayRole);
    }
    else
    {
        leftData  = srcModel->data(source_left, Qt::UserRole + 2);
        rightData = srcModel->data(source_right, Qt::UserRole + 2);

        if((QString(leftData.typeName()) == "MinMaxNUCp") && (QString(rightData.typeName()) == "MinMaxNUCp"))
        {
            MinMaxNUCp leftNUCPs = leftData.value<MinMaxNUCp>();
            MinMaxNUCp rightNUCPs = rightData.value<MinMaxNUCp>();

            if(leftNUCPs.second == rightNUCPs.second)
            {
                return leftNUCPs.first < rightNUCPs.first;
            }

            return leftNUCPs.second < rightNUCPs.second;
        }
    }

    if(leftData.type() == QVariant::Int)
    {
        return leftData.toInt() < rightData.toInt();
    }

    if(leftData.type() == QVariant::UInt)
    {
        return leftData.toUInt() < rightData.toUInt();
    }

    if(leftData.type() == QVariant::Double)
    {
        return leftData.toDouble() < rightData.toDouble();
    }

    if(leftData.type() == QVariant::LongLong)
    {
        return leftData.toLongLong() < rightData.toLongLong();
    }

    return leftData < rightData;
}

