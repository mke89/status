#include "FlightZoneUtils.h"

FlightZoneUtils::FlightZoneUtils()
{

}

FlightZoneUtils::ZoneType FlightZoneUtils::getZoneTypeByCaption(const QString &caption)
{
    if(caption == ZoneTypeCaption::airdromeCaption)
    {
        return ZoneType::AIRDROME;
    }

    if(caption == ZoneTypeCaption::trackCaption)
    {
        return ZoneType::TRACK;
    }

    return ZoneType::Undefined;
}

QStringList FlightZoneUtils::getZonesTypes()
{
    return QStringList{ZoneTypeCaption::trackCaption,
                       ZoneTypeCaption::airdromeCaption};
}

int FlightZoneUtils::filterZoneIndex(int index)
{
    if((index < 0) || (index >= ZoneType::ZonesCount))
    {
        return ZoneType::AIRDROME;
    }

    return index;
}

QString FlightZoneUtils::getZoneCaptionByType(FlightZoneUtils::ZoneType zoneType)
{
    if(zoneType == ZoneType::AIRDROME)
    {
        return ZoneTypeCaption::airdromeCaption;
    }

    if(zoneType == ZoneType::TRACK)
    {
        return ZoneTypeCaption::trackCaption;
    }

    return ZoneTypeCaption::undefinedCaption;
}
