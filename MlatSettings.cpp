#include "MlatSettings.h"
#include "settings.h"


MlatSettings::MlatSettings()
{
    load();
}

void MlatSettings::load()
{
    int zoneType = Settings::value(SettingsKeys::MLAT_ZONE_TYPE).toInt();
    flightZoneType = FlightZoneUtils::filterZoneIndex(zoneType);
    locationZoneCoords = Settings::value(SettingsKeys::MLAT_LOCATION_ZONE_COORDS).toString();
    airdromeUpdateDeltaTimeMs = Settings::value(SettingsKeys::MLAT_AIRDROME_UPDATE_DELTA_MS, 100).toDouble();
    trackUpdateDeltaTimeMs = Settings::value(SettingsKeys::MLAT_TRACK_UPDATE_DELTA_MS, 100).toDouble();
    altitudeFromM = Settings::value(SettingsKeys::MLAT_ALTITUDE_FROM_M).toDouble();
    altitudeToM = Settings::value(SettingsKeys::MLAT_ALTITUDE_TO_M).toDouble();
}

void MlatSettings::save()
{
    Settings::setValue(SettingsKeys::MLAT_ZONE_TYPE, flightZoneType);
    Settings::setValue(SettingsKeys::MLAT_LOCATION_ZONE_COORDS, locationZoneCoords);

    Settings::setValue(SettingsKeys::MLAT_AIRDROME_UPDATE_DELTA_MS, airdromeUpdateDeltaTimeMs);
    Settings::setValue(SettingsKeys::MLAT_TRACK_UPDATE_DELTA_MS, trackUpdateDeltaTimeMs);
    Settings::setValue(SettingsKeys::MLAT_ALTITUDE_FROM_M, altitudeFromM);
    Settings::setValue(SettingsKeys::MLAT_ALTITUDE_TO_M, altitudeToM);
}




