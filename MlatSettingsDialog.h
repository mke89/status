#ifndef MLATSETTINGSDIALOG_H
#define MLATSETTINGSDIALOG_H

#include <QDialog>
#include "MlatSettings.h"

namespace Ui {
class MlatSettingsDialog;
}

class MlatSettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MlatSettingsDialog(QWidget *parent = nullptr);
    ~MlatSettingsDialog();
    static QStringList coordinatesList(const QString &coordsString);

signals:
    void sendUpdatedSettings(MlatSettings);

private slots:
    void on_pb_apply_clicked();
    void on_pb_cancel_clicked();
    void on_cb_flightZone_activated(const QString &arg1);
    void on_pb_showMlatZone_clicked();

private:
    void loadSettings();
    MlatSettings saveSettings();

    bool correctCoord(const QString &coord) const;
    int findIncorrectCoordNumber(const QStringList &coordsList);

private:
    Ui::MlatSettingsDialog *ui;
};

#endif // MLATSETTINGSDIALOG_H
