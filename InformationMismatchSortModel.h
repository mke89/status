#ifndef INFORMATIONMISMATCHSORTMODEL_H
#define INFORMATIONMISMATCHSORTMODEL_H

#include <QSortFilterProxyModel>

namespace MismatchColumns
{
    enum MismatchColumns
    {
        IntersectionNumber = 0,
        Track1Id,
        Pa1,
        Pc1,
        Ph1,
        Track2Id,
        Pa2,
        Pc2,
        Ph2,
        IntersectedPoints,
        ColumnsCount
    };
}


class InformationMismatchSortModel: public QSortFilterProxyModel
{
    Q_OBJECT

    public:
        InformationMismatchSortModel();
        virtual ~InformationMismatchSortModel();

    protected:
        bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;
};

#endif // INFORMATIONMISMATCHSORTMODEL_H
