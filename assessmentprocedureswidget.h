#ifndef ASSESSMENTPROCEDURESWIDGET_H
#define ASSESSMENTPROCEDURESWIDGET_H

#include <QWidget>

#include <QDateEdit>
#include "archivereader.h"
#include "trackprofile.h"
#include <QStandardItemModel>
#include <QSortFilterProxyModel>
#include <QStyledItemDelegate>
#include <QPushButton>
#include "SettingsDialog.h"
#include "MlatSettingsDialog.h"
#include "reportwidget.h"
#include "xprotohashreader.h"
#include "AssessmentSortModel.h"
#include "LocatorUtils.h"
#include "DataSource.h"

namespace Ui {
class AssessmentProceduresWidget;
}

class TooltipItemDelegate : public QStyledItemDelegate
{
    Q_OBJECT

    bool space;
    public:
    TooltipItemDelegate()
    {
        space = false;
    }
    virtual bool helpEvent(QHelpEvent *event, QAbstractItemView *view, const QStyleOptionViewItem &option, const QModelIndex &index) override;
};



class AssessmentProceduresWidget : public QWidget
{
    Q_OBJECT

public:
    explicit AssessmentProceduresWidget( QWidget *parent = 0 );
    ~AssessmentProceduresWidget();

signals:
    void sendFilters(QList<TrackType::TrackType> filters);
    void compute(void);
    void addToTracks(void);
    void mergeFiles(void);
    void locatorModeChanged(QString mode);

private slots:
    void showCustomContextMenu(QPoint position);
    void on_newTracksProfiles(QList<TrackProfile> tracksProfiles, bool append);
    void updateTracksOnButtons(QHash<TrackType::TrackType, int> tracksPoints, bool append);
    void setPointsCounters(QHash<TrackType::TrackType, int> tracksPoints, bool append);
    void receiveSubtrack(TrackProfile trackProfile);
    void receiveUnusedPoints(QList<SPoint> unusedPoints, bool append);
    void on_pb_useRbs_clicked();
    void on_pb_useUvd_clicked();
    void on_pb_useAdsb_clicked();
    void on_p_useModeS_clicked();
    void on_pb_mlat_clicked();
    void on_pb_unused_clicked();
    void clearReportPtr(ReportWidget* reportWidget);
    void sendSelectedRowsToShowOnMap(const QModelIndexList &indexes, bool withFormulars = false,
                                     bool withLine = false, bool useRecalc = false);
    void updateSettings(StatusSettings settings);
    void on_tb_showPoints_clicked();
    void on_tb_splitAutomatically_clicked();
    void on_tb_showOnMapOriginal_clicked();
    void on_tb_showOnMapSmoothed_clicked();
    void on_tb_addToReport_clicked();
    void on_tb_setPosition_clicked();
    void on_tb_settings_clicked();
    void on_tb_save_clicked();
    void on_tb_merge_clicked();
    void on_tb_compute_clicked();
    void on_tb_addToTracks_clicked();
    void on_tb_mergeFiles_clicked();
    void processResponceFromMap(const QByteArray &data);
    void showTracksOnMap(QList<int> tracksIdList);
    void on_tb_showLocatorOnMap_clicked();
    void on_tb_fakeSamples_clicked();
    void on_tb_calculateFaultPointsCount_clicked();
    void on_cb_mode_activated(const QString &arg1);
    void on_tb_mlatSettings_clicked();
    void on_filesTableHided(int tableSize);

    void on_cb_fileSelector_currentTextChanged(const QString &arg1);

private:
    QList<TrackType::TrackType> fillFilters();
    void calculateAllStatistics(QList<TrackProfile> &tracksProfiles);
    void calculateProfileStatistics(TrackProfile &trackProfile);
    void toggleColor(QPushButton *button);
    double percentFromTotal(const QPair<int, int> &intervalParams, int totalPoints) const;
    QList<QPair<int, int>> getSubIntervalsParams(const TrackProfile *profile) const;
    bool addSubintervals(const QList<QPair<int, int> > intervalsList, const TrackProfile *profile);
    void updateView(QPushButton *button);
    void splitAutomatically(QModelIndex &index);
    void addToReportData();
    QList<TrackProfile> applayRlsParamsAsFilter(const QList<TrackProfile> trackProfiles);
    void removeSelectedRow(QModelIndex &rowIndex);
    QString datagramToText(const QByteArray& datagram);
    QList<QPair<QDateTime, QByteArray>> dumpTrack(QList<SPoint> trackPoints, int newTrackNumber);
    void saveTrackWithId();
    void sendMapCommandAnswer(xProtoHashReader &mapCmd);
    int getTrackIdFromIndexId(QString itemId);
    bool isTrackSmoothed(QString itemId);
    TrackProfile* getTrackProfileByTrackId(int trackId);
    bool showTrackProfilePoints(TrackProfile* profile, bool smoothed);
    int getTrackRowInTableByTrackId(int trackId);
    TrackProfile* getTrackProfileByTrackIdFromFullModel(int trackId);
    bool areTracksTypeTheSame(const QModelIndexList &selectedRows);
    bool areTracksIdTheSame(const QModelIndexList &selectedRows);
    void refreshTracksCountsOnButtons();
    bool tracksIntersectsByTime(const TrackProfile* lhs, const TrackProfile* rhs) const;
    bool azimuthsInDelta(double azimuth1, double azimuth2, double deltaDeg = 2.3) const;
    int getTrackNumberByTrackNumber(int trackNumber);
    void clearPointsMarks(QList<SPoint> &points);

    QList<SPoint> sortPointsByTime(QList<SPoint> points);
    QList<SPoint> extractUnstablePoints(QList<SPoint> &points);
    QList<SPoint> extractReflectedAndFilteredPoints(QList<SPoint> &points);
    QPair<SPoint, SPoint> getPointsNeighbouringByTime(double pointTime, const QList<SPoint> &track);
    bool pointInParameters(double checkingParam, double point1Param, double point2Param);
    bool pointInAzimuth(double checkingAzimuth, double checkingAzimuth1, double checkingAzimuth2);
    int getRbsAndUvdTracksCount();
    QList<TrackProfile*> getTracksWithUvdCode(uint32_t uvdCode);
    QList<TrackProfile*> getTracksWithSquawk(uint16_t squawk);
    QList<SPoint> getTracksPoints();
    void mlatSettingsButtonChangeEnabledState(LocatorUtils::LocatorType locatorType);

    bool initModels(const QStringList &availableLocators, const QString &currentLocator);
    bool switchModels(const QString &locatorType);
    void processTracksTypesButtos(const QString &locatorType);

    void appendDataToFileSelector(const QString &fileName);
    QList<TrackProfile> convertToTrackProfile(QMap<QString, QList<SPoint>> mminfo);
    void filterDuplicatingTrack();

private:
    Ui::AssessmentProceduresWidget *ui;
    QStandardItemModel                  *currentModel;
    LocatorSortModel                    *currentSorter;
    QList<TrackProfile>                 *currentTrackProfiles;
    LocatorsTracksViewController        *currentController;
    QHash<TrackType::TrackType, int>    *currentTracksCounts;
    QHash<TrackType::TrackType, int>    *currentPointsCounts;
    QList<SPoint>                       *currentUnusedPoints;

    QString currerntFileName;
    QHash<QString, DataSource> dataSources; // LocatorTypeCaption type-keys OR filenames

    QColor initialColor;
    QList<SPoint> faultPoints;
    StatusSettings statusSettings;

    ReportWidget *rwAll;
    ReportWidget *rwAdsb;
    int calculatedRotationPeriod = 6000;
    bool interpretEachFileAsDataStorage;
    QString lastLocatorType;
};

#endif // ASSESSMENTPROCEDURESWIDGET_H
