#ifndef LOCATORVIEWSORTMODEL_H
#define LOCATORVIEWSORTMODEL_H

#include <QWidget>
#include <QSortFilterProxyModel>
#include "SettingsDialog.h"
#include "trackprofile.h"
#include "LocatorModelColumns.h"

class LocatorSortModel: public QSortFilterProxyModel
{
    Q_OBJECT

    public:
        LocatorSortModel();
        virtual ~LocatorSortModel(){};
        void setFilterSettings(StatusSettings settings);
        virtual QString locatiorType() const = 0;

    public slots:
        void refilter(QList<TrackType::TrackType> filters);

    private:
        virtual bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const = 0;
        virtual bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const = 0;

    protected:
        QList<TrackType::TrackType> passFilters;
        StatusSettings filterSettings;
};

class RlsSortModel: public LocatorSortModel
{
    Q_OBJECT

    public:
        RlsSortModel();
        virtual ~RlsSortModel(){};
        virtual QString locatiorType() const override;

    private:
        bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const override;
        bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;

        RlsModelColumns rlsModelColumns;
};

class MlatSortModel: public LocatorSortModel
{
    Q_OBJECT

    public:
        MlatSortModel();
        virtual ~MlatSortModel(){};
        virtual QString locatiorType() const override;

    private:
        bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const override;
        bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;

        MlatModelColumns mlatModelColumns;
};

class SdpdSortModel: public LocatorSortModel
{
    Q_OBJECT

    public:
        SdpdSortModel();
        virtual ~SdpdSortModel(){};
        virtual QString locatiorType() const override;

    private:
        bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const override;
        bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;

        SdpdModelColumns sdpdModelColumns;
};

class LocatorSortModelFactory
{
    public:
        static LocatorSortModel* create(const QString &locatorType);
};


#endif // LOCATORVIEWSORTMODEL_H
