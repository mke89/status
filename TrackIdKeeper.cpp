#include "TrackIdKeeper.h"

TrackIdKeeper::TrackIdKeeper(): id(0)
{
}

TrackIdKeeper& TrackIdKeeper::Instance()
{
    static TrackIdKeeper instance;
    return instance;
}

int TrackIdKeeper::nextId()
{
    return id++;
}


