#ifndef MAPACTIONS_H
#define MAPACTIONS_H

#include <QObject>
#include "trackprofile.h"
#include "xprotohashpacker.h"

namespace MapCommands
{
    const QString DELETE_TARJECTORY = "DELETE_TARJECTORY";
    const QString SELECT_TRAJECTORY = "SELECT_TARJECTORY";
    const QString SHOW_POINTS_LIST = "SHOW_POINTS_LIST";
}

class MapActions: public QObject
{
    Q_OBJECT

    public:
        static MapActions& Instance();
        void showTrackOnMap(const TrackProfile &trackProfile, bool withFormulars = false,
                            bool withLine = false, bool useRecalc = false, bool withMenu = false);

    private:
        void createExecutableMenu(xProtoHashPacker &menuCmd, const QString &commandId, const QString &itemId, const QString &text);
        MapActions();
        MapActions(const MapActions& root) = delete;
        MapActions& operator=(const MapActions&) = delete;
};

#endif // MAPACTIONS_H
