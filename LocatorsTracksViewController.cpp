#include "LocatorsTracksViewController.h"


QModelIndexList LocatorsTracksViewController::getSelectedRowsFromTableView() const
{
    if((!tableView) || (!locatorModelColumns))
    {
        return QModelIndexList();
    }

    return tableView->selectionModel()->selectedRows(locatorModelColumns->HiddenService);
}



