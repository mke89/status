#include "assessmentprocedureswidget.h"
#include "ui_assessmentprocedureswidget.h"
#include <QMessageBox>
#include <QFileDialog>
#include "archivereader.h"
#include "settings.h"
#include "functional"
#include "gcalc.h"
#include <QDebug>
#include "cutil.h"
#include "adsbutils.h"
#include "pointlistwidget.h"
#include <QMenu>
#include <QAction>
#include "statistics.h"
#include "xprotohashpacker.h"
#include "xprotodecoder.h"
#include <QHostInfo>
#include "channelkeeper.h"
#include "MapStrings.h"
#include "CoordinatesDialog.h"
#include "MapConstants.h"
#include "spoint.h"
#include <iostream>
#include <fstream>
#include "TrackIdKeeper.h"
#include "MapConstants.h"
#include "asterixencoder.h"
#include <QToolTip>
#include <QHelpEvent>
#include <QDateTime>
#include "asterixutils.h"
#include "TcpConnector.h"
#include "MapSpherical.h"
#include "InformationMismatchWidget.h"
#include "MapActions.h"
#include "FaultTargetsCalculatorWidget.h"
#include "FalsePointsStatisticsCalculator.h"
#include "MlatSettingsDialog.h"
#include "MlatStatistics.h"


AssessmentProceduresWidget::AssessmentProceduresWidget(QWidget *parent):
    QWidget(parent),
    ui(new Ui::AssessmentProceduresWidget),
    currentModel(nullptr),
    currentSorter(nullptr),
    currentTrackProfiles(nullptr),
    currentController(nullptr),
    currentTracksCounts(nullptr),
    currentPointsCounts(nullptr),
    statusSettings(StatusSettings()),
    rwAll(nullptr),
    rwAdsb(nullptr),
    interpretEachFileAsDataStorage(false),
    lastLocatorType(QString())
{
    ui->setupUi(this);
    ui->tableView->setContextMenuPolicy(Qt::CustomContextMenu);

    connect(ui->tableView, SIGNAL(customContextMenuRequested(QPoint)), SLOT(showCustomContextMenu(QPoint)));
    connect(ui->widget, SIGNAL(tracksReday(QList<TrackProfile>, bool)),
            this, SLOT(on_newTracksProfiles(QList<TrackProfile>, bool)));
    connect(ui->widget, SIGNAL(updateTracksCounters(QHash<TrackType::TrackType, int>, bool)),
            this, SLOT(updateTracksOnButtons(QHash<TrackType::TrackType, int>, bool)));
    connect(ui->widget, SIGNAL(updatePointsCounters(QHash<TrackType::TrackType, int>, bool)),
            this, SLOT(setPointsCounters(QHash<TrackType::TrackType, int>, bool)));
    connect(ui->widget, SIGNAL(sendUnusedPoints(QList<SPoint>, bool)),
            this, SLOT(receiveUnusedPoints(QList<SPoint>, bool)));
    connect(ui->widget, SIGNAL(tableHided(int)), this, SLOT(on_filesTableHided(int)));

    connect(this, SIGNAL(compute(void)), ui->widget, SLOT(on_compute(void)));
    connect(this, SIGNAL(addToTracks(void)), ui->widget, SLOT(on_addToTracks(void)));
    connect(this, SIGNAL(mergeFiles(void)), ui->widget, SLOT(on_mergeFiles(void)));
    connect(this, SIGNAL(locatorModeChanged(QString)), ui->widget, SLOT(on_changeLocatorMode(QString)));
    connect(this, SIGNAL(locatorModeChanged(QString)), ui->widget, SLOT(on_changeLocatorMode(QString)));

    connect(&TcpConnector::instance(), SIGNAL(messageReceived(const QByteArray&)),
            this, SLOT(processResponceFromMap(const QByteArray&)));

    ui->tableView->setSortingEnabled(true);
    ui->tableView->setItemDelegate(new TooltipItemDelegate());

    int rlsType = Settings::value(SettingsKeys::MLAT_RLS_TYPE).toInt();
    rlsType = LocatorUtils::filterLocatorIndex(rlsType);
    mlatSettingsButtonChangeEnabledState((LocatorUtils::LocatorType)rlsType);
    const QStringList &availableLocators = LocatorUtils::getLocatorsTypes();
    ui->cb_mode->addItems(availableLocators);
    ui->cb_mode->setCurrentIndex(rlsType);
    const QString &currentLocator = ui->cb_mode->currentText();
    emit(locatorModeChanged(currentLocator));

    initModels(availableLocators, currentLocator);

    initialColor = ui->pb_useRbs->palette().button().color();
    ui->splitter->setStyleSheet("QSplitter::handle { background-color: gray }");
    ui->splitter->setStretchFactor(0, -1);
    ui->splitter->setStretchFactor(1, 2);

    //m_usePerFileStorage = true; // TODO make param
    ui->cb_fileSelector->setVisible(interpretEachFileAsDataStorage);
    ui->cb_fileSelector->setSizeAdjustPolicy(QComboBox::AdjustToContents);
}

AssessmentProceduresWidget::~AssessmentProceduresWidget()
{
    delete ui;
}

QList<TrackProfile> AssessmentProceduresWidget::applayRlsParamsAsFilter(const QList<TrackProfile> trackProfiles)
{
    return trackProfiles;
    // TODO фильтр работает некооретно. Разобраться почему

    QList<TrackProfile> result;
    for(int i = 0; i < trackProfiles.size(); i++)
    {
        const TrackProfile &profile = trackProfiles[i];
        QList<SPoint> points = profile.points;
        QList<SPoint> resultPoints;
        for(int k = 0; k < points.size(); k++)
        {
            if((!MapGeometry::isNan(points[k].altitude) &&
                    ((points[k].altitude < statusSettings.minAlitude) ||
                    (points[k].altitude > statusSettings.maxAlitude))) ||
                (!MapGeometry::isNan( points[k].rho) &&
                    ((points[k].rho*converter::NMILE_TO_KM < statusSettings.minDistance) ||
                     (points[k].rho*converter::NMILE_TO_KM > statusSettings.maxDistance)))
              )
            {
                continue;
            }
            else
            {
                resultPoints << points[k];
            }
        }

        result << profile;
        result.last().points = resultPoints;
    }

    return result;
}

void AssessmentProceduresWidget::on_newTracksProfiles(QList<TrackProfile> tracksProfiles, bool append)
{
    QApplication::setOverrideCursor(Qt::WaitCursor);

    tracksProfiles = applayRlsParamsAsFilter(tracksProfiles);
    calculateAllStatistics(tracksProfiles);

    QString fileName = ui->widget->getCurrnetFileName();
    QString key = ui->cb_mode->currentText();
    QString profileKey = interpretEachFileAsDataStorage ? fileName : key;
    if(append)
    {
        dataSources[profileKey].trackProfiles << tracksProfiles;
    }
    else
    {
        dataSources[profileKey].trackProfiles = tracksProfiles;

        //filterDuplicatingTrack();
    }

    currentController->fillView();
    repaint();

    QApplication::restoreOverrideCursor();
}

void AssessmentProceduresWidget::filterDuplicatingTrack()
{
    QList<TrackProfile> netp;
    for(int i = 0; i < currentTrackProfiles->size(); i++)
    {
        TrackProfile one = (*currentTrackProfiles)[i];
        bool have = false;
        for(int k = 0; k < netp.size(); k++)
        {
              if(netp[k].points.first().trackId == one.points.first().trackId)
              {
                    have = true;
                    qDebug() << "skip D uplications: id:" << one.points.first().trackId
                             << " callsign1:" << one.points.first().callsign
                             << " callsign2" << netp[k].points.first().callsign
                             << " reply1:" << one.points.first().reply
                             << " reply2" << netp[k].points.first().reply;

                    break;
              }
        }

        if(!have)
        {
            netp << one;
        }
    }

    (*currentTrackProfiles) = netp;
}

void AssessmentProceduresWidget::calculateAllStatistics(QList<TrackProfile> &tracksProfiles)
{
    for(int i = 0; i < tracksProfiles.size(); i++)
    {
        calculateProfileStatistics(tracksProfiles[i]);
    }
}

void AssessmentProceduresWidget::calculateProfileStatistics(TrackProfile &trackProfile)
{
    double timeAverage = trackProfile.calcAverageValue(trackProfile.timeDiffsMsec);
    // TODO rls period from settings by checkbox

    trackProfile.pO = trackProfile.reportMissedPercent(trackProfile.timeDiffsMsec, timeAverage); // пропуски отчетов
    trackProfile.pN = trackProfile.codeModeAPercent(); // пропуски кода А во всех имеющихся отчетах
    trackProfile.pH = trackProfile.modeCPercent(); // пропуски высоты во всех имеющихся отчетах (режим C(Ц))

    int zoneType = Settings::value(SettingsKeys::MLAT_ZONE_TYPE).toInt();
    trackProfile.mlatStatistics.PU = MlatStatistics::calcPU(trackProfile, MlatStatistics::mlatUpdateTimeMsecs(zoneType),
                                                            MlatStatistics::mlatDeltaUpdateTimeMsecs(zoneType));


    trackProfile.direction = trackProfile.detectDirection();

    QList<double> rhoList = trackProfile.getRhoList();
    QList<double> thetaList = Statistics::fix360_0_gap(trackProfile.getThetaList());
    QList<double> todList = trackProfile.getTODList();

    if(Settings::value(SettingsKeys::USE_TRACKS_VALIDATION).toBool())
    {
        double rhoThreshold = Settings::value(SettingsKeys::RHO_VALIDATION_THRESHOLD).toDouble();
        double thetaThreshold = Settings::value(SettingsKeys::THETA_VALIDATION_THRESHOLD).toDouble();
        std::tie(rhoList, thetaList, todList) = Statistics::validateTrack(rhoList, thetaList, todList,
                                                                          rhoThreshold, thetaThreshold);
    }

    std::tie(trackProfile.azimuthMiddleVal, trackProfile.azimuthDeviation) = Statistics::getAzimuthDeviation(thetaList);

    if(statusSettings.calcMethod == StatusSettings::Chebyshev)
    {
        if(Settings::value(SettingsKeys::WAVELETS_CALCULATION_METHOD).toInt() == ChebyshevSourceTime::RLS)
        {
            timeAverage = Settings::value(SettingsKeys::LOCATOR_ROTATION_PERIOD).toInt() / 1000.;
        }

        if(Settings::value(SettingsKeys::APPROXIMATE_POINTS_FOR_CHEBYSHEW).toBool())
        {
            std::tie(todList, rhoList, thetaList) = Statistics::approximateMissedPoints(todList, rhoList, thetaList, timeAverage);
        }

        int r = statusSettings.calcParams.value("ch_r", 3).toInt();
        int n = statusSettings.calcParams.value("ch_n", 7).toInt();

        double skoChebRho = 0;
        double skoChebTheta = 0;
        trackProfile.reconstructedTrajectory = Statistics::constructReferenceTrajectoryByChebishev(todList, rhoList, thetaList,
                                                                                                   r, n, &skoChebRho, &skoChebTheta);
        trackProfile.skoRho = skoChebRho;
        trackProfile.skoTheta = skoChebTheta;
    }
    else if( statusSettings.calcMethod == StatusSettings::Wavelets )
    {
        int waveletLevel = statusSettings.calcParams.value("w_n", 3).toInt();
        if( Settings::value( SettingsKeys::WAVELET_SKO_USE_POINT_TO_POINT_METHOD ).toBool() )
        {
            double skoRho=0,skoTheta =0;
            std::tie(trackProfile.reconstructedTrajectory, skoRho,skoTheta )
             = Statistics::constructReferenceTrajectoryByHaar(todList, rhoList,
                                                                                                  thetaList, waveletLevel);
            if(trackProfile.reconstructedTrajectory.size() == 0)
            {
                return;
            }

            // ищем стартовую точку для синхронизации траектории восстановленной и траектории исходной
            double timeR = trackProfile.reconstructedTrajectory.first().time;
            int k = 0;
            while(todList[k] < timeR)
            {
                k++;
            }

            double timeS = todList[k];
            trackProfile.reconstructedTrajectory = Statistics::rebreakRefTrajectoryToUniformSegments(trackProfile.reconstructedTrajectory,
                                                                                                     10000, timeS);

            int calculationMethod = Settings::value(SettingsKeys::WAVELETS_CALCULATION_METHOD).toInt();
            if(calculationMethod == WaveletsMethod::BY_POINTS)
            {
                QList<double> distRho;
                QList<double> distTheta;
                for(int n = k, t = 0; t < trackProfile.reconstructedTrajectory.size() &&
                                      n < rhoList.size() && n<thetaList.size(); n++, t++)
                {
                    double distanceRho = trackProfile.reconstructedTrajectory[t].rho - rhoList[n];
                    double distanceTheta = trackProfile.reconstructedTrajectory[t].theta - thetaList[n];
                    distTheta << distanceTheta;
                    distRho << distanceRho;
                }

                trackProfile.skoRho   = Statistics::meanSKO(distRho);
                trackProfile.skoTheta = Statistics::meanSKO(distTheta);
            }
            else if(calculationMethod == WaveletsMethod::BY_PERPENDICULAR)
            {
                trackProfile.skoRho   = skoRho;
                trackProfile.skoTheta = skoTheta;
            }
        }
        else
        if(Settings::value( SettingsKeys::WAVELET_SKO_USE_NORMAL_TO_REF_TRAJECTORY).toBool())
        {
            double result = 0;
            QPair<QVector<double>, QVector<double>> vec1 =
                    Statistics::processXYByHaar(todList, rhoList, &result, waveletLevel);

            trackProfile.skoRho = result;

            result = 0;
            QPair<QVector<double>, QVector<double>> vec2 =
                    Statistics::processXYByHaar(todList, thetaList, &result, waveletLevel);
            trackProfile.skoTheta = result;

            QList<Statistics::XYT> trajectory;
            for(int i = 0; i < vec1.first.size(); i++)
            {
                trajectory << Statistics::XYT(QPointF(vec1.second.value(i), vec2.second.value(i)), vec1.first.value(i));
            }
            trackProfile.reconstructedTrajectory = trajectory;
       }
    }
    else if( statusSettings.calcMethod == StatusSettings::AB_Filter )
    {
        double a = statusSettings.calcParams.value("ab_a", 0.7).toDouble();
        double b = statusSettings.calcParams.value("ab_b", 0.5).toDouble();

        double result = 0;
        QPair<QVector<double>, QVector<double>> vec1 = Statistics::processXYByABfilter(todList, rhoList, &result, a, b);
        trackProfile.skoRho = result;

        result = 0;
        QPair<QVector<double>, QVector<double>> vec2 = Statistics::processXYByABfilter(todList, thetaList, &result, a, b);
        trackProfile.skoTheta = result;


        QList<Statistics::XYT> trajectory;
        for(int i = 0; i < vec1.first.size(); i++)
        {
            trajectory << Statistics::XYT(QPointF(vec1.second.value(i), vec2.second.value(i)), vec1.first.value(i));
        }
        trackProfile.reconstructedTrajectory = trajectory;

    }
    else if( statusSettings.calcMethod == StatusSettings::Kalman_Filter)
    {
        double e_mea = statusSettings.calcParams.value("kf_e_mea",2.).toDouble();
        double e_est = statusSettings.calcParams.value("kf_e_est", 2.).toDouble();
        double q     = statusSettings.calcParams.value("kf_q", 0.1).toDouble();

        double result = 0;
        QPair<QVector<double>, QVector<double>> vec1 =
                Statistics::processXYByKalman(todList, rhoList, &result, e_mea, e_est, q);
        trackProfile.skoRho = result;

        result = 0;
        QPair<QVector<double>, QVector<double>> vec2 =
                Statistics::processXYByKalman(todList, thetaList, &result, e_mea, e_est, q);
        trackProfile.skoTheta = result;

        QList<Statistics::XYT> trajectory;
        for(int i = 0; i < vec1.first.size(); i++)
        {
            trajectory << Statistics::XYT(QPointF(vec1.second.value(i), vec2.second.value(i)), vec1.first.value(i));
        }
        trackProfile.reconstructedTrajectory = trajectory;
    }
    else if ( statusSettings.calcMethod == StatusSettings::PolinominalInterpolation )
    {
        int n = statusSettings.calcParams.value("pl_n", 3).toInt();

        int pnt_count = trackProfile.points.count();
        if((pnt_count < 2) || (pnt_count < n)) return;

        double skoChebRho = 0;
        double skoChebTheta = 0;
        trackProfile.reconstructedTrajectory = Statistics::constructReferenceTrajectoryByPolinominal(todList, rhoList, thetaList, n,
                                                                                                     &skoChebRho, &skoChebTheta);
        trackProfile.skoRho = skoChebRho;
        trackProfile.skoTheta = skoChebTheta;
    }
    else if(statusSettings.calcMethod == StatusSettings::Coiflet)
    {
        int coifletLevel = 1;
        double result = 0;
        QPair<QVector<double>, QVector<double>> vec1 =
                Statistics::processXYByCoiflet(todList, rhoList, &result, coifletLevel);

        trackProfile.skoRho = result;

        result = 0;
        QPair<QVector<double>, QVector<double>> vec2 =
                Statistics::processXYByCoiflet(todList, thetaList, &result, coifletLevel);

        trackProfile.skoTheta = result;

         QList<Statistics::XYT> trajectory;
        for(int i = 0; i < vec1.first.size(); i++)
        {
            trajectory << Statistics::XYT(QPointF(vec1.second.value(i), vec2.second.value(i)), vec1.first.value(i));
        }
        trackProfile.reconstructedTrajectory =trajectory;
    }

    trackProfile.pO1 = trackProfile.getProbabilityOfUpdateByTime(1000.);
    trackProfile.pO4 = trackProfile.getProbabilityOfUpdateByTime(4000.);
    trackProfile.pO10= trackProfile.getProbabilityOfUpdateByTime(10000.);
}

bool lessThenSpointByTimestamp(const SPoint &a, const SPoint &b)
{
    return a.reciveTimestamp < b.reciveTimestamp;
}

QList<TrackProfile> AssessmentProceduresWidget::convertToTrackProfile(QMap<QString, QList<SPoint>> mminfo)
{
    QList<TrackProfile> result;
    QList<QString> keys = mminfo.keys();
    for(const QString &key: keys)
    {
        QList<SPoint> points = mminfo[key];
        std::sort(points.begin(), points.end(), lessThenSpointByTimestamp);

        TrackProfile profile;
        for(int i = 0; i < points.size(); i++)
        {
            if(profile.isPointSutable(points[i]))
            {
                profile.applayPoint(points[i]);
            }
        }
        result << profile;
    }
    return result;
}

void AssessmentProceduresWidget::showCustomContextMenu(QPoint position)
{
    QTableView *view = dynamic_cast<QTableView*>(sender());
    if(!view) return;

    QModelIndex index = ui->tableView->indexAt(position);
    QMenu *context = new QMenu(this);
    QAction *showOriginalPoints = context->addAction("Показать исходные точки");
    QAction *showSmoothedPoints = context->addAction("Показать сглаженные точки");
    QAction *showAllPoints = context->addAction("Показать все точки");
    QAction *addToReport = context->addAction("Добавить в отчет");
    QAction *autoSplit = context->addAction("Разбить автоматически");
    QAction *showOriginalTrack = context->addAction("Показать на карте исходный трек");
    QAction *showSmoothedTrack = context->addAction("Показать на карте сглаженный трек");
    QAction *showAllTracks = context->addAction("Показать на карте оба трека");
    QAction *deleteTrack = context->addAction("Удалить трек");
    QAction *saveTracks = context->addAction("Сохранить выбранные треки в файл");
    QAction *saveTracksWithId = context->addAction("Сохранить выбранные треки в файл с ID");
    QAction *mergeTracks = context->addAction("Объединить треки в один");

    context->popup(ui->tableView->viewport()->mapToGlobal(position));
    QAction *result = context->exec();
    delete context;

    if(result == showOriginalPoints)
    {
        if(index.isValid())
        {
            QVariant var = currentSorter->data(currentSorter->index(index.row(), 0), Qt::UserRole + 2);
            TrackProfile *profile = (TrackProfile *)var.toULongLong();
            showTrackProfilePoints(profile, false);
        }
    }
    else if(result == showSmoothedPoints)
    {
        if(index.isValid())
        {
            QVariant var = currentSorter->data(currentSorter->index(index.row(), 0), Qt::UserRole + 2);
            TrackProfile *profile = (TrackProfile *)var.toULongLong();
            if(profile->reconstructedTrajectory.size())
            {
                showTrackProfilePoints(profile, true);
            }
            else
            {
                QMessageBox::information(this, "Внимание!", "Для данного трека не найдено сглаженных точек!");
            }
        }
    }
    else if(result == showAllPoints)
    {
        if(index.isValid())
        {
            QVariant var = currentSorter->data(currentSorter->index(index.row(), 0), Qt::UserRole + 2);
            TrackProfile *profile = (TrackProfile *)var.toULongLong();
            showTrackProfilePoints(profile, false);
            if(profile->reconstructedTrajectory.size())
            {
                showTrackProfilePoints(profile, true);
            }
            else
            {
                QMessageBox::information(this, "Внимание!", "Для данного трека не найдено сглаженных точек!");
            }
        }
    }
    else if(result == addToReport)
    {
        addToReportData();
    }
    else if(result == showOriginalTrack)
    {
        QModelIndexList selectedRows = ui->tableView->selectionModel()->selectedRows();
        sendSelectedRowsToShowOnMap(selectedRows);
    }
    else if(result == showSmoothedTrack)
    {
        QModelIndexList selectedRows = ui->tableView->selectionModel()->selectedRows();
        sendSelectedRowsToShowOnMap(selectedRows, false, true, true);
    }
    else if(result == showAllTracks)
    {
        QModelIndexList  selectedRows = ui->tableView->selectionModel()->selectedRows();
        sendSelectedRowsToShowOnMap(selectedRows);
        sendSelectedRowsToShowOnMap(selectedRows, false, true, true);
    }
    else if(result == autoSplit)
    {
        splitAutomatically(index);
    }
    else if(result == deleteTrack)
    {
        QModelIndexList  selectedRows = ui->tableView->selectionModel()->selectedRows();
        if(selectedRows.size() != 1)
        {
            return;
        }

        removeSelectedRow(selectedRows[0]);
    }
    else if(result == saveTracks)
    {
        on_tb_save_clicked();
    }
    else if(result == saveTracksWithId)
    {
        saveTrackWithId();
    }
    else if(result == mergeTracks)
    {
        on_tb_merge_clicked();
    }
}

void AssessmentProceduresWidget::processResponceFromMap(const QByteArray &data)
{
    const QList< QByteArray > &blocks = xProtoDecoder::Instance().getxProtoBlockMap(data);
    if(blocks.size() < 2)
    {
        return;
    }

    xProtoHashReader cmd = xProtoHashReader(xProtoDecoder::Instance().decodexProto(blocks[1]));
    sendMapCommandAnswer(cmd);

    const QString &commandId = cmd.getFieldData("I341/010", "commandId").toString();
    const QString &itemId = cmd.getFieldData("I341/010", "itemId").toString();

    int trackId = getTrackIdFromIndexId(itemId);
    bool smoothed = isTrackSmoothed(itemId);

    if(commandId == MapCommands::DELETE_TARJECTORY)
    {
        xProtoHashPacker hpCommand(300);
        hpCommand.addData("I300/010", "type", "TO_MAP_OBJECT_DELETE");
        hpCommand.addData("I300/020", "object_type", "FLIGHT_ROUTE");
        hpCommand.addData("I300/060", "host_name", QHostInfo::localHostName());
        hpCommand.addData("I300/060", "app_name", "pivp2panda");

        xProtoHashPacker track(312);
        track.addData("I312/010", "id", itemId);

        ChannelKeeper::Instance().sendToMap(hpCommand.getPackedData() + track.getPackedData());
    }
    else if(commandId == MapCommands::SELECT_TRAJECTORY)
    {
        int trackRow = getTrackRowInTableByTrackId(trackId);
        if(trackRow < 0)
        {
            QString messageText;
            TrackProfile* profile = getTrackProfileByTrackIdFromFullModel(trackId);
            if(!profile)
            {
                messageText = QString("Трека с ID=%1 не найдено.\nВозможно он находится в списке с другим типом треков.").arg(trackId);
            }
            else
            {
                messageText = QString("Трека с ID=%1 не найдено.\nОн находится в списке с типом треков '%2'").arg(trackId).
                              arg(TrackType::toString(profile->trackKey.type));

            }
            QMessageBox::information(this, "Внимание!", messageText);
            return;
        }

        const QModelIndex& index = ui->tableView->model()->index(trackRow, StatisticsColumns::HiddenService);
        ui->tableView->selectionModel()->select(index, QItemSelectionModel::SelectCurrent | QItemSelectionModel::Rows);
    }
    else if(commandId == MapCommands::SHOW_POINTS_LIST)
    {
        TrackProfile* profile = getTrackProfileByTrackId(trackId);
        if(!profile)
        {
            QMessageBox::information(this, "Внимание!", QString("Трека с ID=%1 не найдено").arg(trackId));
            return;
        }

        showTrackProfilePoints(profile, smoothed);
    }
}

int AssessmentProceduresWidget::getTrackRowInTableByTrackId(int trackId)
{
    for(int i = 0; i < ui->tableView->selectionModel()->model()->rowCount(); i++)
    {
        int currentTrackId = ui->tableView->selectionModel()->model()->data(currentSorter->index(i, 0), Qt::DisplayRole).toInt();
        if(currentTrackId == trackId)
        {
            qDebug() << "row:  " << i;
            return i;
        }
    }

    return -1;
}

TrackProfile* AssessmentProceduresWidget::getTrackProfileByTrackIdFromFullModel(int trackId)
{
    for(int i = 0; i < currentModel->rowCount(); i++)
    {
        int currentTrackId = currentModel->data(currentModel->index(i, 0), Qt::DisplayRole).toInt();
        if(currentTrackId == trackId)
        {
            QVariant var = currentModel->data(currentModel->index(i, 0), Qt::UserRole + 2);
            TrackProfile *profile = (TrackProfile *)var.toULongLong();

            return profile;
        }
    }

    return nullptr;
}

TrackProfile* AssessmentProceduresWidget::getTrackProfileByTrackId(int trackId)
{
    for(int i = 0; i < currentSorter->rowCount(); i++)
    {
        int currentTrackId = currentSorter->data(currentSorter->index(i, 0), Qt::DisplayRole).toInt();
        if(currentTrackId == trackId)
        {
            QVariant var = currentSorter->data(currentSorter->index(i, 0), Qt::UserRole + 2);
            TrackProfile *profile = (TrackProfile *)var.toULongLong();

            return profile;
        }
    }

    return nullptr;
}

bool AssessmentProceduresWidget::showTrackProfilePoints(TrackProfile* profile, bool smoothed)
{
    if(!profile)
    {
        return false;
    }

    PointListWidget *plw = new PointListWidget();
    if(!plw)
    {
        return false;
    }

    plw->setRefPoint(statusSettings.referncePoint);
    plw->setProfile(*profile, true, smoothed);
    plw->show();
    connect(plw, SIGNAL(sendSubtrack(TrackProfile)), this, SLOT(receiveSubtrack(TrackProfile)));

    return true;
}

int AssessmentProceduresWidget::getTrackIdFromIndexId(QString itemId)
{
    itemId.remove("packet_inspector_");
    int firstUnderlinePos = itemId.indexOf('_');
    if(firstUnderlinePos == 0)
    {
        return 0;
    }

    itemId.remove(firstUnderlinePos, itemId.size() - firstUnderlinePos);
    bool ok;
    int trackId = itemId.toInt(&ok);
    if(!ok)
    {
        return 0;
    }

    return trackId;
}

bool AssessmentProceduresWidget::isTrackSmoothed(QString itemId)
{
    const QStringList strings = itemId.split(QChar('_'));
    if(strings.size() < 3)
    {
        return false;
    }

    bool ok;
    int smoothedTrack = strings[3].toInt(&ok);
    if(!ok)
    {
        return false;
    }

    return smoothedTrack;
}

void AssessmentProceduresWidget::sendMapCommandAnswer(xProtoHashReader &mapCmd)
{
    xProtoHashPacker p(300);
    p.addData("I300/010", "type", "MENU_COMMAND_REPLY");
    p.addData("I300/020", "object_type", "FLIGHT_ROUTE");
    p.addData("I300/060", "host_name", QHostInfo::localHostName());
    p.addData("I300/060", "app_name", "pivp2panda");

    xProtoHashPacker d(342);

    for(int i = 0; i < mapCmd.recordCount(); i++)
    {
        mapCmd.selectRecord(i);
        const QString &requestUuid = mapCmd.getFieldData("I341/010", "requestUuid").toString();
        if(!requestUuid.isEmpty())
        {
            d.addData("I342/010", "requestUuid", requestUuid);
            d.addData("I342/010", "message", "");//message );
            d.addNewRecord();
        }
    }

    if(!d.isEmpty())
        ChannelKeeper::Instance().sendToMap(p.getPackedData() + d.getPackedData());
}

void AssessmentProceduresWidget::removeSelectedRow(QModelIndex &rowIndex)
{
    qDebug() << "SORTER ROW = " << rowIndex.row();
    QModelIndex sourceIndex = currentSorter->mapToSource(rowIndex);
    qDebug() << "MODEL ROW = " << sourceIndex.row();
    currentModel->takeRow(sourceIndex.row());
}

void AssessmentProceduresWidget::clearReportPtr(ReportWidget* reportWidget)
{
    if(rwAll == reportWidget)
    {
        rwAll = nullptr;
    }
    else if(rwAdsb == reportWidget)
    {
        rwAdsb = nullptr;
    }
}

int AssessmentProceduresWidget::getTrackNumberByTrackNumber(int trackNumber)
{
    for(int profileNum = 0; profileNum < currentTrackProfiles->size(); profileNum++)
    {
        if((*currentTrackProfiles)[profileNum].trackKey.trackNumber == trackNumber)
        {
            return profileNum;
        }
    }

    return -1;
}

void AssessmentProceduresWidget::addToReportData()
{
    if(rwAll == nullptr)
    {
        rwAll = new ReportWidget((LocatorUtils::LocatorType)Settings::value(SettingsKeys::MLAT_RLS_TYPE).toInt());
        connect(rwAll, SIGNAL(windowClosed(ReportWidget*)),
                this, SLOT(clearReportPtr(ReportWidget*)));
    }

    if(rwAdsb == nullptr)
    {
        rwAdsb = new ReportWidget((LocatorUtils::LocatorType)Settings::value(SettingsKeys::MLAT_RLS_TYPE).toInt());
        connect(rwAdsb, SIGNAL(windowClosed(ReportWidget*)),
                this, SLOT(clearReportPtr(ReportWidget*)));
    }

    QModelIndexList selection = ui->tableView->selectionModel()->selectedRows();
    QList<TrackProfile> profiles;
    QList<TrackProfile> profilesAdsb;
    for(int i = 0; i < selection.count(); i++)
    {
        QVariant trackNumberVar = selection[i].data(Qt::DisplayRole).toInt();
        if(!trackNumberVar.isValid())
        {
            CDEBUG << "!!! QVariant NOT valid";
        }

        int trackNumber = selection[i].data(Qt::DisplayRole).toInt();
        int profileNumber = getTrackNumberByTrackNumber(trackNumber);
        if(profileNumber < 0)
        {
            CDEBUG << "INCORRECT PROFILE NUMBER";
        }

        const TrackProfile &profile = (*currentTrackProfiles)[profileNumber];

        if( profile.trackKey.type == TrackType::ADSB )
            profilesAdsb << profile;
        else
              profiles << profile;
    }
    if(profiles.size() >0 ){
        rwAll->addTrackProfiles(profiles);
        rwAll->show();
        rwAll->refreshTable();
    }

    if(profilesAdsb.size() > 0)
    {
        rwAdsb->setWindowTitle("Отчет ADS-B");
        rwAdsb->addTrackProfiles(profilesAdsb);
        rwAdsb->show();
        rwAdsb->refreshTable();
    }
}

bool AssessmentProceduresWidget::addSubintervals(const QList<QPair<int, int>> intervalsList, const TrackProfile *profile)
{
    double minAllowedPercent = Settings::value(SettingsKeys::MIN_PERCENT_FOR_SPLIT_ADD).toDouble();
    bool updated = false;
    //StatisticsColumns::HidedFlgs hidedFlgs;
    for(int i = 0; i < intervalsList.size(); i++)
    {
        double pecent = percentFromTotal(intervalsList[i], profile->points.size());
        int pointsInSubtrack = intervalsList[i].second - intervalsList[i].first + 1;

        if(pecent >= minAllowedPercent)
        {
            QPair<int, int> params = qMakePair(intervalsList[i].first, pointsInSubtrack);
            TrackProfile trackProfile = profile->mid(params.first, params.second);

            int nextTrackNumber = currentTrackProfiles->size();
            (*currentTrackProfiles) << trackProfile;

            (*currentTracksCounts)[trackProfile.trackKey.type]++;
            (*currentPointsCounts)[trackProfile.trackKey.type] += trackProfile.points.size();
            currentController->addTrackToTable((*currentTrackProfiles)[nextTrackNumber], nextTrackNumber);

            updated = true;
        }
    }

    currentController->processColumnsHiding();
    return updated;
}

QList<QPair<int, int>> AssessmentProceduresWidget::getSubIntervalsParams(const TrackProfile *profile) const
{
    QList<QPair<int, int>> intervalsList;
    QPair<int, int> interval = qMakePair(0, 0);
    double avgTimeVal = Statistics::calcAverageValue(profile->timeDiffsMsec);
    int i = 0;
    for(i = 0; i < profile->points.size(); i++)
    {
        if(i < profile->timeDiffsMsec.size())
        {
            if(qAbs(avgTimeVal - profile->timeDiffsMsec[i]) > (avgTimeVal * .1))
            {
                interval.second = i;
                intervalsList << interval;

                interval = qMakePair(i + 1, i + 1);
            }
        }
    }

    interval.second = i - 1;
    intervalsList << interval;

    return intervalsList;
}

double AssessmentProceduresWidget::percentFromTotal(const QPair<int, int> &intervalParams, int totalPoints) const
{
    return (double)(intervalParams.second - intervalParams.first + 1) * 100. / (double)totalPoints;
}

void AssessmentProceduresWidget::receiveSubtrack(TrackProfile trackProfile)
{
    QApplication::setOverrideCursor(Qt::WaitCursor);

    calculateProfileStatistics(trackProfile);
    int nextTrackNumber = currentTrackProfiles->size();

    int maxTrackId=-1;
    for(const auto &tp: (*currentTrackProfiles))
    {
        if( tp.trackKey.trackNumber> maxTrackId )
        {
            maxTrackId = tp.trackKey.trackNumber;
        }
    }

    trackProfile.trackKey.trackNumber= ( maxTrackId + 1) ;

    (*currentTrackProfiles) << trackProfile;

    (*currentTracksCounts)[trackProfile.trackKey.type]++;
    (*currentPointsCounts)[trackProfile.trackKey.type] += trackProfile.points.size();

    updateTracksOnButtons(*currentTracksCounts, false);
    setPointsCounters((*currentPointsCounts), false);

    currentController->addTrackToTable((*currentTrackProfiles)[nextTrackNumber], nextTrackNumber);
    currentController->processColumnsHiding();

    repaint();
    QApplication::restoreOverrideCursor();
    QMessageBox::information(this, "Внимание!", "Добавлен трек с номером " + QString::number( trackProfile.trackKey.trackNumber ));
}

void AssessmentProceduresWidget::toggleColor(QPushButton *button)
{
    QPalette pal = button->palette();

    if(button->isChecked())
    {
        pal.setColor(QPalette::Button, QColor(Qt::green));
    }
    else
    {
        pal.setColor(QPalette::Button, initialColor);
    }

    button->setAutoFillBackground(true);
    button->setPalette(pal);
    button->update();
}

void AssessmentProceduresWidget::updateTracksOnButtons(QHash<TrackType::TrackType, int> tracksPoints, bool append)
{
    if(append)
    {
        QList<TrackType::TrackType> keys = tracksPoints.keys();
        for(TrackType::TrackType key: keys)
        {
            (*currentTracksCounts)[key] += tracksPoints[key];
        }
    }
    else
    {
        (*currentTracksCounts) = tracksPoints;
    }

    refreshTracksCountsOnButtons();
}

void AssessmentProceduresWidget::refreshTracksCountsOnButtons()
{
    ui->pb_useRbs->setText("RBS: " + QString::number((*currentTracksCounts)[TrackType::RBS]));
    ui->pb_useUvd->setText("UVD: " + QString::number((*currentTracksCounts)[TrackType::UVD]));
    ui->pb_useAdsb->setText("ADS-B: " + QString::number((*currentTracksCounts)[TrackType::ADSB]));
    ui->p_useModeS->setText("Mode-S: " + QString::number((*currentTracksCounts)[TrackType::ModeS]));
}

void AssessmentProceduresWidget::setPointsCounters(QHash<TrackType::TrackType, int> tracksPoints, bool append)
{
    if(append)
    {
        QList<TrackType::TrackType> keys = tracksPoints.keys();
        for(TrackType::TrackType key: keys)
        {
            (*currentPointsCounts)[key] += tracksPoints[key];
        }
    }
    else
    {
        (*currentPointsCounts) = tracksPoints;
    }

    ui->lb_unused->setText("Неисп. точки: " + QString::number((*currentPointsCounts)[TrackType::UNUSED]));
    ui->pb_unused->setText("Неисп.: " + QString::number((*currentPointsCounts)[TrackType::UNUSED]));

    int totalPoints = (*currentPointsCounts)[TrackType::RBS] +
                      (*currentPointsCounts)[TrackType::UVD] +
                      (*currentPointsCounts)[TrackType::ADSB] +
                      (*currentPointsCounts)[TrackType::ModeS] +
                      (*currentPointsCounts)[TrackType::UNUSED];
    ui->lb_total->setText("Всего точек: " + QString::number(totalPoints));
    ui->lb_unusedPercent->setText("Процент неисп. точек: " +
                                  QString::number((double)(*currentPointsCounts)[TrackType::UNUSED] * 100. /
                                  (double)totalPoints, 'f', 4));
}

void AssessmentProceduresWidget::updateView(QPushButton *button)
{
    toggleColor(button);
    emit sendFilters(fillFilters());
}

void AssessmentProceduresWidget::on_pb_useRbs_clicked()
{
    updateView(ui->pb_useRbs);
    currentController->processRbsClicked();
}

void AssessmentProceduresWidget::on_pb_useUvd_clicked()
{
    updateView(ui->pb_useUvd);
    currentController->processUvdClicked();
}

void AssessmentProceduresWidget::on_pb_useAdsb_clicked()
{
    updateView(ui->pb_useAdsb);
    bool flag = ui->pb_useAdsb->isChecked();
    currentController->processAdsbClicked(flag);
}

void AssessmentProceduresWidget::on_p_useModeS_clicked()
{
    updateView(ui->p_useModeS);
}

void AssessmentProceduresWidget::on_pb_mlat_clicked()
{

}

void AssessmentProceduresWidget::updateSettings(StatusSettings settings)
{
    statusSettings = settings;
    currentSorter->setFilterSettings(statusSettings);
    emit sendFilters(fillFilters());
}

void AssessmentProceduresWidget::sendSelectedRowsToShowOnMap(const QModelIndexList &indexes, bool withFormulars, bool withLine, bool useRecalc)
{
    for(const QModelIndex &index: indexes)
    {
        QVariant var = currentSorter->data(currentSorter->index(index.row(), 0), Qt::UserRole + 2);
        TrackProfile *trackProfile = (TrackProfile*)(var.toULongLong());

        if(trackProfile)
        {
            MapActions::Instance().showTrackOnMap(*trackProfile, withFormulars, withLine, useRecalc, true);
        }
    }
}

QList<TrackType::TrackType> AssessmentProceduresWidget::fillFilters()
{
    QList<TrackType::TrackType> passFilters;
    if(ui->pb_useRbs->isChecked())
    {
        passFilters << TrackType::RBS;
    }

    if(ui->pb_useUvd->isChecked())
    {
        passFilters << TrackType::UVD;
    }

    if(ui->pb_useAdsb->isChecked())
    {
        passFilters << TrackType::ADSB;
    }

    if(ui->p_useModeS->isChecked())
    {
        passFilters << TrackType::ModeS;
    }

    return passFilters;
}

void AssessmentProceduresWidget::on_tb_showPoints_clicked()
{
    QModelIndexList selection = ui->tableView->selectionModel()->selectedRows(StatisticsColumns::HiddenService);

    if(selection.size() == 0)
    {
        QString message = "Не выбрано ни одного трека! Укажите интересующий трек.";
        QMessageBox::information(this, "Внимание!", message);
        return;
    }

    if(selection.size() > 1)
    {
        QString message = "Выбрано более одного трека! Точки будут отображены для первого выделенного трека.";
        QMessageBox::information(this, "Внимание!", message);
    }

    QModelIndex index = selection[0];

    if(index.isValid())
    {
        PointListWidget *plw = new PointListWidget();
        if(plw)
        {
            QVariant var = currentSorter->data(currentSorter->index(index.row(), 0), Qt::UserRole + 2);
            TrackProfile *profile = (TrackProfile *)var.toULongLong();
            plw->setProfile(*profile, true, false);
            plw->show();
            connect(plw, SIGNAL(sendSubtrack(TrackProfile)), this, SLOT(receiveSubtrack(TrackProfile)));
        }
    }
}

void AssessmentProceduresWidget::on_tb_splitAutomatically_clicked()
{
    QModelIndexList selection = ui->tableView->selectionModel()->selectedRows(StatisticsColumns::HiddenService);

    if(selection.size() == 0)
    {
        QString message = "Не выбрано ни одного трека! Укажите интересующий трек.";
        QMessageBox::information(this, "Внимание!", message);
        return;
    }

    if(selection.size() > 1)
    {
        QString message = "Выбрано более одного трека! Разбиение будет осуществляться для первого выделенного трека.";
        QMessageBox::information(this, "Внимание!", message);
    }

    splitAutomatically(selection[0]);
}

void AssessmentProceduresWidget::on_tb_showOnMapOriginal_clicked()
{
    QModelIndexList selectedRows = ui->tableView->selectionModel()->selectedRows(StatisticsColumns::HiddenService);

    if(selectedRows.size() == 0)
    {
        QString message = "Не выбрано ни одного трека! Укажите интересующий трек.";
        QMessageBox::information(this, "Внимание!", message);
        return;
    }

    sendSelectedRowsToShowOnMap(selectedRows);
}

void AssessmentProceduresWidget::on_tb_showOnMapSmoothed_clicked()
{
    QModelIndexList selectedRows = ui->tableView->selectionModel()->selectedRows(StatisticsColumns::HiddenService);

    if(selectedRows.size() == 0)
    {
        QString message = "Не выбрано ни одного трека! Укажите интересующий трек.";
        QMessageBox::information(this, "Внимание!", message);
        return;
    }

    sendSelectedRowsToShowOnMap(selectedRows, false, true, true);
}

void AssessmentProceduresWidget::splitAutomatically(QModelIndex &index)
{
    if(index.isValid())
    {
        QVariant var = currentSorter->data(currentSorter->index(index.row(), 0), Qt::UserRole + 2);
        TrackProfile *profile = (TrackProfile *)var.toULongLong();

        int minAllowedPoints = Settings::value(SettingsKeys::MIN_POINTS_FOR_SPLIT).toInt();

        if(profile->points.size() < minAllowedPoints)
        {
            QString message = "Недостаточное количество отчек в треке.\n"
                              "Минимально допустимое количество точек - " + QString::number(minAllowedPoints);
            QMessageBox::information(this, "Внимание!", message);
            return;
        }

        QList<QPair<int, int>> intervalsList = getSubIntervalsParams(profile);
        if(addSubintervals(intervalsList, profile))
        {
            updateTracksOnButtons((*currentTracksCounts), false);
            setPointsCounters((*currentPointsCounts), false);
            repaint();
        }
    }
}

void AssessmentProceduresWidget::on_tb_addToReport_clicked()
{
    addToReportData();
}

void AssessmentProceduresWidget::on_tb_setPosition_clicked()
{
    CoordinatesDialog dialog(this);
    dialog.exec();
    ui->widget->showPosition();
}

void AssessmentProceduresWidget::on_tb_settings_clicked()
{
    SettingsDialog *dlg = new SettingsDialog(this);
    connect(dlg, SIGNAL(sendUpdatedSettings(StatusSettings)), this, SLOT(updateSettings(StatusSettings)));
    dlg->show();
}

void AssessmentProceduresWidget::on_pb_unused_clicked()
{
    if(currentUnusedPoints->count() > 0)
    {
        PointListWidget *plw = new PointListWidget();
        if(plw)
        {

            TrackProfile profile;
            profile.points = sortPointsByTime(*currentUnusedPoints);
            plw->setProfile(profile, false, false);
            plw->show();
        }
    }
    else
    {
        QMessageBox::information(this, "Внимание!", "Неиспользуемые точки отсутствуют");
    }
}

void AssessmentProceduresWidget::receiveUnusedPoints(QList<SPoint> unusedPoints, bool append)
{
    if(unusedPoints.count() > 0)
    {
        if(append)
        {
            (*(this->currentUnusedPoints)) += unusedPoints;
        }
        {
            (*(this->currentUnusedPoints)) = unusedPoints;
        }
    }
    else
    {
        unusedPoints.clear();
    }
}

void AssessmentProceduresWidget::on_tb_save_clicked()
{
    QModelIndexList selectedRows = ui->tableView->selectionModel()->selectedRows(StatisticsColumns::HiddenService);
    int rows = selectedRows.size();
    if(rows == 0)
    {
        QMessageBox::information(this, "Внимание!", "Нет данных для сохранения!");
        return;
    }

    QString inputDir = Settings::instance().value(SettingsKeys::LAST_INPUT_DIRECTORY).toString();
    QString fileName = QFileDialog::getSaveFileName(nullptr, "Укажите файл для сохранения данных как dat", inputDir, "*.dat");
    if(fileName.isEmpty())
    {
        return;
    }

    if(!fileName.endsWith(".dat"))
    {
        fileName.append(".dat");
    }

    ofstream output_file;
    output_file.open(fileName.toStdString().c_str(), ofstream::binary);
    if(!output_file.is_open())
    {
        QMessageBox::warning(this, tr("Сохранение файла"), tr("Невозможно открыть файл для сохранения данных"));
        return;
    }

    QString date_time_str = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss");
    output_file << "# " << date_time_str.toLocal8Bit().constData() << endl;
    output_file << "#" << endl;

    QList<QPair<QDateTime, QByteArray>> pointsList;
    for(int row = 0; row < rows; row++)
    {
         QVariant trackProfileAddr = currentSorter->data(selectedRows[row], Qt::UserRole + 2);
         TrackProfile *profile = (TrackProfile *)trackProfileAddr.toULongLong();

         if(profile->points.size() == 0)
         {
             continue;
         }

         for(SPoint &point: profile->points)
         {
            pointsList << qMakePair(point.reciveTimestamp, point.rawByteArray);
         }
    }

    if(pointsList.isEmpty())
    {
        QMessageBox::warning(this, "Внимание!", "В выбранных треках нет точек!");
        output_file.close();
        return;
    }

    std::sort(pointsList.begin(), pointsList.end(),
              [](QPair<QDateTime, QByteArray> &left, QPair<QDateTime, QByteArray> &right){ return left.first < right.first; });

    QDateTime initStamp = pointsList.at(0).first;
    for(QPair<QDateTime, QByteArray> &record: pointsList)
    {
        QString s;
        s.sprintf("%.6f ", (static_cast<double>(initStamp.msecsTo(record.first)) / 1000.));
        s += datagramToText(record.second);
        s += "\n";output_file.write(s.toLocal8Bit().constData(), s.size());
    }

    output_file.close();
    //model
}

void AssessmentProceduresWidget::saveTrackWithId()
{
    QModelIndexList selectedRows = ui->tableView->selectionModel()->selectedRows(StatisticsColumns::HiddenService);
    int rows = selectedRows.size();
    if(rows == 0)
    {
        QMessageBox::information(this, tr("Внимание!"), tr("Нет данных для сохранения!"));
        return;
    }

    QString inputDir = Settings::instance().value(SettingsKeys::LAST_INPUT_DIRECTORY).toString();
    QString fileName = QFileDialog::getSaveFileName(nullptr, "Укажите файл для сохранения данных как dat", inputDir, "*.dat");
    if(fileName.isEmpty())
    {
        return;
    }

    if(!fileName.endsWith(".dat"))
    {
        fileName.append(".dat");
    }

    QList<QPair<QDateTime, QByteArray>> pointsList;
    for(int row = 0; row < rows; row++)
    {
        QVariant trackProfileAddr = currentSorter->data(selectedRows[row], Qt::UserRole + 2);
        TrackProfile *profile = (TrackProfile *)trackProfileAddr.toULongLong();

        if(profile->points.size() == 0)
        {
            continue;
        }

        int newTrackNumber = currentSorter->data(selectedRows[row], Qt::DisplayRole).toInt();
        pointsList.append(dumpTrack(profile->points, newTrackNumber));
    }

    if(pointsList.isEmpty())
    {
        QMessageBox::warning(this, "Внимание!", "В выбранных треках нет точек!");
        return;
    }

    std::sort(pointsList.begin(), pointsList.end(),
              [](QPair<QDateTime, QByteArray> &left, QPair<QDateTime, QByteArray> &right){ return left.first < right.first; });

    ofstream output_file;
    output_file.open(fileName.toStdString().c_str(), ofstream::binary);
    if(!output_file.is_open())
    {
        QMessageBox::warning(this, tr("Сохранение файла"), tr("Невозможно открыть файл для сохранения данных"));
        return;
    }

    QString date_time_str = pointsList[0].first.toString("dd.MM.yyyy hh:mm:ss");
    output_file << "#" << endl;
    output_file << "# " <<"source_name: " << "Track Imitator" << endl;
    output_file << "# " << "Track Imitator" << endl;
    output_file << "# record host=" << "127.0.0.1" << " port=" << "101011" << endl;
    output_file << "# " << date_time_str.toLocal8Bit().constData() << endl;
    output_file << "#" << endl;

    QDateTime initStamp = pointsList.at(0).first;
    for(QPair<QDateTime, QByteArray> &record: pointsList)
    {
        QString s;
        qint64 mSecsDiff = initStamp.msecsTo(record.first);
        double mSecsDouble = static_cast<double>(mSecsDiff) / 1000.;
        s.sprintf("%.6f ", mSecsDouble);
        s += datagramToText(record.second);
        s += "\n";
        output_file.write(s.toLocal8Bit().constData(), s.size());
    }

    output_file.close();

    QMessageBox::information(this, "Сохранено", "Данные сохранены");
}

QList<QPair<QDateTime, QByteArray>> AssessmentProceduresWidget::dumpTrack(QList<SPoint> trackPoints, int newTrackNumber)
{
    QList<QPair<QDateTime, QByteArray>> dumpedTrackskPoints;

    for(SPoint point: trackPoints)
    {
        AsterixHashPacker hp(AsterixUtils::decodeAsterix(point.rawByteArray));
        quint8 category = AsterixUtils::category((const uchar*)point.rawByteArray.constData());
        if(category == 21)
        {
            hp.addData("I021/161", "Track Number", newTrackNumber);
        }
        else if(category == 48)
        {
            hp.addData("I048/161", "Track Number", newTrackNumber);
        }

        dumpedTrackskPoints << qMakePair(point.reciveTimestamp, AsterixEncoder::Instance().encodeData(hp.getHash()));
    }
    return dumpedTrackskPoints;
}

bool AssessmentProceduresWidget::areTracksTypeTheSame(const QModelIndexList &selectedRows)
{
    int rows = selectedRows.size();
    if(rows < 2)
    {
        return false;
    }

    QVariant trackProfileAddr = currentSorter->data(selectedRows[0], Qt::UserRole + 2);
    TrackProfile *profile = (TrackProfile *)trackProfileAddr.toULongLong();
    if(!profile)
    {
        return false;
    }

    TrackType::TrackType prevTrackType = profile->trackKey.type;
    for(int row = 1; row < rows; row++)
    {
        trackProfileAddr = currentSorter->data(selectedRows[0], Qt::UserRole + 2);
        profile = (TrackProfile *)trackProfileAddr.toULongLong();
        if(!profile)
        {
            return false;
        }

        if(prevTrackType != profile->trackKey.type)
        {
            return false;
        }
    }

    return true;
}

bool AssessmentProceduresWidget::areTracksIdTheSame(const QModelIndexList &selectedRows)
{
    int rows = selectedRows.size();
    if(rows < 2)
    {
        return false;
    }

    QVariant trackProfileAddr = currentSorter->data(selectedRows[0], Qt::UserRole + 2);
    TrackProfile *profile = (TrackProfile *)trackProfileAddr.toULongLong();
    if(!profile)
    {
        return false;
    }

    int prevTrackValue = profile->trackKey.value;
    for(int row = 1; row < rows; row++)
    {
        trackProfileAddr = currentSorter->data(selectedRows[0], Qt::UserRole + 2);
        profile = (TrackProfile *)trackProfileAddr.toULongLong();
        if(!profile)
        {
            return false;
        }

        if(prevTrackValue != profile->trackKey.value)
        {
            return false;
        }
    }

    return true;
}

void AssessmentProceduresWidget::on_tb_merge_clicked()
{
    QApplication::setOverrideCursor(Qt::WaitCursor);

    QModelIndexList selectedRows = ui->tableView->selectionModel()->selectedRows(StatisticsColumns::HiddenService);
    int rows = selectedRows.size();
    if(rows == 0)
    {
        QMessageBox::information(this, "Внимание!", "Треки не выбраны!");
        return;
    }

    if(rows < 2)
    {
        QMessageBox::information(this, "Внимание!", "Выберите несколько треков!");
        return;
    }

    if(!areTracksTypeTheSame(selectedRows))
    {
        QMessageBox::information(this, "Внимание!", "Выбраны треки различных типов!");
        return;
    }

    if(!areTracksIdTheSame(selectedRows))
    {
        QMessageBox::information(this, "Внимание!", "Выбраны треки с различными идентификаторами!");
        return;
    }

    int nextId = TrackIdKeeper::Instance().nextId();
    qDebug() << "Added new track ID: " << nextId;

    QVariant trackProfileAddr = currentSorter->data(selectedRows[0], Qt::UserRole + 2);
    TrackProfile *profile = (TrackProfile *)trackProfileAddr.toULongLong();
    if(!profile)
    {
        QMessageBox::information(this, "Внимание!", "Ошибка чтения данных трека!");
        return;
    }

    TrackProfile newTrackProfile;
    TrackKey trackKey;
    trackKey.type = profile->trackKey.type;
    trackKey.trackNumber = nextId;
    trackKey.value = profile->trackKey.value;
    QList<SPoint> points;
    for(int row = 0; row < rows; row++)
    {
        trackProfileAddr = currentSorter->data(selectedRows[row], Qt::UserRole + 2);
        profile = (TrackProfile *)trackProfileAddr.toULongLong();
        if(!profile)
        {
            QMessageBox::information(this, "Внимание!", "Ошибка чтения данных трека!");
            return;
        }

        if((newTrackProfile.address24bit == 0) && (profile->address24bit != 0))
        {
            newTrackProfile.address24bit = profile->address24bit;
        }

        if((newTrackProfile.uvdReplay == 0) && (profile->uvdReplay != 0))
        {
            newTrackProfile.uvdReplay = profile->uvdReplay;
        }

        if((newTrackProfile.callsign == 0) && (profile->callsign != 0))
        {
            newTrackProfile.callsign = profile->callsign;
        }

        newTrackProfile.reply << profile->reply;
        points << profile->points;
    }

    newTrackProfile.trackKey = trackKey;
    std::sort(points.begin(), points.end(), [](const SPoint &a, const SPoint &b)
                                              {return a.reciveTimestamp < b.reciveTimestamp;});
    newTrackProfile.startDate = points.first().reciveTimestamp;
    newTrackProfile.endDate = points.last().reciveTimestamp;

    int pointsCount = points.size();
    newTrackProfile.haveModeC.push_back(!MapGeometry::isNan(points.at(0).altitude));
    newTrackProfile.haveCodeModeA.push_back((points.at(0).reply != 0));
    newTrackProfile.haveCodeUvd.push_back((points.at(0).uvdReplay != 0));
    for(int pointNum = 1; pointNum < pointsCount; pointNum++)
    {
        GPoint p1 = GPoint(points.at(pointNum - 1).latitude, points.at(pointNum - 1).longitude);
        GPoint p2 = GPoint(points.at(pointNum).latitude, points.at(pointNum).longitude);

        double dist = GCalc().distance(p1, p2);
        newTrackProfile.distancesKm << dist;
        newTrackProfile.timeDiffsMsec << points.at(pointNum - 1).reciveTimestamp.msecsTo(points.at(pointNum).reciveTimestamp);
        newTrackProfile.lengthKm += dist;

        newTrackProfile.haveModeC.push_back(!MapGeometry::isNan(points.at(pointNum).altitude));
        newTrackProfile.haveCodeModeA.push_back((points.at(pointNum).reply != 0));
        newTrackProfile.haveCodeUvd.push_back((points.at(pointNum).uvdReplay != 0));
    }

    newTrackProfile.durationSecs = newTrackProfile.startDate.secsTo(newTrackProfile.endDate);
    newTrackProfile.points = std::move(points);

    QApplication::restoreOverrideCursor();

    QList<TrackProfile> tracksProfiles;
    tracksProfiles << newTrackProfile;
    on_newTracksProfiles(tracksProfiles, true);

    (*currentTracksCounts)[trackKey.type]++;
    refreshTracksCountsOnButtons();
}

QString AssessmentProceduresWidget::datagramToText(const QByteArray& datagram)
{
    static char const hex_char[16] = {'0',
                                      '1', '2', '3', '4', '5',
                                      '6', '7', '8', '9', 'A',
                                      'B', 'C', 'D', 'E', 'F'};
    char byte;
    QString s;
    s.reserve(datagram.size() * 3);
    const char* data = datagram.constData();
    for(int i=0; i<datagram.size(); i++)
    {
        byte = data[i];
        s += hex_char[(byte & 0xF0) >> 4];
        s += hex_char[(byte & 0x0F)];
        s += " ";
    }
    s.chop(1);
    return s;
}

void AssessmentProceduresWidget::on_tb_compute_clicked()
{
    emit compute();
}

void AssessmentProceduresWidget::on_tb_addToTracks_clicked()
{
    emit addToTracks();
}

void AssessmentProceduresWidget::on_tb_mergeFiles_clicked()
{
    emit mergeFiles();
}

bool TooltipItemDelegate::helpEvent(QHelpEvent *event, QAbstractItemView *view, const QStyleOptionViewItem &option, const QModelIndex &index)
{
    if(!event || !view)
    {
        return false;
    }

    if(event->type() == QEvent::ToolTip)
    {
        if(index.column() == StatisticsColumns::HiddenService)
        {
            QVariant tooltip = index.data(Qt::UserRole + 3);
            if(tooltip.canConvert<QString>())
            {
                QString text = tooltip.toString();
                if(!space)
                {
                    space = true;
                    text.append(' ');
                }
                else
                {
                    space = false;
                }

                QToolTip::showText(event->globalPos(), text, view);
                return true;
            }

            if(!QStyledItemDelegate::helpEvent(event, view, option, index))
                QToolTip::hideText();
            return true;
        }
    }

    return QStyledItemDelegate::helpEvent(event, view, option, index);
}

void AssessmentProceduresWidget::on_tb_fakeSamples_clicked()
{
    int rowCount = ui->tableView->model()->rowCount();
    if(rowCount == 0)
    {
        QMessageBox::information(this, "Внимание!", "Не обнаружены треки для анализа!");
        return;
    }

    if(rowCount == 1)
    {
        QMessageBox::information(this, "Внимание!", "Для анализа необходимы минимум два трека!");
        return;
    }

    int minIntersectedPoints = Settings::value(SettingsKeys::MIN_INTERSECTED_POINTS).toInt();
    QList<QPair<MismatchInfoType::IntersectedTrack, MismatchInfoType::IntersectedTrack>> intersectedTracks;
    double aircraftVerticalSpeedMS = Settings::value(SettingsKeys::AIRCRAFT_AVG_VERTICAL_SPEED).toDouble();
    for(int i = 0; i < rowCount; i++)
    {
        QVariant var = currentSorter->data(currentSorter->index(i, 0), Qt::UserRole + 2);
        TrackProfile* profileIPtr = (TrackProfile *)var.toULongLong();
        if(!profileIPtr)
        {
            continue;
        }

        TrackProfile profileI = *profileIPtr;
        if(profileI.points.size() == 0)
        {
            continue;
        }

        for(int j = i + 1; j < rowCount; j++)
        {
            if(i == j)
            {
                continue;
            }

            QVariant var = currentSorter->data(currentSorter->index(j, 0), Qt::UserRole + 2);
            TrackProfile *profileJPtr = (TrackProfile *)var.toULongLong();
            if(!profileJPtr)
            {
                continue;
            }

            TrackProfile profileJ = *profileJPtr;
            if(profileJ.points.size() == 0)
            {
                continue;
            }

            if(!Statistics::MismatchInfo::tracksIntersectsByTime(profileI.points, profileJ.points))
            {
                continue;
            }

            QPair<QList<SPoint>, QList<SPoint>> lists =
                    Statistics::MismatchInfo::getPointsIntersectedByTime(profileI.points, profileJ.points);
            if((lists.first.isEmpty()) || (lists.second.isEmpty()))
            {
                continue;
            }

            QList<QPair<SPoint, SPoint>> intersectedPointsByAzimuth =
                    Statistics::MismatchInfo::getPointsIntersectedByAzimuth(lists.first, lists.second, 2.3);

            if(intersectedPointsByAzimuth.isEmpty())
            {
                continue;
            }

            QList<QPair<SPoint, SPoint>> intersectedPointsByDistance =
                    Statistics::MismatchInfo::getPointsDistantMoreThenDeltaM(intersectedPointsByAzimuth, 4000);
            QList<SPoint> firstTrack;
            QList<SPoint> secondTrack;
            for(const QPair<SPoint, SPoint> &pointsPair: intersectedPointsByDistance)
            {
                firstTrack << pointsPair.first;
                secondTrack << pointsPair.second;
            }

            std::tuple<int, int> callsignReportsTrack1 = Statistics::MismatchInfo::analyzeCallsign(firstTrack);
            double callsignTrack1Probability = Statistics::MismatchInfo::calcFalseProbability(callsignReportsTrack1, firstTrack.size());
            std::tuple<int, int> callsignReportsTrack2 = Statistics::MismatchInfo::analyzeCallsign(secondTrack);
            double callsignTrack2Probability = Statistics::MismatchInfo::calcFalseProbability(callsignReportsTrack2, secondTrack.size());

            std::tuple<int, int> squawkReportsTrack1 = Statistics::MismatchInfo::analyzeSquawk(firstTrack, 2);
            double squawkTrack1Probability = Statistics::MismatchInfo::calcFalseProbability(squawkReportsTrack1, firstTrack.size());
            std::tuple<int, int> squawkReportsTrack2 = Statistics::MismatchInfo::analyzeSquawk(secondTrack, 2);
            double squawkTrack2Probability = Statistics::MismatchInfo::calcFalseProbability(squawkReportsTrack2, secondTrack.size());

            std::tuple<int, int> altitudeReportsTrack1 = Statistics::MismatchInfo::analyzeAltitude(firstTrack, aircraftVerticalSpeedMS);
            double altitudeTrack1Probability = Statistics::MismatchInfo::calcFalseProbability(altitudeReportsTrack1, firstTrack.size());
            std::tuple<int, int> altitudeReportsTrack2 = Statistics::MismatchInfo::analyzeSquawk(secondTrack, aircraftVerticalSpeedMS);
            double altitudeTrack2Probability = Statistics::MismatchInfo::calcFalseProbability(altitudeReportsTrack2, secondTrack.size());

            int pointsCount = std::min(firstTrack.size(), secondTrack.size());
            if(pointsCount <= minIntersectedPoints)
            {
                continue;
            }

            if((pointsCount > 100) && ((squawkTrack1Probability < .1) || (squawkTrack2Probability < .1)))
            {
                int Px_e = 0;
                int Px_v = 0;
                std::tie(Px_e, Px_v) = squawkReportsTrack1;
                qDebug() << "squawkReportsTrack";
                qDebug() << "Px_e: " << Px_e << "   Px_v: " << Px_v;

                std::tie(Px_e, Px_v) = squawkReportsTrack2;
                qDebug() << "squawkReportsTrack2";
                qDebug() << "Px_e: " << Px_e << "   Px_v: " << Px_v;
                qDebug() << "STOP";
            }

            profileI.clear();
            profileJ.clear();
            for(int i = 0; i < pointsCount; i++)
            {
                profileI.applayPoint(firstTrack[i]);
                profileJ.applayPoint(secondTrack[i]);
            }

            intersectedTracks << qMakePair(std::make_tuple(profileI, squawkTrack1Probability, callsignTrack1Probability, altitudeTrack1Probability),
                                           std::make_tuple(profileJ, squawkTrack2Probability, callsignTrack2Probability, altitudeTrack2Probability));
        }
    }

    if(intersectedTracks.size() == 0)
    {
        QMessageBox::information(this, "Внимание!", "Пересекающихся треков не найдено!");
        return;
    }

    InformationMismatchWidget *imw = new InformationMismatchWidget();
    if(!imw)
    {
        return;
    }

    imw->addTracksIntersectionInfo(intersectedTracks);
    connect(imw, SIGNAL(showOriginalTracks(QList<int>)), this, SLOT(showTracksOnMap(QList<int>)));
    imw->show();
}

void AssessmentProceduresWidget::showTracksOnMap(QList<int> tracksIdList)
{
    for(const TrackProfile& trackProfile: (*currentTrackProfiles))
    {
        if(tracksIdList.contains(trackProfile.trackKey.trackNumber))
        {
            MapActions::Instance().showTrackOnMap(trackProfile, false, false, false, true);
        }
    }
}


void AssessmentProceduresWidget::on_tb_showLocatorOnMap_clicked()
{
    xProtoHashPacker hp(300);
    hp.addData("I300/010", "type", "TO_MAP_OBJECT_PUT");
    hp.addData("I300/020", "object_type", "POINT");
    hp.addData("I300/060", "host_name", QHostInfo::localHostName());
    //QByteArray msgHead = hp.getPackedData();

    //
    xProtoHashPacker hp_point(305);
    hp_point.addData("I305/010", "id", "MVRL_AURORA_LOCATOR_POS");
    hp_point.addData("I305/010", "group", "RTOP/RadarPoints");
    hp_point.addData("I305/020", "icao_name", "MVRL_AURORA");
    GPoint centerPoint = GPoint(Settings::instance().value(SettingsKeys::LOCATOR_POSITION).toString());
    hp_point.addData("I305/020", "lat", centerPoint.latitude());
    hp_point.addData("I305/020", "lon", centerPoint.longitude());
    hp_point.addNewRecord();

    QByteArray packet = hp.getPackedData();
    packet.append(hp_point.getPackedData());
    ChannelKeeper::Instance().sendToMap(packet);
/*
    bool isEnglish = getLanguage() == AbstractRequestHandler::ENGLISH;
    int y=0;
    foreach (const MAP_ANI::Point &mPoint, points)
    {
        y++;

        hp_point.addData("I305/010", "id", mPoint.getId());
        ---hp_point.addData("I305/010", "z_order", 0);

        if (!mPoint.group.isEmpty())
            hp_point.addData("I305/010", "group", mPoint.group);

        if (!mPoint.style.isEmpty())
            ---hp_point.addData("I305/010", "style", mPoint.style);

//        if( mPoint.group.isEmpty() && mPoint.style.isEmpty() )
//            qDebug() << mPoint.icao_name( false ) << mPoint.id;

        ---hp_point.addData("I305/010", "tool_tip", mPoint.tool_tip);

        // MVRL AURORA
        hp_point.addData("I305/020", "icao_name", mPoint.icao_name(isEnglish));

        ----hp_point.addData("I305/020", "display_name", mPoint.display_name(isEnglish));

        hp_point.addData("I305/020", "lat",mPoint.lat);
        hp_point.addData("I305/020", "lon",mPoint.lon);

        hp_point.addNewRecord();

        if((y%1000)==0)
        {
            PandaKeeper::send( msgHead + hp_point.getPackedData() , toHost );

            qDebug()<<Q_FUNC_INFO<<"SENDED";
            hp_point.clear();
        }
    }

    if( hp_point.recordCount() > 0 )
        PandaKeeper::send( msgHead + hp_point.getPackedData(), toHost );
        */
}

int AssessmentProceduresWidget::getRbsAndUvdTracksCount()
{
    int rowCount = ui->tableView->model()->rowCount();
    int rbsTracksCount = 0;
    int uvdTracksCount = 0;
    for(int currentRow = 0; currentRow < rowCount; currentRow++)
    {
        QVariant var = currentSorter->data(currentSorter->index(currentRow, 0), Qt::UserRole + 2);
        TrackProfile* profilePtr = (TrackProfile *)var.toULongLong();
        if(!profilePtr)
        {
            continue;
        }

        if(profilePtr->points.size() == 0)
        {
            continue;
        }

        if(profilePtr->trackKey.type == TrackType::RBS)
        {
            rbsTracksCount++;
        }
        else if(profilePtr->trackKey.type == TrackType::UVD)
        {
            uvdTracksCount++;
        }
    }

    return rbsTracksCount + uvdTracksCount;
}

void AssessmentProceduresWidget::clearPointsMarks(QList<SPoint> &points)
{
    for(int i = 0; i < points.size(); i++)
    {
        points[i].pointClass = SPoint::PointClass::UNDEFINED;
    }
}

void AssessmentProceduresWidget::on_tb_calculateFaultPointsCount_clicked()
{
    int rowCount = ui->tableView->model()->rowCount();
    if(rowCount == 0)
    {
        QMessageBox::information(this, "Внимание!", "Не обнаружены треки для анализа!");
        return;
    }

    int rbsUvdTracksCount = getRbsAndUvdTracksCount();
    if(rbsUvdTracksCount == 0)
    {
        QMessageBox::information(this, "Внимание!", "Не обнаружены треки RBS или UVD!");
        return;
    }

    FalsePointsStatisticsCalculator falsePointsStatisticsCalculator;
    falsePointsStatisticsCalculator.setUnusedPoints(*currentUnusedPoints);
    falsePointsStatisticsCalculator.setDataSourceModel(currentSorter);

    FalsePointsStatisticsCalculator::StatisticsResult result = falsePointsStatisticsCalculator.calculate();
    if(result == FalsePointsStatisticsCalculator::StatisticsResult::LARGE_INTERVAL)
    {
        QString message = "Выбран набор данных с длительностью наблюдения более недели!";
        message.append("\nВыберите данные с длительность наблюдения менее недели!");
        QMessageBox::information(this, "Внимание!", message);
        return;
    }

    QVector<double> x = falsePointsStatisticsCalculator.getIntervals();
    QVector<double> y = falsePointsStatisticsCalculator.getProbabilities();
    QVector<QString> labels = falsePointsStatisticsCalculator.getLabels();

    FaultTargetsCalculatorWidget *faultTargetsCalculatorWidget = new FaultTargetsCalculatorWidget();
    if(!faultTargetsCalculatorWidget)
    {
        QMessageBox::information(this, "Внимание!", "При создании графика возникла проблема!");
        return;
    }
    faultTargetsCalculatorWidget->setAvgProbability(falsePointsStatisticsCalculator.getAvgProbability(),
                                                    falsePointsStatisticsCalculator.getIntervalsCount());
    faultTargetsCalculatorWidget->addData(x, y, labels);
    faultTargetsCalculatorWidget->setDateTimeList(falsePointsStatisticsCalculator.getDateTimeList());
    faultTargetsCalculatorWidget->show();
}

QList<SPoint> AssessmentProceduresWidget::getTracksPoints()
{
    QList<SPoint> tracksPoints;
    int rowCount = ui->tableView->model()->rowCount();
    if(rowCount == 0)
    {
        return tracksPoints;
    }

    for(int currentRow = 0; currentRow < rowCount; currentRow++)
    {
        QVariant var = currentSorter->data(currentSorter->index(currentRow, 0), Qt::UserRole + 2);
        TrackProfile* profilePtr = (TrackProfile *)var.toULongLong();
        if(!profilePtr)
        {
            continue;
        }

        if(profilePtr->points.size() == 0)
        {
            continue;
        }

        if((profilePtr->trackKey.type == TrackType::UVD) || (profilePtr->trackKey.type == TrackType::RBS))
        {
            tracksPoints << profilePtr->points;
        }
    }

    return tracksPoints;
}

QList<SPoint> AssessmentProceduresWidget::sortPointsByTime(QList<SPoint> points)
{
    std::sort(points.begin(), points.end(), [](const SPoint &a, const SPoint &b) {return a.dateTime < b.dateTime;});
    return points;
}

QList<SPoint> AssessmentProceduresWidget::extractUnstablePoints(QList<SPoint> &points)
{
    QList<SPoint> unstablePoints;
    GPoint centerPoint = GPoint(Settings::instance().value(SettingsKeys::LOCATOR_POSITION).toString());
    double locatorVisibilityDistanceMeters = Settings::value(SettingsKeys::MAX_LOCATOR_DISTANCE).toInt() * 1000.;
    double unstableZoneDeltaPercent = Settings::value(SettingsKeys::UNSTABLE_ZONE_WIDTH_PERCENT).toDouble();
    double unstableZoneDeltaMeters = locatorVisibilityDistanceMeters * unstableZoneDeltaPercent / 100.;
    double lowerUnstableZoneBorder = locatorVisibilityDistanceMeters - unstableZoneDeltaMeters;
    double upperUnstableZoneBorder = locatorVisibilityDistanceMeters + unstableZoneDeltaMeters;

    int pointsCount = points.size();
    for(int currentPoint = pointsCount - 1; currentPoint >= 0; currentPoint--)
    {
        double locatorToPointDistanceMeters = GCalc().distance(centerPoint, GPoint(points[currentPoint].latitude, points[currentPoint].longitude)) * 1000.;
        if((points[currentPoint].reply == 0) && (points[currentPoint].uvdReplay == 0) &&
           (points[currentPoint].pointClass == SPoint::PointClass::UNDEFINED) &&
           (locatorToPointDistanceMeters >= lowerUnstableZoneBorder) &&
           (locatorToPointDistanceMeters <= upperUnstableZoneBorder))
        {
            points[currentPoint].pointClass = SPoint::PointClass::UNSTABLE;
            unstablePoints << points[currentPoint];
        }
    }

    return unstablePoints;
}

QList<SPoint> AssessmentProceduresWidget::extractReflectedAndFilteredPoints(QList<SPoint> &points)
{
    QList<SPoint> reflectedAndFilteredPoints;

    int pointsCount = points.size();
    for(int currentPoint = pointsCount - 1; currentPoint >= 0; currentPoint--)
    {
        if((points[currentPoint].uvdReplay == 0) && (points[currentPoint].reply == 0))
        {
            continue;
        }

        if(points[currentPoint].pointClass != SPoint::PointClass::UNDEFINED)
        {
            continue;
        }

        QList<TrackProfile*> suitableTracksByCode;
        if(points[currentPoint].uvdReplay)
        {
            suitableTracksByCode << getTracksWithUvdCode(points[currentPoint].uvdReplay);
        }

        if(points[currentPoint].reply)
        {
            suitableTracksByCode << getTracksWithSquawk(points[currentPoint].reply);
        }

        if(suitableTracksByCode.size() == 0)
        {
            continue;
        }

        QList<TrackProfile*> suitableTracksByTime;
        for(TrackProfile* profile: suitableTracksByCode)
        {
            if((points[currentPoint].dateTime > profile->points.first().dateTime) &&
               (points[currentPoint].dateTime > profile->points.last().dateTime))
            {
                suitableTracksByTime << profile;
            }
        }

        if(suitableTracksByTime.size() == 0)
        {
            continue;
        }

        QList<TrackProfile*> filteredTracks;
        QList<TrackProfile*> reflectedTracks;
        for(TrackProfile* profile: suitableTracksByTime)
        {
            QPair<SPoint, SPoint> includingPoints = getPointsNeighbouringByTime(points[currentPoint].dateTime, profile->points);
            if((!includingPoints.first.isValid()) || (!includingPoints.second.isValid()))
            {
                continue;
            }

            double rlsRotationPeriodMs = Settings::value(SettingsKeys::LOCATOR_ROTATION_PERIOD).toDouble();
            double deltaTime = includingPoints.second.dateTime - includingPoints.first.dateTime;

            bool pointSuitsByDistance = pointInParameters(points[currentPoint].theta,
                                                          includingPoints.first.theta,
                                                          includingPoints.second.theta);

            bool pointSuitsByAltitude = pointInParameters(points[currentPoint].altitude,
                                                          includingPoints.first.altitude,
                                                          includingPoints.second.altitude);

            bool pointSuitsByAzimuth = pointInAzimuth(points[currentPoint].rho,
                                                      includingPoints.first.rho,
                                                      includingPoints.second.rho);

            if(pointSuitsByDistance && pointSuitsByAltitude && pointSuitsByAzimuth)
            {
                if(deltaTime <= (rlsRotationPeriodMs * 1.1))
                {
                    points[currentPoint].pointClass = SPoint::PointClass::FAKE;
                    faultPoints << points[currentPoint];
                    continue;
                }

                if((deltaTime >= (rlsRotationPeriodMs * 1.5)) && (deltaTime <= (rlsRotationPeriodMs * 2 * 1.1)))
                {
                    filteredTracks << profile;
                    continue;
                }
            }

            if(pointSuitsByDistance && pointSuitsByAltitude && (!pointSuitsByAzimuth))
            {
                reflectedTracks << profile;
            }
        }

        if(filteredTracks.size())
        {
            points[currentPoint].pointClass = SPoint::PointClass::FILTERED;
            reflectedAndFilteredPoints << points[currentPoint];
        }

        if(reflectedTracks.size())
        {
            points[currentPoint].pointClass = SPoint::PointClass::REFLECTED;
            reflectedAndFilteredPoints << points[currentPoint];
        }
    }

    return reflectedAndFilteredPoints;
}

bool AssessmentProceduresWidget::pointInAzimuth(double checkingAzimuth, double checkingAzimuth1, double checkingAzimuth2)
{
    if((!Statistics::MismatchInfo::azimuthsInDelta(checkingAzimuth, checkingAzimuth1, 3)) &&
       (!Statistics::MismatchInfo::azimuthsInDelta(checkingAzimuth, checkingAzimuth2, 3)))
    {
        return false;
    }

    return true;
}

bool AssessmentProceduresWidget::pointInParameters(double checkingParam, double point1Param, double point2Param)
{
    if(MapGeometry::isNan(point1Param) || MapGeometry::isNan(point2Param))
    {
        return true;
    }

    if((checkingParam >= point1Param) && (checkingParam <= point2Param))
    {
        return true;
    }

    if((checkingParam >= point2Param) && (checkingParam <= point1Param))
    {
        return true;
    }

    double deltaPercentScale = 0.2;
    double avgDist = (point2Param + point1Param) / 2;
    if((checkingParam >= (avgDist - avgDist * deltaPercentScale)) || (checkingParam <= (avgDist + avgDist * deltaPercentScale)))
    {
        return true;
    }

    return false;
}

QPair<SPoint, SPoint> AssessmentProceduresWidget::getPointsNeighbouringByTime(double pointTime, const QList<SPoint> &track)
{
    for(int i = 1; i < track.size(); i++)
    {
        if((track[i - 1].dateTime > pointTime) && (track[i].dateTime < pointTime))
        {
            return qMakePair(track[i - 1], track[i]);
        }
    }

    return qMakePair(SPoint(), SPoint());
}

QList<TrackProfile*> AssessmentProceduresWidget::getTracksWithUvdCode(uint32_t uvdCode)
{
    QList<TrackProfile*> uvdTracks;
    int rowCount = ui->tableView->model()->rowCount();
    if(rowCount == 0)
    {
        return uvdTracks;
    }

    for(int currentRow = 0; currentRow < rowCount; currentRow++)
    {
        QVariant var = currentSorter->data(currentSorter->index(currentRow, 0), Qt::UserRole + 2);
        TrackProfile* profilePtr = (TrackProfile *)var.toULongLong();
        if(!profilePtr)
        {
            continue;
        }

        if(profilePtr->points.size() == 0)
        {
            continue;
        }

        if((profilePtr->trackKey.type == TrackType::UVD) && (profilePtr->uvdReplay == uvdCode))
        {
            uvdTracks << profilePtr;
        }
    }

    return uvdTracks;
}

QList<TrackProfile*> AssessmentProceduresWidget::getTracksWithSquawk(uint16_t squawk)
{
    QList<TrackProfile*> rbsTracks;
    int rowCount = ui->tableView->model()->rowCount();
    if(rowCount == 0)
    {
        return rbsTracks;
    }

    for(int currentRow = 0; currentRow < rowCount; currentRow++)
    {
        QVariant var = currentSorter->data(currentSorter->index(currentRow, 0), Qt::UserRole + 2);
        TrackProfile* profilePtr = (TrackProfile *)var.toULongLong();
        if(!profilePtr)
        {
            continue;
        }

        if(profilePtr->points.size() == 0)
        {
            continue;
        }

        if((profilePtr->trackKey.type == TrackType::RBS) && (profilePtr->reply.contains(squawk)))
        {
            rbsTracks << profilePtr;
        }
    }

    return rbsTracks;
}

bool AssessmentProceduresWidget::initModels(const QStringList &availableLocators, const QString &currentLocator)
{
    for(const QString &locator: availableLocators)
    {
        LocatorSortModel *locatorViewSortModel = LocatorSortModelFactory::create(locator);
        LocatorModelColumns *locatorModelColumns = LocatorModelColumnsFactory::create(locator);
        HidedColumns *hidedColumns = HidedColumnsFactory::create(locator);
        dataSources[locator] = DataSource(locatorViewSortModel, ui->tableView,
                                          locatorModelColumns, hidedColumns);
    }

    return switchModels(currentLocator);
}

bool AssessmentProceduresWidget::switchModels(const QString &locatorType)
{
    if(!dataSources.contains(locatorType))
    {
        CDEBUG << "ERROR! Locator type: '" << locatorType << "' not exists!";
        return false;
    }

    if((!lastLocatorType.isEmpty()) && dataSources.contains(lastLocatorType) &&
       dataSources[lastLocatorType].filterConnected)
    {
        disconnect(this, SIGNAL(sendFilters(QList<TrackType::TrackType>)),
                   currentSorter, SLOT(refilter(QList<TrackType::TrackType>)));
        dataSources[lastLocatorType].filterConnected = false;
    }
    lastLocatorType = locatorType;

    currentModel = dataSources[locatorType].model;
    currentSorter = dataSources[locatorType].sorter;
    currentTrackProfiles = &(dataSources[locatorType].trackProfiles);
    currentController = dataSources[locatorType].controller;
    currentTracksCounts = &(dataSources[locatorType].tracksCounts);
    currentPointsCounts = &(dataSources[locatorType].pointsCounts);
    currentUnusedPoints = &(dataSources[locatorType].unusedPoints);

    connect(this, SIGNAL(sendFilters(QList<TrackType::TrackType>)),
            currentSorter, SLOT(refilter(QList<TrackType::TrackType>)));
    dataSources[locatorType].filterConnected = true;

    ui->tableView->setModel(currentSorter);
    currentSorter->setFilterSettings(statusSettings);
    currentController->fillView();

    processTracksTypesButtos(locatorType);
    refreshTracksCountsOnButtons();
    setPointsCounters((*currentPointsCounts), false);

    emit sendFilters(fillFilters());
    return true;
}

void AssessmentProceduresWidget::processTracksTypesButtos(const QString &locatorType)
{
    if(locatorType == LocatorTypeCaption::rlsCaption)
    {
        ui->pb_useUvd->setDisabled(false);
        return;
    }

    if(locatorType == LocatorTypeCaption::mlatCaption)
    {
        ui->pb_useUvd->setDisabled(true);
        return;
    }

    if(locatorType == LocatorTypeCaption::sdpdCaption)
    {
        ui->pb_useUvd->setDisabled(false);
        return;
    }
}

void AssessmentProceduresWidget::on_cb_mode_activated(const QString &arg1)
{
    switchModels(arg1);

    ui->tableView->setModel(dataSources[arg1].sorter);
    //if(!dataSources[arg1].filterConnected)
    //{
    //    connect(this, SIGNAL(sendFilters(QList<TrackType::TrackType>)),
    //            dataSources[arg1].sorter, SLOT(refilter(QList<TrackType::TrackType>)));
    //    dataSources[arg1].filterConnected = true;
    //}

    QString locatorType = ui->cb_mode->currentText();
    mlatSettingsButtonChangeEnabledState(LocatorUtils::getLocatorTypeByCaption(locatorType));
    Settings::setValue(SettingsKeys::MLAT_RLS_TYPE, LocatorUtils::getLocatorTypeByCaption(locatorType));
    emit(locatorModeChanged(locatorType));
}

void AssessmentProceduresWidget::mlatSettingsButtonChangeEnabledState(LocatorUtils::LocatorType locatorType)
{
    if(locatorType == LocatorUtils::LocatorType::MLAT)
    {
        ui->tb_mlatSettings->setEnabled(true);
        return;
    }

    ui->tb_mlatSettings->setEnabled(false);
}

void AssessmentProceduresWidget::appendDataToFileSelector(const QString &fileName)
{
    Q_UNUSED(fileName);
}

void AssessmentProceduresWidget::on_tb_mlatSettings_clicked()
{
    MlatSettingsDialog *dlg = new MlatSettingsDialog(this);
    dlg->show();
}

void AssessmentProceduresWidget::on_filesTableHided(int tableSize)
{
    if(tableSize <= 0)
    {
        return;
    }
    QList<int> oldSizes = ui->splitter->sizes();
    QList<int> newSizes = QList<int> {0, oldSizes[1] + tableSize};
    ui->splitter->setSizes(newSizes);
}

void AssessmentProceduresWidget::on_cb_fileSelector_currentTextChanged(const QString &arg1)
{
    Q_UNUSED(arg1);
}
