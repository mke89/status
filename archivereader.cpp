#include "archivereader.h"
#include <QFile>
#include <QFileInfo>
#include <QTime>
#include <QDebug>
#include <QDir>
#include <QtConcurrent>
#include <QProgressDialog>
#include <QObject>
#include <QApplication>
#include <QSharedPointer>

#include "rtoplogreader.h"
#include "asterixhashreader.h"
#include "asterixutils.h"
#include "adsbutils.h"
#include "settings.h"

//#include "MapConstants.h"
#include "MapPoint.h"
#include "MapSpherical.h"
#include "spoint.h"
#include <functional>
#include "CommonAsterixReader.h"

SPoint copyHashDataToSpoint(AsterixHashReader* ahr, QDateTime recordTime)
{
    if(ahr->category().toInt() == 1)
    {
        SPoint point;
        point.category = 1;
        if(ahr->contains("I001/040"))
        {
            point.rho   = ahr->getFieldData( "I001/040", "RHO", 0).toDouble();
            point.theta = ahr->getFieldData( "I001/040", "Theta", 0).toDouble();
        }

        if(ahr->contains("I001/010"))
        {
            point.sac   = ahr->getFieldData( "I001/010", "SAC", 0).toInt();
            point.sic   = ahr->getFieldData( "I001/010", "SIC", 0).toInt();
        }

        if ( ahr->contains( "I001/090" ) )
        {
            point.flightLevel = ahr->getFieldData( "I001/090", "Flight Level", 0 ).toDouble();
            point.altitude    = converter::flightLevel_To_Meter(point.flightLevel);
        }

        if( ahr->contains( "I001/220" ) )
        {
            point.address24bit=ahr->getFieldData( "I001/090", "Target Address", 0 ).toInt();
        }

        if( ahr->contains( "I010/060" ) )
        {
            point.reply=ahr->getFieldData( "I001/060", "Reply", 0 ).toInt() ;
            point.replayValideted=ahr->getFieldData( "I001/060", "V", 0 ).toInt()  == 0;
        }

        if( ahr->contains( "I010/245" ) )
        {
            point.callsign=ahr->getFieldData( "I001/245", "Callsign", "" ).toString();
        }
        return point;
    }
    else if(ahr->category().toInt() == 2)
    {
        SPoint point;
        point.category = 2;
        QSharedPointer<SPoint::RadarReport> rpp(new SPoint::RadarReport());
        int messageType=ahr->getFieldData( "I002/000" , "Message Type", -1 ).toInt();

        if( ahr->contains( "I002/041 " ) )
            rpp->antennaRotationSpeed=ahr->getFieldData( "I002/041" , "Antenna Rotation Speed", -1 ).toDouble();

        rpp->sac               = ahr->getFieldData("I002/010","SAC").toInt();
        rpp->sic               = ahr->getFieldData("I002/010","SIC").toInt();

        rpp->timeOfDay         = ahr->getFieldData("I002/030","Time of Day").toDouble();
        rpp->sectorNumber      = ahr->getFieldData("I002/020", "SECTOR NUMBER").toDouble();
        if( messageType == 1 )//NORTH mark
        {
            rpp->sectorNumber=0.001;
        }
        point.radareport=rpp;
        return point;
    }
    else if(ahr->category().toInt() == 20)
    {
        SPoint point;
        point.category = 20;
        point.reciveTimestamp = recordTime;
        if(ahr->contains("I020/041"))
        {
            point.latitude = ahr->getFieldData("I020/041", "Latitude", 0).toDouble();
            point.longitude = ahr->getFieldData("I020/041", "Longitude", 0).toDouble();

            if((point.latitude == 0) && (point.longitude == 0))
            {
                return SPoint();
            }
        }

        if(ahr->contains("I020/010"))
        {
            point.sac = ahr->getFieldData("I020/010", "SAC", 0).toInt();
            point.sic = ahr->getFieldData("I020/010", "SIC", 0).toInt();
        }

        if(ahr->contains("I020/090"))
        {
            point.altitude = converter::flightLevel_To_Meter(ahr->getFieldData("I020/090", "Flight Level").toDouble());
        }

        if(ahr->contains("I020/220"))
        {
            point.address24bit = ahr->getFieldData("I020/220", "Target Address", 0).toUInt();
        }

        if(ahr->contains("I020/245"))
        {
            point.callsign = ahr->getFieldData("I020/245", "Target Identification").toString();
        }

        if(ahr->contains("I020/070"))
        {
            point.reply = ahr->getFieldData("I020/070", "Reply").toInt();
        }

        if(ahr->containsItem("I020/020", "GBS"))
        {
            point.gbs = ahr->getFieldData("I020/020", "GBS").toBool();
        }

        if(ahr->contains("I020/161"))
        {
            point.trackId = ahr->getFieldData("I020/161", "Track Number").toInt();
        }

        return point;
    }
    else if(ahr->category().toInt() == 21)
    {
        SPoint point;
        point.category = 21;
        point.reciveTimestamp = recordTime;
        if(ahr->contains("I021/130"))
        {
            point.latitude = ahr->getFieldData("I021/130", "Latitude", 0).toDouble();
            point.longitude = ahr->getFieldData("I021/130", "Longitude", 0).toDouble();

            if(point.latitude == 0 && point.longitude == 0)
                return SPoint();
        }

        if(ahr->contains("I021/010"))
        {
            point.sac = ahr->getFieldData("I021/010", "SAC", 0).toInt();
            point.sic = ahr->getFieldData("I021/010", "SIC", 0).toInt();
        }

        if(ahr->contains("I021/145"))
        {
            point.altitude = converter::flightLevel_To_Meter(ahr->getFieldData("I021/145", "Flight Level").toDouble());
        }

        if(ahr->contains("I021/080"))
        {
            point.address24bit = ahr->getFieldData("I021/080", "Target Address", 0).toUInt();
        }

        if(ahr->containsItem("I021/090", "NUCp or NIC"))
        {
            point.quality.NUCp = ahr->getFieldData("I021/090", "NUCp or NIC").toInt();
        }

        if(ahr->containsItem("I021/090", "NACp"))
        {
            point.quality.NACp = ahr->getFieldData("I021/090", "NACp").toInt();
        }

        if(ahr->containsItem("I021/090", "SIL"))
        {
            point.quality.SIL = ahr->getFieldData("I021/090", "SIL").toInt();
        }

        if(ahr->containsItem("I021/090", "PIC"))
        {
            point.quality.PIC = ahr->getFieldData("I021/090", "PIC").toInt();
        }

        if(ahr->contains("I021/170"))
        {
            point.callsign = ahr->getFieldData("I021/170", "Target Identification").toString();
        }

        if(ahr->contains("I021/073"))
        {
            point.dateTime = ahr->getFieldData("I021/073", "TOMRp", 0).toDouble();
        }

        if(ahr->contains("I021/070"))
        {
            point.reply = ahr->getFieldData("I021/070", "Reply").toInt();
        }

        if(ahr->contains("I021/250"))
        {
            point.s_ehs = true;
        }

        if(ahr->containsItem("I021/040", "GBS"))
        {
            point.gbs = ahr->getFieldData("I021/040", "GBS").toBool();
        }

        if(MapGeometry::isNan(point.latitude) || MapGeometry::isNan(point.longitude))
        {
            return point;
        }

        MapGeometry::point locatorPosition(Settings::instance().value(SettingsKeys::LOCATOR_POSITION).toString());
        MapGeometry::point currentPoint(point.latitude, point.longitude);
        point.theta = MapGeometry::azimuth(locatorPosition, currentPoint);
        point.rho = MapGeometry::distance(locatorPosition, currentPoint) * converter::METRES_TO_NMILE;

        return point;
    }
    else if(ahr->category().toInt() == 34)
    {
        SPoint point;
        point.category = 34;

        QSharedPointer<SPoint::RadarReport> rpp(new SPoint::RadarReport());
        int messageType=ahr->getFieldData( "I034/000" , "Message Type", -1 ).toInt();

        rpp->messageType=messageType;

        if( ahr->contains( "I034/041" ) )
            rpp->antennaRotationSpeed=ahr->getFieldData( "I034/041" , "Antenna Rotation Speed", -1 ).toDouble();

        rpp->sac               = ahr->getFieldData("I034/010","SAC").toInt();
        rpp->sic               = ahr->getFieldData("I034/010","SIC").toInt();

        double latitude          = ahr->getFieldData("I034/120","Latitude").toDouble();
        double longitude         = ahr->getFieldData("I034/120","Longitude").toDouble();
        double altitude          = ahr->getFieldData("I034/120","Height",0).toDouble();

        rpp->lat=latitude;
        rpp->lon=longitude;
        rpp->alt=altitude;

        rpp->timeOfDay         = ahr->getFieldData("I034/030","Time of Day").toDouble();
        rpp->timestamp         = QDateTime::currentDateTime();

        if( ahr->containsItem("I034/020", "SECTOR NUMBER") )
        {
            rpp->sectorNumber      = ahr->getFieldData("I034/020", "SECTOR NUMBER",0.00001).toDouble();
        }
        point.radareport=rpp;
        return point;
    }
    else if(ahr->category().toInt() == 48)
    {
        SPoint point;
        point.category = 48;
        point.reciveTimestamp = recordTime;
        if ( ahr->contains( "I048/040" ) )
        {
            point.rho   = ahr->getFieldData( "I048/040", "RHO",   0 ).toDouble();
            point.theta = ahr->getFieldData( "I048/040", "THETA", 0 ).toDouble();
        }

        if ( ahr->contains( "I048/010" ) )
        {
            point.sac = ahr->getFieldData( "I048/010", "SAC", 0 ).toInt();
            point.sic = ahr->getFieldData( "I048/010", "SIC", 0 ).toInt();
        }

        if ( ahr->contains( "I048/090" ) )
        {
            point.flightLevel      = ahr->getFieldData( "I048/090", "Flight Level", 0 ).toDouble();
            point.altitude         = converter::flightLevel_To_Meter( point.flightLevel    );
        }

        if ( ahr->contains( "I048/220" ) )
        {
            point.address24bit       = ahr->getFieldData("I048/220", "AIRCRAFT ADDRESS").toUInt();
        }

        if ( ahr->contains( "I048/070" ) )
        {
            point.reply       = ahr->getFieldData( "I048/070", "Reply", 0 ).toUInt();
        }

        if ( ahr->contains( "I048/140" ) )
        {
            //point.dateTime    = ahr->getFieldData( "I048/140", "Time of Day", 0 ).toDouble();
            point.dateTime = 0; /// !!!!!!
            if(point.dateTime == 0)
            {
                QDateTime startOfDay = QDateTime(recordTime.date());
                point.dateTime = startOfDay.msecsTo(recordTime) / 1000.;
            }
        }

        if ( ahr->contains( "I048/240" ) )
        {
            point.callsign    = ahr->getFieldData( "I048/240", "Aircraft Identification", "" ).toString();
        }

        if( ahr->contains("I048/250") )
        {
            point.s_ehs=true;
        }

        if( ahr->contains("I048/SP") )
        {
            if(ahr->containsSubField( "I048/SP", 6 ))
            {
                point.uvdReplay     = ahr->getSubFieldData( "I048/SP", 6, "Aircraft number" ).toInt();
                point.flightLevel   = ahr->getSubFieldData("I048/SP", 7, "Height").toInt();
                point.altitude      = point.flightLevel;
            }
        }

        if(ahr->contains( "I048/161" ))
        {
            point.trackId = ahr->getFieldData("I048/161", "Track Number", 0).toInt();
        }

        if(ahr->contains("I048/230"))
        {
            int gbs = ahr->getFieldData("I048/020","TYP").toInt();
            switch(gbs)
            {
                case 0:
                case 2:
                {
                    point.gbs = false;
                    break;
                }

                case 1:
                case 3:
                {
                    point.gbs = true;
                    break;
                }
            }
        }

        return point;
    }
    else if(ahr->category().toInt() == 62)
    {
        SPoint point;
        point.category = 62;
        point.reciveTimestamp = recordTime;
        if(ahr->contains("I062/040"))
            point.trackId = ahr->getFieldData("I062/040", "Track Number").toUInt();

        if(ahr->contains("I062/040"))
        {
            point.sac = ahr->getFieldData("I062/010", "SAC").toInt();
            point.sic = ahr->getFieldData("I062/010", "SIC").toInt();
        }

        if(ahr->contains("I062/105"))
        {
            point.latitude = ahr->getFieldData("I062/105", "Latitude").toDouble();
            point.longitude = ahr->getFieldData("I062/105", "Longitude").toDouble();
        }

        double rho = -1.;
        double theta = -1.;
        if(ahr->containsSubField("I062/340", 2))
        {
            rho = ahr->getSubFieldData("I062/340", 2, "RHO", -1.).toDouble();
            theta = ahr->getSubFieldData("I062/340", 2, "Theta", -1.).toDouble();
        }
        if ((rho != -1) && (theta != -1))
        {
            point.rho = rho;
            point.theta = theta;
            MapGeometry::point recalculatePoint(Settings::instance().value(SettingsKeys::LOCATOR_POSITION).toString());
            MapGeometry::point pnt = MapGeometry::walk(recalculatePoint, rho * converter::NMILE_TO_METRES, theta);
            point.latitude = pnt.lat;
            point.longitude = pnt.lon;
        }

        if(ahr->contains("I062/136"))
        {
            point.flightLevel = ahr->getFieldData("I062/136", "Measured Flight Level", 0).toDouble();
            point.altitude = converter::flightLevel_To_Meter(point.flightLevel);
        }

        if(ahr->containsItem("I062/060", "Reply"))
        {
            point.reply = ahr->getFieldData("I062/060", "Reply").toInt();
        }
        else if (ahr->containsSubItem("I062/SP", 6, "Aircraft number"))
        {
            point.reply = ahr->getSubFieldData("I062/SP", 6, "Aircraft number").toInt();
        }

        if(ahr->containsSubField("I062/380", 1))
        {
            point.address24bit = ahr->getSubFieldData("I062/380", 1, "Target Address", 0).toDouble();
        }

        if(ahr->containsSubField("I062/390", 2))
        {
            point.callsign = ahr->getSubFieldData("I062/390", 2, "Callsign").toString().trimmed();
        }

        if(ahr->containsSubField("I062/RE", 1))
        {
            point.quality.NUCp = ahr->getSubFieldData("I062/RE", 1, "NUCp or NIC").toInt();
            point.quality.PIC = ahr->getSubFieldData("I062/RE", 1, "PIC").toInt();
        }

        QDateTime startOfDay = QDateTime(recordTime.date());
        point.dateTime = startOfDay.msecsTo(recordTime) / 1000.;

        return point;
    }

    return SPoint();
}

QList< SPoint > convertBaToSpoint(QByteArray data, QDateTime recordTime, QString asterixSpecNum, bool withRawData)
{
    Q_UNUSED(asterixSpecNum);
 //   int interestedSpec=asterixSpecNum.split("_").value(0).toInt() ;

    AsterixHashReader ahr(AsterixUtils::decodeAsterix(data));
    QList<SPoint> result;
    SPoint point = copyHashDataToSpoint(&ahr, recordTime);

    if(withRawData)
    {
        point.rawByteArray = data;
        point.reciveTimestamp = recordTime;
        result << point;
    }
    else if(point.category != 0)
    {
        result << point;
    }

    return result;
}

ArchiveReader::ArchiveReader(){}

QList<SPoint> ArchiveReader::getData(const QString &path, const QString &filename, const QString &asterixSpecNum)
{
    QTime timer;
    timer.start();
    QFileInfo finfo(QDir(path), filename);
    RtopLogReader reader(finfo.absoluteFilePath());
    QProgressDialog prd;
    prd.setMaximum( reader.getProcessingFilesCount() );
    QObject::connect( &reader, SIGNAL(currentFileNum(int)), &prd, SLOT(setValue(int)) );
    prd.show();
    prd.setAutoClose(true);
    prd.setLabelText( ("Reading files...") );

    QMultiMap<QDateTime, QByteArray> result = reader.getPacketsInCurrentThread();

    qDebug() << Q_FUNC_INFO << "READING: elapsed time msec: " << timer.restart();

    prd.close();

    QList<QDateTime> keys = result.uniqueKeys();
    std::sort(keys.begin(), keys.end());
    QList<SPoint> spoitList;

    qDebug() << Q_FUNC_INFO << "SORTINGS: elapsed time msec: " << timer.restart();

    QSet<uint> validatos;
    for(QDateTime dkey: keys)
    {
        QList<QByteArray> datas = result.values(dkey);
        for(const QByteArray &md: datas)
        {
            uint key = qHash(md, 0);
            if(validatos.contains(key))
            {
                continue;
            }
            validatos.insert(key);

            QList<SPoint> points = convertBaToSpoint(md, dkey, asterixSpecNum, false);

            for(auto &var: points)
            {
                var.reciveTimestamp = dkey;
            }
            spoitList << points;
        }
    }

    qDebug() << Q_FUNC_INFO << "PROCESSING: elapsed time msec: " << timer.elapsed() << " result size = " << spoitList.size();

    return spoitList;
}

QList<SPoint> ArchiveReader::getData(const QString &path, const QString &filename, const char *asterixSpecNum)
{
    return getData(path, filename, QString(asterixSpecNum));
}

QList<SPoint> ArchiveReader::getData(const QString &file, const QString &asterixSpecNum, bool withRawData, int cutFirstBytes)
{
    QList<QPair<QDateTime, QByteArray>> rawData;
    const QList<VnrRW::RecordType> &records = CommonAsterixReader(file).readAll(cutFirstBytes);
    for(const VnrRW::RecordType &record: records)
    {
        rawData << qMakePair(std::get<0>(record), std::get<1>(record));
    }

    QList<SPoint> spoitList;
    for(int i = 0; i < rawData.size(); i++)
    {
        QList<SPoint> points = convertBaToSpoint(rawData[i].second, rawData[i].first, asterixSpecNum, withRawData);
        spoitList << points;
    }

    return spoitList;
}

struct decoderPredicate
{
    decoderPredicate(AsterixUtils &utils): m_utils(utils)
    {
    }

    SPoint operator()(QPair<QDateTime, QByteArray> data)
    {
        const QVariantHash &h = m_utils.decodeAsterix(data.second);
        asterixHashReader *ahr = nullptr;
        try
        {
            ahr = new asterixHashReader(h);
        }
        catch( ... )
        {
            delete ahr;
            return SPoint();
        }

        const SPoint &result = copyHashDataToSpoint(ahr, data.first);
        delete ahr;
        return result;
    }

private:
    AsterixUtils &m_utils;

};
QList<SPoint> ArchiveReader::getDataParallel(QString path, QString filename)
{
    QElapsedTimer timer;
    timer.start();

    QProgressDialog prd;
    prd.setCancelButton(0);
    prd.setAutoClose(true);
    prd.setLabelText(("Reading files..."));
    prd.show();
    QApplication::processEvents();

    QString filePath = path;
    if(!filePath.endsWith('/'))
    {
        filePath += "/";
    }
    filePath += filename;

    qDebug() << "Files reading...";
    const QList<QPair<QDateTime, QByteArray>> &dataForParralell = CommonAsterixReader::readDir(filePath);
    prd.setValue(50);
    qDebug() << "Files read: " << timer.restart() << "  msec";

    prd.setLabelText("Processing...");
    prd.setMaximum(100);
    prd.show();
    QApplication::processEvents();

    qDebug() << "Records count: " << dataForParralell.size();
    qDebug() << "Converting from 'raw' data to 'SPoint'...";
    std::function<SPoint(const QPair<QDateTime,QByteArray> &p)> d = decoderPredicate(AsterixUtils::Instance());
    const QVector<SPoint> &spoitList = QtConcurrent::blockingMapped<QVector<SPoint>>(dataForParralell, d);
    qDebug() << "Converted from 'raw' data to 'SPoint': " << timer.elapsed() << "  msec" ;


    return QList<SPoint>::fromVector(spoitList);
}

QMap<QString, QList<SPoint>> ArchiveReader::mergeTrajectories(const QList<SPoint> &list , MERGE_TYPE mergeType , bool useSicSac)
{
    QMap<QString, QList<SPoint> > result;
    for( SPoint point: list )
    {
        QString key;
        if ( mergeType == MERGE_BY_SQUAWK ) {
            if (point.reply > 0  && point.reply < 65000 ) {
                key = QString::number(point.reply);
            }
            else {
                result["UNUSED"] << point;
                continue;
            }

        }
        else if( mergeType == MERGE_BY_CALLSIGN ) {
            if (!point.callsign.isEmpty()) {
                key = point.callsign;
            }
            else {
                result["UNUSED"]<<point;
                continue;
            }
        }
        else if( mergeType == MERGE_BY_24BIT) {
            if (point.address24bit > 0)
            {
                key = QString::number(point.address24bit);
            }
            else {
                result["UNUSED"] << point;
                continue;
            }
        }

        if (!key.isEmpty())
        {
            if(useSicSac)
            {
                key= QString::number(point.sic)
                    +"/"
                    +QString::number(point.sac)
                    +":"+key;
            }
            result[key] << point;
        }
        else
        {
            result["UNUSED"] << point;
        }
    }
    return result;

}

QList<SPoint> ArchiveReader::filterByFl(const QList<SPoint> &list, int filterFl, int bottomLf)
{
    QList<SPoint> listFilt;

    int altitude = 0;
    if(!list.isEmpty())
    {
        for(const SPoint &b: list)
        {
            if((b.flightLevel >= bottomLf) && (b.flightLevel <= filterFl))
            {
                listFilt.append(b);
                altitude++;
            }
        }
    }
    Settings::setValue(SettingsKeys::LBL_ALTITUDE, altitude);
    return listFilt;
}

QList<SPoint> ArchiveReader::filterByDate(const QList<SPoint> &list)
{
    QList<SPoint> listFilt;
    QDateTime logFrom = QDateTime::fromString(Settings::value(SettingsKeys::RDR_DATE_FROM).toString(), "dd.MM.yyyy");
    QDateTime logTo = QDateTime::fromString(Settings::value(SettingsKeys::RDR_DATE_TO).toString(), "dd.MM.yyyy");
    if(!list.isEmpty())
    {
        for(const SPoint &b: list)
        {
            if(logFrom <= b.reciveTimestamp && b.reciveTimestamp <= logTo)
            {
                listFilt.append(b);
            }
        }
    }
    return listFilt;
}

QList<SPoint> ArchiveReader::filterByMaxCov(const QList<SPoint> &list)
{
    QList<SPoint> listFilt;
    int range = 0;
    double theta = 0.;
    if(!list.isEmpty())
    {
        for(const SPoint &b: list)
        {
            if(b.rho <= Settings::value(SettingsKeys::RDR_MAX_COV).toInt())
            {
                if(b.theta > theta)
                {
                    theta = b.theta;
                }

                listFilt.append(b);
                range++;
            }
        }
    }

    Settings::setValue(SettingsKeys::LBL_RANGE, range);
    return listFilt;
}

QList<SPoint> ArchiveReader::filterBySacSic(const QList<SPoint> &list)
{
    QList<SPoint> listFilt;
    int sac = 0, sic = 0;
    if(!list.isEmpty())
    {
        for(const SPoint &b: list)
        {
            if((b.sac == Settings::value(SettingsKeys::RDR_SAC) && b.sic == Settings::value(SettingsKeys::RDR_SIC)))
            {
                listFilt.append(b);
                sac++;
                sic++;
            }
        }
    }

    Settings::setValue(SettingsKeys::LBL_SACSIC, QString("%1/%2").arg(sac).arg(sic));
    return listFilt;
}

void ArchiveReader::filterByCat(const QList<SPoint> &list)
{
    int cat01 = 0, cat02 = 0, cat21 = 0, cat23 = 0, cat34 = 0, cat48 = 0;
    if(!list.isEmpty())
    {
        for(const SPoint &b: list)
        {
            if(b.category == 01)
                cat01++;
            if(b.category == 02)
                cat02++;
            if(b.category == 21)
                cat21++;
            if(b.category == 23)
                cat23++;
            if(b.category == 34)
                cat34++;
            if(b.category == 48)
                cat48++;
        }
    }
    Settings::setValue(SettingsKeys::LBL_CAT0102, QString("%1/%2").arg(cat01).arg(cat02));
    Settings::setValue(SettingsKeys::LBL_CAT2123, QString("%1/%2").arg(cat21).arg(cat23));
    Settings::setValue(SettingsKeys::LBL_CAT3448, QString("%1/%2").arg(cat34).arg(cat48));
}

QList<SPoint> ArchiveReader::filterBySquawk(const QList<SPoint> &list)
{
    QList<SPoint> listFilt;
    if(!list.isEmpty())
    {
        for(const SPoint &b: list)
        {
            if((b.flightLevel != 0) && (b.reply != 0))
            {
                listFilt.append(b);
            }
        }
    }

    return listFilt;
}

QList<SPoint> ArchiveReader::filterBy24Bit(const QList<SPoint> &list)
{
    QList<SPoint> listFilt;
    if(!list.isEmpty())
    {
        for(const SPoint &b: list)
        {
            if(b.address24bit != 0)
            {
                listFilt.append(b);
            }
        }
    }

    return listFilt;
}

QList<SPoint> ArchiveReader::filterByCallsing(const QList<SPoint> &list)
{
    QList<SPoint> listFilt;
    if(!list.isEmpty())
    {
        for(const SPoint &b: list)
        {
            if(b.callsign != 0)
            {
                listFilt.append(b);
            }
        }
    }

    return listFilt;
}

QList<SPoint> ArchiveReader::filterByEHS(const QList<SPoint> &list)
{
    QList<SPoint> listFilt;
    if(!list.isEmpty())
    {
        for(const SPoint &b: list)
        {
            if(b.s_ehs == true)
            {
                listFilt.append(b);
            }
        }
    }

    return listFilt;
}


