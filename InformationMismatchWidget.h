#ifndef INFORMATIONMISMATCHWIDGET_H
#define INFORMATIONMISMATCHWIDGET_H

#include <QWidget>
#include "trackprofile.h"
#include <QStandardItemModel>
#include "InformationMismatchSortModel.h"

namespace MismatchInfoType
{
    typedef double SquawkProb;
    typedef double CallsignProb;
    typedef double AltitudeProb;
    typedef std::tuple<TrackProfile, SquawkProb, CallsignProb, AltitudeProb> IntersectedTrack;
}

namespace Ui {
class InformationMismatchWidget;
}

class InformationMismatchWidget : public QWidget
{
    Q_OBJECT

    enum TrackType
    {
        BOTH,
        FIRST,
        SECOND
    };

public:
    explicit InformationMismatchWidget(QWidget *parent = nullptr);
    ~InformationMismatchWidget();

private slots:
    void showCustomContextMenu(QPoint position);
    void on_tb_showReport_clicked();
    void on_tb_showIntersectedTracks_clicked();
    void on_tb_showIntersectedTrack1_clicked();
    void on_tb_showIntersectedTrack2_clicked();
    void on_tb_showOriginalTrack1_clicked();
    void on_tb_showOriginalTrack2_clicked();
    void on_tb_showPointsList_clicked();
    void on_tb_deleteSelectedStrings_clicked();

private:
    void showIntersectedTracksOnMap(const QPair<MismatchInfoType::IntersectedTrack,
                                                MismatchInfoType::IntersectedTrack> *intersectedTracks, TrackType track);
    QList<int> getOriginalTracksIds(const QPair<MismatchInfoType::IntersectedTrack,
                                                MismatchInfoType::IntersectedTrack> *intersectedTracks, TrackType track);
    QPair<MismatchInfoType::IntersectedTrack, MismatchInfoType::IntersectedTrack> *getSelectedRow();
    void showIntersectedTracksPointsList(const QPair<MismatchInfoType::IntersectedTrack,
                                                     MismatchInfoType::IntersectedTrack> *intersectedTracks);

signals:
    void showOriginalTracks(QList<int> tracksIdList);

public slots:
    void addTracksIntersectionInfo(QList<QPair<MismatchInfoType::IntersectedTrack, MismatchInfoType::IntersectedTrack>> intersectedTracksList);

private:
    Ui::InformationMismatchWidget *ui;
    QList<QPair<MismatchInfoType::IntersectedTrack, MismatchInfoType::IntersectedTrack>> intersectedTracks;
    QStandardItemModel *model;
    InformationMismatchSortModel *sorter;
};

#endif // INFORMATIONMISMATCHWIDGET_H
