#include "CustomSortModel.h"

CustomSortModel::CustomSortModel(bool showServiceData, int filterColumn):
    showServiceData(showServiceData),
    categoriesToPass{"19", "20", "21", "23", "34", "48", "62"},
    filterColumn(filterColumn)
{
}

CustomSortModel::~CustomSortModel()
{
}

void CustomSortModel::setCategoriesToPass(QStringList categoriesToPass)
{
    this->categoriesToPass = categoriesToPass;
}

void CustomSortModel::setFilterColumn(int filterColumn)
{
    this->filterColumn = filterColumn;
}

void CustomSortModel::refilter(bool showServiceData)
{
    if(this->showServiceData != showServiceData)
    {
        this->showServiceData = showServiceData;
        invalidateFilter();
    }
}

bool CustomSortModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    const QAbstractItemModel* const srcModel = sourceModel();
    if(!srcModel) return false;

    QVariant leftData;
    QVariant rightData;

    if(left.column() == Columns::FileSize)
    {
        leftData  = srcModel->data(left, Qt::UserRole + 2);
        rightData = srcModel->data(right, Qt::UserRole + 2);

        if(!(leftData.isValid() && rightData.isValid()))
        {
            leftData = srcModel->data(left, Qt::DisplayRole);
            rightData = srcModel->data(right, Qt::DisplayRole);
        }
    }
    else
    {
        leftData = srcModel->data(left, Qt::DisplayRole);
        rightData = srcModel->data(right, Qt::DisplayRole);
    }

    if(leftData.type() == QVariant::Int)
    {
        return leftData.toInt() < rightData.toInt();
    }

    return leftData < rightData;
}

bool CustomSortModel::filterAcceptsRow(int source_row, const QModelIndex &source_parent) const
{
    QModelIndex index = sourceModel()->index(source_row, filterColumn, source_parent);
    QVariant value = sourceModel()->data(index, Qt::DisplayRole);

    if(showServiceData)
    {
        return true;
    }

    if(value.isValid())
    {
        QString categories = value.toString();
        for(const QString &category: categoriesToPass)
        {
            if(categories.indexOf(category) != -1)
            {
                return true;
            }
        }
    }

    return false;
}
