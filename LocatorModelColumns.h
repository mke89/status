#ifndef STATISTICSCOLUMNS_H
#define STATISTICSCOLUMNS_H

#include "LocatorUtils.h"
#include <QHash>

namespace ViewsColumns
{
    enum class Rls
    {
        HiddenService = 0,
        UvdReplay,
        Squawk,
        Addr24Bit,
        Callsign,
        Altitude,
        Distance,
        AzimuthDeviation,
        TotalLength,
        StartDate,
        PointsCount,
        PO,
        PN,
        PH,
        SkoRho,
        SkoTheta,
        pO1,
        pO4,
        pO10,
        NUCp,
        ColumnsCount
    };

    enum class Mlat
    {
        HiddenService = 0,
        Squawk,
        Addr24Bit,
        Callsign,
        Altitude,
        TotalLength,
        StartDate,
        PointsCount,
        TrackUpdateTime,
        ColumnsCount
    };

    enum class Sdpd
    {
        HiddenService = 0,
        Squawk,
        Addr24Bit,
        Callsign,
        Altitude,
        TotalLength,
        StartDate,
        PointsCount,
        ColumnsCount
    };
}

struct LocatorModelColumns
{
    LocatorModelColumns();
    virtual ~LocatorModelColumns(){}
    virtual QStringList headersList() const = 0;
    int HiddenService;
    QHash<int, QString> headers;
    QHash<int, QString> tooltips;
    QHash<int, int> columnsWidth;
};

struct RlsModelColumns: public LocatorModelColumns
{
    int UvdReplay;
    int Squawk;
    int Addr24Bit;
    int Callsign;
    int Altitude;
    int Distance;
    int AzimuthDeviation;
    int TotalLength;
    int StartDate;
    int PointsCount;
    int PO;
    int PN;
    int PH;
    int SkoRho;
    int SkoTheta;
    int pO1;
    int pO4;
    int pO10;
    int NUCp;
    int ColumnsCount;

    RlsModelColumns();
    virtual ~RlsModelColumns(){}
    virtual QStringList headersList() const;
};

struct MlatModelColumns: public LocatorModelColumns
{
    int Squawk;
    int Addr24Bit;
    int Callsign;
    int Altitude;
    int TotalLength;
    int StartDate;
    int PointsCount;
    int TrackUpdateTime;

    MlatModelColumns();
    virtual ~MlatModelColumns(){}
    virtual QStringList headersList() const;
};

struct SdpdModelColumns: public LocatorModelColumns
{
    int Squawk;
    int Addr24Bit;
    int Callsign;
    int Altitude;
    int TotalLength;
    int StartDate;
    int PointsCount;

    SdpdModelColumns();
    virtual ~SdpdModelColumns(){}
    virtual QStringList headersList() const;
};

#endif // STATISTICSCOLUMNS_H
