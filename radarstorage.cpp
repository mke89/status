#include "radarstorage.h"
#include <QStringList>
#include <QSettings>

RadarStorage::RadarStorage()
{
}

QList<SimplePoint> RadarStorage::takePointsByRadar(RadarIdentificator radarId)
{

    if(!unusedRadarPoints.contains(radarId))
    {
        return QList<SimplePoint>();
    }

    QList<SimplePoint> points = unusedRadarPoints[radarId];
    unusedRadarPoints[radarId].clear();
    return points;
}

QList<RadarIdentificator> RadarStorage::getRadarIds()
{
    return unusedRadarPoints.keys();
}

void RadarStorage::addSimplePoint(const RadarIdentificator &radarId, const SimplePoint &targetPoint)
{
    static int MAX_SIZE = 1000;
    while( unusedRadarPoints[radarId].size() > MAX_SIZE )
    {
        unusedRadarPoints[radarId].removeFirst();
    }

     unusedRadarPoints[radarId].append(targetPoint);
 }

QDataStream &operator<<(QDataStream &stream, const RadarStorage &radarStorage)
{
    stream << radarStorage.unusedRadarPoints;

    return stream;
}

QDataStream &operator>>(QDataStream &stream, RadarStorage &radarStorage)
{
    stream >> radarStorage.unusedRadarPoints;

    return stream;
}

