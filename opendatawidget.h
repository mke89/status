#ifndef OPENDATAWIDGET_H
#define OPENDATAWIDGET_H

#include <QWidget>
#include <QMap>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>
#include <QFileInfoList>
#include "trackprofile.h"
#include "AsterixFileInfo.h"
#include "gpoint.h"

struct SPoint;
class CustomSortModel;

namespace Ui {
class OpenDataWidget;
}

class OpenDataWidget : public QWidget
{
    Q_OBJECT

    typedef std::tuple<QFileInfo, QDateTime, int, QList<int>> FileInfo;

    enum DefaultParams
    {
        READ_STR_FOR_CAT  = 100,
    };

public:
    explicit OpenDataWidget(QWidget *parent = 0);
    ~OpenDataWidget();
    void showPosition(bool show=true);

    bool getOnly34Cat() const;
    void setOnly34Cat(bool value);

    QString getCurrnetFileName();

signals:
    void tracksReday(QList<TrackProfile> tracksProfiles, bool append);
    void updateTracksCounters(QHash<TrackType::TrackType, int> tracksPoints, bool append);
    void updatePointsCounters(QHash<TrackType::TrackType, int> tracksPoints, bool append);
    void sendUnusedPoints(QList<SPoint> unusedPoints, bool append);
    void readedPoints(QList<SPoint> points);
    void refilterTable(bool showServiceData);
    void tableHided(int tableHeight);

private slots:
    void on_pb_SelectDir_clicked();
    void on_cb_hideServiceDataFiles_clicked();
    void onTableViewDoubleClicked(const QModelIndex &index);
    void on_cb_hideFiles_stateChanged(int arg1);

public slots:
    void on_compute(void);
    void on_addToTracks(void);
    void on_mergeFiles(void);
    void on_changeLocatorMode(QString mode);

private:
    void addFileListToTable(const QList<FileInfo> &filesToAnalyze);
    QList<SPoint> applayCenterPoint(const QList<SPoint> &list, GPoint centerPoint);
    QHash<TrackKey, QList<SPoint>> mergeTrajectories(const QList<SPoint> &pointsList);
    void processPoint(TrackType::TrackType trackType, uint32_t trackParameter,
                      const SPoint &point, QHash<TrackKey, QList<SPoint> > &tracksContainer);
    QList<TrackKey> filterValueInKeysList(const QList<TrackKey> &keys, int value);
    QList<TrackProfile> convertToTrackProfile(QHash<TrackKey, QList<SPoint> > tracks);
    void clearTracksCounters();
    bool removeUvdFromMlat(QList<TrackProfile> &tracksProfiles, QList<SPoint> &unusedPoints);

    QString fileSizeToString(qint64 size);
    QString intListToString(QList<int> &ints);
    QString mSecsDurationToString(int mSecs);

    void extractTracks(bool append);
    QString datagramToText(const QByteArray& datagram);
    QList<FileInfo> defineFilesInfo(const QFileInfoList &files);


private:
    Ui::OpenDataWidget *ui;
    GPoint centerPoint;
    QStandardItemModel *Model;
    CustomSortModel *SortModel;
    QString analyzeDirectory;
    int rbsTracksCount;
    int uvdTracksCount;
    int adsbTracksCount;
    int smodeTracksCount;
    int mlatCount;
    int unusedPointsCount;
    int rbsPointsCount;
    int uvdPointsCount;
    int adsbPointsCount;
    int smodePointsCount;

    bool only34Cat;
};

#endif // OPENDATAWIDGET_H


