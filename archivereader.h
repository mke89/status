#ifndef ARCHIVEREADER_H
#define ARCHIVEREADER_H
#include <QDateTime>
#include <QMap>

struct SPoint;

class ArchiveReader
{
public:
    enum MERGE_TYPE
    {
        NONE                    = 0,
        MERGE_BY_SQUAWK         = 1,
        MERGE_BY_CALLSIGN       = 2,
        MERGE_BY_24BIT          = 3,
        MERGE_BY_TRACK_NUMBER   = 4
    };

    ArchiveReader();

    QList<SPoint> getData(const QString &path, const QString &filename, const QString &asterixSpecNum);
    QList<SPoint> getData(const QString &path, const QString &filename, const char* asterixSpecNum);
    QList<SPoint> getData(const QString &file, const QString &asterixSpecNum, bool withRawData, int cutFirstBytes = 0);

    QList<SPoint> getDataParallel(QString path, QString filename);

    ///
    /// \brief mergeTrjectorys
    /// \param list
    /// \return  в элементе хэша с именем UNUSED будут лежать неиспользованные точки.
    ///
    QMap<QString, QList<SPoint>> mergeTrajectories(  const QList<SPoint>& list
                                                 , MERGE_TYPE mergeType= MERGE_BY_SQUAWK
                                                 , bool useSicSac=false);

    QList<SPoint> filterByFl(const QList<SPoint> &list, int filterFl, int bottomLf);
    QList<SPoint> filterByDate(const QList<SPoint> &list);
    QList<SPoint> filterByMaxCov(const QList<SPoint> &list);
    QList<SPoint> filterBySacSic(const QList<SPoint> &list);
    void filterByCat(const QList<SPoint> &list);
    QList<SPoint> filterBySquawk(const QList<SPoint> &list);
    QList<SPoint> filterBy24Bit(const QList<SPoint> &list);
    QList<SPoint> filterByCallsing(const QList<SPoint> &list);
    QList<SPoint> filterByEHS(const QList<SPoint> &list);
};

#endif // ARCHIVEREADER_H
