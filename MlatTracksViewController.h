#ifndef MLATTRACKSVIEWCONTROLLER_H
#define MLATTRACKSVIEWCONTROLLER_H

#include "LocatorsTracksViewController.h"

class MlatTracksViewController: public LocatorsTracksViewController
{
    public:
        MlatTracksViewController() = delete;
        MlatTracksViewController(QTableView *tableView, LocatorModelColumns *locatorModelColumns,
                                 HidedColumns *hidedColumns, QStandardItemModel *model,
                                 QList<TrackProfile> *trackProfiles):
            LocatorsTracksViewController(tableView, locatorModelColumns, hidedColumns,
                                         model, trackProfiles)
        {}
        ~MlatTracksViewController(){}
        virtual void processColumnsHiding();
        virtual void addTrackToTable(const TrackProfile &trackProfile, int index);
        virtual void fillView();
        virtual void processRbsClicked();
        virtual void processUvdClicked();
        virtual void processAdsbClicked(bool flag);
};

#endif // MLATTRACKSVIEWCONTROLLER_H
