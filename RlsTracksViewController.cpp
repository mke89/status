#include "RlsTracksViewController.h"
#include "settings.h"
#include "MapStrings.h"
#include "adsbutils.h"

void RlsTracksViewController::processColumnsHiding()
{
    const RlsHidedColumns *rlsHidedColumns = dynamic_cast<const RlsHidedColumns*>(hidedColumns);
    const RlsModelColumns *rlsModelColumns = dynamic_cast<const RlsModelColumns*>(locatorModelColumns);

    bool hideUvdReplay = tableView->isColumnHidden(rlsModelColumns->UvdReplay) && rlsHidedColumns->hideUvdReplay;
    bool hideSquawk = tableView->isColumnHidden(rlsModelColumns->Squawk) && rlsHidedColumns->hideSquawk;
    bool hideAddr24Bit = tableView->isColumnHidden(rlsModelColumns->Addr24Bit) && rlsHidedColumns->hideAddr24Bit;
    bool hideCallsign = tableView->isColumnHidden(rlsModelColumns->Callsign) && rlsHidedColumns->hideCallsign;

    tableView->setColumnHidden(rlsModelColumns->UvdReplay, hideUvdReplay);
    tableView->setColumnHidden(rlsModelColumns->Squawk, hideSquawk);
    tableView->setColumnHidden(rlsModelColumns->Addr24Bit, hideAddr24Bit);
    tableView->setColumnHidden(rlsModelColumns->Callsign, hideCallsign);
}

void RlsTracksViewController::addTrackToTable(const TrackProfile &trackProfile, int index)
{
    const RlsModelColumns *rlsModelColumns = dynamic_cast<const RlsModelColumns*>(locatorModelColumns);
    RlsHidedColumns *rlsHidedColumns = dynamic_cast<RlsHidedColumns*>(hidedColumns);
    int trackNumber = index;

    int squawk = trackProfile.reply.size() > 0  ? trackProfile.reply.last() : 0;
    QString uvdReplay = trackProfile.uvdReplay > 0 ?
                QString( "A%1" ).arg(trackProfile.uvdReplay, 5, 10, QChar('0')) : QString();

    const auto direction = trackProfile.direction;

    QStandardItem *item;
    item = new QStandardItem();
    item->setData(QString::number(trackProfile.trackKey.trackNumber), Qt::DisplayRole);
    //item->setData(trackProfiles.directionToString(), Qt::ToolTipRole);
    item->setData((quint64)(&trackProfile), Qt::UserRole + 2);
    item->setData(trackProfile.directionToRussianString(), Qt::UserRole + 3);
    item->setTextAlignment(Qt::AlignCenter);

    if(direction == TrackProfile::DEPARTURE)
    {
        item->setBackground(QColor(179, 236, 255));
    }
    else if(direction == TrackProfile::ARRIVAL)
    {
        item->setBackground(QColor(194, 169, 122));
    }
    else if(direction == TrackProfile::TRANSIT)
    {
        item->setBackground(QColor(189, 189, 189));
    }
    else if(direction == TrackProfile::GROUND)
    {
        item->setBackground(QColor(219, 240, 204));
    }
    else if(direction == TrackProfile::NONE)
    {
        item->setBackground(QColor(255, 255, 255));
    }

    model->setItem(trackNumber, rlsModelColumns->HiddenService, item);

    item = new QStandardItem();
    item->setData(uvdReplay, Qt::DisplayRole);
    item->setData(trackProfile.uvdReplay, Qt::UserRole + 2);
    item->setTextAlignment(Qt::AlignCenter);
    model->setItem(trackNumber, rlsModelColumns->UvdReplay, item);
    if(trackProfile.uvdReplay != 0) rlsHidedColumns->hideUvdReplay = false;

    item = new QStandardItem();
    item->setData(QString("%1").arg(squawk, 4, 10, QChar('0')), Qt::DisplayRole);
    item->setData(squawk, Qt::UserRole + 2);
    item->setTextAlignment(Qt::AlignCenter);
    model->setItem(trackNumber, rlsModelColumns->Squawk, item);
    if(squawk != 0) rlsHidedColumns->hideSquawk = false;

    item = new QStandardItem();
    item->setData(QString::number(trackProfile.address24bit, 16), Qt::DisplayRole);
    item->setData(trackProfile.address24bit, Qt::UserRole + 2);
    model->setItem(trackNumber, rlsModelColumns->Addr24Bit, item);
    if(trackProfile.address24bit != 0) rlsHidedColumns->hideAddr24Bit = false;

    item = new QStandardItem();
    item->setData(trackProfile.callsign, Qt::DisplayRole);
    model->setItem(trackNumber, rlsModelColumns->Callsign, item);
    if(!trackProfile.callsign.isEmpty()) rlsHidedColumns->hideCallsign = false;

    item = new QStandardItem();
    double verticalSpeedMS = Settings::value(SettingsKeys::AIRCRAFT_AVG_VERTICAL_SPEED).toDouble();
    const QList<double> &altitudes = Statistics::MismatchInfo::filterAltitudeGlitches(trackProfile.points, verticalSpeedMS);
    double minAltitude = trackProfile.minAltitude(altitudes);
    double maxAltitude = trackProfile.maxAltitude(altitudes);
    item->setData(trackProfile.minMaxAltitudeToString(minAltitude, maxAltitude), Qt::DisplayRole);
    item->setData(minAltitude, Qt::UserRole + 2);
    item->setData(maxAltitude, Qt::UserRole + 3);
    item->setTextAlignment(Qt::AlignCenter);
    model->setItem(trackNumber, rlsModelColumns->Altitude, item);

    item = new QStandardItem();
    item->setData(trackProfile.minMaxDistanceToString(), Qt::DisplayRole);
    item->setData(trackProfile.minDistanceKm(), Qt::UserRole + 2);
    item->setData(trackProfile.maxDistanceKm(), Qt::UserRole + 3);
    item->setTextAlignment(Qt::AlignCenter);
    model->setItem(trackNumber, rlsModelColumns->Distance, item);

    item = new QStandardItem();
    item->setData(trackProfile.azimuthDeviationToString(), Qt::DisplayRole);
    item->setData(trackProfile.azimuthMiddleVal, Qt::UserRole + 2);
    item->setData(trackProfile.azimuthDeviation, Qt::UserRole + 3);
    item->setTextAlignment(Qt::AlignCenter);
    model->setItem(trackNumber, rlsModelColumns->AzimuthDeviation, item);

    item = new QStandardItem();
    item->setData(QString::number(trackProfile.lengthKm), Qt::DisplayRole);
    item->setData(trackProfile.lengthKm, Qt::UserRole + 2);
    model->setItem(trackNumber, rlsModelColumns->TotalLength, item);

    item = new QStandardItem();
    item->setData(trackProfile.startDate.toString("dd.MM.yyyy hh:mm:ss"), Qt::DisplayRole);
    item->setData(trackProfile.startDate.toMSecsSinceEpoch()/1000, Qt::UserRole + 2);
    model->setItem(trackNumber, rlsModelColumns->StartDate, item);

    item = new QStandardItem();
    QString pointsCount = QString("%1 / %2").arg(trackProfile.points.size()).
                                             arg(trackProfile.reconstructedTrajectory.size());
    //item->setData( QString::number(trackProfile.points.size()), Qt::DisplayRole);
    item->setData( pointsCount, Qt::DisplayRole);
    item->setData( trackProfile.points.size(), Qt::UserRole + 2);
    item->setTextAlignment(Qt::AlignCenter);
    model->setItem(trackNumber, rlsModelColumns->PointsCount, item);

    item = new QStandardItem();
    QString pOStr = trackProfile.pO > 1 ? "NaN" : MapGeometry::fromDouble(trackProfile.pO, 3);
    item->setData( pOStr, Qt::DisplayRole);
    item->setData( trackProfile.pO, Qt::UserRole + 2);
    model->setItem(trackNumber, rlsModelColumns->PO, item);

    item = new QStandardItem();
    QString pNStr = trackProfile.pN > 1 ? "NaN" : MapGeometry::fromDouble(trackProfile.pN, 3);
    item->setData( pNStr, Qt::DisplayRole);
    item->setData( trackProfile.pN, Qt::UserRole + 2);
    model->setItem(trackNumber, rlsModelColumns->PN, item);

    item = new QStandardItem();
    QString pHStr = trackProfile.pH > 1 ? "NaN" : MapGeometry::fromDouble(trackProfile.pH, 3);
    item->setData(pHStr, Qt::DisplayRole);
    item->setData(trackProfile.pH, Qt::UserRole + 2);
    model->setItem(trackNumber, rlsModelColumns->PH, item);

    item = new QStandardItem();
    QString skoRhoStr;
    if(trackProfile.skoRho == -1)
        skoRhoStr = "";
    else if((trackProfile.skoRho * converter::NMILE_TO_METRES) > 5000)
        skoRhoStr = "NaN";
    else
        skoRhoStr = MapGeometry::fromDouble(trackProfile.skoRho * converter::NMILE_TO_METRES, 2);

    item->setData(skoRhoStr, Qt::DisplayRole);
    item->setData(trackProfile.skoRho, Qt::UserRole + 2);
    model->setItem(trackNumber, rlsModelColumns->SkoRho, item);

    item = new QStandardItem();
    QString skoThetaStr;
    if(trackProfile.skoTheta == -1)
        skoThetaStr = "";
    else if(trackProfile.skoTheta > 10)
        skoThetaStr = "NaN";
    else
        skoThetaStr = MapGeometry::fromDouble(trackProfile.skoTheta * converter::DEGREES_TO_MIN,  3);

    item->setData(skoThetaStr, Qt::DisplayRole);
    item->setData(trackProfile.skoTheta, Qt::UserRole + 2);
    model->setItem(trackNumber, rlsModelColumns->SkoTheta , item);

    if(trackProfile.trackKey.type == TrackType::ADSB)
    {
        item = new QStandardItem();
        QString pO1Str = trackProfile.pO1 > 1 ? "NaN" : MapGeometry::fromDouble(trackProfile.pO1, 3);
        item->setData(pO1Str , Qt::DisplayRole);
        item->setData(trackProfile.pO1, Qt::UserRole + 2);
        model->setItem(trackNumber, rlsModelColumns->pO1, item);

        item = new QStandardItem();
        QString pO4Str = trackProfile.pO4 > 1 ? "NaN" : MapGeometry::fromDouble(trackProfile.pO4, 3);
        item->setData(pO4Str, Qt::DisplayRole);
        item->setData(trackProfile.pO4, Qt::UserRole + 2);
        model->setItem(trackNumber, rlsModelColumns->pO4, item);

        item = new QStandardItem();
        QString pO10Str = trackProfile.pO10 > 1 ? "NaN" : MapGeometry::fromDouble(trackProfile.pO10, 3);
        item->setData(pO10Str, Qt::DisplayRole);
        item->setData(trackProfile.pO10, Qt::UserRole + 2);
        model->setItem(trackNumber, rlsModelColumns->pO10, item);

        MinMaxNUCp minMaxNUCp = trackProfile.getMinMaxNUCp();
        item = new QStandardItem();
        item->setData(QString("%1 / %2").arg(minMaxNUCp.first).arg(minMaxNUCp.second), Qt::DisplayRole);
        item->setData(QVariant::fromValue(minMaxNUCp), Qt::UserRole + 2);
        model->setItem(trackNumber, rlsModelColumns->NUCp, item);
   }
}

void RlsTracksViewController::fillView()
{
    if((!model) || (!tableView) || (!trackProfiles))
    {
        return;
    }

    const RlsModelColumns *rlsModelColumns = dynamic_cast<const RlsModelColumns*>(locatorModelColumns);
    const QStringList &tableHeaders = rlsModelColumns->headersList();
    model->clear();
    model->setRowCount(0);
    model->setColumnCount(tableHeaders.size());
    model->setHorizontalHeaderLabels(tableHeaders);

    for(int column = 0; column < (int)ViewsColumns::Rls::ColumnsCount; column++)
    {
        model->horizontalHeaderItem(column)->setToolTip(rlsModelColumns->tooltips[column]);
        tableView->setColumnWidth(column, rlsModelColumns->columnsWidth[column]);
    }

    tableView->setColumnHidden(rlsModelColumns->pO1,  true);
    tableView->setColumnHidden(rlsModelColumns->pO4,  true);
    tableView->setColumnHidden(rlsModelColumns->pO10, true);
    tableView->setColumnHidden(rlsModelColumns->NUCp, true);

    for(int i = 0; i < trackProfiles->size(); i++)
    {
        addTrackToTable((*trackProfiles)[i], i);
    }

    RlsHidedColumns *rlsHidedColumns = dynamic_cast<RlsHidedColumns*>(hidedColumns);
    tableView->setColumnHidden(rlsModelColumns->UvdReplay, rlsHidedColumns->hideUvdReplay);
    tableView->setColumnHidden(rlsModelColumns->Squawk, rlsHidedColumns->hideSquawk);
    tableView->setColumnHidden(rlsModelColumns->Addr24Bit, rlsHidedColumns->hideAddr24Bit);
    tableView->setColumnHidden(rlsModelColumns->Callsign, rlsHidedColumns->hideCallsign);

    tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
}

void RlsTracksViewController::processRbsClicked()
{
    const RlsModelColumns *rlsModelColumns = dynamic_cast<const RlsModelColumns*>(locatorModelColumns);
    tableView->setColumnHidden(rlsModelColumns->PO, false);
}

void RlsTracksViewController::processUvdClicked()
{
    const RlsModelColumns *rlsModelColumns = dynamic_cast<const RlsModelColumns*>(locatorModelColumns);
    tableView->setColumnHidden(rlsModelColumns->PO,  false);
}

void RlsTracksViewController::processAdsbClicked(bool flag)
{
    const RlsModelColumns *rlsModelColumns = dynamic_cast<const RlsModelColumns*>(locatorModelColumns);

    tableView->setColumnHidden(rlsModelColumns->pO1, !flag);
    tableView->setColumnHidden(rlsModelColumns->pO4, !flag);
    tableView->setColumnHidden(rlsModelColumns->pO10,!flag);
    tableView->setColumnHidden(rlsModelColumns->NUCp,!flag);

    // тут все правильно, скрываем расчет при азн кнопке
    tableView->setColumnHidden(rlsModelColumns->PO,  flag);
}

