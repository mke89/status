#include "MlatStatistics.h"
#include "FlightZoneUtils.h"
#include "settings.h"

MlatStatistics::MlatStatistics()
{

}

int MlatStatistics::mlatUpdateTimeMsecs(int zoneType)
{
    int flightZoneType = FlightZoneUtils::filterZoneIndex(zoneType);
    if(flightZoneType == FlightZoneUtils::ZoneType::Undefined)
    {
        return 0;
    }
    else if(flightZoneType == FlightZoneUtils::ZoneType::AIRDROME)
    {
        return MlatStatistics::DefaultParams::AIRDROME_ZONE_UPDATE_TIME_SEC * 1000;
    }
    else if(flightZoneType == FlightZoneUtils::ZoneType::TRACK)
    {
        return MlatStatistics::DefaultParams::TRACK_ZONE_UPDATE_TIME_SEC * 1000;
    }

    return 0;
}

int MlatStatistics::mlatDeltaUpdateTimeMsecs(int zoneType)
{
    int flightZoneType = FlightZoneUtils::filterZoneIndex(zoneType);
    if(flightZoneType == FlightZoneUtils::ZoneType::Undefined)
    {
        return 0;
    }
    else if(flightZoneType == FlightZoneUtils::ZoneType::AIRDROME)
    {
        return Settings::value(SettingsKeys::MLAT_AIRDROME_UPDATE_DELTA_MS, 100).toInt() * 1000;
    }
    else if(flightZoneType == FlightZoneUtils::ZoneType::TRACK)
    {
        return Settings::value(SettingsKeys::MLAT_TRACK_UPDATE_DELTA_MS, 100).toDouble() * 1000;
    }

    return 0;
}

double MlatStatistics::calcPU(const TrackProfile &tracksProfile, int updateTimeMs, int deltaUpdateTime)
{
    int n = tracksProfile.points.size();
    int k = 0;
    int maxUpdateIntervalMs = updateTimeMs + deltaUpdateTime;
    for(int currPointNum = 0; currPointNum < (n - 1); currPointNum++)
    {
        const SPoint &currPoint = tracksProfile.points[currPointNum];
        const SPoint &nextPoint = tracksProfile.points[currPointNum + 1];
        int msecsBetweenPoints = currPoint.reciveTimestamp.msecsTo(nextPoint.reciveTimestamp);
        if(msecsBetweenPoints <= maxUpdateIntervalMs)
        {
            k++;
        }
    }

    double PU = (double)k / (double)(n - 1);
    return PU;
}

