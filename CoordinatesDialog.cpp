#include "CoordinatesDialog.h"
#include "ui_CoordinatesDialog.h"
#include "settings.h"
#include "gpoint.h"
#include <QDebug>
#include <QMessageBox>

namespace Columns
{
    enum Columns
    {
        Description = 0,
        Coordinates,
        RotationPeriod,
        LocatorAltitude,
        ColumnsCount
    };
}

CoordinatesDialog::~CoordinatesDialog()
{
    delete ui;
}

CoordinatesDialog::CoordinatesDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CoordinatesDialog)
{
    ui->setupUi(this);

    QStringList headersList;
    headersList << "Описание" << "Координаты\n(DDMMSSE)" << "Период\nвращения (мсек)" << "Высота\nлокатора (м)";
    ui->tableWidget->setHorizontalHeaderLabels(headersList);

    ui->tableWidget->setColumnWidth(Columns::Description, 140);
    ui->tableWidget->setColumnWidth(Columns::Coordinates, 160);
    ui->tableWidget->setColumnWidth(Columns::RotationPeriod, 160);
    ui->tableWidget->setColumnWidth(Columns::LocatorAltitude, 120);


    QString centerPointsString = Settings::instance().value(SettingsKeys::CENTER_POINTS_LIST).toString();
    QList<CenterPointInfo> centerPoints = getCenterPoints(centerPointsString);

    for(int row = 0; row < centerPoints.size(); row++)
    {
        addTableRow(ui->tableWidget, centerPoints[row]);
    }

    highLightCurrentPoint();
}

void CoordinatesDialog::highLightCurrentPoint()
{
    QString locatorPos = Settings::instance().value(SettingsKeys::LOCATOR_POSITION).toString();
    int selectionPos = currentPointRow(locatorPos);
    if(selectionPos >= 0)
    {
        ui->tableWidget->selectRow(selectionPos);
    }
}

void CoordinatesDialog::addTableRow(QTableWidget *table, CenterPointInfo point)
{
    table->insertRow(table->rowCount());

    QTableWidgetItem *item = new QTableWidgetItem(point.description);
    table->setItem(table->rowCount() - 1, Columns::Description, item);

    item = new QTableWidgetItem(point.coordinates);
    table->setItem(table->rowCount() - 1, Columns::Coordinates, item);

    item = new QTableWidgetItem(point.rotationPeriod);
    table->setItem(table->rowCount() - 1, Columns::RotationPeriod, item);

    item = new QTableWidgetItem(point.altitude);
    table->setItem(table->rowCount() - 1, Columns::LocatorAltitude, item);
}

void CoordinatesDialog::on_pb_close_clicked()
{
    QString centerPoints = serilizeTableData(ui->tableWidget);

    if(centerPoints.size() == 0)
    {
        QMessageBox::information(this, "Внимание!", "Точки стояния отсутствуют, либо указаны не верно!");
        return;
    }

    Settings::instance().setValue(SettingsKeys::CENTER_POINTS_LIST, centerPoints);
    accept();
}

QList<CenterPointInfo> CoordinatesDialog::getCenterPoints(QString pointsString)
{
    QList<CenterPointInfo> centerPoints;
    QStringList pointsList = pointsString.split(',');

    for(QString point: pointsList)
    {
        if(point.size() == 0) continue;

        QStringList pointParams = point.split(':');
        if(pointParams.size() != 4) continue;

        if(!correctCoord(pointParams[1])) continue;

        centerPoints << CenterPointInfo(pointParams);
    }

    QString currentCenterPoint = Settings::instance().value(SettingsKeys::LOCATOR_POSITION).toString();

    if(!coordPresents(centerPoints, currentCenterPoint))
    {
        CenterPointInfo currentPoint;
        currentPoint.description = Settings::instance().value(SettingsKeys::LOCATOR_POSITION_DESCRIPTION).toString();
        currentPoint.coordinates = currentCenterPoint;
        currentPoint.rotationPeriod = Settings::instance().value(SettingsKeys::LOCATOR_ROTATION_PERIOD).toString();
        currentPoint.altitude = Settings::instance().value(SettingsKeys::LOCATOR_ALTITUDE).toString();
        centerPoints << currentPoint;
    }

    return centerPoints;
}

QString CoordinatesDialog::serilizeTableData(QTableWidget *table)
{
    QString result;

    for(int i = 0; i < table->rowCount(); i++)
    {
        QString description = table->item(i, Columns::Description)->text();
        if(description.size() == 0) continue;

        QString coordStr = table->item(i, Columns::Coordinates)->text();
        if(!correctCoord(coordStr)) continue;

        QString rotationPeriod = table->item(i, Columns::RotationPeriod)->text();
        if(rotationPeriod.size() == 0) continue;

        QString altitude = table->item(i, Columns::LocatorAltitude)->text();
        if(!altitude.size()) altitude = "0";

        result += (table->item(i, 0)->text() + ":" + coordStr + ":" + rotationPeriod + ":" + altitude + ",");
    }

    return result;
}

bool CoordinatesDialog::correctCoord(QString coord)
{
    GPoint pointCoords(coord);
    if((!pointCoords.isLatLonValid()) || pointCoords.isNull())
    {
        return false;
    }

    return true;
}

bool CoordinatesDialog::coordPresents(QList<CenterPointInfo> coordsList, QString coord)
{
    for(int i = 0; i < coordsList.size(); i++)
    {
        if(coordsList[i].coordinates == coord)
        {
            return true;
        }
    }

    return false;
}

int CoordinatesDialog::currentPointRow(QString coord)
{
    int tableRows = ui->tableWidget->rowCount();

    for(int i = 0; i < tableRows; i++)
    {
        QString rowCoord = ui->tableWidget->item(i, Columns::Coordinates)->data(Qt::DisplayRole).toString();
        if(coord == rowCoord)
        {
            return i;
        }
    }

    return -1;
}

bool CoordinatesDialog::coordinatePresentsinTable(QString coord)
{
    int tableRows = ui->tableWidget->rowCount();

    for(int i = 0; i < tableRows; i++)
    {
        QString rowCoord = ui->tableWidget->item(i, Columns::Coordinates)->data(Qt::DisplayRole).toString();
        if(coord == rowCoord)
        {
            return true;
        }
    }

    return false;
}

void CoordinatesDialog::on_pb_addNew_clicked()
{
    CenterPointInfo newPoint;
    newPoint.description = ui->le_description->text();
    if(newPoint.description.size() == 0)
    {
        QMessageBox::information(this, "Внимание!", "Описание не указано!");
        return;
    }

    newPoint.coordinates = ui->le_coordinates->text();
    if(newPoint.coordinates.size() == 0)
    {
        QMessageBox::information(this, "Внимание!", "Координаты не указаны!");
        return;
    }

    if(!correctCoord(newPoint.coordinates))
    {
        QMessageBox::information(this, "Внимание!", "Координаты не корректны!");
        return;
    }

    if(coordinatePresentsinTable(newPoint.coordinates))
    {
        QMessageBox::information(this, "Внимание!", "Точка с аналогичной координатой уже задана!");
        return;
    }

    newPoint.rotationPeriod = ui->le_rotationPeriod->text();
    bool ok;
    int period = newPoint.rotationPeriod.toInt(&ok, 10);
    if(!ok)
    {
        QMessageBox::information(this, "Внимание!", "Некорректное значение периода оборота локатора!");
        return;
    }

    if(period == 0)
    {
        QMessageBox::information(this, "Внимание!", "Период оборота локатора не может быть нулевым!");
        return;
    }

    newPoint.altitude = ui->le_locatorAltitude->text();

    if(newPoint.altitude.size() == 0)
    {
        newPoint.altitude = "0";
    }

    int altitude = newPoint.altitude.toInt(&ok, 10);
    if(!ok)
    {
        QMessageBox::information(this, "Внимание!", "Некорректное значение высоты точки стояния локатора!");
        return;
    }

    addTableRow(ui->tableWidget, newPoint);
}

static bool intLessThan(const int &i1, const int &i2)
{
    return i1 < i2;
}

static bool moreLessThan(const int &i1, const int &i2)
{
    return i1 > i2;
}

QList<int> CoordinatesDialog::selectedRows(QList<QTableWidgetItem *> &selectedItems)
{
    QList<int> result;

    for(int i = 0; i < selectedItems.size(); i++)
    {
        int currentRow = ui->tableWidget->row(selectedItems[i]);

        if(!result.contains(currentRow))
        {
            result << currentRow;
        }
    }

    std::sort(result.begin(), result.end(), intLessThan);

    return result;
}

void CoordinatesDialog::on_pb_Remove_clicked()
{
    QString currentCenterPoint = Settings::instance().value(SettingsKeys::LOCATOR_POSITION).toString();
    QList<QTableWidgetItem *> selectedItems = ui->tableWidget->selectedItems();

    QList<int> rows = selectedRows(selectedItems);

    if(rows.size() == 1)
    {
        QString coord = ui->tableWidget->item(rows[0], Columns::Coordinates)->data(Qt::DisplayRole).toString();

        if(coord == currentCenterPoint)
        {
            QMessageBox::information(this, "Внимание!", "Текущая установленная точка не может быть удалена!");
        }
    }

    std::sort(rows.begin(), rows.end(), moreLessThan);
    for(int i = 0; i < rows.size(); i++)
    {
        int currentRow = rows[i];
        QString coord = ui->tableWidget->item(currentRow, Columns::Coordinates)->data(Qt::DisplayRole).toString();

        if(coord != currentCenterPoint)
        {
            ui->tableWidget->removeRow(currentRow);
        }
    }

    highLightCurrentPoint();
}

void CoordinatesDialog::on_pb_setCurrent_clicked()
{
    QList<QTableWidgetItem *> selectedItems = ui->tableWidget->selectedItems();

    if(selectedItems.size() > 1)
    {
        QMessageBox::information(this, "Внимание!", "В качестве текущей точки будет установлена первая з списка!");
    }

    if(selectedItems.size() > 0)
    {
        int selectedRow = ui->tableWidget->row(selectedItems[0]);

        QString descroption = ui->tableWidget->item(selectedRow, Columns::Description)->data(Qt::DisplayRole).toString();
        QString coord = ui->tableWidget->item(selectedRow, Columns::Coordinates)->data(Qt::DisplayRole).toString();
        QString rotaionPeriodString = ui->tableWidget->item(selectedRow, Columns::RotationPeriod)->data(Qt::DisplayRole).toString();
        QString locatorAltitudeString = ui->tableWidget->item(selectedRow, Columns::LocatorAltitude)->data(Qt::DisplayRole).toString();

        bool rotaionPeriodOk = false;
        bool locatorAltitudeOk = false;

        int rotaionPeriod = rotaionPeriodString.toInt(&rotaionPeriodOk, 10);
        int locatorAltitude = locatorAltitudeString.toInt(&locatorAltitudeOk, 10);

        if(!rotaionPeriodOk)
        {
            QMessageBox::information(this, "Внимание!", "Некорректное значение периода оборота локатора!");
            return;
        }

        if(!locatorAltitudeOk)
        {
            locatorAltitude = 0;
        }

        Settings::instance().setValue(SettingsKeys::LOCATOR_POSITION, coord);
        Settings::instance().setValue(SettingsKeys::LOCATOR_POSITION_DESCRIPTION, descroption);
        Settings::instance().setValue(SettingsKeys::LOCATOR_ROTATION_PERIOD, rotaionPeriod);
        Settings::instance().setValue(SettingsKeys::LOCATOR_ALTITUDE, locatorAltitude);
        highLightCurrentPoint();
    }
}
