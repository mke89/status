#ifndef LOCATORSTRACKSVIEWCONTROLLER_H
#define LOCATORSTRACKSVIEWCONTROLLER_H

#include <QModelIndex>
#include <QStandardItemModel>
#include <QTableView>
#include "LocatorModelColumns.h"
#include "trackprofile.h"

struct HidedColumns
{
    HidedColumns(){}
    virtual ~HidedColumns(){}
};

struct RlsHidedColumns: public HidedColumns
{
    bool hideUvdReplay;
    bool hideSquawk;
    bool hideAddr24Bit;
    bool hideCallsign;

    RlsHidedColumns(): hideUvdReplay(true), hideSquawk(true),
        hideAddr24Bit(true), hideCallsign(true)
    {}

    virtual ~RlsHidedColumns(){}
};

struct MlatHidedColumns: public HidedColumns
{
    bool hideSquawk;
    bool hideAddr24Bit;
    bool hideCallsign;

    MlatHidedColumns(): hideSquawk(true),
        hideAddr24Bit(true), hideCallsign(true)
    {}
    virtual ~MlatHidedColumns(){}
};

struct SdpdHidedColumns: public HidedColumns
{
    bool hideSquawk;
    bool hideAddr24Bit;
    bool hideCallsign;

    SdpdHidedColumns(): hideSquawk(true),
        hideAddr24Bit(true), hideCallsign(true)
    {}
    virtual ~SdpdHidedColumns(){}
};

class LocatorsTracksViewController
{
    protected:
        QTableView *tableView;
        LocatorModelColumns *locatorModelColumns;
        HidedColumns *hidedColumns;
        QStandardItemModel *model;
        QList<TrackProfile> *trackProfiles;
    public:
        LocatorsTracksViewController() = delete;
        LocatorsTracksViewController(QTableView *tableView, LocatorModelColumns *locatorModelColumns,
                                     HidedColumns *hidedColumns, QStandardItemModel *model,
                                     QList<TrackProfile> *trackProfiles):
            tableView(tableView),
            locatorModelColumns(locatorModelColumns),
            hidedColumns(hidedColumns),
            model(model),
            trackProfiles(trackProfiles)
        {}
        virtual ~LocatorsTracksViewController(){}
        QModelIndexList getSelectedRowsFromTableView() const;
        virtual void processColumnsHiding() = 0;
        virtual void addTrackToTable(const TrackProfile &trackProfile, int index) = 0;
        virtual void fillView() = 0;
        virtual void processRbsClicked() = 0;
        virtual void processUvdClicked() = 0;
        virtual void processAdsbClicked(bool flag) = 0;

};


#endif // LOCATORSTRACKSVIEWCONTROLLER_H
