#include "spoint.h"
#include "MapConstants.h"
//#include "MapPoint.h"
bool SPoint::isValid() const
{
    return sic!=0 && sac !=0 && rho > 0 && theta > -361 && theta <361;
}

bool SPoint::isRadarReport() const
{
    return !radareport.isNull();
}

bool SPoint::isDateTimeValid() const
{
    return !MapGeometry::isNan(dateTime) || dateTime > 0.01;
}

bool SPoint::QualityIdentificators::hasNACp() const
{
    return NACp != 0;
}

bool SPoint::QualityIdentificators::hasNUCp() const
{
    return NUCp != 0;
}

bool SPoint::QualityIdentificators::hasSIL() const
{
    return SIL != 0;
}

bool SPoint::QualityIdentificators::hasPIC() const
{
    return PIC != 0;
}

bool SPoint::QualityIdentificators::noQualityBits() const
{
    return !hasNACp() && !hasNUCp() && !hasSIL() && !hasPIC();
}

QString SPoint::QualityIdentificators::toString() const
{
    QString result=QString("NUCp=%1 NACp=%2 SIL=%3 PIC=%4").arg(NUCp).arg(NACp).arg(SIL).arg(PIC);
    return result;
}

QString SPoint::pointClassToString() const
{
    switch(pointClass)
    {
        case PointClass::FAKE:
        {
            return QString("Ложная");
        }
        case PointClass::FILTERED:
        {
            return QString("Отфильтрованная");
        }
        case PointClass::UNSTABLE:
        {
            return QString("Нестабильная");
        }
        case PointClass::REFLECTED:
        {
            return QString("Переотраженная");
        }
        case PointClass::UNDEFINED:
        {
            return QString("Не определено");
        }
    }

    return QString("Не определено");
}

SPoint::SPoint() :
    trackId(0)
  , category(0)
  , sac(0)
  , sic(0)
  , rho(MapGeometry::nan)
  , theta(MapGeometry::nan)
  , flightLevel(MapGeometry::nan)
  , altitude(MapGeometry::nan)
  , reply(0)
  , replayValideted(false)
  , uvdReplay(0)
  , address24bit(0)
  , latitude(MapGeometry::nan)
  , longitude(MapGeometry::nan)
  , dateTime(MapGeometry::nan)
  , s_ehs(false)
  , gbs(false)
  , pointClass(PointClass::UNDEFINED)
{}

SPoint::RadarReport::RadarReport()
{

}
