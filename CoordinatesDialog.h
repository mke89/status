#ifndef COORDINATESDIALOG_H
#define COORDINATESDIALOG_H

#include <QDialog>
#include <QTableWidget>

namespace Ui {
class CoordinatesDialog;
}

struct CenterPointInfo
{
    QString description;
    QString coordinates;
    QString rotationPeriod;
    QString altitude;

    CenterPointInfo(){}

    CenterPointInfo(QStringList &pointParams)
    {
        if(pointParams.size() == 4)
        {
            description = pointParams[0];
            coordinates = pointParams[1];
            rotationPeriod = pointParams[2];
            altitude = pointParams[3];
        }
    }
};

class CoordinatesDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CoordinatesDialog(QWidget *parent = 0);
    ~CoordinatesDialog();

private slots:
    void on_pb_close_clicked();
    void on_pb_addNew_clicked();
    void on_pb_Remove_clicked();
    void on_pb_setCurrent_clicked();

private:
    QList<CenterPointInfo> getCenterPoints(QString pointsString);
    QString serilizeTableData(QTableWidget *table);
    bool correctCoord(QString coord);
    void addTableRow(QTableWidget *table, CenterPointInfo point);
    bool coordPresents(QList<CenterPointInfo> coordsList, QString coord);
    bool coordinatePresentsinTable(QString coord);
    int currentPointRow(QString coord);
    void highLightCurrentPoint();
    QList<int> selectedRows(QList<QTableWidgetItem *> &selectedItems);

private:
    Ui::CoordinatesDialog *ui;
};

#endif // COORDINATESDIALOG_H
