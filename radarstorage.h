#ifndef RadarStorage_H
#define RadarStorage_H

#include <QList>
#include <QHash>

#include "structs.h"

class RadarStorage
{

public:
    RadarStorage();
    QList<SimplePoint> takePointsByRadar(RadarIdentificator radarId);
    QList<RadarIdentificator> getRadarIds();
    void addSimplePoint(const RadarIdentificator &radarId , const SimplePoint& targetPoint);

    friend QDataStream &operator<<(QDataStream &stream, const RadarStorage &radarStorage);
    friend QDataStream &operator>>(QDataStream &stream, RadarStorage &radarStorage);

private:    

    QHash< RadarIdentificator, QList<SimplePoint> > unusedRadarPoints;
};

#endif
