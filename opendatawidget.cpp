#include "opendatawidget.h"
#include "ui_opendatawidget.h"
#include <QInputDialog>
#include "settings.h"
#include <QFileDialog>
#include <QDir>
#include <QDebug>
#include "gcalc.h"
#include "adsbutils.h"
#include <QMessageBox>
#include "CoordinatesDialog.h"
#include "CommonAsterixReader.h"
#include "highlevelfunctions.h"
#include "MapConstants.h"
#include <iostream>
#include <fstream>
#include "spoint.h"
#include "CustomSortModel.h"
#include "archivereader.h"
#include "TrackIdKeeper.h"
#include "TrackBuilder.h"
#include "LocatorUtils.h"
#include "MlatSettingsDialog.h"


namespace FileSize
{
    enum FileSize
    {
        KILO_BYTE   = 1000,
        MEGA_BYTE   = 1000 * 1000
    };
}

OpenDataWidget::OpenDataWidget(QWidget *parent) :
      QWidget(parent)
    , ui(new Ui::OpenDataWidget)
    , centerPoint(Settings::instance().value(SettingsKeys::LOCATOR_POSITION).toString())
    , rbsTracksCount(0)
    , uvdTracksCount(0)
    , adsbTracksCount(0)
    , smodeTracksCount(0)
    , mlatCount(0)
    , unusedPointsCount(0)
    , rbsPointsCount(0)
    , uvdPointsCount(0)
    , adsbPointsCount(0)
    , smodePointsCount(0)
    , only34Cat(false)
{
    ui->setupUi(this);
    showPosition();

    Model = new QStandardItemModel();
    Model->setHorizontalHeaderLabels(QStringList{"Дата", "Время", "Длительность", "Категории", "Размер", "Файл"});

    SortModel = new CustomSortModel(!ui->cb_hideServiceDataFiles->isChecked(), Columns::Categories);
    SortModel->setSourceModel(Model);
    SortModel->setDynamicSortFilter(false);

    connect(this, SIGNAL(refilterTable(bool)), SortModel, SLOT(refilter(bool)));
    connect(ui->tableView, SIGNAL(doubleClicked(const QModelIndex &)),
            this, SLOT(onTableViewDoubleClicked(const QModelIndex &)));

    ui->tableView->setModel(SortModel);
    ui->tableView->setSortingEnabled(true);
    ui->tableView->horizontalHeader()->setSortIndicator(Columns::FileName, Qt::AscendingOrder);
    ui->tableView->setColumnWidth(Columns::Date, 200);
    ui->tableView->setColumnWidth(Columns::Time, 200);
    ui->tableView->setColumnWidth(Columns::Duration, 200);
    ui->tableView->setColumnWidth(Columns::Categories, 200);
    ui->tableView->setColumnWidth(Columns::FileSize, 200);
    ui->tableView->setAlternatingRowColors(true);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
}

void OpenDataWidget::onTableViewDoubleClicked(const QModelIndex &index)
{
    Q_UNUSED(index);
    extractTracks(false);
}

OpenDataWidget::~OpenDataWidget()
{
    delete SortModel;
    delete Model;
    delete ui;
}

void OpenDataWidget::showPosition(bool show)
{
    if(!show)
    {
        ui->lb_positionName->setVisible(false);
        ui->lb_positionLat->setVisible(false);
        ui->lb_positionLon->setVisible(false);
        ui->lb_rotationPeriod->setVisible(false);
        ui->lb_locatorAltitude->setVisible(false);
    }
    else
    {
        ui->lb_positionName->setVisible(true);
        ui->lb_positionLat->setVisible(true);
        ui->lb_positionLon->setVisible(true);
        ui->lb_rotationPeriod->setVisible(true);
        ui->lb_locatorAltitude->setVisible(true);
    }
    //ui->lb_positionLat->setText("LAT: " + GPoint::formatLatitude(centerPoint.latitude(), "DDMMSSE"));
    //ui->lb_positionLon->setText("LON: " + GPoint::formatLongitude(centerPoint.longitude(), "DDMMSSE"));
    centerPoint = GPoint(Settings::instance().value(SettingsKeys::LOCATOR_POSITION).toString());
    ui->lb_positionName->setText(Settings::instance().value(SettingsKeys::LOCATOR_POSITION_DESCRIPTION).toString());

    ui->lb_positionLat->setText("Широта:  " + QString::number(centerPoint.latitude()));
    ui->lb_positionLon->setText("Долгота:  " + QString::number(centerPoint.longitude()));

    QString rotationPeriod = Settings::instance().value(SettingsKeys::LOCATOR_ROTATION_PERIOD).toString();
    ui->lb_rotationPeriod->setText("Период вращения:  " + rotationPeriod + " мс");

    QString locatorAltitude = Settings::instance().value(SettingsKeys::LOCATOR_ALTITUDE).toString();
    ui->lb_locatorAltitude->setText("Высота локатора:  " + locatorAltitude + " м");
}

void OpenDataWidget::on_pb_SelectDir_clicked()
{
    QString inputDir = Settings::instance().value(SettingsKeys::LAST_INPUT_DIRECTORY).toString();
    QString analyzeDirPath = QFileDialog::getExistingDirectory(this, tr("Укажите папку с файлами для анализа"), inputDir,
                                                               QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);

    if(analyzeDirPath.size() > 0)
    {
        Model->removeRows(0, Model->rowCount());

        Settings::instance().setValue(SettingsKeys::LAST_INPUT_DIRECTORY, analyzeDirPath);
        ui->le_analyzeDir->setText(Settings::instance().value(SettingsKeys::LAST_INPUT_DIRECTORY).toString());

        QDir analyzeDir(analyzeDirPath);
        analyzeDirectory = analyzeDirPath;
        QStringList fileFilter;
        fileFilter << "*.dat" << "*.vnr" << "*.pcap";

        QFileInfoList filesToAnalyze = analyzeDir.entryInfoList(fileFilter, QDir::Files);
        const QList<OpenDataWidget::FileInfo> &filesInfoList = defineFilesInfo(filesToAnalyze);
        addFileListToTable(filesInfoList);
    }
}

void OpenDataWidget::on_cb_hideServiceDataFiles_clicked()
{
    if(ui->cb_hideServiceDataFiles->isChecked())
    {
        emit refilterTable(false);
    }
    else
    {
        emit refilterTable(true);
    }
}

QList<OpenDataWidget::FileInfo> OpenDataWidget::defineFilesInfo(const QFileInfoList &files)
{
    QList<OpenDataWidget::FileInfo> filesInfoList;

    for(const QFileInfo &fileInfo: files)
    {
        QDateTime startTime;
        int durationMsecs;
        QList<int> categories;

        std::tie(startTime, durationMsecs, categories) =
                CommonAsterixReader::getFileDataInfo(analyzeDirectory + "/" + fileInfo.fileName(), READ_STR_FOR_CAT,
                                                     Settings::value(SettingsKeys::CUT_BYTES).toInt());

        filesInfoList << std::make_tuple(fileInfo, startTime, durationMsecs, categories);
    }

    return filesInfoList;
}

void OpenDataWidget::addFileListToTable(const QList<FileInfo> &filesToAnalyze)
{
    QStandardItem *item;

    QFileInfo file;
    QDateTime dateTime;
    int durationMsecs;
    QList<int> categories;
    for(int i = 0; i < filesToAnalyze.size(); i++)
    {
        std::tie(file, dateTime, durationMsecs, categories) = filesToAnalyze[i];
        std::sort(categories.begin(), categories.end());

        item = new QStandardItem();
        item->setData(dateTime.toString("dd.MM.yyyy"), Qt::DisplayRole);
        item->setTextAlignment(Qt::AlignCenter);
        Model->setItem(i, Columns::Date, item);

        item = new QStandardItem();
        item->setData(dateTime.toString("hh:mm:ss"), Qt::DisplayRole);
        item->setTextAlignment(Qt::AlignCenter);
        Model->setItem(i, Columns::Time, item);

        item = new QStandardItem();
        item->setData(mSecsDurationToString(durationMsecs), Qt::DisplayRole);
        item->setData(durationMsecs, Qt::UserRole + 2);
        item->setTextAlignment(Qt::AlignCenter);
        Model->setItem(i, Columns::Duration, item);

        item = new QStandardItem();
        item->setData(intListToString(categories), Qt::DisplayRole);
        //item->setTextAlignment(Qt::AlignCenter);
        Model->setItem(i, Columns::Categories, item);

        item = new QStandardItem();
        item->setData(file.fileName(), Qt::DisplayRole);
        Model->setItem(i, Columns::FileName, item);

        item = new QStandardItem();
        item->setData(fileSizeToString(file.size()), Qt::DisplayRole);
        item->setData(file.size(), Qt::UserRole + 2);
        Model->setItem(i, Columns::FileSize, item);
    }
}

QString OpenDataWidget::mSecsDurationToString(int mSecs)
{
    int seconds = mSecs / 1000;

    int hours = seconds / 3600;
    if(hours)
    {
        seconds -= (hours * 3600);
    }

    int minutes = seconds / 60;
    if(minutes)
    {
        seconds -= (minutes * 60);
    }

    return QString("%1:%2:%3").arg(hours, 2, 10, QChar('0')).arg(minutes, 2, 10, QChar('0')).arg(seconds, 2, 10, QChar('0'));
}

QString OpenDataWidget::fileSizeToString(qint64 size)
{
    if(size < FileSize::KILO_BYTE)
    {
        return QString::number(size) + "bytes";
    }
    else if(size < FileSize::MEGA_BYTE)
    {
        return QString::number((float)size / (float)FileSize::KILO_BYTE, 'f', 2) + " kB";
    }
    else
    {
        return QString::number((float)size / (float)FileSize::MEGA_BYTE, 'f', 2) + " MB";
    }

    return QString();
}

QString OpenDataWidget::intListToString(QList<int> &ints)
{
    QString result;
    for(int i = 0; i < ints.size(); i++)
    {
        if(i == 0)
        {
            result.append(QString::number(ints[i]));
        }
        else
        {
            result.append(", ").append(QString::number(ints[i]));
        }
    }

    return result;
}

QList<SPoint> OpenDataWidget::applayCenterPoint(const QList<SPoint> &list, GPoint centerPoint)
{
    QList<SPoint> result;
    for(SPoint var: list)
    {
        if(MapGeometry::isNan(var.rho) && MapGeometry::isNan(var.theta) &&
           (!MapGeometry::isNan(var.latitude)) && (!MapGeometry::isNan(var.longitude)))
        {
            // calc rho,theta of ADS-B position
            var.rho = GCalc().distance(centerPoint, GPoint(var.latitude, var.longitude));
            var.theta = GCalc().azimuth(centerPoint, GPoint(var.latitude, var.longitude));
        }

        if((!MapGeometry::isNan(var.rho)) && (!MapGeometry::isNan(var.theta)) &&
            MapGeometry::isNan(var.latitude) && MapGeometry::isNan(var.longitude))
        {
            // calc lat lon by radar data
            GPoint pnt = GCalc().azimutWalk(centerPoint, var.theta, var.rho * converter::NMILE_TO_METRES / 1000.);
            if(pnt.isLatLonValid() && !pnt.isNull())
            {
                var.latitude   = pnt.latitude();
                var.longitude  = pnt.longitude();
            }
        }
        result << var;
    }
    return result;
}

void OpenDataWidget::clearTracksCounters()
{
    rbsTracksCount = 0;
    uvdTracksCount = 0;
    adsbTracksCount = 0;
    smodeTracksCount = 0;
    mlatCount = 0;
    unusedPointsCount = 0;
}



bool OpenDataWidget::getOnly34Cat() const
{
    return only34Cat;
}

void OpenDataWidget::setOnly34Cat(bool value)
{
    only34Cat = value;
}

QString OpenDataWidget::getCurrnetFileName()
{
    QString res;
    QModelIndexList selection = ui->tableView->selectionModel()->selectedRows(Columns::FileName);

    if(selection.count() == 0)
    {
        return res;
    }

    QModelIndex index = selection.at(0);
    res = SortModel->data(index, Qt::DisplayRole).toString();

    return res;
}

static bool lessThenSPointByTimestamp(const SPoint &a, const SPoint &b)
{
    return a.reciveTimestamp < b.reciveTimestamp;
}

QList<TrackProfile> OpenDataWidget::convertToTrackProfile(QHash<TrackKey, QList<SPoint>> tracks)
{
    QList<TrackProfile> resultTracks;
    QList<TrackKey> keys = tracks.keys();
    for(const TrackKey &key: keys)
    {
        if(key.type == TrackType::UNUSED)
        {
            continue;
        }
        QList<SPoint> points = tracks[key];
        std::sort(points.begin(), points.end(), lessThenSPointByTimestamp);
        TrackProfile profile;
        profile.trackKey = key;
        for(int i = 0; i < points.size(); i++)
        {
            if(profile.isPointSutable(points[i]))
            {
                profile.applayPoint(points[i]);
            }
        }

        resultTracks << profile;
    }

    return resultTracks;
}

QList<TrackKey> OpenDataWidget::filterValueInKeysList(const QList<TrackKey> &keys, int value)
{
    QList<TrackKey> result;
    for(const auto &key: keys)
    {
        if(key.value == value)
        {
            result << key;
        }
    }
    return result;
}

void OpenDataWidget::processPoint(TrackType::TrackType trackType, uint32_t trackParameter, const SPoint &point,
                                  QHash<TrackKey, QList<SPoint> > &tracksContainer)
{
    QList<TrackKey> potentionalKeys = filterValueInKeysList(tracksContainer.keys(), trackParameter);
    bool used = false;
    int trackSplitInterval = Settings::instance().value(SettingsKeys::TRACK_SPLIT_INTERVAL_SEC).toInt();
    if(potentionalKeys.size() > 0)
    {
        for(const auto &key: potentionalKeys)
        {
            SPoint prewPoint = tracksContainer[key].value(tracksContainer[key].size() - 1);
            if(qAbs(prewPoint.reciveTimestamp.secsTo(point.reciveTimestamp)) < trackSplitInterval)
            {
                tracksContainer[key] << point;
                used = true;
            }
        }
    }

    if(!used)
    {
        tracksContainer.insert(TrackKey(trackType, TrackIdKeeper::Instance().nextId(), trackParameter), QList<SPoint>() << point);
    }
}

//!!! Реализовать получение кода UVD
QHash<TrackKey, QList<SPoint>> OpenDataWidget::mergeTrajectories(const QList<SPoint> &pointsList)
{
    QHash<TrackKey, QList<SPoint> > result;
    QHash<TrackKey, QList<SPoint> > rbs;
    QHash<TrackKey, QList<SPoint> > s_mode;
    QHash<TrackKey, QList<SPoint> > ads_b;
    QHash<TrackKey, QList<SPoint> > uvd;

    for(SPoint point: pointsList)
    {
        if(point.category == 48)
        {
            // Process UVD
            if(point.uvdReplay != 0)
            {
                processPoint(TrackType::UVD, point.uvdReplay, point, uvd);
            }
            // Process ModeS
            else if(point.address24bit != 0)
            {
                processPoint(TrackType::ModeS, point.address24bit, point, s_mode);
            }
            // Process RBS
            else if(point.reply > 0)
            {
                processPoint(TrackType::RBS, point.reply, point, rbs);
            }
            // Process UNUSED
            else
            {
                result[TrackKey(TrackType::UNUSED, 0, 0)] << point;
            }
        }
        else if((point.category == 21) && (point.address24bit != 0))
        {
            processPoint(TrackType::ADSB, point.address24bit, point, ads_b);
        }
        else
        {
            result[TrackKey(TrackType::UNUSED, 0, 0)] << point;
        }
    }

    rbsTracksCount = rbs.size();
    uvdTracksCount = uvd.size();
    adsbTracksCount = ads_b.size();
    smodeTracksCount = s_mode.size();
    unusedPointsCount = result[TrackKey(TrackType::UNUSED, 0, 0)].size();

    rbsPointsCount = 0;
    QList<TrackKey> keys = rbs.keys();
    for(const TrackKey &key: keys)
    {
        rbsPointsCount += rbs[key].size();
    }

    uvdPointsCount = 0;
    keys = uvd.keys();
    for(const TrackKey &key: keys)
    {
        uvdPointsCount += uvd[key].size();
    }

    adsbPointsCount = 0;
    keys = ads_b.keys();
    for(const TrackKey &key: keys)
    {
        adsbPointsCount += ads_b[key].size();
    }

    smodePointsCount = 0;
    keys = s_mode.keys();
    for(const TrackKey &key: keys)
    {
        smodePointsCount += s_mode[key].size();
    }

    result = result.unite(rbs);
    result = result.unite(ads_b);
    result = result.unite(s_mode);
    result = result.unite(uvd);

    return result;
}

void OpenDataWidget::extractTracks(bool append)
{
    QModelIndexList selection = ui->tableView->selectionModel()->selectedRows(Columns::FileName);
    QList<SPoint> validPointsList;

    clearTracksCounters();

    if(selection.count() == 0)
    {
        QMessageBox::information(this, "Внимание!", "Выберите файлы для анализа из списка!");
    }

    QApplication::setOverrideCursor(Qt::WaitCursor);

    for(int i = 0; i < selection.count(); i++)
    {
        QModelIndex index = selection.at(i);
        QVariant data = SortModel->data(index, Qt::DisplayRole);

        QList<SPoint> pointsList = ArchiveReader().getData(analyzeDirectory + "/" + data.toString(), QString(), true);
        GPoint recalcPoint(centerPoint);

        if( !only34Cat )
            pointsList = hlf::filter( pointsList, [] ( const SPoint& p){ return !p.isRadarReport(); });
        else
            pointsList = hlf::filter( pointsList, [] ( const SPoint& p){ return p.isRadarReport();  });


        if(recalcPoint.isLatLonValid() && (!recalcPoint.isNull()))
        {
            pointsList = applayCenterPoint(pointsList, recalcPoint);
        }

        validPointsList << pointsList;
    }

    TrackBuilder::Instance().setLocatorRotationPeriod(Settings::instance().value(SettingsKeys::LOCATOR_ROTATION_PERIOD).toInt());
    TrackBuilder::Instance().startNewTrackOnRbsChange(Settings::value(SettingsKeys::NEW_TRACK_ON_RBS_CHANGE).toBool());
    TrackBuilder::Instance().startNewTrackOnDeltaAzimuth(Settings::value(SettingsKeys::NEW_TRACK_ON_DELTA_CHANGE).toBool());
    TrackBuilder::Instance().setMaxLocatorEdgeDistance(Settings::value(SettingsKeys::MAX_LOCATOR_DISTANCE).toInt());
    TrackBuilder::Instance().setDeltaAzimuthForNewTrack(Settings::value(SettingsKeys::NEW_TRACK_DELTA_AZIMUTH).toInt());
    TrackBuilder::Instance().setDistanceThreshold(Settings::value(SettingsKeys::DISTANCE_THRESHOLD_KM).toInt());
    TrackBuilder::Instance().setTimeThreshold(Settings::value(SettingsKeys::TIME_THRESHOLD_SEC).toInt());
    TrackBuilder::Instance().setBindingByTrackNumber(Settings::value(SettingsKeys::BIND_BY_TRACK_NUMBER).toBool());
    TrackBuilder::Instance().setMinTrackPointsCount(Settings::value(SettingsKeys::MIN_TRACK_POINTS_COUNT).toInt());
    if(Settings::value(SettingsKeys::MLAT_RLS_TYPE).toInt() == LocatorUtils::LocatorType::MLAT)
    {
        const QString coordsString = Settings::value(SettingsKeys::MLAT_LOCATION_ZONE_COORDS).toString();
        const QStringList coordsStringsList = MlatSettingsDialog::coordinatesList(coordsString);
        QList<GPoint> sector;
        for(const QString &coordStr: coordsStringsList)
        {
            sector << GPoint(coordStr);
        }
        TrackBuilder::Instance().setLocatorType(LocatorUtils::LocatorType::MLAT);
        TrackBuilder::Instance().setSector(GCalc().pointsToLoop(sector));
    }

    TrackBuilder::Instance().process(validPointsList);
    QList<TrackProfile> tracksProfiles = TrackBuilder::Instance().getTracksProfiles();
    QList<SPoint> unusedPoints = TrackBuilder::Instance().unusedPoints();
    rbsTracksCount = TrackBuilder::Instance().rbsTracksCount();
    uvdTracksCount = TrackBuilder::Instance().uvdTracksCount();
    adsbTracksCount = TrackBuilder::Instance().adsbTracksCount();
    smodeTracksCount = TrackBuilder::Instance().sModeTracksCount();

    rbsPointsCount = TrackBuilder::Instance().rbsPointsCount();
    uvdPointsCount = TrackBuilder::Instance().uvdPointsCount();
    adsbPointsCount = TrackBuilder::Instance().adsbPointsCount();
    smodePointsCount = TrackBuilder::Instance().smodePointsCount();
    unusedPointsCount = TrackBuilder::Instance().unusedPointsCount();

    removeUvdFromMlat(tracksProfiles, unusedPoints);

    QHash<TrackType::TrackType, int> tracksPoints;
    tracksPoints[TrackType::RBS] = rbsTracksCount;
    tracksPoints[TrackType::UVD] = uvdTracksCount;
    tracksPoints[TrackType::ADSB] = adsbTracksCount;
    tracksPoints[TrackType::ModeS] = smodeTracksCount;

    QHash<TrackType::TrackType, int> pointsCounters;
    pointsCounters[TrackType::UNUSED] = unusedPointsCount;
    pointsCounters[TrackType::RBS] = rbsPointsCount;
    pointsCounters[TrackType::UVD] = uvdPointsCount;
    pointsCounters[TrackType::ADSB] = adsbPointsCount;
    pointsCounters[TrackType::ModeS] = smodePointsCount;

    emit sendUnusedPoints(unusedPoints, append);
    emit updatePointsCounters(pointsCounters, append);
    emit updateTracksCounters(tracksPoints, append);
    emit tracksReday(tracksProfiles, append);
    emit readedPoints(validPointsList);

    QApplication::restoreOverrideCursor();
}

bool OpenDataWidget::removeUvdFromMlat(QList<TrackProfile> &tracksProfiles, QList<SPoint> &unusedPoints)
{
    if((uvdTracksCount == 0) && (uvdPointsCount == 0))
    {
        return false;
    }

    int rlsType = Settings::value(SettingsKeys::MLAT_RLS_TYPE).toInt();
    const QString &locatorType = LocatorUtils::getLocatorCaptionByType((LocatorUtils::LocatorType)rlsType);
    if(locatorType != LocatorTypeCaption::mlatCaption)
    {
        return false;
    }

    uvdTracksCount = 0;
    uvdPointsCount = 0;

    //int profilesCount = tracksProfiles.size();
    for(int currProfile = tracksProfiles.size() - 1; currProfile >= 0; currProfile--)
    {
        if(tracksProfiles[currProfile].trackKey.type == TrackType::UVD)
        {
            unusedPointsCount += tracksProfiles[currProfile].points.size();
            unusedPoints << tracksProfiles[currProfile].points;
            tracksProfiles.removeAt(currProfile);
        }
    }

    return true;
}

QString OpenDataWidget::datagramToText(const QByteArray& datagram)
{
    static char const hex_char[16] = {'0',
                                      '1', '2', '3', '4', '5',
                                      '6', '7', '8', '9', 'A',
                                      'B', 'C', 'D', 'E', 'F'};
    char byte;
    QString s;
    s.reserve(datagram.size() * 3);
    const char* data = datagram.constData();
    for(int i=0; i<datagram.size(); i++)
    {
        byte = data[i];
        s += hex_char[(byte & 0xF0) >> 4];
        s += hex_char[(byte & 0x0F)];
        s += " ";
    }
    s.chop(1);
    return s;
}

void OpenDataWidget::on_compute(void)
{
    extractTracks(false);
}

void OpenDataWidget::on_addToTracks(void)
{
    extractTracks(true);
}

void OpenDataWidget::on_mergeFiles(void)
{
    QModelIndexList selection = ui->tableView->selectionModel()->selectedRows(Columns::FileName);
    if(selection.count() <= 1)
    {
        QMessageBox::information(this, "Внимание!", "Выберите несколько файлов из списка!");
    }

    QString workDir = Settings::instance().value(SettingsKeys::LAST_INPUT_DIRECTORY).toString();
    QString fileName = QFileDialog::getSaveFileName(nullptr, "Укажите файл для сохранения данных как dat", workDir, "*.dat");
    if(fileName.isEmpty())
    {
        return;
    }

    if(!fileName.endsWith(".dat"))
    {
        fileName.append(".dat");
    }

    ofstream output_file;
    output_file.open(fileName.toStdString().c_str(), ofstream::binary);
    if(!output_file.is_open())
    {
        QMessageBox::warning(this, tr("File, save"), tr("Can`t open the file to saving data"));
        return;
    }

    int eventsProcessTimeMs = 10;
    QApplication::setOverrideCursor(Qt::WaitCursor);

    QList<SPoint> filesPoints;
    for(int i = 0; i < selection.count(); i++)
    {
        QVariant data = SortModel->data(selection.at(i), Qt::DisplayRole);
        filesPoints.append(ArchiveReader().getData(analyzeDirectory + "/" + data.toString(), QString(), true));
        QCoreApplication::processEvents(QEventLoop::AllEvents, eventsProcessTimeMs);
    }

    std::sort(filesPoints.begin(), filesPoints.end(),
               [](const SPoint &left, const SPoint &right) { return left.reciveTimestamp < right.reciveTimestamp; });

    QString date_time_str = QDateTime::currentDateTime().toString("dd.MM.yyyy hh:mm:ss");
    output_file << "# " << date_time_str.toLocal8Bit().constData() << endl;
    output_file << "#" << endl;

    if(filesPoints.isEmpty())
    {
        QApplication::restoreOverrideCursor();
        QMessageBox::warning(this, "Внимание!", "В выбранных файлах нет точек!");
        output_file.close();
        return;
    }

    int pointsThreshold = 5000;
    int processedPoints = 0;
    QDateTime initStamp = filesPoints.at(0).reciveTimestamp;
    for(SPoint &point: filesPoints)
    {
        QString s;
        s.sprintf("%.6f ", (static_cast<double>(initStamp.msecsTo(point.reciveTimestamp)) / 1000.));
        s += datagramToText(point.rawByteArray);
        s += "\n";output_file.write(s.toLocal8Bit().constData(), s.size());

        processedPoints++;
        if(processedPoints > pointsThreshold)
        {
            processedPoints = 0;
            QCoreApplication::processEvents(QEventLoop::AllEvents, eventsProcessTimeMs);
        }
    }

    output_file.close();

    QApplication::restoreOverrideCursor();
}

void OpenDataWidget::on_changeLocatorMode(QString mode)
{
    ui->lb_locatorMode->setText(mode);
}

void OpenDataWidget::on_cb_hideFiles_stateChanged(int arg1)
{
    if(arg1)
    {
        int tableHeigth = ui->tableView->height();
        int thisHeight = this->height();
        this->resize(this->width(), thisHeight - tableHeigth);
        ui->tableView->setHidden(true);
        emit tableHided(tableHeigth);
        return;
    }

    ui->tableView->setHidden(false);
    emit tableHided(0);
}
