#include "SettingsDialog.h"
#include "ui_SettingsDialog.h"
#include "MapPoint.h"

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    hideMethodsSettings();


    QStringList ListItems;
    ListItems << "Чебышева" << "Вейвлеты" << "AB - фильтр" << "Фильтр Калмана" << "Полиномиальный" << "Коифлет";
    ui->cb_calcMethod->addItems(ListItems);

    QIntValidator* intValidator = new QIntValidator();
    intValidator->setLocale(QLocale(QLocale::English));
    ui->le_minPointForSplit->setValidator(intValidator);

    QDoubleValidator* doubleValidator = new QDoubleValidator(0., 100., 4);
    doubleValidator->setLocale(QLocale(QLocale::English));
    ui->le_minPercentForAdd->setValidator(doubleValidator);

    doubleValidator = new QDoubleValidator(0., 1., 4);
    doubleValidator->setLocale(QLocale(QLocale::English));
    ui->le_minPO->setValidator(doubleValidator);

    doubleValidator = new QDoubleValidator(0., 1., 4);
    doubleValidator->setLocale(QLocale(QLocale::English));
    ui->le_minPN->setValidator(doubleValidator);

    doubleValidator = new QDoubleValidator(0., 1., 4);
    doubleValidator->setLocale(QLocale(QLocale::English));
    ui->le_minPH->setValidator(doubleValidator);


    doubleValidator = new QDoubleValidator(0., 1000., 4);
    doubleValidator->setLocale(QLocale(QLocale::English));
    ui->le_maxRmsRho->setValidator(doubleValidator);

    doubleValidator = new QDoubleValidator(0., 360., 4);
    doubleValidator->setLocale(QLocale(QLocale::English));
    ui->le_maxRmsTheta->setValidator(doubleValidator);

    intValidator = new QIntValidator();
    intValidator->setLocale(QLocale(QLocale::English));
    ui->le_minPointsInGoodTrack->setValidator(intValidator);

    intValidator = new QIntValidator();
    intValidator->setLocale(QLocale(QLocale::English));
    ui->le_pl_n->setValidator( intValidator );

    loadSettings();
}

void SettingsDialog::hideMethodsSettings()
{
    ui->lb_methodSettings->setHidden(true);
    hideChebyshevMethodsSettings();
    hideWaveletsMethodsSettings();
    hideABFilterMethodsSettings();
    hideKalmanMethodsSettings();
    hidePlynomialMethodsSettings();
    hideSynchronization();
}

void SettingsDialog::hideChebyshevMethodsSettings()
{
    ui->lb_ChebyshevMethod->setHidden(true);
    ui->lb_ChebyshevN->setHidden(true);
    ui->le_ch_n->setHidden(true);
    ui->lb_ChebyshevR->setHidden(true);
    ui->le_ch_r->setHidden(true);
}

void SettingsDialog::hideWaveletsMethodsSettings()
{
    ui->lb_WaveletsMethod->setHidden(true);
    ui->lb_WaveletsN->setHidden(true);
    ui->le_wav_n->setHidden(true);
}

void SettingsDialog::hideABFilterMethodsSettings()
{
    ui->lb_ABFilterMethod->setHidden(true);
    ui->lb_ABFilterA->setHidden(true);
    ui->le_ab_a->setHidden(true);
    ui->lb_ABFilterB->setHidden(true);
    ui->le_ab_b->setHidden(true);
}

void SettingsDialog::hideKalmanMethodsSettings()
{
    ui->lb_KalmanMethod->setHidden(true);
    ui->lb_kf_e_mea->setHidden(true);
    ui->le_kf_e_mea->setHidden(true);
    ui->lb_kf_e_est->setHidden(true);
    ui->le_kf_e_est->setHidden(true);
    ui->lb_kf_q->setHidden(true);
    ui->le_kf_q->setHidden(true);
}

void SettingsDialog::hidePlynomialMethodsSettings()
{
    ui->lb_PolynimialMethod->setHidden(true);
    ui->lb_pl_n->setHidden(true);
    ui->le_pl_n->setHidden(true);
}

void SettingsDialog::hideSynchronization()
{
    ui->gb_synchronization->setHidden(true);
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::setWaveletCalculationMethod(int calcMethod)
{
    switch(calcMethod)
    {
        case WaveletsMethod::BY_POINTS:
        {
            ui->rb_byPoints->setChecked(true);
            ui->rb_byPerpendicylar->setChecked(false);
            break;
        }

        case WaveletsMethod::BY_PERPENDICULAR:
        {
            ui->rb_byPerpendicylar->setChecked(true);
            ui->rb_byPoints->setChecked(false);
            break;
        }

        default:
        {
            ui->rb_byPoints->setChecked(true);
            ui->rb_byPerpendicylar->setChecked(false);
        }
    }
}

void SettingsDialog::setChebyshevSourceTime(int sourceTime)
{
    switch(sourceTime)
    {
        case ChebyshevSourceTime::RLS:
        {
            ui->rb_fromRls->setChecked(true);
            ui->rb_fromTrackAvgTime->setChecked(false);
            break;
        }

        case ChebyshevSourceTime::TRACK_AVG_TIME:
        {
            ui->rb_fromRls->setChecked(false);
            ui->rb_fromTrackAvgTime->setChecked(true);
            break;
        }

        default:
        {
            ui->rb_fromRls->setChecked(false);
            ui->rb_fromTrackAvgTime->setChecked(true);
        }
    }
}

int SettingsDialog::getWaveletCalculationMethod()
{
    if(ui->rb_byPoints->isChecked())
    {
        return WaveletsMethod::BY_POINTS;
    }
    else if(ui->rb_byPerpendicylar->isChecked())
    {
        return WaveletsMethod::BY_PERPENDICULAR;
    }

    return WaveletsMethod::BY_POINTS;
}

int SettingsDialog::getChebyshevSourceTime()
{
    if(ui->rb_fromRls->isChecked())
    {
        return ChebyshevSourceTime::RLS;
    }
    else if(ui->rb_fromTrackAvgTime->isChecked())
    {
        return ChebyshevSourceTime::TRACK_AVG_TIME;
    }

    return ChebyshevSourceTime::TRACK_AVG_TIME;
}

void SettingsDialog::loadSettings()
{
    StatusSettings settings;
    //ui->cb_calcMethod->setCurrentIndex(settings.calcMethod);
    ui->cb_calcMethod->setCurrentIndex(0);
    setWaveletCalculationMethod(settings.waveletCalculationMethod);
    setChebyshevSourceTime(settings.chebyshevSourceTime);
    ui->le_minPointForSplit->setText(QString::number(settings.minPointsForSplitting));
    ui->le_minPercentForAdd->setText(QString::number(settings.minPercentForAdding));
    ui->gb_useGoodTracks->setChecked(settings.useGoodTracksOnly);
    ui->le_minPO->setText(QString::number(settings.minPOProbability));
    ui->le_minPN->setText(QString::number(settings.minPNProbability));
    ui->le_minPH->setText(QString::number(settings.minPHProbability));
    ui->le_maxRmsRho->setText(QString::number(settings.maxRmsRhoMeters));
    ui->le_maxRmsTheta->setText(QString::number(settings.maxRmsThetaMinutes));
    ui->le_minPointsInGoodTrack->setText(QString::number(settings.minPointsInGoodTrack));

    ui->le_ch_n->setText( QString::number( settings.calcParams.value("ch_n").toInt() ) );
    ui->le_ch_r->setText( QString::number( settings.calcParams.value("ch_r").toInt() ) );

    ui->le_wav_n->setText( QString::number( settings.calcParams.value("w_n").toInt()) );

    ui->le_ab_a->setText( QString::number(settings.calcParams.value("ab_a").toDouble() ));
    ui->le_ab_b->setText( QString::number(settings.calcParams.value("ab_b").toDouble() ));

    ui->le_kf_e_est->setText(QString::number( settings.calcParams.value("kf_e_est").toDouble() ) );
    ui->le_kf_e_mea->setText(QString::number( settings.calcParams.value("kf_e_mea").toDouble() ) );
    ui->le_kf_q->setText(    QString::number( settings.calcParams.value("kf_q").toDouble() ));
    ui->le_pl_n->setText(    QString::number( settings.calcParams.value("pl_n").toInt() ));


    ui->cb_periodValue->setCurrentText( QString::number(settings.rotationSecManual) );
    ui->le_maxDistance->setText( QString::number(settings.maxDistance) );
    ui->le_minDistance->setText( QString::number(settings.minDistance) );
    ui->le_minAltitude->setText( QString::number( settings.minAlitude ) );
    ui->le_maxAltitude->setText( QString::number(settings.maxAlitude) );
    ui->le_blindAngle->setText( QString::number( settings.blindAngle ) );
    ui->le_refPoint->setText(  settings.referncePoint);

    ui->cb_newTrackOnRbs->setChecked(settings.nextTrackOnRbsChange);
    ui->le_maxLocatorDistance->setText(QString::number(settings.maxLocatorDistanceKm));
    ui->le_minPointsCount->setText(QString::number(settings.minTrackPointsCount));
    ui->gb_newTarckOnDeltaAzimuth->setChecked(settings.nextTrackOnDeltaAzimuth);
    ui->le_maxDeltaAzimuth->setText(QString::number(settings.newTrackDeltaAzimuthDegree));

    ui->le_distanceThreshold->setText(QString::number(settings.distanceThresholdKm));
    ui->le_timeThreshold->setText(QString::number(settings.timeThresholdSec));
    ui->cb_bindByTrackNumber->setChecked(settings.bindByTrackNumber);

    ui->gb_enableTracksValidation->setChecked(settings.useTracksValidation);
    ui->le_rhoValidationThreshold->setText(QString::number(settings.rhoValidationThreshold));
    ui->le_thetaValidationThreshold->setText(QString::number(settings.thetaValidationThreshold));

    ui->le_minIntersectedPoints->setText(QString::number(settings.minIntersectedPoints));
    ui->cb_approximatePointsForChebyshew->setChecked(settings.approximatePointsForChebyshew);

    ui->le_aircraftAvgVerticalSpeed->setText(QString::number(settings.aircraftAvgVerticalSpeed));

    ui->cb_modeADistortion->setChecked(settings.showPa);
    ui->cb_callsignDistortion->setChecked(settings.showPc);
    ui->cb_altitudeDistortion->setChecked(settings.showPh);

    ui->le_unstableZoneWidth->setText(QString::number(settings.unstableZoneWidthPercent));
    ui->le_cutBytes->setText(QString::number(settings.cutBytes));
}

StatusSettings SettingsDialog::saveSettings()
{
    StatusSettings settings;

    StatusSettings::CalculationMethod method = StatusSettings::Chebyshev;
    QString methodText = ui->cb_calcMethod->currentText();

    QVariantHash calcParams = settings.calcParams;
    calcParams.insert("ch_n", ui->le_ch_n->text().toInt());
    calcParams.insert("ch_r", ui->le_ch_r->text().toInt());
    calcParams.insert("w_n", ui->le_wav_n->text().toInt());
    calcParams.insert("ab_a", ui->le_ab_a->text().toDouble());
    calcParams.insert("ab_b", ui->le_ab_b->text().toDouble());
    calcParams.insert("kf_e_mea", ui->le_kf_e_mea->text().toDouble());
    calcParams.insert("kf_e_est", ui->le_kf_e_est->text().toDouble());
    calcParams.insert("kf_q", ui->le_kf_q->text().toDouble());
    calcParams.insert("pl_n", ui->le_pl_n->text().toInt());

    if((methodText == "Chebyshev") || (methodText == "Чебышева"))
    {
        method = StatusSettings::Chebyshev;
    }
    else if((methodText == "Wavelets") || (methodText == "Вейвлеты"))
    {
        method = StatusSettings::Wavelets;
    }
    else if((methodText == "AB_Filter") || (methodText == "AB - фильтр"))
    {
        method = StatusSettings::AB_Filter;
    }
    else if((methodText == "Kalman_filter") || (methodText == "Фильтр Калмана"))
    {
        method = StatusSettings::Kalman_Filter;
    }
    else if((methodText =="Polynominal") || (methodText == "Полиномиальный"))
    {
        method = StatusSettings::PolinominalInterpolation;
    }
    else if((methodText =="Coiflet") || (methodText =="Коифлет"))
    {
        method = StatusSettings::Coiflet;
    }

    settings.calcMethod = method;
    settings.waveletCalculationMethod = getWaveletCalculationMethod();
    settings.chebyshevSourceTime = getChebyshevSourceTime();
    settings.calcParams = calcParams;

    settings.minPointsForSplitting = ui->le_minPointForSplit->text().toInt();
    settings.minPercentForAdding = ui->le_minPercentForAdd->text().toDouble();
    settings.useGoodTracksOnly = ui->gb_useGoodTracks->isChecked();
    settings.minPOProbability = ui->le_minPO->text().toDouble();
    settings.minPNProbability = ui->le_minPN->text().toDouble();
    settings.minPHProbability = ui->le_minPH->text().toDouble();
    settings.maxRmsRhoMeters = ui->le_maxRmsRho->text().toDouble();
    settings.maxRmsThetaMinutes = ui->le_maxRmsTheta->text().toDouble();
    settings.minPointsInGoodTrack = ui->le_minPointsInGoodTrack->text().toInt();

    settings.rotationSecManual = ui->cb_periodValue->currentText().toDouble(); // из интерфейса, из данных значение должно иметь приоритет
    settings.maxDistance       = ui->le_maxDistance->text().toDouble();  // km
    settings.minDistance       = ui->le_minDistance->text().toDouble(); // km
    settings.minAlitude        = ui->le_minAltitude->text().toDouble();  // m
    settings.maxAlitude        = ui->le_maxAltitude->text().toDouble();  // m
    settings.blindAngle        = ui->le_blindAngle->text().toDouble();  // grad

    settings.nextTrackOnRbsChange = ui->cb_newTrackOnRbs->isChecked();
    settings.maxLocatorDistanceKm = ui->le_maxLocatorDistance->text().toInt();
    settings.minTrackPointsCount = ui->le_minPointsCount->text().toInt();
    settings.nextTrackOnDeltaAzimuth = ui->gb_newTarckOnDeltaAzimuth->isChecked();
    settings.newTrackDeltaAzimuthDegree = ui->le_maxDeltaAzimuth->text().toInt();

    settings.distanceThresholdKm = ui->le_distanceThreshold->text().toInt();
    settings.timeThresholdSec = ui->le_timeThreshold->text().toInt();
    settings.bindByTrackNumber = ui->cb_bindByTrackNumber->isChecked();

    settings.useTracksValidation = ui->gb_enableTracksValidation->isChecked();
    settings.rhoValidationThreshold = ui->le_rhoValidationThreshold->text().toDouble();
    settings.thetaValidationThreshold = ui->le_thetaValidationThreshold->text().toDouble();

    settings.minIntersectedPoints = ui->le_minIntersectedPoints->text().toInt();
    settings.approximatePointsForChebyshew = ui->cb_approximatePointsForChebyshew->isChecked();

    settings.aircraftAvgVerticalSpeed = ui->le_aircraftAvgVerticalSpeed->text().toDouble();

    settings.showPa = ui->cb_modeADistortion->isChecked();
    settings.showPc = ui->cb_callsignDistortion->isChecked();
    settings.showPh = ui->cb_altitudeDistortion->isChecked();

    settings.unstableZoneWidthPercent = ui->le_unstableZoneWidth->text().toDouble();
    settings.cutBytes = ui->le_cutBytes->text().toInt();

    if(MapGeometry::point(settings.referncePoint).isValid())
    {
        settings.referncePoint = MapGeometry::point(settings.referncePoint).toString("DDMMSSssE");
    }

    settings.save();
    return settings;
}

void SettingsDialog::on_pb_apply_clicked()
{
    StatusSettings settings = saveSettings();
    sendUpdatedSettings(settings);
    accept();
}

void SettingsDialog::on_pb_cancel_clicked()
{
    reject();
}

