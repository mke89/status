#include "rdpsystem.h"
#include "settings.h"
#include "structs.h"
#include <qmath.h>
#include "mkfu.h"
#include "gcalc.h"
#include "cutil.h"
#include "MapPoint.h"
#include "MapSpherical.h"
#include "MapConstants.h"

int stopCounter = 0;
const int STOP_NUMBER = 18;


RdpSystem::RdpSystem(bool testMode, QObject *parent): QObject(parent)
                                     , lastAvailableNumber(0)
                                     , MIN_TRACK_NUMBER(0)
                                     , MAX_TRACK_NUMBER(65530)
{
    PREDICTIONS_LIMIT = Settings::instance().value(SettingsKeys::RDP_PREDICTIONS_BEFORE_DELETE).toInt();
    enableTrackFiltering = Settings::instance().value(SettingsKeys::RDP_ENABLE_TRACK_FILTERING).toBool();

    checkTracksNumbersIntervalsCorrectness();
    lastAvailableNumber = MIN_TRACK_NUMBER;

    removeOldTracksCoefficent = Settings::instance().value(SettingsKeys::RDP_FORCE_END_TRAGECTORY_COEFFICENT).toInt();

    this->testMode = testMode;

    if(!testMode)
    {
        QObject::connect(&predictionTimer, SIGNAL(timeout()), this, SLOT(processPrediction()));
        predictionTimer.start(Settings::instance().value(SettingsKeys::RDP_PREDICTION_PERIOD_MS).toInt());
    }
}

void RdpSystem::checkTracksNumbersIntervalsCorrectness()
{
    int globalMin = 0;
    int globalMax = 65530;
    int rdpMinTrackNumber = Settings::instance().value(SettingsKeys::RDP_MIN_TRACKS_NUMBER).toInt();
    int rdpMaxTrackNumber = Settings::instance().value(SettingsKeys::RDP_MAX_TRACKS_NUMBER).toInt();

    int vrMinTrackNumber = Settings::instance().value(SettingsKeys::VR_MIN_TRACK_NUMBER).toInt();
    int vrMaxTrackNumber = Settings::instance().value(SettingsKeys::VR_MAX_TRACK_NUMBER).toInt();

    bool incorrectIntervals = false;

    if( !((rdpMinTrackNumber >= globalMin) && (rdpMinTrackNumber <= globalMax)) ||
        !((rdpMaxTrackNumber >= globalMin) && (rdpMaxTrackNumber <= globalMax)) ||
        !((vrMinTrackNumber >= globalMin) && (vrMinTrackNumber <= globalMax))   ||
        !((vrMaxTrackNumber >= globalMin) && (vrMaxTrackNumber <= globalMax))   )
    {
        incorrectIntervals = true;
    }

    if(vrMaxTrackNumber >= rdpMinTrackNumber)
    {
        incorrectIntervals = true;
    }

    if(vrMinTrackNumber >= vrMaxTrackNumber)
    {
        incorrectIntervals = true;
    }

    if(rdpMinTrackNumber >= rdpMaxTrackNumber)
    {
        incorrectIntervals = true;
    }

    if(incorrectIntervals)
    {
        rdpMinTrackNumber = 32000;
        rdpMaxTrackNumber = 65530;

        qDebug() << "RDP ERROR!!! : Incorrect tracks numbers intervals settings!!!";
    }

    MIN_TRACK_NUMBER = rdpMinTrackNumber;
    MAX_TRACK_NUMBER = rdpMaxTrackNumber;
}

void RdpSystem::createNewBucket(RadarIdentificator radarKey, SimplePoint point, quint16 trackId)
{
    PointsContainer pointsBucket(radarKey, trackId);
    pointsBucket.addPoint(point);
    buckets[radarKey] << pointsBucket;
}

void RdpSystem::addPointList(const QList<SimplePoint> &pointList, RadarIdentificator radarId)
{
    for(SimplePoint point: pointList)
    {
        addPoint(radarId, point);
    }
}

int stopCtr = 0;
void RdpSystem::addPoint(RadarIdentificator &radarId, const SimplePoint &targetPoint)
{
    storage.addSimplePoint(radarId, targetPoint);

    stopCtr++;
    if(stopCtr == 8)
    {
        stopCtr = 8;
    }

    //sysTime.validateIncomingTimestamp(point.timestamp());
    static bool RDP_USE_IMMIDIATLY_PROCESS_BY_ADD = Settings::instance().value(SettingsKeys::RDP_USE_IMMIDIATLY_PROCESS_BY_ADD).toBool();
    static bool RDP_USE_IMMIDIATLY_POST_PROCESS_BY_ADD = Settings::instance().value(SettingsKeys::RDP_USE_IMMIDIATLY_POST_PROCESS_BY_ADD).toBool();
    if(RDP_USE_IMMIDIATLY_PROCESS_BY_ADD)
    {
        preProcess();
    }

    if(RDP_USE_IMMIDIATLY_POST_PROCESS_BY_ADD)
    {
        postProcess();
    }

    if(testMode)
    {
        //postProcess();
    }
}

void RdpSystem::autotune(const RadarIdentificator radarKey, const int bucketNum, const SimplePoint point)
{
    double timeDiffMsec = point.receivedTimestamp - buckets[radarKey][bucketNum].points.last().receivedTimestamp;
    GPoint predictedPoint = buckets[radarKey][bucketNum].predictionFilter.gPureSimulatedPredict(timeDiffMsec / 1000.0);

    QPointF predictedPointCartesianXY(predictedPoint.latitude(), predictedPoint.longitude());
    QPointF pointF(point.x_meters, point.y_meters);
    QPointF lastPoint(buckets[radarKey][bucketNum].points.last().x_meters, buckets[radarKey][bucketNum].points.last().y_meters);
    double distanceToPredicted = distanceBetweenPoints(predictedPointCartesianXY, lastPoint);
    double distBetweenPredictAndMew = distanceBetweenPoints(pointF, predictedPointCartesianXY);

    double timeDiff = (point.receivedTimestamp - buckets[radarKey][bucketNum].points.last().receivedTimestamp) / 1000.0;
    double searchRadius = buckets[radarKey][bucketNum].getNextPointRadiusSearchInMeters(distanceToPredicted, timeDiff);

    double halfOfSearchRadius = searchRadius / 2.0;

    if(distBetweenPredictAndMew >= searchRadius)
    {
        buckets[radarKey][bucketNum].alphaFilterCoeff = FILTER_MAX_VAL;
        buckets[radarKey][bucketNum].betaFilterCoeff = FILTER_MAX_VAL;
    }
    else if(distBetweenPredictAndMew == 0)
    {
        buckets[radarKey][bucketNum].alphaFilterCoeff = FILTER_MAX_VAL;
        buckets[radarKey][bucketNum].betaFilterCoeff = FILTER_MAX_VAL;
    }
    else if(distBetweenPredictAndMew == (halfOfSearchRadius))
    {
        buckets[radarKey][bucketNum].alphaFilterCoeff = FILTER_MIN_VAL;
        buckets[radarKey][bucketNum].betaFilterCoeff = FILTER_MIN_VAL;
    }
    else if(distBetweenPredictAndMew > (halfOfSearchRadius))
    {
        double percent = (distBetweenPredictAndMew - (halfOfSearchRadius)) / (halfOfSearchRadius);

        double coeff = FILTER_MIN_VAL + ((FILTER_MAX_VAL - FILTER_MIN_VAL) * percent);
        buckets[radarKey][bucketNum].alphaFilterCoeff = coeff;
        buckets[radarKey][bucketNum].betaFilterCoeff = coeff;
    }
    else if(distBetweenPredictAndMew < (halfOfSearchRadius))
    {
        double percent = ((halfOfSearchRadius) - distBetweenPredictAndMew) / (halfOfSearchRadius);

        double coeff = FILTER_MIN_VAL + ((FILTER_MAX_VAL - FILTER_MIN_VAL) * percent);
        buckets[radarKey][bucketNum].alphaFilterCoeff = coeff;
        buckets[radarKey][bucketNum].betaFilterCoeff = coeff;
    }

    buckets[radarKey][bucketNum].predictionFilter.setCoefficents(buckets[radarKey][bucketNum].alphaFilterCoeff,
                                                                 buckets[radarKey][bucketNum].betaFilterCoeff);
}

bool RdpSystem::processTrack(RadarIdentificator& radarKey, SimplePoint& point)
{
    bool pointPreciselyAddedToTrack = false;
    int bucketsCount = buckets[radarKey].size();
    for(int i = 0; i < bucketsCount; i++)
    {
        if(buckets[radarKey][i].type == PointsContainer::ContainerType::TRACK)
        {
            if(DebugHalt::isStop())
            {
                DebugHalt::nop();
            }

            if(buckets[radarKey][i].isPointSuitable(point) > SUITABLE_VALUE_THRESHOLD)
            {
                autotune(radarKey, i, point);
                buckets[radarKey][i].addPoint(point);
                pointPreciselyAddedToTrack = true;
            }
        }
    }

    return pointPreciselyAddedToTrack;
}

bool RdpSystem::processPotentialTrack(RadarIdentificator& radarKey, SimplePoint &point)
{
    bool potentialTrackHandled = false;
    int bucketsCount = buckets[radarKey].size();
    for(int i = 0; i < bucketsCount; i++)
    {
        if(buckets[radarKey][i].type == PointsContainer::ContainerType::POTENTIAL_TRACK)
        {
            if(DebugHalt::isStop())
            {
                DebugHalt::nop();
            }

            if(buckets[radarKey][i].isPointSuitable(point) > SUITABLE_VALUE_THRESHOLD)
            {
                buckets[radarKey][i].addPoint(point);
                potentialTrackHandled = true;
            }
        }
    }

    return potentialTrackHandled;
}

bool RdpSystem::processSinglePoint(RadarIdentificator& radarKey, SimplePoint &point)
{
    bool singlePointHandled = false;
    int bucketsCount = buckets[radarKey].size();
    for(int i = 0; i < bucketsCount; i++)
    {
        if(buckets[radarKey][i].type == PointsContainer::ContainerType::SINGLE_POINT)
        {
            if(buckets[radarKey][i].isPointSuitable(point) > SUITABLE_VALUE_THRESHOLD)
            {
                buckets[radarKey][i].addPoint(point);
                singlePointHandled = true;
            }
        }
    }

    return singlePointHandled;
}

bool RdpSystem::processUnmarkedPoints(RadarIdentificator& radarKey, SimplePoint &point)
{
    bool unmarkedPointHandled = false;
    int bucketsCount = buckets[radarKey].size();

    for(int i = 0; i < bucketsCount; i++)
    {
        if(buckets[radarKey][i].type == PointsContainer::ContainerType::UNMARKED)
        {
            if(buckets[radarKey][i].isPointSuitable(point) > SUITABLE_VALUE_THRESHOLD)
            {
                buckets[radarKey][i].addPoint(point);
                unmarkedPointHandled = true;
            }
        }
    }

    return unmarkedPointHandled;
}

void RdpSystem::preProcess()
{
    for(RadarIdentificator radarKey: storage.getRadarIds())
    {
        QList<SimplePoint> list = storage.takePointsByRadar(radarKey);

        for(SimplePoint point: list)
        {
            stopCounter++;

            if(stopCounter == STOP_NUMBER)
            {
                 DebugHalt::setStopFlag();
            }
            else
            {
                DebugHalt::clearStopFlag();
            }

            radarLastPointTime[radarKey] = point.receivedTimestamp;

            if(buckets[radarKey].size() == 0)
            {
                createNewBucket(radarKey, point, generateUnicTrackNumber());
                continue;
            }

            bool trackProcessed = processTrack(radarKey, point);
            if(trackProcessed)
            {
                continue;
            }

            processPotentialTrack(radarKey, point);
            processSinglePoint(radarKey, point);
            processUnmarkedPoints(radarKey, point);

            if(!point.sutableFlags.testFlag(SuitableFlags::Flag::BY_NEAR))
            {
                createNewBucket(radarKey, point, generateUnicTrackNumber());
            }
        }
    }
}

void RdpSystem::postProcess()
{
    //QDateTime currentDt = QDateTime ::currentDateTime();
    QList<BucketIndex> indexes;
    QMultiMap<SimplePoint, BucketIndex> allPoints;
    bool tracksUpdated = false;

    //
    // Получаем список точек и индексов (RadarId, номер корзины и тип трека), в которых эти точки хранятся
    //
    createBucketsListsForPoints(allPoints, indexes);
    std::sort(indexes.begin(),indexes.end(), [](const BucketIndex &a, const BucketIndex &b){return (int)a.type > (int)b.type;});
    QList<BucketIndex> alreadyDeletedIndexes;

    for(BucketIndex bucketIndex: indexes)
    {
        if(DebugHalt::isStop())
        {
            DebugHalt::nop();
        }

        if(alreadyDeletedIndexes.contains(bucketIndex))
        {
            continue;
        }

        RadarIdentificator radarKey = bucketIndex.radarKey;
        int i = bucketIndex.index;
        int pretendersCount = buckets[radarKey][i].pretenderPoints.size();

        if(pretendersCount == 0)
        {
            continue;
        }

        QList<SimplePoint> pointsToPostAnalyze;
        while(buckets[radarKey][i].pretenderPoints.size())
        {
            buckets[radarKey][i].trimmPoinstLists();
            SimplePoint newPoint = buckets[radarKey][i].pretenderPoints.takeFirst();

            bool isOutOfCurrentStrobe = processNearestPointsOfAirTarget(radarKey, i,  newPoint);
            if(isOutOfCurrentStrobe)
            {
                tracksUpdated = updateContainerOrTrackState(newPoint, radarKey, i, pointsToPostAnalyze);
            }
        }

        if(DebugHalt::isStop())
        {
            DebugHalt::nop();
        }

        // Получение индексов, необходимых для удаления
        markTempBucketsToRemove(pointsToPostAnalyze, allPoints, alreadyDeletedIndexes, i);
    }

    if(DebugHalt::isStop())
    {
        DebugHalt::nop();
    }

    //processPrediction();

    // Удаляем корзины, помеченные для удаления
    deleteMarkedBuckets(alreadyDeletedIndexes);


    //QMultiHash<RadarIdentificator, int> toDelete = getBucketsToRemove();
    //QList<SimplePoint> endPoints = removePastDueBuckets(toDelete);
    //sendEndPoints(endPoints);
    //sendTracks(tracksUpdated);
}

void RdpSystem::sendEndPoints(QList<SimplePoint> endPoints)
{
    QList<TargetPoint> result;
    for(auto var: endPoints)
    {
        MainTrackIndex index = MainTrackIndex(0, var.trackNumber);
        TargetPoint targetPoint = convertToTargetPoint(var, index);
        result << targetPoint;
    }

    if(result.size())
    {
        emit tracksReday(result);
    }
}

bool RdpSystem::processNearestPointsOfAirTarget(const RadarIdentificator& radarKey, int containerNum,
                                                const SimplePoint& newPoint)
{
    if(buckets[radarKey][containerNum].points.size() > 0)
    {
        SimplePoint& lastPoint = buckets[radarKey][containerNum].points.last();
        double distToLastPoint = distanceBetweenPoints(QPointF(lastPoint.x_meters,lastPoint.y_meters),
                                                       QPointF(newPoint.x_meters, newPoint.y_meters));

        if(distToLastPoint <= buckets[radarKey][containerNum].MULTI_POINT_MERGE_RADIUS_M * SEARCH_STROBE_MULTIPLIER)
        {
            buckets[radarKey][containerNum].addCoords(newPoint);
            return false;
        }

        if(buckets[radarKey][containerNum].nearestAvg.pointsCounter != 0)
        {
            buckets[radarKey][containerNum].modifyLastPoint();
        }
    }

    return true;
}

bool RdpSystem::updateContainerOrTrackState(SimplePoint& newPoint, RadarIdentificator &radarKey,
                                            int bucketIndex, QList<SimplePoint>& pointsToPostAnalyze)
{
    if(DebugHalt::isStop())
    {
        DebugHalt::nop();
    }

    bool tracksUpdated = false;
    double secondsDiff = 0;
    if(buckets[radarKey][bucketIndex].points.size() > 0)
    {
        secondsDiff = (newPoint.receivedTimestamp -
                       buckets[radarKey][bucketIndex].points.last().receivedTimestamp) / 1000.0;
    }

    SimplePoint predictedPoint = getNextFilteredTrackPoint(newPoint, radarKey, bucketIndex, secondsDiff);
    double predictionErrorDistMeters = distanceBetweenPoints(QPointF(newPoint.x_meters, newPoint.y_meters),
                                                             QPointF(predictedPoint.x_meters, predictedPoint.y_meters));
    if(enableTrackFiltering)
    {
        buckets[radarKey][bucketIndex].points << predictedPoint;
    }
    else
    {
        buckets[radarKey][bucketIndex].points << newPoint;
    }

/*
    if(predictionErrorDistMeters > 10000)
    {
        qDebug() << "PREDICTION FAIL !!!: DISTANCE_KM: " << predictionErrorDistMeters / 1000
                 << "   SQUAWK = " << newPoint.squawk
                 << "   TA: " << newPoint.targetAddress;
    }
*/
    if(predictionErrorDistMeters > buckets[radarKey][bucketIndex].MULTI_POINT_MERGE_RADIUS_M)
    {
        buckets[radarKey][bucketIndex].MULTI_POINT_MERGE_RADIUS_M = predictionErrorDistMeters + 1;
    }

    if(buckets[radarKey][bucketIndex].points.size() == 1)
    {
        buckets[radarKey][bucketIndex].type = PointsContainer::SINGLE_POINT;
        buckets[radarKey][bucketIndex].points.last().sended = true;
    }
    else if(buckets[radarKey][bucketIndex].points.size() == 2)
    {
        buckets[radarKey][bucketIndex].type = PointsContainer::POTENTIAL_TRACK;
        buckets[radarKey][bucketIndex].points.last().sended = true;
        buckets[radarKey][bucketIndex].recalcCourseParams();
    }
    else if(buckets[radarKey][bucketIndex].points.size() > 2)
    {
        if(buckets[radarKey][bucketIndex].points.size() == 3)
        {
            buckets[radarKey][bucketIndex].points.last().isFirst = true;
        }

        buckets[radarKey][bucketIndex].type = PointsContainer::TRACK;
        buckets[radarKey][bucketIndex].recalcCourseParams();
        pointsToPostAnalyze << newPoint;

        tracksUpdated = true;
    }

    return tracksUpdated;
}

void RdpSystem::sendTracks(bool tracksUpdated)
{
    if(tracksUpdated)
    {
        QList<TargetPoint> tracksList = getTracks();

        if(tracksList.size())
        {
            emit tracksReday(tracksList);
        }
    }
}

void RdpSystem::deleteMarkedBuckets(QList<BucketIndex>& alreadyDeletedIndexes)
{
    std::sort(alreadyDeletedIndexes.begin(), alreadyDeletedIndexes.end(),
              [](const BucketIndex &a, const BucketIndex &b){return (int)a.index > (int)b.index;});

    for (BucketIndex var: alreadyDeletedIndexes)
    {
        quint16 idToClear = buckets[var.radarKey][var.index].unicContainerId;
        releaseUnicTrackNumber(idToClear);
        buckets[var.radarKey].removeAt(var.index);
    }
}

void RdpSystem::markTempBucketsToRemove(const QList<SimplePoint>& pointsToPostAnalyze, QMultiMap<SimplePoint, BucketIndex>& allPoints,
                                        QList<BucketIndex>& alreadyDeletedIndexes, int bucketNum)
{
    for(const SimplePoint &sp: pointsToPostAnalyze)
    {
        QList<BucketIndex> potentialSuspects = allPoints.values(sp);
        allPoints.remove(sp);

        for(BucketIndex var: potentialSuspects)
        {
            if(bucketNum == var.index)
            {
                continue;
            }

            if(var.type != PointsContainer::TRACK)
            {
                alreadyDeletedIndexes << var;
            }
        }
    }
}

/*
     allPoints - дл/ каждой точки указываются индексы (RadarId, номер корзины и тип трека), в которых точка хранится
     indexes - список индексов (RadarId, номер корзины и тип трека)
*/
void RdpSystem::createBucketsListsForPoints(QMultiMap<SimplePoint, BucketIndex>& allPoints, QList<BucketIndex>& indexes)
{
    for(RadarIdentificator radarKey: buckets.keys())
    {
        int bucketsCount = buckets[radarKey].size();
        for(int i = 0; i < bucketsCount; i++)
        {
            for(SimplePoint pt: buckets[radarKey][i].pretenderPoints)
            {
                allPoints.insertMulti(pt , BucketIndex(radarKey, i, buckets[radarKey][i].type));
            }

            indexes << BucketIndex(radarKey, i, buckets[radarKey][i].type);
        }
    }
}

QMultiHash<RadarIdentificator, int> RdpSystem::getBucketsToRemove()
{
    QMultiHash<RadarIdentificator, int> result;
    for(RadarIdentificator radarKey: buckets.keys())
    {
        int bucketsCount = buckets[radarKey].size();
        for(int i = 0; i < bucketsCount; i++)
        {
            if(buckets[radarKey][i].points.isEmpty())
            {
                continue;
            }

            double currentDt = 0;
            if(testMode)
            {
                currentDt = radarKey.lastPointReceiveTime;
            }
            else
            {
                currentDt = QDateTime::currentDateTime().toMSecsSinceEpoch();
            }

            double diff = (currentDt - buckets[radarKey][i].points.last().receivedTimestamp);
            double value = (radarKey.rotationPeriodMs * removeOldTracksCoefficent);
            if(diff > value)
            {
                result.insert(radarKey, i);
            }

        }
    }

    return result;
}

SimplePoint RdpSystem::getNextFilteredTrackPoint(const SimplePoint& newPoint, RadarIdentificator radarKey, int bucketNum, double timeDiff)
{
    QPointF convertedPoint = buckets[radarKey][bucketNum].projection.wgsToScene(MapGeometry::point(newPoint.latitude, newPoint.longitude));
    QPointF filteredPonitF = buckets[radarKey][bucketNum].predictionFilter.process(convertedPoint.x(), convertedPoint.y(), timeDiff);
    MapGeometry::point filteredPonitLatLon = buckets[radarKey][bucketNum].projection.sceneToWgs(filteredPonitF);

    SimplePoint predictedPoint = newPoint;
    predictedPoint.latitude    = filteredPonitLatLon.lat;
    predictedPoint.longitude   = filteredPonitLatLon.lon;
    predictedPoint.x_meters    = filteredPonitF.x();
    predictedPoint.y_meters    = filteredPonitF.y();

    return predictedPoint;
}

QList<SimplePoint> RdpSystem::removePastDueBuckets(const QMultiHash< RadarIdentificator, int> &toDelete)
{
    QList<SimplePoint> endPoints;
        QList<RadarIdentificator> uniqueKeys = toDelete.uniqueKeys();
        for (RadarIdentificator radarId: uniqueKeys)
        {
            QList<int> list = toDelete.values(radarId);
            std::sort(list.begin(), list.end(), std::greater<int>());
            for(int i = 0; i < list.size(); i++)
            {
                CDEBUG << "DELETING SQUAWK:" << buckets[radarId][list.at(i)].points.last().squawk << " id:" << buckets[radarId][list.at(i)].unicContainerId ;

                endPoints<<buckets[radarId][list.at(i)].points.last();
                endPoints.last().isEnd=true;
                endPoints.last().trackNumber = buckets[radarId][list.at(i)].unicContainerId;

                quint16 idToClear = buckets[radarId][list.at(i)].unicContainerId;
                releaseUnicTrackNumber(idToClear);
                buckets[radarId].removeAt(list.at(i));
            }
        }
        return endPoints;
}

void RdpSystem::markAllPointsAsSended(RadarIdentificator rid, int containerIndex)
{
    QList<SimplePoint>& list = buckets[rid][containerIndex].points;
    for(int i = list.size() - 1; i >= 0; i--)
    {
        if(list[i].sended)
        {
            break;
        }

        list[i].sended = true;
    }
}

QList<TargetPoint> RdpSystem::getTracks()
{
    QList<TargetPoint> trackPoints;
    for( RadarIdentificator radarKey: buckets.keys() )
    {
        for( int i = 0; i < buckets[radarKey].size(); i++ )
        {
            if(buckets[radarKey][i].type != PointsContainer::ContainerType::TRACK)
            {
                continue;
            }

            QList<SimplePoint> pointsList = buckets[radarKey][i].points;

            for(int k = pointsList.size() - 1;  k >= 0; k--)
            {
                SimplePoint point = pointsList[k];

                //if(point.sended)
                //    break;

                point.course = buckets[radarKey][i].course;
                point.speed = buckets[radarKey][i].speed_m_s;
                point.trackNumber = buckets[radarKey][i].unicContainerId;
                MainTrackIndex index = MainTrackIndex(radarKey.portNumber, buckets[radarKey][i].unicContainerId);
                TargetPoint targetPoint = convertToTargetPoint(point, index);

                if(buckets[radarKey][i].unicContainerId != 0)
                {
                    buckets[radarKey][i].unicContainerId = buckets[radarKey][i].unicContainerId;
                }

                //CDEBUG<<VAR(point.targetAddress) << VAR( point.rho )<<VAR(point.theta)<<VAR(point.latitude)<<VAR(point.longitude);
                trackPoints << targetPoint;
                buckets[radarKey][i].points[k].sended = true;
            }
        }
    }

    return trackPoints;
}

QPointF RdpSystem::rotatePoint(QPointF centerPoint, QPointF rotationPoint, double rotationAngleDeg)
{
    double x = rotationPoint.x() - centerPoint.x();
    double y = rotationPoint.y() - centerPoint.y();


    double rotationAngleRad = MapGeometry::degToRad * (rotationAngleDeg);

    double x1 = x * qCos(rotationAngleRad) - y * qSin(rotationAngleRad) + centerPoint.x();
    double y1 = x * qSin(rotationAngleRad) + y * qCos(rotationAngleRad) + centerPoint.y();

    return QPointF(x1, y1);
}

double RdpSystem::getPointAngle(QPointF centerPoint, QPointF interestPoint)
{
    QPointF northPoint(centerPoint.x(), centerPoint.y() + 10);

    QPointF northPointCentered(northPoint.x() - centerPoint.x(), northPoint.y() - centerPoint.y());
    QPointF interestPointCentered(interestPoint.x() - centerPoint.x(), interestPoint.y() - centerPoint.y());

    double scalarProduct = northPointCentered.x() * interestPointCentered.x() + northPointCentered.y() * interestPointCentered.y();
    double northVectorLen = qSqrt(northPointCentered.x() * northPointCentered.x() + northPointCentered.y() * northPointCentered.y());
    double interestVectorLen = qSqrt(interestPointCentered.x() * interestPointCentered.x() + interestPointCentered.y() * interestPointCentered.y());

    double angle = (qAcos(scalarProduct / (northVectorLen * interestVectorLen))) / MapGeometry::degToRad;
    return angle;
}

TargetPoint RdpSystem::convertToTargetPoint( SimplePoint &point, MainTrackIndex index)
{
    TargetPoint targetPoint;
    targetPoint.latitude            = point.latitude;
    targetPoint.longitude           = point.longitude;
    targetPoint.x                   = point.x_meters;
    targetPoint.y                   = point.y_meters;
    targetPoint.course              = point.course;
    targetPoint.velocity            = point.speed;
    targetPoint.deviceTimestamp     = point.receivedTimestamp;
    targetPoint.sic                 = point.sic;
    targetPoint.sac                 = point.sac;
    targetPoint.index               = index;
    targetPoint.squawk              = point.squawk;
    targetPoint.vrTrackNumber       = point.trackNumber;
    targetPoint.registers           = point.registers;
    targetPoint.rho                 = point.rho  ;
    targetPoint.azimuth             = point.theta;
    targetPoint.callsign            = point.callsign;
    targetPoint.targetAddress       = point.targetAddress;
    targetPoint.altitude            = point.altitude;
    targetPoint.isTst               = point.isTst;
    targetPoint.isRab               = point.isRab;
    targetPoint.displaySymbol       = point.displaySymbol;
    targetPoint.sourceName          = point.sourceName;
    targetPoint.index.trackNumber   = point.trackNumber;
    targetPoint.isEnd               = point.isEnd;
    targetPoint.predicted           = point.predicted;
    targetPoint.dateTime            = point.dateTime;

    return targetPoint;
}

QList<QPair<RadarIdentificator, int>> RdpSystem::getTracksKeys()
{
    QList<QPair<RadarIdentificator, int>> trackKeys;

    for(RadarIdentificator radarKey: buckets.keys())
    {
        int bucketsCount = buckets[radarKey].size();
        for(int i = 0; i < bucketsCount; i++)
        {
            if(buckets[radarKey][i].type == PointsContainer::TRACK)
            {
                trackKeys.append(QPair<RadarIdentificator, int>(radarKey, i));
            }
        }
    }

    return trackKeys;
}

QList<TargetPoint> RdpSystem::getTrackPoints(QPair<RadarIdentificator, int> id)
{
    QList<TargetPoint> trackPoints;

    QList<SimplePoint>& pointsList = buckets[id.first][id.second].points;
    for(int i = 2; i < pointsList.size(); i++)
    {
        SimplePoint& point = pointsList[i];

        TargetPoint targetPoint;
        targetPoint.latitude = point.latitude;
        targetPoint.longitude = point.longitude;
        targetPoint.x = point.x_meters;
        targetPoint.y = point.y_meters;
        targetPoint.course = buckets[id.first][id.second].course;
        targetPoint.velocity = buckets[id.first][id.second].speed_m_s;
        targetPoint.deviceTimestamp = point.receivedTimestamp;
        targetPoint.squawk = point.squawk;
        //targetPoint.sic =
        //targetPoint.sac =

        trackPoints << targetPoint;

        buckets[id.first][id.second].points[i].sended = true;
    }

    return trackPoints;
}

QList<TargetPoint> RdpSystem::getAllTracks()
{
    QList<TargetPoint> tracksList;
    QList<QPair<RadarIdentificator, int>> tracksKeys = getTracksKeys();
    for(auto trackKey: tracksKeys)
    {
        tracksList << getTrackPoints(trackKey).last();
    }

    return tracksList;
}

quint16 RdpSystem::generateUnicTrackNumber()
{
    quint16 COUNTER = MIN_TRACK_NUMBER;
    while(trackNumbersStorage.contains(lastAvailableNumber) && (COUNTER < MAX_TRACK_NUMBER))
    {
        COUNTER++;
        lastAvailableNumber++;
    }

    if(COUNTER == MAX_TRACK_NUMBER)
    {
        qDebug() << "ERROR!!! RdpSystem: Out of free Rdp tracks IDs";
        exit(-9);
    }

    trackNumbersStorage.insert(lastAvailableNumber);
    return lastAvailableNumber;
}

void RdpSystem::releaseUnicTrackNumber(quint16 trashNumber)
{
    if(trackNumbersStorage.contains(trashNumber))
    {
        trackNumbersStorage.remove(trashNumber);
    }
}

void RdpSystem::processPrediction()
{
    double rdpPredictionPeriodMs = Settings::instance().value(SettingsKeys::RDP_PREDICTION_PERIOD_COEFFICIENT).toDouble();
    GCalc calc;
    for(RadarIdentificator radarKey: buckets.keys())
    {
        int bucketsCount = buckets[radarKey].size();
        int rotationPeriodMs = radarKey.rotationPeriodMs;
        double maxSkipTime = rotationPeriodMs * rdpPredictionPeriodMs;
        for(int i = 0; i < bucketsCount; i++)
        {
            if(buckets[radarKey][i].type == PointsContainer::TRACK)
            {
                double currentTimeMs = 0;

                if(testMode)
                {
                    currentTimeMs = radarLastPointTime[radarKey];
                }
                else
                {
                    currentTimeMs = QDateTime::currentDateTime().toMSecsSinceEpoch();
                }

                if(stopCounter == STOP_NUMBER)
                {
                     DebugHalt::setStopFlag();
                }
                else
                {
                    DebugHalt::clearStopFlag();
                }

                double pointsSkippedTimeMs = currentTimeMs - buckets[radarKey][i].points.last().receivedTimestamp;
                if( (pointsSkippedTimeMs >= maxSkipTime) &&
                    (buckets[radarKey][i].pretenderPoints.size() == 0) &&
                    (!buckets[radarKey][i].points.last().isEnd))
                {
                    int speed_m_s = buckets[radarKey][i].getNewSpeed();

                    double timeDiffMsec = currentTimeMs - buckets[radarKey][i].points.last().receivedTimestamp;
                    GPoint predictedPoint = buckets[radarKey][i].predictionFilter.gPureSimulatedPredict(timeDiffMsec / 1000.0);

                    QPointF predictedCartesianXY(predictedPoint.latitude(), predictedPoint.longitude());
                    double distanceDiff = distanceBetweenPoints(predictedCartesianXY, QPointF(buckets[radarKey][i].points.last().x_meters,
                                                                                              buckets[radarKey][i].points.last().y_meters));

                    if(distanceDiff > 10000)
                    {
                        //qDebug() << "distanceDiff: " << distanceDiff;
                    }

                    double maxSearchRadius = buckets[radarKey][i].maxSearchRadius;
                    if(distanceDiff > (maxSearchRadius * 2))
                    {
                        double maxPredictDistance = maxSearchRadius * 2;
                        double divider = distanceDiff / maxPredictDistance;

                        distanceDiff = maxPredictDistance;
                        double xDist = predictedCartesianXY.x() - buckets[radarKey][i].points.last().x_meters;
                        double yDist = predictedCartesianXY.y() - buckets[radarKey][i].points.last().y_meters;
                        double newXCoord = (xDist) / divider;
                        double newYCoord = (yDist) / divider;
                        predictedPoint.setLatitude(newXCoord + buckets[radarKey][i].points.last().x_meters);
                        predictedPoint.setLongitude(newYCoord + buckets[radarKey][i].points.last().y_meters);

                        predictedCartesianXY = QPointF(predictedPoint.latitude(), predictedPoint.longitude());
                        distanceDiff = distanceBetweenPoints(predictedCartesianXY, QPointF(buckets[radarKey][i].points.last().x_meters,
                                                                                           buckets[radarKey][i].points.last().y_meters));
                    }

                    if(distanceDiff > 5000)
                    {
                        qDebug() << "distanceDiff: " << distanceDiff;
                    }

                    MapGeometry::point predictedPointLatLon = buckets[radarKey][i].projection.sceneToWgs(QPointF(predictedPoint.latitude(),
                                                                                                                 predictedPoint.longitude()));
                    double course = MapGeometry::azimuth(buckets[radarKey][i].points.last().point(), predictedPointLatLon );

                    SimplePoint newPoint = buckets[radarKey][i].points.last();
                    newPoint.rho         = MapGeometry::distance( radarKey.pointGeoposition , predictedPointLatLon ) / MapGeometry::mile;
                    newPoint.latitude    = predictedPointLatLon.lat;
                    newPoint.longitude   = predictedPointLatLon.lon;
                    newPoint.x_meters    = predictedPoint.latitude();
                    newPoint.y_meters    = predictedPoint.longitude();
                    newPoint.course      = course;
                    buckets[radarKey][i].course     = course;
                    newPoint.speed       = speed_m_s;
                    buckets[radarKey][i].speed_m_s  = speed_m_s;
                    newPoint.predicted   = true;
                    newPoint.sended      = false;
                    newPoint.theta       = MapGeometry::azimuth( radarKey.pointGeoposition , predictedPointLatLon );

                    buckets[radarKey][i].uninterrupredPredictionsCount++;
                    if(buckets[radarKey][i].uninterrupredPredictionsCount > PREDICTIONS_LIMIT)
                    {
                        newPoint.isEnd = true;
                        //qDebug() << "IS END TRUE !!!";
                    }

                    //qDebug() << "PREDICT COUNT: " << buckets[radarKey][i].uninterrupredPredictionsCount;

                    if(testMode)
                    {
                        newPoint.receivedTimestamp = radarLastPointTime[radarKey];
                    }
                    else
                    {
                        newPoint.receivedTimestamp = QDateTime::currentDateTime().toMSecsSinceEpoch();
                    }

                    buckets[radarKey][i].pretenderPoints << newPoint;

                    if(newPoint.receivedTimestamp > radarLastPointTime[radarKey])
                    {
                        radarLastPointTime[radarKey] = newPoint.receivedTimestamp;
                    }

                    //qDebug() << "PREDICTED: " << GPoint(newPoint.latitude, newPoint.longitude).format("DDMMSSE");
                }
            }
        }
    }
}

QDataStream &operator<<(QDataStream &stream, const RdpSystem &rdpSystem)
{
    stream << rdpSystem.storage << rdpSystem.buckets << rdpSystem.radarLastPointTime;

    return stream;
}

QDataStream &operator>>(QDataStream &stream, RdpSystem &rdpSystem)
{
    stream >> rdpSystem.storage >> rdpSystem.buckets >> rdpSystem.radarLastPointTime;

    return stream;
}

