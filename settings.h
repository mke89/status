#ifndef SETTINGS_H
#define SETTINGS_H

#include "QVariant"
#include <QSettings>
#include <QSharedPointer>

namespace WaveletsMethod
{
    enum WaveletsMethod
    {
        BY_POINTS,
        BY_PERPENDICULAR,

        METHODS_COUNT,
    };
}

namespace ChebyshevSourceTime
{
    enum ChebyshevSourceTime
    {
        RLS,
        TRACK_AVG_TIME,
    };
}

namespace SettingsKeys
{
    const QString LAST_TAB_INDEX            = "last_tab_index";
    const QString MAIN_TIMER_DELAY          = "main_timer_delay";
    const QString PORT                      = "port";
    const QString IP_ADDRESS                = "ip_address";
    const QString MAP_IP_PORT               = "map_ip_port";
    const QString FROM_MAP_PORT             = "from_map_port";
    const QString LAST_DIR_TO_OPEN          = "last_dir_to_open";
    const QString LAST_DIR_TO_SAVE          = "last_dir_to_save";
    const QString MAIN_WINDOW_POS           = "main_window_pos";
    const QString HV_MAX_DMODEL_SIZE        = "HexView/max_dmodel_size";
    const QString HV_BYTES_FOR_CUT          = "HexView/bytes_for_cut";
    const QString HV_CATEGORY               = "HexView/category";
    const QString HV_SPLITTER               = "HexView/splitter";
    const QString TRK_MAX_RTMODEL_SIZE      = "Tracks/max_rtmodel_size";
    const QString TRK_DELTA_SECONDS         = "Tracks/delta_seconds";
    const QString TRK_CATEGORY              = "Tracks/category";
    const QString TRK_SPLITTER              = "Tracks/splitter";
    const QString TRK_FLTR_KEY_SEQUENCE     = "Tracks/fltr_key_sequence";

    const QString RDR_NAME                  = "Name of radar";
    const QString RDR_SAC                   = "SAC of radar";
    const QString RDR_SIC                   = "SIC of radar";
    const QString RDR_FORMAT                = "Format of radar";
    const QString RDR_UAP                   = "UAP of radar";
    const QString RDR_MAX_COV               = "Maximum coverage of radar";
    const QString RDR_FRAME_TYPE            = "Frame type of radar";
    const QString RDR_VERIFY                = "Verify of radar";
    const QString RDR_MIN_LEV               = "Minimum level of radar";
    const QString RDR_MAX_LEV               = "Maximum level of radar";
    const QString RDR_LEV_PER_LAYER         = "Level per layer of radar";
    const QString RDR_DATA_DIR              = "Feed dir of radar";
    const QString RDR_VERIFY_PORT           = "VERIFY_PORT";
    const QString RDR_PORT_NUM              = "PORT_NUM";
    const QString RDR_DATE_FROM             = "DATE_FROM";
    const QString RDR_DATE_TO               = "DATE_TO";
    const QString RDR_VERIFY_DATE           = "VERIFY_DATE";
    const QString RDR_VERIFY_AC             = "VERIFY_AC";
    const QString RDR_VERIFY_SB             = "VERIFY_SB";
    const QString RDR_VERIFY_SS             = "VERIFY_SS";
    const QString RDR_VERIFY_SE             = "VERIFY_SE";
    const QString RDR_VERIFY_LEVEL          = "VERIFY_LEVEL";
    const QString RDR_USE_PROBABILITY_CALC  = "USE_PROBABILITY_CALC";
    const QString RDR_THETA_STEP            = "THETA_STEP";
    const QString RDR_RHO_STEP              = "RHO_STEP";
    const QString RDR_ROUTES_MAX_CALC_POINS = "ROUTES_MAX_CALC_POINS";
    const QString RDR_ROUTES_MAX_DELTA_T    = "ROUTES_MAX_DELTA_T";
    const QString RDR_BYPASS_FILTERS        = "RDR_BYPASS_FILTERS";

    const QString LBL_CAT0102               = "Cat01/02 of data";
    const QString LBL_CAT2123               = "Cat21/23 of data";
    const QString LBL_CAT3448               = "Cat34/48 of data";
    const QString LBL_RANGE                 = "Range of data";
    const QString LBL_SACSIC                = "Sac/Sic of data";
    const QString LBL_ALTITUDE              = "Altitude of data";

    const QString LAST_X_POS                    = "LAST_X_POS";
    const QString LAST_Y_POS                    = "LAST_Y_POS";
    const QString LAST_INPUT_DIRECTORY          = "LAST_INPUT_DIRECTORY";
    const QString TRACK_SPLIT_INTERVAL_SEC      = "TRACK_SPLIT_INTERVAL_SEC";

    const QString CALCULATION_METHOD            = "CALCULATION_METHOD";
    const QString WAVELETS_CALCULATION_METHOD   = "WAVELETS_CALCULATION_METHOD";
    const QString CALCULATION_PARAMS            = "CALCULATION_PARAMS";
    const QString MIN_POINTS_FOR_SPLIT          = "MIN_POINTS_FOR_SPLIT";
    const QString MIN_PERCENT_FOR_SPLIT_ADD     = "MIN_PERCENT_FOR_SPLIT_ADD";
    const QString SHOW_ONLY_GOOD_TRACKS         = "SHOW_ONLY_GOOD_TRACKS";
    const QString MIN_PROBABILITY_P_O           = "MIN_PROBABILITY_P_O";
    const QString MIN_PROBABILITY_P_N           = "MIN_PROBABILITY_P_N";
    const QString MIN_PROBABILITY_P_H           = "MIN_PROBABILITY_P_H";
    const QString MAX_RMS_RHO                   = "MAX_RMS_RHO";
    const QString MAX_RMS_THETA                 = "MAX_RMS_THETA";
    const QString GOOD_TRACK_SIZE               = "GOOD_TRACK_SIZE";
    const QString ROTATION_SEC_MANUAL           = "ROTATION_SEC_MANUAL";
    const QString MAX_DISTANCE                  = "MAX_DISTANCE";
    const QString MIN_DISTANCE                  = "MIN_DISTANCE";
    const QString MIN_ALITUDE                   = "MIN_ALITUDE";
    const QString MAX_ALITUDE                   = "MAX_ALITUDE";
    const QString BLIND_ANGLE                   = "BLIND_ANGLE";
    const QString REFERENCE_POINT               = "REFERENCE_POINT";

    const QString USE_TRACKS_VALIDATION         = "USE_TRACKS_VALIDATION";
    const QString RHO_VALIDATION_THRESHOLD      = "RHO_VALIDATION_VALIDATION";
    const QString THETA_VALIDATION_THRESHOLD    = "THETA_VALIDATION_VALIDATION";

    const QString NEW_TRACK_ON_RBS_CHANGE       = "NEW_TRACK_ON_RBS_CHANGE";
    const QString NEW_TRACK_ON_DELTA_CHANGE     = "NEW_TRACK_ON_DELTA_CHANGE";
    const QString NEW_TRACK_DELTA_AZIMUTH       = "NEW_TRACK_DELTA_AZIMUTH";
    const QString MAX_LOCATOR_DISTANCE          = "MAX_LOCATOR_DISTANCE";
    const QString MIN_TRACK_POINTS_COUNT        = "MIN_TRACK_POINTS_COUNT";

    const QString DISTANCE_THRESHOLD_KM         = "DISTANCE_THRESHOLD";
    const QString TIME_THRESHOLD_SEC            = "TIME_THRESHOLD";
    const QString BIND_BY_TRACK_NUMBER          = "BIND_BY_TRACK_NUMBER";

    const QString LOCATOR_POSITION              = "LOCATOR_POSITION";
    const QString LOCATOR_POSITION_DESCRIPTION  = "LOCATOR_POSITION_DESCRIPTION";
    const QString LOCATOR_ROTATION_PERIOD       = "LOCATOR_ROTATION_PERIOD";
    const QString LOCATOR_ALTITUDE              = "LOCATOR_ALTITUDE";
    const QString CENTER_POINTS_LIST            = "CENTER_POINTS_LIST";

    const QString WAVELET_SKO_USE_NORMAL_TO_REF_TRAJECTORY  = "WAVELET_SKO/USE_NORMAL_TOREF_TRAJECTORY";
    const QString WAVELET_SKO_USE_POINT_TO_POINT_METHOD     = "WAVELET_SKO/POINT_TO_POINT_METHOD";
    const QString WAVELET_SKO_LEVEL_OF_DECOMPOSITION        = "WAVELET_SKO/LEVEL_OF_DECOMPOSITION";

    const QString LAST_PSR_INPUT_DIRECTORY    = "LAST_PSR_INPUT_DIRECTORY";
    const QString LAST_SSR_INPUT_DIRECTORY    = "LAST_SSR_INPUT_DIRECTORY";
    const QString LAST_RDP_INPUT_DIRECTORY    = "LAST_RDP_INPUT_DIRECTORY";


    const QString HISTORY_DEPTH                                         = "PRL/HISTORY_DEPTH";
    const QString RDP_INITIAL_TRAJECTORY_DEVIATION_DEGREES_PER_SECOND   = "PRL/INITIAL_TRAJECTORY_DEVIATION_DEGREES_PER_SECOND";
    const QString PC_SEARCH_FORWARD_DIST_COEFF                          = "PRL/POINTS_CONTAINER/SEARCH_FORWARD_DIST_COEFF";
    const QString PC_SEARCH_FORWARD_DIST_MIN_COEFF                      = "PRL/POINTS_CONTAINER/SEARCH_FORWARD_DIST_MIN_COEFF";
    const QString PC_SEARCH_BACKWARD_DIST_COEFF                         = "PRL/POINTS_CONTAINER/SEARCH_BACKWARD_DIST_COEFF";
    const QString PC_SEARCH_BACKWARD_DIST_MAX_COEFF                     = "PRL/POINTS_CONTAINER/SEARCH_BACKWARD_DIST_MAX_COEFF";
    const QString PC_SEARCH_RADIUS_MULTIPLIER_COEFF                     = "PRL/POINTS_CONTAINER/SEARCH_RADIUS_MULTIPLIER_COEFF";
    const QString PC_RADIUS_REGION_SUITABLE_COEFF                       = "PRL/POINTS_CONTAINER/RADIUS_REGION_SUITABLE_COEFF";
    const QString PC_FORWARD_REGION_SUITABLE_COEFF                      = "PRL/POINTS_CONTAINER/FORWARD_REGION_SUITABLE_COEFF";
    const QString PC_BACK_REGION_SUITABLE_COEFF                         = "PRL/POINTS_CONTAINER/BACK_REGION_SUITABLE_COEFF";
    const QString PC_ADDITIONAL_POINT_BELONGING_LEN_COEFF               = "PRL/POINTS_CONTAINER/ADDITIONAL_POINT_BELONGING_LEN_COEFF";
    const QString PC_CURVATURE_SENSITIVITY_MULTIPLIER_COEFF             = "PRL/POINTS_CONTAINER/CURVATURE_SENSITIVITY_MULTIPLIER_COEFF";
    const QString PC_MIN_POINT_FOR_CURVATURE_CALCULATION                = "PRL/POINTS_CONTAINER/MIN_POINT_FOR_CURVATURE_CALCULATION";
    const QString RDP_PREDICTIONS_BEFORE_DELETE                         = "PRL/PREDICTIONS_BEFORE_DELETE";
    const QString RDP_MULTIPLE_POINTS_TO_ONE_MERGE_RADIUS_M             = "PRL/MULTIPLE_POINTS_TO_ONE_MERGE_RADIUS_M";
    const QString RDP_ENABLE_TRACK_FILTERING                            = "PRL/ENABLE_TRACK_FILTERING";
    const QString RDP_FORCE_END_TRAGECTORY_COEFFICENT                   = "PRL/FORCE_END_TRAGECTORY_COEFFICENT";
    const QString RDP_PREDICTION_PERIOD_MS                              = "PRL/PREDICTION_PERIOD_MS";
    const QString RDP_MIN_TRACKS_NUMBER                                 = "PRL/MIN_TRACKS_NUMBER";
    const QString RDP_MAX_TRACKS_NUMBER                                 = "PRL/MAX_TRACKS_NUMBER";
    const QString VR_MAX_TRACK_NUMBER                                   = "PRL/VR_MAX_TRACK_NUMBER";
    const QString VR_MIN_TRACK_NUMBER                                   = "PRLP/R_MIN_TRACK_NUMBER";
    const QString RDP_USE_IMMIDIATLY_PROCESS_BY_ADD                     = "PRL/USE_IMMIDIATLY_PROCESS_BY_ADD";
    const QString RDP_USE_IMMIDIATLY_POST_PROCESS_BY_ADD                = "PRL/USE_IMMIDIATLY_POST_PROCESS_BY_ADD";
    const QString ALPHA_FILTER_COEFFICIENT                              = "PRL/FILTER/ALPHA_COEFFICENT";
    const QString BETA_FILTER_COEFFICIENT                               = "PRL/FILTER/BETA_COEFFICENT";
    const QString GAMMA_FILTER_USING                                    = "PRL/FILTER/GAMMA_USING";
    const QString VR_COMPUTE_VELOCITY                                   = "PRL/VR_COMPUTE_VELOCITY";
    const QString RDP_PREDICTION_PERIOD_COEFFICIENT                     = "RDP/PREDICTION_PERIOD_COEFFICIENT";

    const QString RLS_REPORT_HEADER_NUMBER                              = "RLS_REPORT_HEADER/NUMBER";
    const QString RLS_REPORT_HEADER_CALLSIGN                            = "RLS_REPORT_HEADER/CALLSIGN";
    const QString RLS_REPORT_HEADER_ADDR24BIT                           = "RLS_REPORT_HEADER/ADDR24BIT";
    const QString RLS_REPORT_HEADER_SQUAWK                              = "RLS_REPORT_HEADER/SQUAWK";
    const QString RLS_REPORT_HEADER_UVD_REPLAY                          = "RLS_REPORT_HEADER/UVD_REPLAY";
    const QString RLS_REPORT_HEADER_ALTITUDE                            = "RLS_REPORT_HEADER/ALTITUDE";
    const QString RLS_REPORT_HEADER_DISTANCE                            = "RLS_REPORT_HEADER/DISTANCE";
    const QString RLS_REPORT_HEADER_AZIMUTH_DEVIATION                   = "RLS_REPORT_HEADER/AZIMUTH_DEVIATION";
    const QString RLS_REPORT_HEADER_POINST_COUNT                        = "RLS_REPORT_HEADER/POINST_COUNT";
    const QString RLS_REPORT_HEADER_COL_pO                              = "RLS_REPORT_HEADER/COL_pO";
    const QString RLS_REPORT_HEADER_COL_pN                              = "RLS_REPORT_HEADER/COL_pN";
    const QString RLS_REPORT_HEADER_COL_pH                              = "RLS_REPORT_HEADER/COL_pH";
    const QString RLS_REPORT_HEADER_SKO_RHO                             = "RLS_REPORT_HEADER/SKO_RHO";
    const QString RLS_REPORT_HEADER_SKO_THETA                           = "RLS_REPORT_HEADER/SKO_THETA";
    const QString RLS_REPORT_HEADER_ADSB_PO1_SEC                        = "RLS_REPORT_HEADER/ADSB_PO1_SEC";
    const QString RLS_REPORT_HEADER_ADSB_PO4_SEC                        = "RLS_REPORT_HEADER/ADSB_PO4_SEC";
    const QString RLS_REPORT_HEADER_ADSB_PO10_SEC                       = "RLS_REPORT_HEADER/ADSB_PO10_SEC";
    const QString RLS_REPORT_HEADER_ADSB_NUCp                           = "RLS_REPORT_HEADER/ADSB_NUCp";
    const QString RLS_REPORT_HEADER_STRINGS[] =                         {RLS_REPORT_HEADER_NUMBER,          RLS_REPORT_HEADER_CALLSIGN,
                                                                         RLS_REPORT_HEADER_ADDR24BIT,       RLS_REPORT_HEADER_SQUAWK,
                                                                         RLS_REPORT_HEADER_UVD_REPLAY,      RLS_REPORT_HEADER_ALTITUDE,
                                                                         RLS_REPORT_HEADER_DISTANCE,        RLS_REPORT_HEADER_AZIMUTH_DEVIATION,
                                                                         RLS_REPORT_HEADER_POINST_COUNT,    RLS_REPORT_HEADER_COL_pO,
                                                                         RLS_REPORT_HEADER_COL_pN,          RLS_REPORT_HEADER_COL_pH,
                                                                         RLS_REPORT_HEADER_SKO_RHO,         RLS_REPORT_HEADER_SKO_THETA,
                                                                         RLS_REPORT_HEADER_ADSB_PO1_SEC,    RLS_REPORT_HEADER_ADSB_PO4_SEC,
                                                                         RLS_REPORT_HEADER_ADSB_PO10_SEC,   RLS_REPORT_HEADER_ADSB_NUCp
                                                                        };

    const QString MLAT_REPORT_HEADER_NUMBER                              = "MLAT_REPORT_HEADER/NUMBER";
    const QString MLAT_REPORT_HEADER_CALLSIGN                            = "MLAT_REPORT_HEADER/CALLSIGN";
    const QString MLAT_REPORT_HEADER_ADDR24BIT                           = "MLAT_REPORT_HEADER/ADDR24BIT";
    const QString MLAT_REPORT_HEADER_SQUAWK                              = "MLAT_REPORT_HEADER/SQUAWK";
    const QString MLAT_REPORT_HEADER_ALTITUDE                            = "MLAT_REPORT_HEADER/ALTITUDE";
    const QString MLAT_REPORT_HEADER_POINST_COUNT                        = "MLAT_REPORT_HEADER/POINST_COUNT";
    const QString MLAT_REPORT_HEADER_COL_pU                              = "MLAT_REPORT_HEADER/COL_pU";
    const QString MLAT_REPORT_HEADER_STRINGS[] =                        {MLAT_REPORT_HEADER_NUMBER,         MLAT_REPORT_HEADER_CALLSIGN,
                                                                         MLAT_REPORT_HEADER_ADDR24BIT,      MLAT_REPORT_HEADER_SQUAWK,
                                                                         MLAT_REPORT_HEADER_ALTITUDE,       MLAT_REPORT_HEADER_POINST_COUNT,
                                                                         MLAT_REPORT_HEADER_COL_pU
                                                                        };

    const QString SDPD_REPORT_HEADER_NUMBER                              = "SDPD_REPORT_HEADER/NUMBER";
    const QString SDPD_REPORT_HEADER_CALLSIGN                            = "SDPD_REPORT_HEADER/CALLSIGN";
    const QString SDPD_REPORT_HEADER_ADDR24BIT                           = "SDPD_REPORT_HEADER/ADDR24BIT";
    const QString SDPD_REPORT_HEADER_SQUAWK                              = "SDPD_REPORT_HEADER/SQUAWK";
    const QString SDPD_REPORT_HEADER_ALTITUDE                            = "SDPD_REPORT_HEADER/ALTITUDE";
    const QString SDPD_REPORT_HEADER_DISTANCE                            = "SDPD_REPORT_HEADER/DISTANCE";
    const QString SDPD_REPORT_HEADER_POINST_COUNT                        = "SDPD_REPORT_HEADER/POINST_COUNT";
    const QString SDPD_REPORT_HEADER_COL_pU                              = "SDPD_REPORT_HEADER/COL_pU";
    const QString SDPD_REPORT_HEADER_STRINGS[] =                        {SDPD_REPORT_HEADER_NUMBER,         SDPD_REPORT_HEADER_CALLSIGN,
                                                                         SDPD_REPORT_HEADER_ADDR24BIT,      SDPD_REPORT_HEADER_SQUAWK,
                                                                         SDPD_REPORT_HEADER_ALTITUDE,       SDPD_REPORT_HEADER_DISTANCE,
                                                                         SDPD_REPORT_HEADER_POINST_COUNT,   SDPD_REPORT_HEADER_COL_pU
                                                                        };

    const QString MIN_INTERSECTED_POINTS                                = "MIN_INTERSECTED_POINTS";
    const QString APPROXIMATE_POINTS_FOR_CHEBYSHEW                      = "APPROXIMATE_POINTS_FOR_CHEBYSHEW";
    const QString CHEBYSHEV_SOURCE_TIME                                 = "CHEBYSHEV_SOURCE_TIME";

    const QString AIRCRAFT_AVG_VERTICAL_SPEED                           = "AIRCRAFT_AVG_VERTICAL_SPEED";

    const QString MISMATCH_REPORT_SHOW_P_A                              = "MISMATCH_REPORT_SHOW/P_A";
    const QString MISMATCH_REPORT_SHOW_P_C                              = "MISMATCH_REPORT_SHOW/P_C";
    const QString MISMATCH_REPORT_SHOW_P_H                              = "MISMATCH_REPORT_SHOW/P_H";

    const QString UNSTABLE_ZONE_WIDTH_PERCENT                           = "UNSTABLE_ZONE_WIDTH_PERCENT";
    const QString CUT_BYTES                                             = "CUT_BYTES";

    const QString MLAT_RLS_TYPE                                         = "MLAT/RLS_TYPE";
    const QString MLAT_ZONE_TYPE                                        = "MLAT/ZONE_TYPE";
    const QString MLAT_LOCATION_ZONE_COORDS                             = "MLAT/LOCATION_ZONE_COORDS";
    const QString MLAT_AIRDROME_UPDATE_DELTA_MS                         = "MLAT/AIRDROME_UPDATE_DELTA_MS";
    const QString MLAT_TRACK_UPDATE_DELTA_MS                            = "MLAT/TRACK_UPDATE_DELTA_MS";
    const QString MLAT_ALTITUDE_FROM_M                                  = "MLAT/ALTITUDE_FROM_M";
    const QString MLAT_ALTITUDE_TO_M                                    = "MLAT/ALTITUDE_TO_M";
}

class Settings: public QObject
{
    Q_OBJECT

public:
    static Settings &instance();

    void createDefaultSettings();
    static QVariant value(QString key);
    static QVariant value(QString key, QVariant defaultValue);
    static void setValue(QString key, QVariant value);
    bool contains(QString key);
    void clearToTests();

    void removeGroup(QString groupName);
    void emitChangeEvent(QString settingKey);

signals:
    void settingChanged(QString settingKey);

private:
    Settings();
    Settings(const QString &confFileName);
    Settings(const Settings& root);
    Settings& operator=(const Settings&);

    void _setDefaultValue(QString name, QVariant value);

private:
     QSettings  _settings;
};



#endif

