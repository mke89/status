#ifndef MLATSTATISTICS_H
#define MLATSTATISTICS_H

#include "trackprofile.h"

class MlatStatistics
{
public:
    enum DefaultParams
    {
        MIN_TRACKS_COUNT                = 50,
        AIRDROME_ZONE_UPDATE_TIME_SEC   = 4,
        TRACK_ZONE_UPDATE_TIME_SEC      = 8,
    };

    MlatStatistics();
    static int mlatUpdateTimeMsecs(int zoneType);
    static int mlatDeltaUpdateTimeMsecs(int zoneType);
    static double calcPU(const TrackProfile &tracksProfile, int updateTimeMs, int deltaUpdateTime);
};

#endif // MLATSTATISTICS_H
