#include "LocatorType.h"


LocatorUtils::LocatorUtils()
{

}

LocatorUtils::LocatorType LocatorUtils::getLocatorTypeByCaption(const QString &caption)
{
    if(caption == LocatorTypeCaption::rlsCaption)
    {
        return LocatorType::RLS;
    }

    if(caption == LocatorTypeCaption::mlatCaption)
    {
        return LocatorType::MLAT;
    }

    if(caption == LocatorTypeCaption::sdpdCaption)
    {
        return LocatorType::SDPD;
    }

    return LocatorType::Undefined;
}

QStringList LocatorUtils::getLocatorsTypes()
{
    return QStringList{LocatorTypeCaption::rlsCaption,
                       LocatorTypeCaption::mlatCaption,
                       LocatorTypeCaption::sdpdCaption};
}

int LocatorUtils::filterLocatorIndex(int index)
{
    if((index < 0) || (index >= LocatorType::ModesCount))
    {
        return LocatorType::RLS;
    }

    return index;
}

QString LocatorUtils::getLocatorCaptionByType(LocatorUtils::LocatorType locatorType)
{
    if(locatorType == LocatorType::RLS)
    {
        return LocatorTypeCaption::rlsCaption;
    }

    if(locatorType == LocatorType::MLAT)
    {
        return LocatorTypeCaption::mlatCaption;
    }

    if(locatorType == LocatorType::SDPD)
    {
        return LocatorTypeCaption::sdpdCaption;
    }

    return LocatorTypeCaption::undefinedCaption;
}



