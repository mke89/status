#include "InformationMismatchWidget.h"
#include "ui_InformationMismatchWidget.h"
#include "MapStrings.h"
#include "MapActions.h"
#include <QMenu>
#include <QMessageBox>
#include "pointlistwidget.h"
#include "MismatchReportWidget.h"
#include "settings.h"

InformationMismatchWidget::InformationMismatchWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InformationMismatchWidget)
{
    ui->setupUi(this);
    ui->tableView->setContextMenuPolicy(Qt::CustomContextMenu);

    connect(ui->tableView, SIGNAL(customContextMenuRequested(QPoint)), SLOT(showCustomContextMenu(QPoint)));

    model = new QStandardItemModel();
    sorter = new InformationMismatchSortModel();
    model->setHorizontalHeaderLabels(QStringList{"№", "ID трека 1", "P(код A)", "P(Callsign)", "P(высота)",
                                                      "ID трека 2", "P(код A)", "P(Callsign)", "P(высота)",
                                                 "Количество\nпересекающихся\nточек"});

    sorter->setSourceModel(model);
    ui->tableView->setModel(sorter);
    ui->tableView->setSortingEnabled(true);
    ui->tableView->setColumnWidth(MismatchColumns::IntersectedPoints, 150);
    ui->tableView->setAlternatingRowColors(true);
    ui->tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
}

void InformationMismatchWidget::showCustomContextMenu(QPoint position)
{
    QTableView *view = dynamic_cast<QTableView*>(sender());
    if(!view) return;

    QModelIndex index = ui->tableView->indexAt(position);
    QMenu *context = new QMenu(this);

    QAction *showBothTracksOnMap = context->addAction("Показать на карте оба пересекающихся трека");
    QAction *showTracks1OnMap = context->addAction("Показать на карте пересекающийся трек 1");
    QAction *showTracks2OnMap = context->addAction("Показать на карте пересекающийся трек 2");
    QAction *showOriginalTracksOnMap = context->addAction("Показать на карте оба исходных трека");
    QAction *showOriginalTrack1OnMap = context->addAction("Показать на карте исходный трек 1");
    QAction *showOriginalTrack2OnMap = context->addAction("Показать на карте исходный трек 2");
    QAction *showPointsList = context->addAction("Показать списки точек пересекающихся треков");

    context->popup(ui->tableView->viewport()->mapToGlobal(position));
    QAction *result = context->exec();
    delete context;

    if(!index.isValid())
    {
        QMessageBox::information(this, "Внимание!", "Произошла ошибка чтения данных из модели!");
        return;
    }

    QVariant var = sorter->data(sorter->index(index.row(), MismatchColumns::IntersectionNumber), Qt::UserRole + 3);
    QPair<MismatchInfoType::IntersectedTrack, MismatchInfoType::IntersectedTrack> *intersectedTracks =
            (QPair<MismatchInfoType::IntersectedTrack, MismatchInfoType::IntersectedTrack> *)var.toULongLong();
    if(!intersectedTracks)
    {
        QMessageBox::information(this, "Внимание!", "Произошла ошибка при досупе к треку!");
        return;
    }

    if(result == showBothTracksOnMap)
    {
        showIntersectedTracksOnMap(intersectedTracks, TrackType::BOTH);
    }
    else if(result == showTracks1OnMap)
    {
        showIntersectedTracksOnMap(intersectedTracks, TrackType::FIRST);
    }
    else if(result == showTracks2OnMap)
    {
        showIntersectedTracksOnMap(intersectedTracks, TrackType::SECOND);
    }
    else if(result == showOriginalTracksOnMap)
    {
        const QList<int> &tracksIdList = getOriginalTracksIds(intersectedTracks, TrackType::BOTH);
        emit showOriginalTracks(tracksIdList);
    }
    else if(result == showOriginalTrack1OnMap)
    {
        const QList<int> &tracksIdList = getOriginalTracksIds(intersectedTracks, TrackType::FIRST);
        emit showOriginalTracks(tracksIdList);
    }
    else if(result == showOriginalTrack2OnMap)
    {
        const QList<int> &tracksIdList = getOriginalTracksIds(intersectedTracks, TrackType::SECOND);
        emit showOriginalTracks(tracksIdList);
    }
    else if(result == showPointsList)
    {
        showIntersectedTracksPointsList(intersectedTracks);
    }
}

void InformationMismatchWidget::showIntersectedTracksPointsList(const QPair<MismatchInfoType::IntersectedTrack,
                                                                            MismatchInfoType::IntersectedTrack> *intersectedTracks)
{
    PointListWidget *plw1 = new PointListWidget();
    if(!plw1)
    {
        return;
    }

    PointListWidget *plw2 = new PointListWidget();
    if(!plw1)
    {
        return;
    }

    TrackProfile trackProfile1;
    MismatchInfoType::SquawkProb squawkProb1;
    MismatchInfoType::CallsignProb callsignProb1;
    MismatchInfoType::AltitudeProb altitudeProb1;

    TrackProfile trackProfile2;
    MismatchInfoType::SquawkProb squawkProb2;
    MismatchInfoType::CallsignProb callsignProb2;
    MismatchInfoType::AltitudeProb altitudeProb2;

    std::tie(trackProfile1, squawkProb1, callsignProb1, altitudeProb1) = intersectedTracks->first;
    std::tie(trackProfile2, squawkProb2, callsignProb2, altitudeProb2) = intersectedTracks->second;

    if(trackProfile1.points[0].category == 48)
    {
        plw1->showCat48TrackId();
    }
    plw1->setTrackId(trackProfile1.trackKey.trackNumber);
    plw1->setProfile(trackProfile1, true, false);
    plw1->show();

    if(trackProfile2.points[0].category == 48)
    {
        plw2->showCat48TrackId();
    }
    plw2->setTrackId(trackProfile2.trackKey.trackNumber);
    plw2->setProfile(trackProfile2, true, false);
    plw2->show();
}

QList<int> InformationMismatchWidget::getOriginalTracksIds(const QPair<MismatchInfoType::IntersectedTrack,
                                                                       MismatchInfoType::IntersectedTrack> *intersectedTracks, TrackType track)
{
    QList<int> tracksIdList;

    TrackProfile trackProfile1;
    MismatchInfoType::SquawkProb squawkProb1;
    MismatchInfoType::CallsignProb callsignProb1;
    MismatchInfoType::AltitudeProb altitudeProb1;

    TrackProfile trackProfile2;
    MismatchInfoType::SquawkProb squawkProb2;
    MismatchInfoType::CallsignProb callsignProb2;
    MismatchInfoType::AltitudeProb altitudeProb2;

    if(track == TrackType::BOTH)
    {
        std::tie(trackProfile1, squawkProb1, callsignProb1, altitudeProb1) = intersectedTracks->first;
        std::tie(trackProfile2, squawkProb2, callsignProb2, altitudeProb2) = intersectedTracks->second;

        tracksIdList << trackProfile1.trackKey.trackNumber << trackProfile2.trackKey.trackNumber;
    }
    else if(track == TrackType::FIRST)
    {
        std::tie(trackProfile1, squawkProb1, callsignProb1, altitudeProb1) = intersectedTracks->first;
        tracksIdList << trackProfile1.trackKey.trackNumber;
    }
    else if(track == TrackType::SECOND)
    {
        std::tie(trackProfile2, squawkProb2, callsignProb2, altitudeProb2) = intersectedTracks->second;
        tracksIdList << trackProfile2.trackKey.trackNumber;
    }

    return tracksIdList;
}

void InformationMismatchWidget::showIntersectedTracksOnMap(const QPair<MismatchInfoType::IntersectedTrack,
                                                                       MismatchInfoType::IntersectedTrack> *intersectedTracks, TrackType track)
{
    TrackProfile trackProfile1;
    MismatchInfoType::SquawkProb squawkProb1;
    MismatchInfoType::CallsignProb callsignProb1;
    MismatchInfoType::AltitudeProb altitudeProb1;

    TrackProfile trackProfile2;
    MismatchInfoType::SquawkProb squawkProb2;
    MismatchInfoType::CallsignProb callsignProb2;
    MismatchInfoType::AltitudeProb altitudeProb2;

    if(track == TrackType::BOTH)
    {
        std::tie(trackProfile1, squawkProb1, callsignProb1, altitudeProb1) = intersectedTracks->first;
        std::tie(trackProfile2, squawkProb2, callsignProb2, altitudeProb2) = intersectedTracks->second;

        MapActions::Instance().showTrackOnMap(trackProfile1);
        MapActions::Instance().showTrackOnMap(trackProfile2);
    }
    else if(track == TrackType::FIRST)
    {
        std::tie(trackProfile1, squawkProb1, callsignProb1, altitudeProb1) = intersectedTracks->first;
        MapActions::Instance().showTrackOnMap(trackProfile1);
    }
    else if(track == TrackType::SECOND)
    {
        std::tie(trackProfile2, squawkProb2, callsignProb2, altitudeProb2) = intersectedTracks->second;
        MapActions::Instance().showTrackOnMap(trackProfile2);
    }
}

InformationMismatchWidget::~InformationMismatchWidget()
{
    delete ui;
}

void InformationMismatchWidget::addTracksIntersectionInfo(QList<QPair<MismatchInfoType::IntersectedTrack,
                                                                      MismatchInfoType::IntersectedTrack>> intersectedTracksList)
{
    this->intersectedTracks = intersectedTracksList;

    if(intersectedTracks.isEmpty())
    {
        return;
    }

    bool showPa = Settings::value(SettingsKeys::MISMATCH_REPORT_SHOW_P_A).toBool();
    bool showPc = Settings::value(SettingsKeys::MISMATCH_REPORT_SHOW_P_C).toBool();
    bool showPh = Settings::value(SettingsKeys::MISMATCH_REPORT_SHOW_P_H).toBool();

    QStandardItem *item;
    double squawkProbSumm = 0;
    double callsignProbSumm = 0;
    double altitudeProbSumm = 0;
    QColor track1Clor(0xeb, 0xf0, 0xe0);
    QColor track2Clor(0xdc, 0xf4, 0xef);
    for(int i = 0; i < intersectedTracks.size(); i++)
    {
        TrackProfile trackProfile1;
        MismatchInfoType::SquawkProb squawkProb1;
        MismatchInfoType::CallsignProb callsignProb1;
        MismatchInfoType::AltitudeProb altitudeProb1;

        TrackProfile trackProfile2;
        MismatchInfoType::SquawkProb squawkProb2;
        MismatchInfoType::CallsignProb callsignProb2;
        MismatchInfoType::AltitudeProb altitudeProb2;

        std::tie(trackProfile1, squawkProb1, callsignProb1, altitudeProb1) = intersectedTracks[i].first;
        std::tie(trackProfile2, squawkProb2, callsignProb2, altitudeProb2) = intersectedTracks[i].second;

        squawkProbSumm += (squawkProb1 + squawkProb2);
        callsignProbSumm += (callsignProb1 + callsignProb2);
        altitudeProbSumm += (altitudeProb1 + altitudeProb2);

        item = new QStandardItem();
        item->setData(QString::number(i), Qt::DisplayRole);
        item->setData(i, Qt::UserRole + 2);
        item->setData((quint64)(&intersectedTracks[i]), Qt::UserRole + 3);
        item->setTextAlignment(Qt::AlignCenter);
        model->setItem(i, MismatchColumns::IntersectionNumber, item);

        item = new QStandardItem();
        item->setData(QString::number(trackProfile1.trackKey.trackNumber), Qt::DisplayRole);
        item->setData(trackProfile1.trackKey.trackNumber, Qt::UserRole + 2);
        item->setData(track1Clor, Qt::BackgroundRole);
        item->setTextAlignment(Qt::AlignCenter);

        model->setItem(i, MismatchColumns::Track1Id, item);

        if(showPa)
        {
            item = new QStandardItem();
            item->setData(MapGeometry::fromDouble(squawkProb1, 3), Qt::DisplayRole);
            item->setData(squawkProb1, Qt::UserRole + 2);
            item->setData(track1Clor, Qt::BackgroundRole);
            item->setTextAlignment(Qt::AlignCenter);
            model->setItem(i, MismatchColumns::Pa1, item);

            item = new QStandardItem();
            item->setData(MapGeometry::fromDouble(squawkProb2, 3), Qt::DisplayRole);
            item->setData(squawkProb2, Qt::UserRole + 2);
            item->setData(track2Clor, Qt::BackgroundRole);
            item->setTextAlignment(Qt::AlignCenter);
            model->setItem(i, MismatchColumns::Pa2, item);
        }

        if(showPc)
        {
            item = new QStandardItem();
            item->setData(MapGeometry::fromDouble(callsignProb1, 3), Qt::DisplayRole);
            item->setData(callsignProb1, Qt::UserRole + 2);
            item->setData(track1Clor, Qt::BackgroundRole);
            item->setTextAlignment(Qt::AlignCenter);
            model->setItem(i, MismatchColumns::Pc1, item);

            item = new QStandardItem();
            item->setData(MapGeometry::fromDouble(callsignProb2, 3), Qt::DisplayRole);
            item->setData(callsignProb2, Qt::UserRole + 2);
            item->setData(track2Clor, Qt::BackgroundRole);
            item->setTextAlignment(Qt::AlignCenter);
            model->setItem(i, MismatchColumns::Pc2, item);
        }

        if(showPh)
        {
            item = new QStandardItem();
            item->setData(MapGeometry::fromDouble(altitudeProb1, 3), Qt::DisplayRole);
            item->setData(callsignProb1, Qt::UserRole + 2);
            item->setData(track1Clor, Qt::BackgroundRole);
            item->setTextAlignment(Qt::AlignCenter);
            model->setItem(i, MismatchColumns::Ph1, item);

            item = new QStandardItem();
            item->setData(MapGeometry::fromDouble(altitudeProb2, 3), Qt::DisplayRole);
            item->setData(callsignProb2, Qt::UserRole + 2);
            item->setData(track2Clor, Qt::BackgroundRole);
            item->setTextAlignment(Qt::AlignCenter);
            model->setItem(i, MismatchColumns::Ph2, item);
        }

        item = new QStandardItem();
        item->setData(QString::number(trackProfile2.trackKey.trackNumber), Qt::DisplayRole);
        item->setData(trackProfile2.trackKey.trackNumber, Qt::UserRole + 2);
        item->setData(track2Clor, Qt::BackgroundRole);
        item->setTextAlignment(Qt::AlignCenter);
        model->setItem(i, MismatchColumns::Track2Id, item);

        item = new QStandardItem();
        item->setData(QString::number(trackProfile1.points.size()), Qt::DisplayRole);
        item->setData(trackProfile1.points.size(), Qt::UserRole + 2);
        item->setTextAlignment(Qt::AlignCenter);
        model->setItem(i, MismatchColumns::IntersectedPoints, item);
    }

    if(!showPa)
    {
        ui->tableView->setColumnHidden(MismatchColumns::Pa1, true);
        ui->tableView->setColumnHidden(MismatchColumns::Pa2, true);
        ui->l_paAvg->setHidden(true);
        ui->le_paAvg->setHidden(true);
    }
    else
    {
        ui->le_paAvg->setText(MapGeometry::fromDouble(squawkProbSumm / (intersectedTracks.size() * 2), 4));
    }

    if(!showPc)
    {
        ui->tableView->setColumnHidden(MismatchColumns::Pc1, true);
        ui->tableView->setColumnHidden(MismatchColumns::Pc2, true);
        ui->l_pcAvg->setHidden(true);
        ui->le_pcAvg->setHidden(true);
    }
    else
    {
        ui->le_pcAvg->setText(MapGeometry::fromDouble(callsignProbSumm / (intersectedTracks.size() * 2), 4));
    }

    if(!showPh)
    {
        ui->tableView->setColumnHidden(MismatchColumns::Ph1, true);
        ui->tableView->setColumnHidden(MismatchColumns::Ph2, true);
        ui->l_phAvg->setHidden(true);
        ui->le_phAvg->setHidden(true);
    }
    else
    {
        ui->le_phAvg->setText(MapGeometry::fromDouble(altitudeProbSumm / (intersectedTracks.size() * 2), 4));
    }
}

void InformationMismatchWidget::on_tb_showReport_clicked()
{
    MismatchReportWidget *reportWidget = new MismatchReportWidget();
    if(!reportWidget)
    {
        QMessageBox::information(this, "Внимание!", "Ошибка при создании отчета!");
        return;
    }
    reportWidget->show();
    reportWidget->addTracksIntersectionInfo(intersectedTracks);
}

void InformationMismatchWidget::on_tb_showIntersectedTracks_clicked()
{
    QPair<MismatchInfoType::IntersectedTrack, MismatchInfoType::IntersectedTrack> *intersectedTracks = getSelectedRow();
    if(!intersectedTracks)
    {
        return;
    }

    showIntersectedTracksOnMap(intersectedTracks, TrackType::BOTH);
}

void InformationMismatchWidget::on_tb_showIntersectedTrack1_clicked()
{
    QPair<MismatchInfoType::IntersectedTrack, MismatchInfoType::IntersectedTrack> *intersectedTracks = getSelectedRow();
    if(!intersectedTracks)
    {
        return;
    }

    showIntersectedTracksOnMap(intersectedTracks, TrackType::FIRST);
}

void InformationMismatchWidget::on_tb_showIntersectedTrack2_clicked()
{
    QPair<MismatchInfoType::IntersectedTrack, MismatchInfoType::IntersectedTrack> *intersectedTracks = getSelectedRow();
    if(!intersectedTracks)
    {
        return;
    }

    showIntersectedTracksOnMap(intersectedTracks, TrackType::SECOND);
}

void InformationMismatchWidget::on_tb_showOriginalTrack1_clicked()
{
    QPair<MismatchInfoType::IntersectedTrack, MismatchInfoType::IntersectedTrack> *intersectedTracks = getSelectedRow();
    if(!intersectedTracks)
    {
        return;
    }
    const QList<int> &tracksIdList = getOriginalTracksIds(intersectedTracks, TrackType::FIRST);
    emit showOriginalTracks(tracksIdList);
}

void InformationMismatchWidget::on_tb_showOriginalTrack2_clicked()
{
    QPair<MismatchInfoType::IntersectedTrack, MismatchInfoType::IntersectedTrack> *intersectedTracks = getSelectedRow();
    if(!intersectedTracks)
    {
        return;
    }
    const QList<int> &tracksIdList = getOriginalTracksIds(intersectedTracks, TrackType::SECOND);
    emit showOriginalTracks(tracksIdList);
}

void InformationMismatchWidget::on_tb_showPointsList_clicked()
{
    QPair<MismatchInfoType::IntersectedTrack, MismatchInfoType::IntersectedTrack> *intersectedTracks = getSelectedRow();
    if(!intersectedTracks)
    {
        return;
    }
    showIntersectedTracksPointsList(intersectedTracks);
}

QPair<MismatchInfoType::IntersectedTrack, MismatchInfoType::IntersectedTrack> *InformationMismatchWidget::getSelectedRow()
{
    QPair<MismatchInfoType::IntersectedTrack, MismatchInfoType::IntersectedTrack> *intersectedTracks = nullptr;

    QModelIndexList selectedRows = ui->tableView->selectionModel()->selectedRows(MismatchColumns::IntersectionNumber);
    if(selectedRows.size() == 0)
    {
        QString message = "Не выбрано ни одного трека! Укажите интересующий трек.";
        QMessageBox::information(this, "Внимание!", message);
        return intersectedTracks;
    }

    if(selectedRows.size() > 1)
    {
        QString message = "Выбрано несколько треков! Выберите один трек.";
        QMessageBox::information(this, "Внимание!", message);
        return intersectedTracks;
    }

    QVariant var = sorter->data(sorter->index(selectedRows[0].row(), MismatchColumns::IntersectionNumber), Qt::UserRole + 3);
    intersectedTracks = (QPair<MismatchInfoType::IntersectedTrack, MismatchInfoType::IntersectedTrack> *)var.toULongLong();
    if(!intersectedTracks)
    {
        QMessageBox::information(this, "Внимание!", "Произошла ошибка при досупе к треку!");
        return intersectedTracks;
    }

    return intersectedTracks;
}

void InformationMismatchWidget::on_tb_deleteSelectedStrings_clicked()
{
    QModelIndexList selectedRows = ui->tableView->selectionModel()->selectedRows(MismatchColumns::IntersectionNumber);
    if(selectedRows.size() == 0)
    {
        QString message = "Не выбрано ни одного трека! Укажите интересующий трек.";
        QMessageBox::information(this, "Внимание!", message);
        return;
    }

    QPair<MismatchInfoType::IntersectedTrack, MismatchInfoType::IntersectedTrack> *selectedTracks = nullptr;
    for(const QModelIndex& index: selectedRows)
    {
        QVariant var = sorter->data(sorter->index(index.row(), MismatchColumns::IntersectionNumber), Qt::UserRole + 3);
        selectedTracks = (QPair<MismatchInfoType::IntersectedTrack, MismatchInfoType::IntersectedTrack> *)var.toULongLong();
        if(!selectedTracks)
        {
            continue;
        }

        TrackProfile trackProfile1;
        MismatchInfoType::SquawkProb squawkProb1;
        MismatchInfoType::CallsignProb callsignProb1;
        MismatchInfoType::AltitudeProb altitudeProb1;

        TrackProfile trackProfile2;
        MismatchInfoType::SquawkProb squawkProb2;
        MismatchInfoType::CallsignProb callsignProb2;
        MismatchInfoType::AltitudeProb altitudeProb2;

        std::tie(trackProfile1, squawkProb1, callsignProb1, altitudeProb1) = selectedTracks->first;
        std::tie(trackProfile2, squawkProb2, callsignProb2, altitudeProb2) = selectedTracks->second;

        for(int i = 0; i < intersectedTracks.size(); i++)
        {
            TrackProfile currTrackProfile1;
            TrackProfile currTrackProfile2;
            MismatchInfoType::SquawkProb squawkProb;
            MismatchInfoType::CallsignProb callsignProb;
            MismatchInfoType::AltitudeProb altitudeProb;

            std::tie(currTrackProfile1, squawkProb, callsignProb, altitudeProb) = intersectedTracks[i].first;
            std::tie(currTrackProfile2, squawkProb, callsignProb, altitudeProb) = intersectedTracks[i].second;

            if((currTrackProfile1.trackKey.trackNumber == trackProfile1.trackKey.trackNumber) &&
               (currTrackProfile2.trackKey.trackNumber == trackProfile2.trackKey.trackNumber))
            {
                intersectedTracks.removeAt(i);

                QModelIndex sourceIndex = sorter->mapToSource(index);
                model->takeRow(sourceIndex.row());
                break;
            }
        }
    }
}
