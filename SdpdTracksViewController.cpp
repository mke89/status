#include "SdpdTracksViewController.h"
#include "settings.h"
#include "MapStrings.h"
#include "adsbutils.h"

void SdpdTracksViewController::processColumnsHiding()
{
    const SdpdHidedColumns *sdpdHidedColumns = dynamic_cast<const SdpdHidedColumns*>(hidedColumns);
    const SdpdModelColumns *sdpdModelColumns = dynamic_cast<const SdpdModelColumns*>(locatorModelColumns);

    bool hideSquawk = tableView->isColumnHidden(sdpdModelColumns->Squawk) && sdpdHidedColumns->hideSquawk;
    bool hideAddr24Bit = tableView->isColumnHidden(sdpdModelColumns->Addr24Bit) && sdpdHidedColumns->hideAddr24Bit;
    bool hideCallsign = tableView->isColumnHidden(sdpdModelColumns->Callsign) && sdpdHidedColumns->hideCallsign;

    tableView->setColumnHidden(sdpdModelColumns->Squawk, hideSquawk);
    tableView->setColumnHidden(sdpdModelColumns->Addr24Bit, hideAddr24Bit);
    tableView->setColumnHidden(sdpdModelColumns->Callsign, hideCallsign);
}

void SdpdTracksViewController::addTrackToTable(const TrackProfile &trackProfile, int index)
{
    const SdpdModelColumns *sdpdModelColumns = dynamic_cast<const SdpdModelColumns*>(locatorModelColumns);
    SdpdHidedColumns *sdpdHidedColumns = dynamic_cast<SdpdHidedColumns*>(hidedColumns);
    int trackNumber = index;

    int squawk = trackProfile.reply.size() > 0  ? trackProfile.reply.last() : 0;
    const auto direction = trackProfile.direction;

    QStandardItem *item;
    item = new QStandardItem();
    item->setData(QString::number(trackProfile.trackKey.trackNumber), Qt::DisplayRole);
    //item->setData(trackProfiles.directionToString(), Qt::ToolTipRole);
    item->setData((quint64)(&trackProfile), Qt::UserRole + 2);
    item->setData(trackProfile.directionToRussianString(), Qt::UserRole + 3);
    item->setTextAlignment(Qt::AlignCenter);

    if(direction == TrackProfile::DEPARTURE)
    {
        item->setBackground(QColor(179, 236, 255));
    }
    else if(direction == TrackProfile::ARRIVAL)
    {
        item->setBackground(QColor(194, 169, 122));
    }
    else if(direction == TrackProfile::TRANSIT)
    {
        item->setBackground(QColor(189, 189, 189));
    }
    else if(direction == TrackProfile::GROUND)
    {
        item->setBackground(QColor(219, 240, 204));
    }
    else if(direction == TrackProfile::NONE)
    {
        item->setBackground(QColor(255, 255, 255));
    }

    model->setItem(trackNumber, sdpdModelColumns->HiddenService, item);

    item = new QStandardItem();
    item->setData(QString("%1").arg(squawk, 4, 10, QChar('0')), Qt::DisplayRole);
    item->setData(squawk, Qt::UserRole + 2);
    item->setTextAlignment(Qt::AlignCenter);
    model->setItem(trackNumber, sdpdModelColumns->Squawk, item);
    if(squawk != 0) sdpdHidedColumns->hideSquawk = false;

    item = new QStandardItem();
    item->setData(QString::number(trackProfile.address24bit, 16), Qt::DisplayRole);
    item->setData(trackProfile.address24bit, Qt::UserRole + 2);
    model->setItem(trackNumber, sdpdModelColumns->Addr24Bit, item);
    if(trackProfile.address24bit != 0) sdpdHidedColumns->hideAddr24Bit = false;

    item = new QStandardItem();
    item->setData(trackProfile.callsign, Qt::DisplayRole);
    model->setItem(trackNumber, sdpdModelColumns->Callsign, item);
    if(!trackProfile.callsign.isEmpty()) sdpdHidedColumns->hideCallsign = false;

    item = new QStandardItem();
    double verticalSpeedMS = Settings::value(SettingsKeys::AIRCRAFT_AVG_VERTICAL_SPEED).toDouble();
    const QList<double> &altitudes = Statistics::MismatchInfo::filterAltitudeGlitches(trackProfile.points, verticalSpeedMS);
    double minAltitude = trackProfile.minAltitude(altitudes);
    double maxAltitude = trackProfile.maxAltitude(altitudes);
    item->setData(trackProfile.minMaxAltitudeToString(minAltitude, maxAltitude), Qt::DisplayRole);
    item->setData(minAltitude, Qt::UserRole + 2);
    item->setData(maxAltitude, Qt::UserRole + 3);
    item->setTextAlignment(Qt::AlignCenter);
    model->setItem(trackNumber, sdpdModelColumns->Altitude, item);

    item = new QStandardItem();
    item->setData(QString::number(trackProfile.lengthKm), Qt::DisplayRole);
    item->setData(trackProfile.lengthKm, Qt::UserRole + 2);
    model->setItem(trackNumber, sdpdModelColumns->TotalLength, item);

    item = new QStandardItem();
    item->setData(trackProfile.startDate.toString("dd.MM.yyyy hh:mm:ss"), Qt::DisplayRole);
    item->setData(trackProfile.startDate.toMSecsSinceEpoch()/1000, Qt::UserRole + 2);
    model->setItem(trackNumber, sdpdModelColumns->StartDate, item);

    item = new QStandardItem();
    QString pointsCount = QString("%1 / %2").arg(trackProfile.points.size()).
                                             arg(trackProfile.reconstructedTrajectory.size());
    //item->setData( QString::number(trackProfile.points.size()), Qt::DisplayRole);
    item->setData( pointsCount, Qt::DisplayRole);
    item->setData( trackProfile.points.size(), Qt::UserRole + 2);
    item->setTextAlignment(Qt::AlignCenter);
    model->setItem(trackNumber, sdpdModelColumns->PointsCount, item);
}

void SdpdTracksViewController::fillView()
{
    if((!model) || (!tableView) || (!trackProfiles))
    {
        return;
    }

    const SdpdModelColumns *sdpdModelColumns = dynamic_cast<const SdpdModelColumns*>(locatorModelColumns);
    const QStringList &tableHeaders = sdpdModelColumns->headersList();
    model->clear();
    model->setRowCount(0);
    model->setColumnCount(tableHeaders.size());
    model->setHorizontalHeaderLabels(tableHeaders);

    for(int column = 0; column < (int)ViewsColumns::Sdpd::ColumnsCount; column++)
    {
        model->horizontalHeaderItem(column)->setToolTip(sdpdModelColumns->tooltips[column]);
        tableView->setColumnWidth(column, sdpdModelColumns->columnsWidth[column]);
    }

    for(int i = 0; i < trackProfiles->size(); i++)
    {
        addTrackToTable((*trackProfiles)[i], i);
    }

    SdpdHidedColumns *sdpdHidedColumns = dynamic_cast<SdpdHidedColumns*>(hidedColumns);
    tableView->setColumnHidden(sdpdModelColumns->Squawk, sdpdHidedColumns->hideSquawk);
    tableView->setColumnHidden(sdpdModelColumns->Addr24Bit, sdpdHidedColumns->hideAddr24Bit);
    tableView->setColumnHidden(sdpdModelColumns->Callsign, sdpdHidedColumns->hideCallsign);

    tableView->setEditTriggers(QAbstractItemView::NoEditTriggers);
    tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
}

void SdpdTracksViewController::processRbsClicked()
{

}

void SdpdTracksViewController::processUvdClicked()
{

}

void SdpdTracksViewController::processAdsbClicked(bool flag)
{
    Q_UNUSED(flag);
}
