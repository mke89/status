#ifndef ASSESSMENTSORTMODEL_H
#define ASSESSMENTSORTMODEL_H

#include <QWidget>
#include <QSortFilterProxyModel>
#include "SettingsDialog.h"
#include "trackprofile.h"

namespace StatisticsColumns
{
    enum StatisticsColumns
    {
        HiddenService=0,
        UvdReplay,
        Squawk,
        Addr24Bit,
        Callsign,
        Altitude,
        Distance,
        AzimuthDeviation,
        TotalLength,
        StartDate,
        PointsCount,
        PO,
        PN,
        PH,
        SkoRho,
        SkoTheta,
        pO1,
        pO4,
        pO10,
        NUCp,
        ColumnsCount
    };

    struct HidedFlgs
    {
        bool hideUvdReplay;
        bool hideSquawk;
        bool hideAddr24Bit;
        bool hideCallsign;

        HidedFlgs(): hideUvdReplay(true), hideSquawk(true),
                     hideAddr24Bit(true), hideCallsign(true)
        {}
    };
}

class SortModel: public QSortFilterProxyModel
{
    Q_OBJECT

public:
    SortModel();
    void setFilterSettings(StatusSettings settings);

public slots:
    void refilter(QList<TrackType::TrackType> filters);

private:
    bool lessThan(const QModelIndex &source_left, const QModelIndex &source_right) const override;
    bool filterAcceptsRow(int source_row, const QModelIndex &source_parent) const override;

private:
    QList<TrackType::TrackType> passFilters;
    StatusSettings filterSettings;
};

#endif // ASSESSMENTSORTMODEL_H
