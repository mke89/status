#include "InformationMismatchSortModel.h"

InformationMismatchSortModel::InformationMismatchSortModel()
{

}

InformationMismatchSortModel::~InformationMismatchSortModel()
{
}

bool InformationMismatchSortModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    const QAbstractItemModel* const srcModel = sourceModel();
    if(!srcModel) return false;

    QVariant leftData;
    QVariant rightData;

    leftData  = srcModel->data(left, Qt::UserRole + 2);
    rightData = srcModel->data(right, Qt::UserRole + 2);

    if(!(leftData.isValid() && rightData.isValid()))
    {
        leftData = srcModel->data(left, Qt::DisplayRole);
        rightData = srcModel->data(right, Qt::DisplayRole);
    }

    if(leftData.type() == QVariant::Int)
    {
        return leftData.toInt() < rightData.toInt();
    }

    if(leftData.type() == QVariant::Double)
    {
        return leftData.toDouble() < rightData.toDouble();
    }

    return leftData < rightData;
}


