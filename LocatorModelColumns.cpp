#include "LocatorModelColumns.h"

LocatorModelColumns::LocatorModelColumns()
{
    HiddenService = 0;
    headers[HiddenService] = "#";
    tooltips[HiddenService] = "Номер трека";
    columnsWidth[HiddenService] = 70;
}

QStringList RlsModelColumns::headersList() const
{
    QStringList tableHeaders;
    tableHeaders << headers[HiddenService] << headers[UvdReplay] << headers[Squawk]
                 << headers[Addr24Bit] << headers[Callsign] << headers[Altitude]
                 << headers[Distance] << headers[AzimuthDeviation] << headers[TotalLength]
                 << headers[StartDate] << headers[PointsCount] << headers[PO] << headers[PN]
                 << headers[PH] << headers[SkoRho] << headers[SkoTheta] << headers[pO1]
                 << headers[pO4] << headers[pO10] << headers[NUCp];


    return tableHeaders;
}

RlsModelColumns::RlsModelColumns():
    LocatorModelColumns(),
    UvdReplay((int)ViewsColumns::Rls::UvdReplay),
    Squawk((int)ViewsColumns::Rls::Squawk),
    Addr24Bit((int)ViewsColumns::Rls::Addr24Bit),
    Callsign((int)ViewsColumns::Rls::Callsign),
    Altitude((int)ViewsColumns::Rls::Altitude),
    Distance((int)ViewsColumns::Rls::Distance),
    AzimuthDeviation((int)ViewsColumns::Rls::AzimuthDeviation),
    TotalLength((int)ViewsColumns::Rls::TotalLength),
    StartDate((int)ViewsColumns::Rls::StartDate),
    PointsCount((int)ViewsColumns::Rls::PointsCount),
    PO((int)ViewsColumns::Rls::PO),
    PN((int)ViewsColumns::Rls::PN),
    PH((int)ViewsColumns::Rls::PH),
    SkoRho((int)ViewsColumns::Rls::SkoRho),
    SkoTheta((int)ViewsColumns::Rls::SkoTheta),
    pO1((int)ViewsColumns::Rls::pO1),
    pO4((int)ViewsColumns::Rls::pO4),
    pO10((int)ViewsColumns::Rls::pO10),
    NUCp((int)ViewsColumns::Rls::NUCp),
    ColumnsCount((int)ViewsColumns::Rls::ColumnsCount)
{
    headers[UvdReplay] = "Код УВД";
    tooltips[UvdReplay] = "Код УВД";
    columnsWidth[HiddenService] = 100;

    headers[Squawk] = "Код А";
    tooltips[Squawk] = "Код А";
    columnsWidth[Squawk] = 80;

    headers[Addr24Bit] = "24-бит адр.";
    tooltips[Addr24Bit] = "24-х битный адрес";
    columnsWidth[Addr24Bit] = 130;

    headers[Callsign] = "Номер\nрейса";
    tooltips[Callsign] = "Номер рейса";
    columnsWidth[Callsign] = 130;

    headers[Altitude] = "Высота полета, м\n(мин / макс)";
    tooltips[Altitude] = "Высота полета, м(мин / макс)";
    columnsWidth[Altitude] = 140;

    headers[Distance] = "Дальность км\n(мин / макс)";
    tooltips[Distance] = "Дальность км(мин / макс)";
    columnsWidth[Distance] = 140;

    headers[AzimuthDeviation] = "Азимут, гр\nцентр, +/-";
    tooltips[AzimuthDeviation] = "Азимут, гр центр, +/-";
    columnsWidth[AzimuthDeviation] = 140;

    headers[TotalLength] = "Длина\nтрека, км";
    tooltips[TotalLength] = "Длина трека, км";
    columnsWidth[TotalLength] = 130;

    headers[StartDate] = "Время начала";
    tooltips[StartDate] = "Время начала";
    columnsWidth[StartDate] = 160;

    headers[PointsCount] = "Количество\nточек";
    tooltips[PointsCount] = "Количество точек";
    columnsWidth[PointsCount] = 120;

    headers[PO] = "P(O)";
    tooltips[PO] = "Вероятность пропуска отчетов";
    columnsWidth[PO] = 60;

    headers[PN] = "P(N)";
    tooltips[PN] = "Вероятность пропуска кода А";
    columnsWidth[PN] = 60;

    headers[PH] = "P(H)";
    tooltips[PH] = "Вероятность пропуска высоты (режим С)";
    columnsWidth[PH] = 60;

    headers[SkoRho] = "СКО\nдистанции, м";
    tooltips[SkoRho] = "СКО дистанции, м";
    columnsWidth[SkoRho] = 120;

    headers[SkoTheta] = "СКО азимута,\nминут";
    tooltips[SkoTheta] = "СКО азимута, минут";
    columnsWidth[SkoTheta] = 120;

    headers[pO1] = "P(O ⩽ 1 сек.)";
    tooltips[pO1] = "Вероятность обновления ⩽ 1 сек";
    columnsWidth[pO1] = 120;

    headers[pO4] = "P(O ⩽ 4 сек.)";
    tooltips[pO4] = "Вероятность обновления ⩽ 4 сек";
    columnsWidth[pO4] = 120;

    headers[pO10] = "P(O ⩽ 10 сек.)";
    tooltips[pO10] = "Вероятность обновления ⩽ 10 сек";
    columnsWidth[pO10] = 120;

    headers[NUCp] = "Неопределенность\nнавигации\n(мин / макс)";
    tooltips[NUCp] = "Неопределенность навигации (мин / макс)";
    columnsWidth[NUCp] = 150;
}

QStringList MlatModelColumns::headersList() const
{
    QStringList tableHeaders;
    tableHeaders << headers[HiddenService] << headers[Squawk] << headers[Addr24Bit]
                 << headers[Callsign] << headers[Altitude] << headers[TotalLength]
                 << headers[StartDate] << headers[PointsCount] << headers[TrackUpdateTime];
    return tableHeaders;
}

MlatModelColumns::MlatModelColumns():
    LocatorModelColumns(),
    Squawk((int)ViewsColumns::Mlat::Squawk),
    Addr24Bit((int)ViewsColumns::Mlat::Addr24Bit),
    Callsign((int)ViewsColumns::Mlat::Callsign),
    Altitude((int)ViewsColumns::Mlat::Altitude),
    TotalLength((int)ViewsColumns::Mlat::TotalLength),
    StartDate((int)ViewsColumns::Mlat::StartDate),
    PointsCount((int)ViewsColumns::Mlat::PointsCount),
    TrackUpdateTime((int)ViewsColumns::Mlat::TrackUpdateTime)
{
    headers[Squawk] = "Код А";
    tooltips[Squawk] = "Код А";
    columnsWidth[Squawk] = 80;

    headers[Addr24Bit] = "24-бит адр.";
    tooltips[Addr24Bit] = "24-х битный адрес";
    columnsWidth[Addr24Bit] = 130;

    headers[Callsign] = "Номер\nрейса";
    tooltips[Callsign] = "Номер рейса";
    columnsWidth[Callsign] = 130;

    headers[Altitude] = "Высота полета, м\n(мин / макс)";
    tooltips[Altitude] = "Высота полета, м(мин / макс)";
    columnsWidth[Altitude] = 140;

    headers[TotalLength] = "Длина\nтрека, км";
    tooltips[TotalLength] = "Длина трека, км";
    columnsWidth[TotalLength] = 130;

    headers[StartDate] = "Время начала";
    tooltips[StartDate] = "Время начала";
    columnsWidth[StartDate] = 160;

    headers[PointsCount] = "Количество\nточек";
    tooltips[PointsCount] = "Количество точек";
    columnsWidth[PointsCount] = 120;

    headers[TrackUpdateTime] = "PU";
    tooltips[TrackUpdateTime] = "Вероятность достижения интервала обновления";
    columnsWidth[TrackUpdateTime] = 120;
}

QStringList SdpdModelColumns::headersList() const
{
    QStringList tableHeaders;
    tableHeaders << headers[HiddenService] << headers[Squawk] << headers[Addr24Bit]
                 << headers[Callsign] << headers[Altitude] << headers[TotalLength]
                 << headers[StartDate] << headers[PointsCount];

    return tableHeaders;
}

SdpdModelColumns::SdpdModelColumns():
    LocatorModelColumns(),
    Squawk((int)ViewsColumns::Sdpd::Squawk),
    Addr24Bit((int)ViewsColumns::Sdpd::Addr24Bit),
    Callsign((int)ViewsColumns::Sdpd::Callsign),
    Altitude((int)ViewsColumns::Sdpd::Altitude),
    TotalLength((int)ViewsColumns::Sdpd::TotalLength),
    StartDate((int)ViewsColumns::Sdpd::StartDate),
    PointsCount((int)ViewsColumns::Sdpd::PointsCount)
{
    headers[Squawk] = "Код А";
    tooltips[Squawk] = "Код А";
    columnsWidth[Squawk] = 80;

    headers[Addr24Bit] = "24-бит адр.";
    tooltips[Addr24Bit] = "24-х битный адрес";
    columnsWidth[Addr24Bit] = 130;

    headers[Callsign] = "Номер\nрейса";
    tooltips[Callsign] = "Номер рейса";
    columnsWidth[Callsign] = 130;

    headers[Altitude] = "Высота полета, м\n(мин / макс)";
    tooltips[Altitude] = "Высота полета, м(мин / макс)";
    columnsWidth[Altitude] = 140;

    headers[TotalLength] = "Длина\nтрека, км";
    tooltips[TotalLength] = "Длина трека, км";
    columnsWidth[TotalLength] = 130;

    headers[StartDate] = "Время начала";
    tooltips[StartDate] = "Время начала";
    columnsWidth[StartDate] = 160;

    headers[PointsCount] = "Количество\nточек";
    tooltips[PointsCount] = "Количество точек";
    columnsWidth[PointsCount] = 120;
}



