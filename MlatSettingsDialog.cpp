#include "MlatSettingsDialog.h"
#include "ui_MlatSettingsDialog.h"
#include "settings.h"
#include "FlightZoneUtils.h"
#include "gpoint.h"
#include <QDebug>
#include <QMessageBox>
#include <QHostInfo>
#include "xprotohashpacker.h"
#include "MapSpherical.h"
#include "channelkeeper.h"

MlatSettingsDialog::MlatSettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MlatSettingsDialog)
{
    ui->setupUi(this);

    ui->cb_flightZone->addItems(FlightZoneUtils::getZonesTypes());
    loadSettings();
}

MlatSettingsDialog::~MlatSettingsDialog()
{
    delete ui;
}

void MlatSettingsDialog::on_pb_apply_clicked()
{
    MlatSettings settings = saveSettings();
    emit sendUpdatedSettings(settings);
    accept();
}

bool MlatSettingsDialog::correctCoord(const QString &coord) const
{
    GPoint pointCoords(coord);
    if((!pointCoords.isLatLonValid()) || pointCoords.isNull())
    {
        return false;
    }

    return true;
}

QStringList MlatSettingsDialog::coordinatesList(const QString &coordsString)
{
    //QRegExp separator("\\s+");
    QRegExp separator("[;\r\n\t ]+");
    return coordsString.split(separator, Qt::SkipEmptyParts);
}

int MlatSettingsDialog::findIncorrectCoordNumber(const QStringList &coordsList)
{
    for(int coordNum = 0; coordNum < coordsList.size(); coordNum++)
    {
        if(!correctCoord(coordsList[coordNum]))
        {
            return coordNum + 1;
        }
    }

    return 0;
}

void MlatSettingsDialog::on_pb_cancel_clicked()
{
    reject();
}

void MlatSettingsDialog::on_cb_flightZone_activated(const QString &arg1)
{
    if(arg1 == ZoneTypeCaption::trackCaption)
    {
        ui->l_mlatUpdateTimeSecs->setText("8");
    }
    else if(arg1 == ZoneTypeCaption::airdromeCaption)
    {
        ui->l_mlatUpdateTimeSecs->setText("4");
    }
}

void MlatSettingsDialog::loadSettings()
{
    MlatSettings settings;
    ui->cb_flightZone->setCurrentIndex(settings.flightZoneType);
    if(settings.flightZoneType == FlightZoneUtils::ZoneType::TRACK)
    {
        ui->l_mlatUpdateTimeSecs->setText("8");
    }
    else
    {
        ui->l_mlatUpdateTimeSecs->setText("4");
    }
    ui->te_coordinates->setText(settings.locationZoneCoords);
    ui->le_altitudeFromM->setText(QString::number(settings.altitudeFromM));
    ui->le_altitudeToM->setText(QString::number(settings.altitudeToM));
    ui->le_deltaUpdateTimeAirdromeMs->setText(QString::number(settings.airdromeUpdateDeltaTimeMs));
    ui->le_deltaUpdateTimeTrackMs->setText(QString::number(settings.trackUpdateDeltaTimeMs));
}

MlatSettings MlatSettingsDialog::saveSettings()
{
    MlatSettings settings;
    settings.flightZoneType = ui->cb_flightZone->currentIndex();

    const QString &coords = ui->te_coordinates->toPlainText();
    int incorrectCoord = findIncorrectCoordNumber(coordinatesList(coords));
    if(incorrectCoord)
    {
        QMessageBox::warning(this, "Внимание!", QString("Некорректное значение координаты с номером %1").arg(incorrectCoord));
    }
    else
    {
        settings.locationZoneCoords = coords;
    }

    settings.altitudeFromM = ui->le_altitudeFromM->text().toDouble();
    settings.altitudeToM = ui->le_altitudeToM->text().toDouble();
    settings.airdromeUpdateDeltaTimeMs = ui->le_deltaUpdateTimeAirdromeMs->text().toDouble();
    settings.trackUpdateDeltaTimeMs = ui->le_deltaUpdateTimeTrackMs->text().toDouble();

    settings.save();
    return settings;
}

void MlatSettingsDialog::on_pb_showMlatZone_clicked()
{
    QStringList coordsList = coordinatesList(ui->te_coordinates->toPlainText());

    if(coordsList.size() <= 2)
    {
        QMessageBox::warning(this, "Внимание!", QString("В полигоне должно быть более 2-х точек"));
    }

    int incorrectCoord = findIncorrectCoordNumber(coordsList);

    if(incorrectCoord)
    {
        QMessageBox::warning(this, "Внимание!", QString("Некорректное значение координаты с номером %1").arg(incorrectCoord));
        return;
    }

    xProtoHashPacker hpCommand(300);
    hpCommand.addData("I300/010", "type", "TO_MAP_OBJECT_PUT");
    hpCommand.addData("I300/020", "object_type", "FLIGHT_ROUTE");
    hpCommand.addData("I300/060", "host_name", QHostInfo::localHostName());
    hpCommand.addData("I300/060", "app_name", "packet_inspector");

    static QStringList stylesList;
    static int styleNumber = 0;
    if(stylesList.isEmpty())
    {
        stylesList  <<  "Style 01";
        stylesList  <<  "Style 02";
        stylesList  <<  "Style 03";
        stylesList  <<  "Style 04";
        stylesList  <<  "Style 05";
        stylesList  <<  "Style 06";
        stylesList  <<  "Style 07";
        stylesList  <<  "Style 08";
        stylesList  <<  "Style 09";
    }

    QString styleName = stylesList.value((styleNumber++) % 9);
    qDebug() << styleName;
    QString title = "MLAT zone region";
    bool withLine = true;
    bool withFormulars = false;
    xProtoHashPacker hpTrackData(312);

    QString planId = QString("status_%1").arg(styleName);
    hpTrackData.addData("I312/010", "id", planId);
    hpTrackData.addData("I312/040", "style", styleName);
    hpTrackData.addData("I312/020", "acid", title);
    hpTrackData.addData("I312/090", "skeleton", !withLine);

    xProtoHashPacker hpRouteElement(313);
    for(const QString &stringCoord: coordsList)
    {
        GPoint point(stringCoord);
        if(!point.isLatLonValid())
        {
            continue;
        }

        if(MapGeometry::isNan(point.latitude()) || MapGeometry::isNan(point.longitude()))
        {
            continue;
        }

        hpRouteElement.addData("I313/010", "element_type", "POINT");
        hpRouteElement.addData("I313/030", "lat", point.latitude());
        hpRouteElement.addData("I313/030", "lon", point.longitude());
        hpRouteElement.addData("I313/030", "line_width", 25);

        hpRouteElement.addData("I313/020", "name", "Зона наблюдения МПСН");
        hpRouteElement.addData("I313/070", "show_form", withFormulars); //0 = off formular

        hpRouteElement.addNewRecord();
    }

    hpTrackData.addExplicitData("I312/060", hpRouteElement.getPackedData());

    QByteArray packet = hpCommand.getPackedData();
    packet.append(hpTrackData.getPackedData());
    ChannelKeeper::Instance().sendToMap(packet);
}
