#ifndef POINTSCONTAINER_H
#define POINTSCONTAINER_H
#include <QDateTime>
#include <QPointF>
#include "structs.h"
#include "settings.h"
#include "StereoProjection.h"

void SetTestMode(bool testMode);

class DebugHalt
{
    static bool stop;

public:
    static void setStopFlag()
    {
        stop = true;
    }

    static void clearStopFlag()
    {
        stop = false;
    }

    static bool isStop()
    {
        return stop;
    }

    static void nop()
    {
        bool prevFlag = stop;
        stop = prevFlag;
    }

};

struct PointsContainer
{
    enum ContainerType
    {
        UNMARKED        = 0,
        SINGLE_POINT    = 1,
        POTENTIAL_TRACK = 2,
        TRACK           = 3
    };

    enum ContainerParams
    {
        MAX_POINTS = 100,
    };

    static constexpr double MIN_SUBSONIC_PLANE_SPEED_KM_H = 300.;
    static constexpr double MAX_SUBSONIC_PLANE_SPEED_KM_H = 1035.;
    static constexpr double MAX_SUPERSONIC_PLANE_SPEED_KM_H = 3000.;

    static constexpr double KM_H_TO_M_S_DEVIDER = 3.6;
    static constexpr double MIN_SUBSONIC_PLANE_SPEED_M_S = MIN_SUBSONIC_PLANE_SPEED_KM_H / KM_H_TO_M_S_DEVIDER;
    static constexpr double MAX_SUBSONIC_PLANE_SPEED_M_S = MAX_SUBSONIC_PLANE_SPEED_KM_H / KM_H_TO_M_S_DEVIDER;
    static constexpr double MAX_SUPERSONIC_PLANE_SPEED_M_S = MAX_SUPERSONIC_PLANE_SPEED_KM_H / KM_H_TO_M_S_DEVIDER;

    ContainerType type;
    quint16 unicContainerId;
    double course;
    double speed_m_s;
    QDateTime lastUpdateDate;
    SummNearestAvgPoints nearestAvg;

    double alphaFilterCoeff;
    double betaFilterCoeff;

    double SEARCH_FORWARD_DIST_COEFF;
    double SEARCH_FORWARD_DIST_MIN_COEFF;
    double SEARCH_BACKWARD_DIST_COEFF;
    double SEARCH_BACKWARD_DIST_MAX_COEFF;
    double SEARCH_RADIUS_MULTIPLIER_COEFF;//0.9//0.85;//1.01;
    double RADIUS_REGION_SUITABLE_COEFF;
    double NEAR_REGION_SUITABLE_COEFF;
    double FORWARD_REGION_SUITABLE_COEFF;
    double BACK_REGION_SUITABLE_COEFF;
    double ADDITIONAL_POINT_BELONGING_LEN_COEFF;
    int CURVATURE_SENSITIVITY_MULTIPLIER_COEFF;
    int MIN_POINT_FOR_CURVATURE_CALCULATION;
    int MAX_PREDICTIONS;
    double MULTI_POINT_MERGE_RADIUS_M;
    double maxSearchRadius;
    int    uninterrupredPredictionsCount;

    QList<SimplePoint> points;
    QList<SimplePoint> pretenderPoints;
    RadarIdentificator radarId;
    PositionFilter predictionFilter;
    QList<double> previousAvgDistance_m;
    StereoProjection projection;

    PointsContainer(){}
    PointsContainer(RadarIdentificator radarKey, quint16 trackId);

    double getPreviosAvgDist();
    double getDistToPreviousPoint();

    QPointF projectionPoint(QPointF a, QPointF b, QPointF s);
    bool pointBelongsToLineSegment(QPointF a, QPointF b, QPointF point);

    bool pointSuitsToRadiusRegion(SimplePoint newPoint, double searchRadius, double squawkMultiplier);
    bool pointSuitsToForwardRegion(SimplePoint newPoint, double searchRadius);
    bool pointSuitsToBackRegion(SimplePoint newPoint, double searchRadius);
    bool pointSuitsToRegion(SimplePoint newPoint, double searchRadius, double startingPoitMultiplier, double finalPoitMultiplier);
    bool pointSuitsByNearRegion(SimplePoint newPoint, double searchRadius);

    double unmarkedPointSuitable(SimplePoint &newPoint);
    double singlePointSuitable(SimplePoint &newPoint);
    double potentialTrackSuitable(SimplePoint &newPoint);
    double trackSuitable(SimplePoint &newPoint);

    void addCoords(const SimplePoint &newPoint);

    double isPointSuitable(SimplePoint &newPoint);
    void addPoint(SimplePoint newPoint);
    int getNextPointRadiusSearchInMeters(const double distanceTravelledMeters, const double travelTimeSeconds);
    double getInitialSearchRadiusMeters(const double distanceTravelledMeters, const double travelTimeSeconds);
    double getCurvatureCoefficient();

    void trimmPoinstLists();
    void recalcCourseParams();
    double getNewSpeed();

    int getMinSubSonicPlaneMovementMeters(void);
    int getMaxSubSonicPlaneMovementMeters(void);
    int getMaxSuperSonicPlaneMovementMeters(void);

    void modifyLastPoint();
    double checkPointSuitability(SimplePoint &point, double searchRadius);

    friend QDataStream &operator<<(QDataStream &stream, const PointsContainer &pointsContainer);
    friend QDataStream &operator>>(QDataStream &stream, PointsContainer &pointsContainer);

};

#endif // POINTSCONTAINER_H
