#ifndef SPOINT_H
#define SPOINT_H
#include <QString>
#include <QDateTime>
#include <QSharedPointer>
//#include "MapPoint.h"


struct SPoint
{
    enum PointClass
    {
        UNDEFINED   = 0,
        REFLECTED   = 1,
        UNSTABLE    = 2,
        FILTERED    = 3,
        FAKE        = 4,
    };

    struct RadarReport
    {
        public:
        int                 messageType; // 1-north mark
        int                 sic;
        int                 sac;
        double              timeOfDay; //seconds
        double              sectorNumber; // азимут
        double              antennaRotationSpeed; // seconds
        QDateTime           timestamp;
        double              lat;
        double              lon;
        double              alt;


        RadarReport();
    };

    struct QualityIdentificators
    {
        int NUCp; // он-же NIC
        int NACp;
        int SIL ;
        int PIC ;

        QualityIdentificators(): NUCp(0), NACp(0), SIL (0), PIC (0) {}
        bool hasNACp() const;
        bool hasNUCp() const;
        bool hasSIL() const;
        bool hasPIC() const;
        bool noQualityBits() const;
        QString toString() const;
    };

    int         trackId;
    int         category;
    int         sac;
    int         sic;
    double      rho; // nmi
    double      theta; // grad
    double      flightLevel;
    double      altitude; // meters
    uint16_t    reply;
    bool        replayValideted;
    uint        uvdReplay;
    uint32_t    address24bit;
    double      latitude;
    double      longitude;
    double      dateTime; // seconds since midnight
    QString     callsign;
    QDateTime   reciveTimestamp;
    QByteArray  rawByteArray;
    bool        s_ehs;
    bool        gbs;
    QualityIdentificators quality;
    QSharedPointer<RadarReport> radareport;
    PointClass  pointClass;
    SPoint();
    bool isValid() const;
    bool isRadarReport() const;
    bool isDateTimeValid() const;
    QString pointClassToString() const;
};
#endif // SPOINT_H
