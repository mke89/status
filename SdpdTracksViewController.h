#ifndef SDPDTRACKSVIEWCONTROLLER_H
#define SDPDTRACKSVIEWCONTROLLER_H

#include "LocatorsTracksViewController.h"

class SdpdTracksViewController: public LocatorsTracksViewController
{
    public:
        SdpdTracksViewController() = delete;
        SdpdTracksViewController(QTableView *tableView, LocatorModelColumns *locatorModelColumns,
                                 HidedColumns *hidedColumns, QStandardItemModel *model,
                                 QList<TrackProfile> *trackProfiles):
            LocatorsTracksViewController(tableView, locatorModelColumns, hidedColumns,
                                         model, trackProfiles)
        {}
        ~SdpdTracksViewController(){}
        virtual void processColumnsHiding();
        virtual void addTrackToTable(const TrackProfile &trackProfile, int index);
        virtual void fillView();
        virtual void processRbsClicked();
        virtual void processUvdClicked();
        virtual void processAdsbClicked(bool flag);
};

#endif // SDPDTRACKSVIEWCONTROLLER_H
