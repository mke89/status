#include "settings.h"

#include <QDebug>
#include <QStringList>
#include <QDir>
#include <QRect>
#include <QString>
#include <QByteArray>
#include <QCoreApplication>
#include "LocatorUtils.h"
#include "FlightZoneUtils.h"

Settings::Settings() :
    _settings("Vniira", QCoreApplication::applicationName())
{
    createDefaultSettings();
}

Settings::Settings(const QString &confFileName) :
    _settings("Vniira", confFileName)
{
    createDefaultSettings();
}

Settings &Settings::instance()
{
    static Settings instance(QCoreApplication::applicationName());
    return instance;
}

void Settings::_setDefaultValue(QString name, QVariant value)
{
    if(_settings.contains(name))
    {
        return;
    }
    _settings.setValue(name, value);
}

void Settings::createDefaultSettings()
{
    _setDefaultValue(SettingsKeys::LAST_TAB_INDEX,   1);
    _setDefaultValue(SettingsKeys::MAIN_TIMER_DELAY,   200);
    _setDefaultValue(SettingsKeys::PORT,               20034);
    _setDefaultValue(SettingsKeys::IP_ADDRESS,         "127.0.0.1");
    _setDefaultValue(SettingsKeys::FROM_MAP_PORT,      35002);
    _setDefaultValue(SettingsKeys::MAP_IP_PORT,        "127.0.0.1:35001");
    _setDefaultValue(SettingsKeys::LAST_DIR_TO_OPEN,   QDir::currentPath());
    _setDefaultValue(SettingsKeys::LAST_DIR_TO_SAVE,   QDir::currentPath());
    _setDefaultValue(SettingsKeys::MAIN_WINDOW_POS,    QRect());
    _setDefaultValue(SettingsKeys::HV_MAX_DMODEL_SIZE, 2000);
    _setDefaultValue(SettingsKeys::HV_BYTES_FOR_CUT,   0);
    _setDefaultValue(SettingsKeys::HV_CATEGORY,        QString());
    _setDefaultValue(SettingsKeys::HV_SPLITTER,        QByteArray());
    _setDefaultValue(SettingsKeys::TRK_MAX_RTMODEL_SIZE,  2000);
    _setDefaultValue(SettingsKeys::TRK_DELTA_SECONDS,  0);
    _setDefaultValue(SettingsKeys::TRK_CATEGORY,       QString());
    _setDefaultValue(SettingsKeys::TRK_SPLITTER,       QByteArray());
    _setDefaultValue(SettingsKeys::TRK_FLTR_KEY_SEQUENCE,  "TA_TN");  //if TargetAdderss is empty, then TrackNumber

    _setDefaultValue(SettingsKeys::RDR_NAME,           QString());
    _setDefaultValue(SettingsKeys::RDR_SAC,            int());
    _setDefaultValue(SettingsKeys::RDR_SIC,            int());
    _setDefaultValue(SettingsKeys::RDR_FORMAT,         QString());
    _setDefaultValue(SettingsKeys::RDR_UAP,            QString());
    _setDefaultValue(SettingsKeys::RDR_MAX_COV,        int());
    _setDefaultValue(SettingsKeys::RDR_FRAME_TYPE,     QString());
    _setDefaultValue(SettingsKeys::RDR_VERIFY,         bool());
    _setDefaultValue(SettingsKeys::RDR_MIN_LEV,        int());
    _setDefaultValue(SettingsKeys::RDR_MAX_LEV,        int());
    _setDefaultValue(SettingsKeys::RDR_LEV_PER_LAYER,  int());
    _setDefaultValue(SettingsKeys::RDR_DATA_DIR,       QString());
    _setDefaultValue(SettingsKeys::RDR_VERIFY_DATE,    bool());
    _setDefaultValue(SettingsKeys::RDR_VERIFY_AC,      bool());
    _setDefaultValue(SettingsKeys::RDR_VERIFY_SB,      bool());
    _setDefaultValue(SettingsKeys::RDR_VERIFY_SS,      bool());
    _setDefaultValue(SettingsKeys::RDR_VERIFY_SE,      bool());
    _setDefaultValue(SettingsKeys::RDR_VERIFY_LEVEL,   bool());
    _setDefaultValue(SettingsKeys::RDR_ROUTES_MAX_CALC_POINS, 4);
    _setDefaultValue(SettingsKeys::RDR_BYPASS_FILTERS, false);
    _setDefaultValue(SettingsKeys::RDR_USE_PROBABILITY_CALC, false);


    _setDefaultValue(SettingsKeys::LBL_CAT0102,         QString());
    _setDefaultValue(SettingsKeys::LBL_CAT2123,         QString());
    _setDefaultValue(SettingsKeys::LBL_CAT3448,         QString());
    _setDefaultValue(SettingsKeys::LBL_RANGE,           QString());
    _setDefaultValue(SettingsKeys::LBL_SACSIC,          QString());
    _setDefaultValue(SettingsKeys::LBL_ALTITUDE,        QString());

    _setDefaultValue(SettingsKeys::LAST_X_POS,          0);
    _setDefaultValue(SettingsKeys::LAST_Y_POS,          0);
    _setDefaultValue(SettingsKeys::LAST_INPUT_DIRECTORY, "/home");
    _setDefaultValue(SettingsKeys::TRACK_SPLIT_INTERVAL_SEC, 60);

    _setDefaultValue(SettingsKeys::CALCULATION_METHOD, 0);
    _setDefaultValue(SettingsKeys::WAVELETS_CALCULATION_METHOD, WaveletsMethod::BY_POINTS);

    _setDefaultValue(SettingsKeys::MIN_POINTS_FOR_SPLIT, 30);
    _setDefaultValue(SettingsKeys::MIN_PERCENT_FOR_SPLIT_ADD, 30.);
    _setDefaultValue(SettingsKeys::SHOW_ONLY_GOOD_TRACKS, false);
    _setDefaultValue(SettingsKeys::MIN_PROBABILITY_P_O, 0.98);
    _setDefaultValue(SettingsKeys::MIN_PROBABILITY_P_N, 0.98);
    _setDefaultValue(SettingsKeys::MIN_PROBABILITY_P_H, 0.98);
    _setDefaultValue(SettingsKeys::MAX_RMS_RHO, 3.);
    _setDefaultValue(SettingsKeys::MAX_RMS_THETA, 3.);
    _setDefaultValue(SettingsKeys::GOOD_TRACK_SIZE, 100);
    _setDefaultValue(SettingsKeys::RDR_THETA_STEP, 1);
    _setDefaultValue(SettingsKeys::RDR_RHO_STEP, 1);

    _setDefaultValue(SettingsKeys::ROTATION_SEC_MANUAL, 4 );
    _setDefaultValue(SettingsKeys::MAX_DISTANCE       , 250 );
    _setDefaultValue(SettingsKeys::MIN_DISTANCE       , 1 );
    _setDefaultValue(SettingsKeys::MIN_ALITUDE        , 300 );
    _setDefaultValue(SettingsKeys::MAX_ALITUDE        , 25000 );
    _setDefaultValue(SettingsKeys::BLIND_ANGLE        , 45 );
    _setDefaultValue(SettingsKeys::REFERENCE_POINT    , "" );

    _setDefaultValue(SettingsKeys::USE_TRACKS_VALIDATION, false);
    _setDefaultValue(SettingsKeys::RHO_VALIDATION_THRESHOLD, 2.0);
    _setDefaultValue(SettingsKeys::THETA_VALIDATION_THRESHOLD, 10.0);

    _setDefaultValue(SettingsKeys::NEW_TRACK_ON_RBS_CHANGE      , true);
    _setDefaultValue(SettingsKeys::NEW_TRACK_ON_DELTA_CHANGE    , true);
    _setDefaultValue(SettingsKeys::NEW_TRACK_DELTA_AZIMUTH      , 3);
    _setDefaultValue(SettingsKeys::MAX_LOCATOR_DISTANCE         , 470);
    _setDefaultValue(SettingsKeys::MIN_TRACK_POINTS_COUNT       , 10);

    _setDefaultValue(SettingsKeys::DISTANCE_THRESHOLD_KM        , 90);
    _setDefaultValue(SettingsKeys::TIME_THRESHOLD_SEC           , 600);
    _setDefaultValue(SettingsKeys::BIND_BY_TRACK_NUMBER         , false);


    _setDefaultValue(SettingsKeys::LOCATOR_POSITION, "592530N0300653E"); // "592530N0300653E" Деревня "Кобрино"
    _setDefaultValue(SettingsKeys::LOCATOR_POSITION_DESCRIPTION, "Деревня 'Кобрино'");
    _setDefaultValue(SettingsKeys::LOCATOR_ROTATION_PERIOD, 6100);
    _setDefaultValue(SettingsKeys::LOCATOR_ALTITUDE, 0);
    _setDefaultValue(SettingsKeys::CENTER_POINTS_LIST, "Кобрино:592530N0300653E:6100:0,");

    _setDefaultValue(SettingsKeys::WAVELET_SKO_USE_NORMAL_TO_REF_TRAJECTORY, false );
    _setDefaultValue(SettingsKeys::WAVELET_SKO_USE_POINT_TO_POINT_METHOD, true );
    _setDefaultValue(SettingsKeys::WAVELET_SKO_LEVEL_OF_DECOMPOSITION, 3 );

    _setDefaultValue(SettingsKeys::LAST_PSR_INPUT_DIRECTORY, "/home");
    _setDefaultValue(SettingsKeys::LAST_SSR_INPUT_DIRECTORY, "/home");
    _setDefaultValue(SettingsKeys::LAST_RDP_INPUT_DIRECTORY, "/home");


    _setDefaultValue(SettingsKeys::HISTORY_DEPTH, 50);
    _setDefaultValue(SettingsKeys::RDP_INITIAL_TRAJECTORY_DEVIATION_DEGREES_PER_SECOND, 3);
    _setDefaultValue(SettingsKeys::PC_SEARCH_FORWARD_DIST_COEFF, 2.0);
    _setDefaultValue(SettingsKeys::PC_SEARCH_FORWARD_DIST_MIN_COEFF, 1.1);
    _setDefaultValue(SettingsKeys::PC_SEARCH_BACKWARD_DIST_COEFF, 0.5);
    _setDefaultValue(SettingsKeys::PC_SEARCH_BACKWARD_DIST_MAX_COEFF, 0.9);
    _setDefaultValue(SettingsKeys::PC_SEARCH_RADIUS_MULTIPLIER_COEFF, 0.9);
    _setDefaultValue(SettingsKeys::PC_RADIUS_REGION_SUITABLE_COEFF, 1.0);
    _setDefaultValue(SettingsKeys::PC_FORWARD_REGION_SUITABLE_COEFF, 0.8);
    _setDefaultValue(SettingsKeys::PC_BACK_REGION_SUITABLE_COEFF, 0.8);
    _setDefaultValue(SettingsKeys::PC_ADDITIONAL_POINT_BELONGING_LEN_COEFF, 0.1);
    _setDefaultValue(SettingsKeys::PC_CURVATURE_SENSITIVITY_MULTIPLIER_COEFF, 10000);
    _setDefaultValue(SettingsKeys::PC_MIN_POINT_FOR_CURVATURE_CALCULATION, 5);
    _setDefaultValue(SettingsKeys::RDP_PREDICTIONS_BEFORE_DELETE, 3);
    _setDefaultValue(SettingsKeys::RDP_MULTIPLE_POINTS_TO_ONE_MERGE_RADIUS_M, 350.);
    _setDefaultValue(SettingsKeys::RDP_ENABLE_TRACK_FILTERING, false);
    _setDefaultValue(SettingsKeys::RDP_FORCE_END_TRAGECTORY_COEFFICENT, 4);
    _setDefaultValue(SettingsKeys::RDP_PREDICTION_PERIOD_MS, 500);
    _setDefaultValue(SettingsKeys::RDP_MIN_TRACKS_NUMBER, 32000);
    _setDefaultValue(SettingsKeys::RDP_MAX_TRACKS_NUMBER, 65530);
    _setDefaultValue(SettingsKeys::VR_MAX_TRACK_NUMBER, 31990);
    _setDefaultValue(SettingsKeys::VR_MIN_TRACK_NUMBER, 0);
    _setDefaultValue(SettingsKeys::RDP_USE_IMMIDIATLY_PROCESS_BY_ADD, true);
    _setDefaultValue(SettingsKeys::RDP_USE_IMMIDIATLY_POST_PROCESS_BY_ADD, true);
    _setDefaultValue(SettingsKeys::ALPHA_FILTER_COEFFICIENT, 0.7);
    _setDefaultValue(SettingsKeys::BETA_FILTER_COEFFICIENT, 0.5);
    _setDefaultValue(SettingsKeys::GAMMA_FILTER_USING, true);
    _setDefaultValue(SettingsKeys::VR_COMPUTE_VELOCITY, false);
    _setDefaultValue(SettingsKeys::RDP_PREDICTION_PERIOD_COEFFICIENT, 1.1);


    _setDefaultValue(SettingsKeys::RLS_REPORT_HEADER_NUMBER,            "#");
    _setDefaultValue(SettingsKeys::RLS_REPORT_HEADER_CALLSIGN,          "Позывной");
    _setDefaultValue(SettingsKeys::RLS_REPORT_HEADER_ADDR24BIT,         "24-бит адрес");
    _setDefaultValue(SettingsKeys::RLS_REPORT_HEADER_SQUAWK,            "Код режима\n\"А\"");
    _setDefaultValue(SettingsKeys::RLS_REPORT_HEADER_UVD_REPLAY,        "Код УВД");
    _setDefaultValue(SettingsKeys::RLS_REPORT_HEADER_ALTITUDE,          "Высота полета, м\n(мин / макс)");
    _setDefaultValue(SettingsKeys::RLS_REPORT_HEADER_DISTANCE,          "Дальность, км\n(мин / макс)");
    _setDefaultValue(SettingsKeys::RLS_REPORT_HEADER_AZIMUTH_DEVIATION, "Азимут, гр\nцентр, +/-");
    _setDefaultValue(SettingsKeys::RLS_REPORT_HEADER_POINST_COUNT,      "Кол-во точек");
    _setDefaultValue(SettingsKeys::RLS_REPORT_HEADER_COL_pO,            "P(O)");
    _setDefaultValue(SettingsKeys::RLS_REPORT_HEADER_COL_pN,            "P(N)");
    _setDefaultValue(SettingsKeys::RLS_REPORT_HEADER_COL_pH,            "P(H)");
    _setDefaultValue(SettingsKeys::RLS_REPORT_HEADER_SKO_RHO,           "СКО\nпо дальности, м");
    _setDefaultValue(SettingsKeys::RLS_REPORT_HEADER_SKO_THETA,         "СКО\nпо азимуту, минут");
    _setDefaultValue(SettingsKeys::RLS_REPORT_HEADER_ADSB_PO1_SEC,      "Вероятность обновления ⩽\n1 сек");
    _setDefaultValue(SettingsKeys::RLS_REPORT_HEADER_ADSB_PO4_SEC,      "Вероятность обновления ⩽\n4 сек");
    _setDefaultValue(SettingsKeys::RLS_REPORT_HEADER_ADSB_PO10_SEC,     "Вероятность обновления ⩽\n10 сек");
    _setDefaultValue(SettingsKeys::RLS_REPORT_HEADER_ADSB_NUCp,         "NUCp\n(min / max)");

    _setDefaultValue(SettingsKeys::MLAT_REPORT_HEADER_NUMBER,            "#");
    _setDefaultValue(SettingsKeys::MLAT_REPORT_HEADER_CALLSIGN,          "Позывной");
    _setDefaultValue(SettingsKeys::MLAT_REPORT_HEADER_ADDR24BIT,         "24-бит адрес");
    _setDefaultValue(SettingsKeys::MLAT_REPORT_HEADER_SQUAWK,            "Код режима\n\"А\"");
    _setDefaultValue(SettingsKeys::MLAT_REPORT_HEADER_ALTITUDE,          "Высота полета, м\n(мин / макс)");
    _setDefaultValue(SettingsKeys::MLAT_REPORT_HEADER_POINST_COUNT,      "Кол-во точек");
    _setDefaultValue(SettingsKeys::MLAT_REPORT_HEADER_COL_pU,            "P(U)");

    _setDefaultValue(SettingsKeys::SDPD_REPORT_HEADER_NUMBER,            "#");
    _setDefaultValue(SettingsKeys::SDPD_REPORT_HEADER_CALLSIGN,          "Позывной");
    _setDefaultValue(SettingsKeys::SDPD_REPORT_HEADER_ADDR24BIT,         "24-бит адрес");
    _setDefaultValue(SettingsKeys::SDPD_REPORT_HEADER_SQUAWK,            "Код режима\n\"А\"");
    _setDefaultValue(SettingsKeys::SDPD_REPORT_HEADER_ALTITUDE,          "Высота полета, м\n(мин / макс)");
    _setDefaultValue(SettingsKeys::SDPD_REPORT_HEADER_DISTANCE,          "Дальность, км\n(мин / макс)");
    _setDefaultValue(SettingsKeys::SDPD_REPORT_HEADER_POINST_COUNT,      "Кол-во точек");

    _setDefaultValue(SettingsKeys::MIN_INTERSECTED_POINTS, 50);
    _setDefaultValue(SettingsKeys::APPROXIMATE_POINTS_FOR_CHEBYSHEW, true);
    _setDefaultValue(SettingsKeys::CHEBYSHEV_SOURCE_TIME, ChebyshevSourceTime::TRACK_AVG_TIME);

    _setDefaultValue(SettingsKeys::AIRCRAFT_AVG_VERTICAL_SPEED, 20.0);

    _setDefaultValue(SettingsKeys::MISMATCH_REPORT_SHOW_P_A, true);
    _setDefaultValue(SettingsKeys::MISMATCH_REPORT_SHOW_P_C, true);
    _setDefaultValue(SettingsKeys::MISMATCH_REPORT_SHOW_P_H, true);

    _setDefaultValue(SettingsKeys::UNSTABLE_ZONE_WIDTH_PERCENT, 10.);
    _setDefaultValue(SettingsKeys::CUT_BYTES, 0);

    _setDefaultValue(SettingsKeys::MLAT_RLS_TYPE, LocatorUtils::LocatorType::RLS);
    _setDefaultValue(SettingsKeys::MLAT_ZONE_TYPE, FlightZoneUtils::ZoneType::TRACK);


}


void Settings::setValue(QString key, QVariant value)
{
    instance()._settings.setValue(key, value);
}

bool Settings::contains(QString key)
{
    return instance()._settings.contains(key);
}

void Settings::clearToTests()
{
    _settings.clear();
    createDefaultSettings();
}

void Settings::removeGroup(QString groupName)
{
    _settings.beginGroup(groupName);
    _settings.remove("");
    _settings.endGroup();
}

void Settings::emitChangeEvent(QString settingKey)
{
    emit settingChanged(settingKey);
}

QVariant Settings::value(QString key)
{
    return instance()._settings.value(key);
}

QVariant Settings::value(QString key, QVariant defaultValue)
{
    return instance()._settings.value(key, defaultValue);
}
