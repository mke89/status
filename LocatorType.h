#ifndef LOCATOR_TYPE_H
#define LOCATOR_TYPE_H

#include <QString>
#include <QStringList>


namespace LocatorTypeCaption
{
    static QString rlsCaption   = "РЛС";
    static QString mlatCaption  = "МПСН";
    static QString sdpdCaption  = "SDPD";
    static QString undefinedCaption  = "Undefined";
}

class LocatorUtils
{
    public:
        enum LocatorType
        {
            RLS,
            MLAT,
            SDPD,
            ModesCount,
            Undefined,
        };

        LocatorUtils();

        static LocatorType getLocatorTypeByCaption(const QString &caption);
        static QStringList getLocatorsTypes();
        static int filterLocatorIndex(int index);
        static QString getLocatorCaptionByType(LocatorType locatorType);

    private:
};

#endif // LOCATOR_TYPE_H
